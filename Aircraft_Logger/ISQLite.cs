﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();

		SQLiteConnection GetUserDBConnection();
	}
}

