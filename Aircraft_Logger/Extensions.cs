﻿using System;

namespace Aircraft_Logger
{
	public static class Extensions
	{
		public static string DefaultDateFormate (this DateTime date)
		{
			string dateFormate = string.Empty;
			try {
				dateFormate = (date.Day <= 9 ? ("0" + date.Day.ToString ()) : date.Day.ToString ()) + "/" + (date.Month <= 9 ? ("0" + date.Month.ToString ()) : date.Month.ToString ()) + "/" + date.Year.ToString ();
			} catch (Exception ex) {
				date = DateTime.Now;
				dateFormate = (date.Day <= 9 ? ("0" + date.Day.ToString ()) : date.Day.ToString ()) + "/" + (date.Month <= 9 ? ("0" + date.Month.ToString ()) : date.Month.ToString ()) + "/" + date.Year.ToString ();
			}
			return dateFormate;
		}
	}
}

