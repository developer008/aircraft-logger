﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Aircraft_Logger
{
	public class ManufacturersDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		//tblAirForces

		public ManufacturersDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			database.CreateTable<tblManufacturers> ();
		}

		public tblManufacturers GetManufacturersById (string ManufacturerId)
		{
			lock (locker) {
				return database.Table<tblManufacturers> ().Where (c => c.fldManufacturerID == ManufacturerId).FirstOrDefault ();
			}
		}

		public tblManufacturers GetManufacturers (string ManufacturerName)
		{
			lock (locker) {
				return database.Table<tblManufacturers> ().Where (c => c.fldManufacturerName == ManufacturerName).FirstOrDefault ();
			}
		}

		public List<Manufacturers> GetManufacturerList (int Top, int LastCount, string Manufacturer)
		{
			var q = database.Query<Manufacturers> (
				        " select  fldManuFacturerid AS [ManufacturerID], fldmanufacturername AS [ManufacturerName]  from tblManufacturers  WHERE 1 = 1 "
				//+ (LastCount == 0?"":" AND CAST(fldManuFacturerid as INTEGER) > " + LastCount + " ")
				//+ (Convert.ToString( Manufacturer) == string.Empty?"":" AND fldmanufacturername like '%" + Convert.ToString(Manufacturer) + "%' " +  " ")
				//+ (Top == 0?"0":" LIMIT " + Top + " ")
			        ).ToList ();
			return q;
		}

		public string SaveManufacturer (tblManufacturers Manufacturer)
		{
			
				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Manufacturer.IsSync = true;
					if (!string.IsNullOrEmpty (Manufacturer.fldManufacturerID)) {
						lock (locker)
					{
						database.Update(Manufacturer);
					}
						objSync.ManageManufacturersRealTime (Manufacturer);
						return Manufacturer.fldManufacturerID;
					} else {
						Manufacturer.fldManufacturerID = Guid.NewGuid ().ToString ();
					lock (locker)
					{
						database.Insert(Manufacturer);
					}
						objSync.ManageManufacturersRealTime (Manufacturer);
						return Manufacturer.fldManufacturerID;
					}
				} else {

					Manufacturer.IsSync = false;
					if (!string.IsNullOrEmpty (Manufacturer.fldManufacturerID)) {
					lock (locker)
					{
						database.Update(Manufacturer);
					}
						return Manufacturer.fldManufacturerID;
					} else {
						Manufacturer.fldManufacturerID = Guid.NewGuid ().ToString ();
					lock (locker)
					{
						database.Insert(Manufacturer);
					}
						return Manufacturer.fldManufacturerID;
					}
				}

		}
	}
}

