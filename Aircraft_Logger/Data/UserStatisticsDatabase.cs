﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Aircraft_Logger
{
	public class UserStatisticsDatabase
	{
		private SQLiteConnection database;
		static object locker = new object();

		public UserStatisticsDatabase ()
		{
			database = DependencyService.Get<ISQLite>().GetUserDBConnection();
			database.CreateTable<tblUser>();
		}

		public tblUserStatistics GetUserStatistics(string fldUserStatistics){
			lock (locker) {
				return database.Table<tblUserStatistics> ().Where (c => c.fldUserStatistics == fldUserStatistics).FirstOrDefault();
			}
		}

		public List<tblUserStatistics> GetUserUserStatistics(){
			lock (locker) {
				var MyList = (from c in database.Table<tblUserStatistics> ()
					select c).ToList ();
				return MyList;
			}
		}

		public string SaveUserStatistics(tblUserStatistics UserStatistics){
			lock (locker) {
				if (UserStatistics.fldUserStatistics != "") {
					database.Update (UserStatistics);
					return UserStatistics.fldUserStatistics;
				} else {
					return database.Insert (UserStatistics).ToString();
				}
			}
		}

	}
}

