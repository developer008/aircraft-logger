﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Aircraft_Logger
{
	public class AircraftDatabase
	{
		private SQLiteConnection database;
		static object locker = new object();

		public AircraftDatabase()
		{
			database = DependencyService.Get<ISQLite>().GetConnection();
			database.CreateTable<tblAircraft>();
		}

		public tblAircraft GetAircraftById(string AircraftID)
		{
			lock (locker)
			{
				return database.Table<tblAircraft>().Where(c => c.fldAircraftID == AircraftID).FirstOrDefault();
			}
		}

		public tblAircraft GetAircraft(string AircraftName)
		{
			lock (locker)
			{
				return database.Table<tblAircraft>().Where(c => c.fldAircraftName == AircraftName).FirstOrDefault();
			}
		}

		public List<tblAircraft> GetAircraftList()
		{
			lock (locker)
			{
				var MyList = (from c in database.Table<tblAircraft>()
							  select c).ToList();
				return MyList;
			}
		}

		public string SaveAircraft(tblAircraft Aircraft)
		{
			
				if (CommonHelper.IsConnected)
				{
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase();
					Aircraft.IsSync = true;
					if (!string.IsNullOrEmpty(Aircraft.fldAircraftID))
					{
					lock (locker)
					{
						database.Update(Aircraft);
					}
						objSync.ManageAircraftRealTime(Aircraft);
						return Aircraft.fldAircraftID;
					}
					else {
						Aircraft.fldAircraftID = Guid.NewGuid().ToString();
					lock (locker)
					{
						database.Insert(Aircraft);
					}
						objSync.ManageAircraftRealTime(Aircraft);
						return Aircraft.fldAircraftID;
					}
				}
				else {

					Aircraft.IsSync = false;
					if (!string.IsNullOrEmpty(Aircraft.fldAircraftID))
					{
					lock (locker)
					{
						database.Update(Aircraft);
					}
						return Aircraft.fldAircraftID;
					}
					else {
						Aircraft.fldAircraftID = Guid.NewGuid().ToString();
					lock (locker)
					{
						database.Insert(Aircraft);
					}
						return Aircraft.fldAircraftID;
					}
				}

		}

		public List<Aircraft> SearchAircraft(int Top, int LastCount, string AircraftName)
		{
			var q = database.Query<Aircraft>(
						" SELECT  Ac.fldAircraftId as [AircraftID] , Ac.fldAircraftName as [AircraftName] , Ac.fldAircraftNameGenericy as [AircraftNameGenericy],Ac.FldManufacturerId as [ManufacturerID],Mf.fldManufacturerName as [ManufacturerName]   "
						+ " FROM tblAircraft Ac "
						+ " LEFT JOIN tblManufacturers Mf ON Mf.fldManufacturerId = Ac.fldManufacturerId WHERE 1 = 1 "
				+ (LastCount == 0 ? "" : " AND CAST(Ac.fldAircraftId as INTEGER) > " + LastCount + " ")
				+ (Convert.ToString(AircraftName) == string.Empty ? "" : " AND Ac.fldAircraftName like '%" + Convert.ToString(AircraftName) + "%' " + " ")
				+ (Top == 0 ? "0" : " LIMIT " + Top + " ")
					).ToList();
			return q;
		}
	}
}

