﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;

namespace Aircraft_Logger
{
	public class UnitsDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		//tblAirForces

		public UnitsDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			database.CreateTable<tblUnits> ();
		}

		public tblUnits GetUnitsById (string UnitId)
		{
			lock (locker) {
				return database.Table<tblUnits> ().Where (c => c.fldUnitID == UnitId).FirstOrDefault ();
			}
		}

		public tblUnits GetUnits (string UnitName)
		{
			lock (locker) {
				return database.Table<tblUnits> ().Where (c => c.fldUnitName1 == UnitName).FirstOrDefault ();
			}
		}

		public List<tblUnits> GetUnitList ()
		{
			lock (locker) {
				var MyList = (from c in database.Table<tblUnits> ()
				              select c).ToList ();
				return MyList;
			}
		}

		public string SaveUnit (tblUnits Unit)
		{
			

				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Unit.IsSync = true;
					if (!string.IsNullOrEmpty(Unit.fldUnitID)) {
					lock (locker)
					{
						database.Update(Unit);
					}
						objSync.ManageUnitsRealTime (Unit);
						return Unit.fldUnitID;
					} else {
						Unit.fldUnitID = Guid.NewGuid ().ToString ();
					lock (locker)
					{
						database.Insert(Unit);
					}
						objSync.ManageUnitsRealTime (Unit);
						return Unit.fldUnitID;
					}
				} else {
					Unit.IsSync = false;
					if (!string.IsNullOrEmpty(Unit.fldUnitID)) {
					lock (locker)
					{
						database.Update(Unit);
					}
						return Unit.fldUnitID;
					} else {
						Unit.fldUnitID = Guid.NewGuid ().ToString ();
					lock (locker)
					{
						database.Insert(Unit);
					}
						return Unit.fldUnitID;
					}
				}


		}

		public List<Units> SearchUnits (int Top , int LastCount,string UnitName)
		{
			var q = database.Query<Units> (
				" SELECT U.fldUnitID AS [UnitID] ,U.fldUnitName1 AS [UnitName1] ,U.fldUnitName2 AS [UnitName2] ,U.fldTailcode AS [Tailcode]  ,U.fldAirForceID AS [AirForceID] ,Af.fldAirForceNameShort AS [AirForceNameShort] ,Af.fldAirForceName AS [AirForceName]  "
				        + " FROM tblUnits U  "
				+ " LEFT JOIN tblAirForces Af ON Af. fldAirForceID = U. fldAirForceID WHERE 1 = 1 "
				+ (LastCount == 0?"":" AND CAST(fldUnitID as INTEGER) > " + LastCount + " ")
				+ (Convert.ToString( UnitName) == string.Empty?"":" AND U.fldUnitName1 like '%" + Convert.ToString(UnitName) + "%' " +  " ")
				+ (Top == 0?"0":" LIMIT " + Top + " ")
				).ToList ();
			return q;
		}
	}
}

