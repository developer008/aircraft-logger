﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Aircraft_Logger
{
	public class CountriesDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		//tblAirForces

		public CountriesDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			database.CreateTable<tblCountries> ();
		}

		public tblCountries GetCountrie (string CountrieId)
		{
			lock (locker) {
				return database.Table<tblCountries> ().Where (c => c.fldCountryID == CountrieId).FirstOrDefault ();
			}
		}

		public List<tblCountries> GetCountriesList ()
		{
			lock (locker) {
				//var MyList = (from c in database.Table<tblCountries> ()
				//              select c).ToList ();
				//return MyList;
				RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase();
				List<tblCountries> con = objSync.GetCountries();
				return con;
			}
		}

		public string SaveCountries (tblCountries Countrie)
		{
			
			lock (locker) {
				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Countrie.IsSync = true;
					if (!string.IsNullOrEmpty (Countrie.fldCountryID)) {
						database.Update (Countrie);
						objSync.ManageCountriesRealTime (Countrie);
						return Countrie.fldCountryID;
					} else {
						Countrie.fldCountryID = Guid.NewGuid ().ToString ();
						database.Insert (Countrie);
						objSync.ManageCountriesRealTime (Countrie);
						return Countrie.fldCountryID;
					}
				} else {

					Countrie.IsSync = false;
					if (!string.IsNullOrEmpty (Countrie.fldCountryID)) {
						database.Update (Countrie);
						return Countrie.fldCountryID;
					} else {
						Countrie.fldCountryID = Guid.NewGuid ().ToString ();
						database.Insert (Countrie);
						return Countrie.fldCountryID;
					}
				}
			}
		}

		public List<Countries> SearchCountrie (int Top, int LastCount, string CountryName)
		{
			var q = database.Query<Countries> (
				        " SELECT Cou.fldCountryID AS [CountryID] , Cou.fldCountryName AS [CountryName] , Cou.fldCountryAviationCode AS [CountryAviationCode] , Cou.fldContinentID AS [ContinentID] , C.fldContinentName AS [ContinentName]  "
				        + " FROM tblCountries Cou  "
				        + " LEFT JOIN tblContinents C ON Cou. fldContinentID = C. fldContinentID WHERE 1 = 1 "
				+ (LastCount == 0?"":" AND CAST(Cou.fldCountryID as INTEGER) > " + LastCount + " ")
				+ (Convert.ToString( CountryName) == string.Empty?"":" AND Cou.fldCountryName like '%" + Convert.ToString(CountryName) + "%' " +  " ")
				+ (Top == 0?"0":" LIMIT " + Top + " ")
			        ).ToList ();
			return q;
		}
	}
}

