﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using System.Globalization;

namespace Aircraft_Logger
{
	public class AirportsDatabase
	{
		List<YearMonthAirportId> objlstYearMonthAirportId = new List<YearMonthAirportId> ();
		private SQLiteConnection database;
		static object locker = new object ();
		//tblAirForces

		public AirportsDatabase ()
		{
			try {
				lock (locker) {
					database = DependencyService.Get<ISQLite> ().GetConnection ();
					database.CreateTable<tblAirports> ();
				}
			} catch (Exception ex) {
				
			}
		}

		public tblAirports GetAirports (string AirportId)
		{
			try {
				lock (locker) {
					return database.Table<tblAirports> ().Where (c => c.fldAirportID == AirportId).FirstOrDefault ();
				}
			} catch (Exception ex) {
				return new tblAirports ();
			}
		}

		public List<tblAirports> GetAirportsList ()
		{
			lock (locker) {
				var MyList = (from c in database.Table<tblAirports> ()
				              select c).ToList ();
				return MyList;
			}
		}

		public string SaveAirports (tblAirports Airport)
		{
			
			lock (locker) {
				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Airport.IsSync = true;
					if (!string.IsNullOrEmpty (Airport.fldAirportID)) {
						database.Update (Airport);
						objSync.ManageAirportsRealTime (Airport);
						return Airport.fldAirportID;
					} else {
						Airport.fldAirportID = Guid.NewGuid ().ToString ();
						database.Insert (Airport);
						objSync.ManageAirportsRealTime (Airport);
						return Airport.fldAirportID;
					}
				} else {

					Airport.IsSync = false;
					if (!string.IsNullOrEmpty (Airport.fldAirportID)) {
						database.Update (Airport);
						return Airport.fldAirportID;
					} else {
						Airport.fldAirportID = Guid.NewGuid ().ToString ();
						database.Insert (Airport);
						return Airport.fldAirportID;
					}
				}
			}
		}

		public List<Airports> GetAirportsList (int Top, int LastCount, string Airports)
		{
			var q = database.Query<Airports> (
				        " select A.fldAirportID AS [AirportID],A.fldAirportName AS [AirportName], A.fldAirportCity AS [AirportCity], A.fldIATACode AS [IATACode], A.fldICAOCode AS [ICAOCode], A.fldCountryID AS [CountryID], C.fldCountryName AS [CountryName], A.fldGPSCoordinates AS [GPSCoordinates]"
				        + " from tblAirports AS A "
				        + " inner join tblCountries AS C On C.fldCountryID = A.fldCountryID WHERE 1 = 1 "
						+ (LastCount == 0?"":" AND CAST(A.fldAirportID as INTEGER) > " + LastCount + " ")
						+ (Convert.ToString(Airports) == string.Empty? "" :" AND A.fldAirportName like '%" + Convert.ToString(Airports) + "%' " +  " ")
						+ (Top == 0 ? "0" : " LIMIT " + Top + " ")
			        ).ToList ();
			return q;
		}


		//+ (LastCount == 0 ? "" : " AND CAST(Ac.fldAircraftId as INTEGER) > " + LastCount + " ")
		//		+ (Convert.ToString(AircraftName) == string.Empty? "" : " AND Ac.fldAircraftName like '%" + Convert.ToString(AircraftName) + "%' " + " ")
		//		+ (Top == 0 ? "0" : " LIMIT " + Top + " ")
		//			).ToList();

		public List<Registrations> GetRegistrationsByAirportId (int airportId)
		{
			try {
				string Query = " SELECT Tf.fldAirForceID as AirForceID, Tf.fldAirForceName as AirForceName, Tc.fldCountryID as CountryId, Tc.fldCountryName as CountryName, Tc.fldCountryAviationCode as CountryCode, Tp.fldPhotoFilename as [PhotoName], Ta.fldAirportName as AirportName, Av.fldAirportID as AirportId, Reg.fldRegistrationID as [RegistrationID] ,Reg.fldRegistration as [Registration],Reg.fldCn as [Cn], "
				               + " Reg.fldAirlineID AS [AirlineID], Aln.fldAirlineName as [AirlineName], "
				               + " Reg.fldAircraftID AS [AircraftID], Acf.fldAircraftName as [AircraftName] , "
				               + " Reg.fldUnitID AS [UnitID] , fldUnitName1 AS [UnitName1] , fldUnitName2 AS [UnitName2], fldCode AS [Code],"
				               + " Av.fldAirportVisitID AS [AirportVisitID] ,Av.fldLocationGPS AS [LocationGPS] ,Av.fldVisitDate AS [VisitDate], fldAircraftType AS [AircraftType],fldNew AS [New],fldRemarks AS [Remarks] "
				               + " FROM tblRegistrations Reg "
				               + " LEFT JOIN tblAircraft Acf ON Acf.fldAircraftID = Reg.fldAircraftID "
				               + " LEFT JOIN tblAirlines Aln ON Aln.fldAirlineID = Reg.fldAirlineID "
				               + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID "
				               + " LEFT JOIN tblUnits U ON U.fldUnitID = Reg.fldUnitID "
				               + " LEFT JOIN tblAirports Ta ON Ta.fldAirportID = Av.fldAirportID "
				               + " LEFT JOIN tblAirForces Tf ON Tf.fldAirForceID = U.fldAirForceID "
				               + " LEFT JOIN tblPhotos Tp ON Tp.fldRegistrationID = Reg.fldRegistrationID "
				               + " LEFT JOIN tblCountries Tc ON Tc.fldCountryID = Ta.fldCountryID "
				               + " WHERE  Av.fldAirportID = '" + airportId + "'";

				var q = database.Query<Registrations> (Query).ToList ();
				return q;
			} catch (Exception ex) {
				return new List<Registrations> ();
			}
		}

		public List<YearMonthAirportId> GetYearMonthByAirportId (string airportId)
		{
			string Query = " Select  tv.fldVisitDate, tv.fldAirportVisitID, tv.fldAirportID"
			               + " From tblVisits tv "
			               + " WHERE tv.fldAirportVisitID in (Select fldAirportVisitID from tblRegistrations where fldUserId='" + Account.UserId + "') "
			               + (airportId == null ? "" : " and tv.fldAirportID = '" + airportId + "'")
			               + " order by fldAirportVisitID desc";
			List<YearMonthAirportId> q = database.Query<YearMonthAirportId> (Query);

			var result = q;

			foreach (var item in result) {
				try {
					DateTime date = DateTime.ParseExact (item.fldVisitDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
					item.VisitDate = date;
					item.strMonth = item.VisitDate.ToString ("MMMM");
					item.Month = date.Month;
					item.Year = date.Year;
					item.Day = date.Day;
				} catch (Exception ex) {
					DateTime TestVal = DateTime.ParseExact (item.fldVisitDate, "dd-MM-yyyy", null);
					item.VisitDate = TestVal;
					item.strMonth = TestVal.ToString ("MMMM");
					;
					item.Month = TestVal.Month;
					item.Year = TestVal.Year;
				}
			}
			objlstYearMonthAirportId = result;
			var grouped = (from p in result
			               group p by new { month = p.VisitDate.Month,year = p.VisitDate.Year,stMonth = p.strMonth,day = p.Day } into d
			               select new YearMonthAirportId {
				Month = d.Key.month,
				Year = d.Key.year,
				strMonth = d.Key.stMonth,
				Day = d.Key.day
			}).ToList ();	
			foreach (var item in grouped) {
				item.lstRegistration = this.GetRegistrationsByYearMonthAirportId (item.Year, item.Month, item.Day, grouped);
			}
			return grouped;
		}

		public List<Registrations> GetRegistrationsByYearMonthAirportId (int year, int month, int day, List<YearMonthAirportId> lstYearMonth)
		{

			var result2 = objlstYearMonthAirportId.Where (x => x.Year == year && x.Day == day && x.Month == month).ToList ();
			List<Registrations> objReg = new List<Registrations> ();
			//foreach (var item in result2) {
				var reg = new List<Registrations> ();
			reg = (new CommonDatabase ()).GetRegistrationByAirportId1 (result2[0].fldAirportVisitID);
				try {
				foreach (var item in reg) 
				{
					DateTime dt = new DateTime(year,month,day);

					item.VisitDate = dt;

				}
					//reg.VisitDate = item.VisitDate;
				} catch (Exception ex) {
				}
				//objReg.Add (reg);
			//}
			return reg;
		}

	}
}

