﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using System.Globalization;

namespace Aircraft_Logger
{
	public class PhotosDatabase
	{
		private SQLiteConnection database;
		static object locker = new object();
		//tblAirForces

		public PhotosDatabase ()
		{
			database = DependencyService.Get<ISQLite>().GetConnection();
			database.CreateTable<tblPhotos>();
		}

		public tblPhotos GetPhotos(string PhotoID){
			lock (locker) {
				return database.Table<tblPhotos> ().Where(c => c.fldPhotoID == PhotoID).FirstOrDefault();
			}
		}

		public List<tblPhotos> GetPhotosByRegistarId(string RegistrationId){
			lock (locker) {
				return database.Table<tblPhotos> ().Where(c => c.fldRegistrationID == RegistrationId).ToList<tblPhotos>();
			}
		}


		public List<tblPhotos> GetPhotoList(){
			lock (locker) {
				var MyList = (from c in database.Table<tblPhotos> ()
					select c).ToList ();
				return MyList;
			}
		}

		public string SavePhotos(tblPhotos Photo){
			
			lock (locker) {

				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Photo.IsSync = true;
					if (Photo.fldPhotoID!="0") {
						database.Update (Photo);
						objSync.ManagePhotosRealTime (Photo);
						return Photo.fldPhotoID;
					} else {
						Photo.fldPhotoID = Guid.NewGuid ().ToString ();
						database.Insert (Photo);
						objSync.ManagePhotosRealTime (Photo);
						return Photo.fldPhotoID;
					}
				} else {
					Photo.IsSync = false;
					if (Photo.fldPhotoID!="0") {
						database.Update (Photo);
						return Photo.fldPhotoID;
					} else {
						Photo.fldPhotoID = Guid.NewGuid ().ToString ();
						database.Insert (Photo);
						return Photo.fldPhotoID;
					}
				}

			}
		}
		List<YearMonthAirportId> objlstYearMonthAirportId = new List<YearMonthAirportId>();
		public List<YearMonthAirportId> GetYearMonth ()
		{
			var result = (new CommonDatabase ()).GetYearMonth ().Skip(0).Take(30).ToList();

			foreach (var item in result) {
				try {
					DateTime date = DateTime.ParseExact (item.fldVisitDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
					item.VisitDate = date;
					item.Year = date.Year;
				} catch (Exception ex) {

				}
			}
			objlstYearMonthAirportId = result;
			var grouped = (from p in result
				group p by new { year = p.VisitDate.Year } into d
				select new YearMonthAirportId { Year = d.Key.year }).OrderByDescending(x=>x.Year).ToList ();

			return grouped;
		
		}


		public List<TimelineRegistration> GetRegistrationsByYearMonth (int year,  List<YearMonthAirportId> lstYearMonth)
		{

			var result2 = objlstYearMonthAirportId.Where (x => x.Year == year ).ToList ();
			List<Registrations> objReg = new List<Registrations> ();
			foreach (var item in result2) {
				var reg = new List<Registrations> ();
				reg = (new CommonDatabase ()).GetRegistrationByAirportId1 (item.fldAirportVisitID);
				try
				{
					foreach (var a in reg)
					{
						a.VisitDate = item.VisitDate;
						objReg.Add(a);
					}
				}
				catch (Exception ex)
				{
				}
			}

			var grouped = new List<TimelineRegistration> ();
			try {
				grouped = (from p in objReg.Where (x => x.VisitDate != null && x.PhotoName!=null)
					group p by new { day = p.VisitDate.Day,airportId = p.AirportId,airportName = p.AirportName } into d
					select new TimelineRegistration { Day = d.Key.day, AirportId = d.Key.airportId, AirportName = d.Key.airportName }).ToList ();	

				foreach (var item in grouped) {
					item.lstRegistration = objReg.Where (x => x.VisitDate.Day == item.Day && x.VisitDate.Year ==year && x.AirportId == item.AirportId && x.PhotoName!=null).ToList ();
				}
			} catch (Exception ex) {
			}
			//return objReg;
			return grouped;
		}



	}
}

