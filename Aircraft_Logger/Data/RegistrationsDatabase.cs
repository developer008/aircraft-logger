﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Aircraft_Logger
{
	public class RegistrationsDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		//tblAirForces

		public RegistrationsDatabase ()
		{
			try {
				lock (locker) {
					database = DependencyService.Get<ISQLite> ().GetConnection ();
					database.CreateTable<tblRegistrations> ();
				}
			} catch (Exception ex) {

			}
		}

		public tblRegistrations GetRegistrations (string RegistrationId)
		{
			lock (locker) {
				return database.Table<tblRegistrations> ().Where (c => c.fldRegistrationID == RegistrationId).FirstOrDefault ();
			}
		}

		public bool IsRegistrationExists (string Registration)
		{
			try {
				lock (locker) {
					List<tblRegistrations> registers = database.Table<tblRegistrations> ().Where (c => c.fldRegistration == Registration).ToList ();
					bool isExists = registers.Count () > 0;
					return isExists;
				}
			} catch (Exception ex) {
				return false;
			}
		}

		public List<tblRegistrations> IsRegistrationExists (string Registration, string AirportId)
		{
			try {
				lock (locker) {
					string Query = " select * from tblRegistrations Tr "
					               + " Inner join tblVisits Tv On Tv.fldAirportVisitID =  Tr.fldAirportVisitID "
					               + " Where Tr.fldRegistration = '" + Registration + "' And Tv.fldAirportID = '" + AirportId + "'";
					var registers = database.Query<tblRegistrations> (Query).ToList ();
					return registers;
				}
			} catch {
				return new List<tblRegistrations> ();
			}
		}

		public List<tblRegistrations> GetRegistrationsList ()
		{
			lock (locker) {
				var MyList = (from c in database.Table<tblRegistrations> ()
				              select c).ToList ();
				return MyList;
			}
		}

		public bool DeleteRegistration (tblRegistrations Registration)
		{
			try {
				lock (locker) {
					if (CommonHelper.IsConnected) {
						RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase();
						database.Delete (Registration);
						objSync.ManageRegistrationsDeleteRealTime(Registration);

					}
					else
					{
						tblDeletedReg tblDelete = new tblDeletedReg();
						tblDelete.fldRegistrationID =Registration.fldRegistrationID;
						tblDelete.fldUserId = Account.UserId;
						database.Insert(tblDelete);
						database.Delete (Registration);
					}

				}
				return true;
			} catch {
				return false;
			}
		}

		public string SaveRegistrations (tblRegistrations Registration)
		{
			try {

					if (CommonHelper.IsConnected) {
						RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase();
						Registration.IsSync = true;
						if (!string.IsNullOrEmpty (Registration.fldRegistrationID)) {
							Registration.fldUserId = Account.UserId;
						lock (locker)
						{
							database.Update(Registration);
						}
							objSync.ManageRegistrationsRealTime(Registration);
							return Registration.fldRegistrationID;
						} else {
							Registration.fldRegistrationID = Guid.NewGuid ().ToString ();
							Registration.fldUserId = Account.UserId;
						lock(locker)
						{
							database.Insert(Registration);
						}
							objSync.ManageRegistrationsRealTime(Registration);
							return Registration.fldRegistrationID;
						}
					} else {
						Registration.IsSync = false;
						if (!string.IsNullOrEmpty (Registration.fldRegistrationID)) {
							Registration.fldUserId = Account.UserId;
						lock(locker)
						{
							database.Update(Registration);
						}
							return Registration.fldRegistrationID;
						} else {
							Registration.fldRegistrationID = Guid.NewGuid ().ToString ();
							Registration.fldUserId = Account.UserId;
							database.Insert (Registration);
							return Registration.fldRegistrationID;
						}		
			
				}
			} catch {
				return string.Empty;
			}
		}

		public List<AddRegistrationSearchList> SearchRegistrationDetailList (int top, int LastCount, DateTime registerDate, string Registration, char IsCivilOrMilitary, string order, string airportId)
		{
			try {
				string orderBy = string.Empty;

				switch (order) {
				case "Registration": 
					orderBy = " ORDER BY Reg.fldRegistrationID ";
					break;
				case "Aircraft":
					orderBy = " ORDER BY Acf.fldAircraftName ";
					break;
				case "Airline":
					orderBy = " ORDER BY Aln.fldAirlineName ";
					break;
				case "Units":
					orderBy = " ORDER BY Unt.fldUnitName1 ";
					break;
				default:
					orderBy = " ORDER BY Reg.fldRegistrationID ";
					break;
				}

				string Query = " SELECT Reg.IsSync, IFNULL(Reg.fldRegistrationID,0) as RegistrationID ,IFNULL(Reg.fldRegistration,'') as Registration"
				               + ",IFNULL(Reg.fldCn,'') as CN, IFNULL(Acf.fldAircraftName,'') as AircraftName ,Av.fldVisitDate AS [VisitDate] "
				               + "," + (IsCivilOrMilitary == 'C' ? "IFNULL(Aln.fldAirlineName,'')" : "''") + "  as AirlineName  "
				               + "," + (IsCivilOrMilitary == 'M' ? "IFNULL(Unt.fldUnitName1,'')" : "''") + "  as [UnitName1] , fldNew AS [New], fldCode AS [Code] "
				               + " FROM tblRegistrations Reg "
				               + " LEFT JOIN tblAircraft Acf ON Acf.fldAircraftID = Reg.fldAircraftID "
				               + " LEFT JOIN tblAirlines Aln ON Aln.fldAirlineID = Reg.fldAirlineID "
				               + " LEFT JOIN tblUnits Unt ON Unt.fldUnitID = Reg.fldUnitID "
				               + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID WHERE 1 = 1 and Reg.fldUserId='" + Account.UserId + "' and Av.fldAirportID='" + airportId + "'"
				               + " AND Reg.fldAircraftType = '" + IsCivilOrMilitary + "' "
				               + ((Registration == null || Registration == "") ? "" : " AND Reg.fldRegistration = '" + Registration + "' ")
				               //+ (LastCount == 0 ? "" : " AND CAST(fldRegistrationID as INTEGER) > " + LastCount + " ")
							   + (registerDate == null ? "" : " AND  Av.fldVisitDate =  '" + registerDate.DefaultDateFormate()+ "' ")
				               + (top == 0 ? "0" : orderBy + " LIMIT " + LastCount + "," + top);
				var q = database.Query<AddRegistrationSearchList> (Query).ToList ();
				return q;
			} catch {
				return new List<AddRegistrationSearchList> ();
			}
		}

		public Registrations GetRegistrationWithDetail (string registrationId)
		{
			try {
				string Query = " SELECT Reg.fldRegistrationID as [RegistrationID] ,Reg.fldRegistration as [Registration],Reg.fldCn as [Cn], "
				               + " Reg.fldAirlineID AS [AirlineID], Aln.fldAirlineName as [AirlineName], "
				               + " Reg.fldAircraftID AS [AircraftID], Acf.fldAircraftName as [AircraftName] , "
				               + " Reg.fldUnitID AS [UnitID] , fldUnitName1 AS [UnitName1] , fldUnitName2 AS [UnitName2], fldCode AS [Code],"
				               + " Av.fldAirportVisitID AS [AirportVisitID] ,Av.fldLocationGPS AS [LocationGPS] ,Av.fldVisitDate AS [VisitDate],fldAircraftType AS [AircraftType],fldNew AS [New],fldRemarks AS [Remarks] "
				               + " FROM tblRegistrations Reg "
				               + " LEFT JOIN tblAircraft Acf ON Acf.fldAircraftID = Reg.fldAircraftID "
				               + " LEFT JOIN tblAirlines Aln ON Aln.fldAirlineID = Reg.fldAirlineID "
				               + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID "
				               + " LEFT JOIN tblUnits U ON U.fldUnitID = Reg.fldUnitID "
				               + " WHERE Reg.fldRegistrationID = '" + registrationId + "'";
				var q = database.Query<Registrations> (Query).FirstOrDefault ();
				return q;
			} catch (Exception ex) {
				return new Registrations ();
			}
		}

		public List<Registrations> GetRegistrationsByGroup ()
		{
			try {
				string Query = " SELECT Tf.fldAirForceID as AirForceID, Tf.fldAirForceName as AirForceName, Tc.fldCountryID as CountryId, Tc.fldCountryName as CountryName, Tc.fldCountryAviationCode as CountryCode, Tp.fldPhotoFilename as [PhotoName], Ta.fldAirportName as AirportName, Av.fldAirportID as AirportId, Reg.fldRegistrationID as [RegistrationID] ,Reg.fldRegistration as [Registration],Reg.fldCn as [Cn], "
				               + " Reg.fldAirlineID AS [AirlineID], Aln.fldAirlineName as [AirlineName], "
				               + " Reg.fldAircraftID AS [AircraftID], Acf.fldAircraftName as [AircraftName] , "
				               + " Reg.fldUnitID AS [UnitID] , fldUnitName1 AS [UnitName1] , fldUnitName2 AS [UnitName2], fldCode AS [Code],"
				               + " Av.fldAirportVisitID AS [AirportVisitID] ,Av.fldLocationGPS AS [LocationGPS] ,Av.fldVisitDate AS [VisitDate], fldAircraftType AS [AircraftType],fldNew AS [New],fldRemarks AS [Remarks] "
				               + " FROM tblRegistrations Reg "
				               + " LEFT JOIN tblAircraft Acf ON Acf.fldAircraftID = Reg.fldAircraftID "
				               + " LEFT JOIN tblAirlines Aln ON Aln.fldAirlineID = Reg.fldAirlineID "
				               + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID "
				               + " LEFT JOIN tblUnits U ON U.fldUnitID = Reg.fldUnitID "
				               + " LEFT JOIN tblAirports Ta ON Ta.fldAirportID = Av.fldAirportID "
				               + " LEFT JOIN tblAirForces Tf ON Tf.fldAirForceID = U.fldAirForceID "
				               + " LEFT JOIN tblPhotos Tp ON Tp.fldRegistrationID = Reg.fldRegistrationID "
				               + " LEFT JOIN tblCountries Tc ON Tc.fldCountryID = Ta.fldCountryID "
							   + " WHERE Reg.fldUserId = '" + Account.UserId + "'";;
				
				var q = database.Query<Registrations> (Query).ToList ();
				return q;
			} catch (Exception ex) {
				return new List<Registrations> ();
			}
		}

		public List<GroupByCountry> GroupByCountryData (List<Registrations> lsRegistrations)
		{
			var grouped = new List<GroupByCountry> ();
			try {
				grouped = (from p in lsRegistrations
				           group p by new { countryId = p.CountryId,countryName = p.CountryName,countryCode = p.CountryCode } into d
				           select new GroupByCountry {
					CountryId = d.Key.countryId,
					CountryName = d.Key.countryName,
					CountryCode = d.Key.countryCode
				})
					.Where (x => x.CountryId != string.Empty).OrderBy (x => x.CountryName).ToList ();	



				foreach (var item in grouped) {
					item.lsRegistrationCountry = lsRegistrations.Where (x => x.CountryId == item.CountryId && x.CountryId != string.Empty).GroupBy (x => x.RegistrationID).Select (x => x.FirstOrDefault ()).Skip (0).Take (50).ToList ();
				}
			} catch (Exception ex) {
				int i = 0;
			}

			return  grouped;
		}

		public List<GroupByAircraft> GroupByAircraftData (List<Registrations> lsRegistrations)
		{
			var grouped = new List<GroupByAircraft> ();
			try {
				grouped = (from p in lsRegistrations
				           group p by new { aircraftId = p.AircraftID,aircraftName = p.AircraftName } into d
				           select new GroupByAircraft { AircraftId = d.Key.aircraftId, AircraftName = d.Key.aircraftName })
					.Where (x => x.AircraftId != string.Empty).OrderBy (x => x.AircraftName).Skip (0).Take (100).ToList ();	

				foreach (var item in grouped) {
					item.lsRegistrationAircraft = lsRegistrations.Where (x => x.AircraftID == item.AircraftId).GroupBy (x => x.RegistrationID).Select (x => x.FirstOrDefault ()).ToList ();
				}
			} catch (Exception ex) {
				int i = 0;
			}

			return grouped;
		}

		public List<GroupByAirline> GroupByAirelineData (List<Registrations> lsRegistrations)
		{
			var grouped = new List<GroupByAirline> ();
			try {
				grouped = (from p in lsRegistrations
				           group p by new { airlineId = p.AirlineID,airlineName = p.AirlineName} into d
				           select new GroupByAirline { AirlineID = d.Key.airlineId, AirlineName = d.Key.airlineName })
					.Where (x => x.AirlineID != string.Empty).OrderBy (x => x.AirlineName).Skip (0).Take (100).ToList ();	

				foreach (var item in grouped) {
					item.lsRegistrationAirline = lsRegistrations.Where (x => x.AirlineID == item.AirlineID).GroupBy (x => x.RegistrationID).Select (x => x.FirstOrDefault ()).ToList ();
				}
			} catch (Exception ex) {
				int i = 0;
			}

			return grouped;
		
		}

		public List<GroupByAirforce> GroupByAirforceData (List<Registrations> lsRegistrations)
		{
			var grouped = new List<GroupByAirforce> ();
			try {
				grouped = (from p in lsRegistrations
				           group p by new { airforceId = p.AirForceID,airforceName = p.AirForceName } into d
				           select new GroupByAirforce { AirForceID = d.Key.airforceId, AirForceName = d.Key.airforceName })
					.Where (x => x.AirForceID != string.Empty).OrderBy (x => x.AirForceName).Skip (0).Take (100).ToList ();	

				foreach (var item in grouped) {
					item.lsRegistrationAirforce = lsRegistrations.Where (x => x.AirForceID == item.AirForceID).GroupBy (x => x.RegistrationID).Select (x => x.FirstOrDefault ()).ToList ();
				}
			} catch (Exception ex) {
				int i = 0;
			}

			return grouped;
		}
	}
}

