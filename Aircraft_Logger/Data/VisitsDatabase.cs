﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Aircraft_Logger
{
	public class VisitsDatabase
	{
		private SQLiteConnection database;
		static object locker = new object();

		public VisitsDatabase ()
		{
			database = DependencyService.Get<ISQLite>().GetConnection();
			database.CreateTable<tblVisits>();
		}

		public tblVisits GetVisits(string VisitId){
			lock (locker) {
				return database.Table<tblVisits> ().Where (c => c.fldAirportVisitID == VisitId).FirstOrDefault();
			}
		}

		public List<tblVisits> GetVisits(){
			lock (locker) {
				var MyList = (from c in database.Table<tblVisits> ()
					select c).ToList ();
				return MyList;
			}
		}

		public string SaveVisits(tblVisits Visit){
			
			lock (locker) {
				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Visit.IsSync = true;
					if (!string.IsNullOrEmpty(Visit.fldAirportVisitID)) {
						database.Update (Visit);
						objSync.ManageVisitsRealTime (Visit);
						return Visit.fldAirportVisitID;
					} else {
						Visit.fldAirportVisitID = Guid.NewGuid ().ToString ();
						database.Insert (Visit);
						objSync.ManageVisitsRealTime (Visit);
						return Visit.fldAirportVisitID;;
					}
				} else {
					Visit.IsSync = false;
					if (!string.IsNullOrEmpty(Visit.fldAirportVisitID)) {
						database.Update (Visit);
						return Visit.fldAirportVisitID;
					} else {
						Visit.fldAirportVisitID = Guid.NewGuid ().ToString ();
						database.Insert (Visit);
						return Visit.fldAirportVisitID;;
					}
				}

			}
		}

		public tblVisits GetIfVisitExists (string AirportId , string GpsLocation , DateTime visitDate){
			lock (locker) {

				string strVisitDate = visitDate.ToString ("dd/MM/yyyy");
				//tblVisits visits = database.Table<tblVisits> ().Where (c => c.fldAirportID == AirportId && c.fldLocationGPS == GpsLocation && c.fldVisitDate == strVisitDate ).FirstOrDefault(); 
				//return visits;
				if (AirportId != null && AirportId != "") {
					var query = " SELECT * FROM tblVisits where fldAirportVisitID in ( SELECT fldAirportVisitID from tblregistrations where fldUserId ='" + Account.UserId + "') "
						+ " and fldAirportID='" + AirportId + "'  and fldVisitDate='" + visitDate.DefaultDateFormate () + "'";
					var result = database.Query<tblVisits> (query).FirstOrDefault ();
					return result;	
				} else {
					var query = " SELECT * FROM tblVisits where fldAirportVisitID in ( SELECT fldAirportVisitID from tblregistrations where fldUserId ='" + Account.UserId + "') "
						+ " and fldAirportID='" + AirportId + "' and fldLocationGPS='"+GpsLocation+"'  and fldVisitDate='" + visitDate.DefaultDateFormate () + "'";
					var result = database.Query<tblVisits> (query).FirstOrDefault ();
					return result;
				}

//				tblVisits visits = database.Table<tblVisits> ().Where (c => c.fldAirportID == AirportId && c.fldLocationGPS == GpsLocation && c.fldVisitDate == visitDate.ToString("dd/MM/yyyy") ).FirstOrDefault(); 
//				return visits;
			}
		}
	}
}

