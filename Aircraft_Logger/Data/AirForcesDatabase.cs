﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Aircraft_Logger
{
	public class AirForcesDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		//tblAirForces
		public AirForcesDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			database.CreateTable<tblAirForces> ();
		}

		public tblAirForces GetAirforce (string AirForceID)
		{
			lock (locker) {
				return database.Table<tblAirForces> ().Where (c => c.fldAirForceID == AirForceID).FirstOrDefault ();
			}
		}

		public List<tblAirForces> GetAirforceList ()
		{
			lock (locker) {
				var MyList = (from c in database.Table<tblAirForces> ()
				              select c).ToList ();
				return MyList;
			}
		}

		public string SaveAirforce (tblAirForces AirForce)
		{
			
			lock (locker) {
				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					AirForce.IsSync = true;
					if (!string.IsNullOrEmpty(AirForce.fldAirForceID)) {
						database.Update (AirForce);
						objSync.ManageAirForcesRealTime (AirForce);
						return AirForce.fldAirForceID;
					} else {
						AirForce.fldAirForceID = Guid.NewGuid ().ToString ();
						database.Insert (AirForce);
						objSync.ManageAirForcesRealTime (AirForce);
						return AirForce.fldAirForceID;
					}
				} else {
					AirForce.IsSync = false;
					if (!string.IsNullOrEmpty(AirForce.fldAirForceID)) {
						database.Update (AirForce);
						return AirForce.fldAirForceID;
					} else {
						AirForce.fldAirForceID = Guid.NewGuid ().ToString ();
						database.Insert (AirForce);
						return AirForce.fldAirForceID;
					}
				}

			}
		}

		public List<AirForces> SearchAirForces (int Top , int LastCount,string AirforceName)
		{
			var q = database.Query<AirForces> (
				" SELECT Af.fldAirForceID AS [AirForceID] , Af.fldAirForceNameShort  AS [AirForceNameShort] , Af.fldAirForceName  AS [AirForceName] , Af.fldCountryID  AS [CountryID] , Af.fldAirForceNameLocal  AS [AirForceNameLocal] , C.fldCountryName   AS [CountryName]  "
				+ " FROM tblAirForces Af  "
				+ " LEFT JOIN tblCountries C ON Af. fldCountryID = C. fldCountryID WHERE 1 = 1 "
				+ (LastCount == 0?"":" AND CAST(Af.fldAirForceID as INTEGER) > " + LastCount + " ")
				+ (Convert.ToString( AirforceName) == string.Empty?"":" AND Af.fldAirForceName like '%" + Convert.ToString(AirforceName) + "%' " +  " ")
				+ (Top == 0?"0":" LIMIT " + Top + " ")
				).ToList ();
			return q;
		}

		public List<AirForces> SearchAirForces1(int Top, int LastCount, string AirforceName)
		{
			var q = database.Query<AirForces>(
				" SELECT Af.fldAirForceID AS [AirForceID] , Af.fldAirForceNameShort  AS [AirForceNameShort] , Af.fldAirForceName  AS [AirForceName] , Af.fldCountryID  AS [CountryID] , Af.fldAirForceNameLocal  AS [AirForceNameLocal] , C.fldCountryName   AS [CountryName]  "
				+ " FROM tblAirForces Af  "
				+ (LastCount == 0 ? "" : " AND CAST(Af.fldAirForceID as INTEGER) > " + LastCount + " ")
				+ (Convert.ToString(AirforceName) == string.Empty ? "" : " AND Af.fldAirForceNameShort like '%" + Convert.ToString(AirforceName) + "%' " + " ")
				+ (Top == 0 ? "0" : " LIMIT " + Top + " ")
				).ToList();
			return q;
		}
	}
}