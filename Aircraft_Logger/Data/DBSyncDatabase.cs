﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;

namespace Aircraft_Logger
{
	public class DBSyncDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		string baseUrl = CommonHelper.BaseUrl;

		public DBSyncDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
		}

		public async Task ManageDatabase ()
		{

			await ManageUsers ();
//			ManageContinents ();
//			ManageCountries ();
//			ManageAirForces ();
//			ManageAirlines();
//			ManageAirports();
//			ManageManufacturers();
//			ManageAircraft();
//			ManageUnits();
			await ManageRegistrationsDelete ();
			await ManageRegistrations ();
			await ManageVisits ();
			await ManagePhotos ();
		}

		public void TuncateTable ()
		{
			var result = database.Query<tblRegistrations> ("SELECT * FROM tblRegistrations");
			foreach (var item in result) {
				database.Delete (item);
			}
			var result1 = database.Query<tblVisits> ("SELECT * FROM tblVisits");
			foreach (var item in result1) {
				database.Delete (item);
			}
			var result2 = database.Query<tblPhotos> ("SELECT * FROM tblPhotos");
			foreach (var item in result2) {
				database.Delete (item);
			}
		}

		#region Users

		public async Task ManageUsers ()
		{
			try {
				
			
				var result = database.Query<tblUser> ("SELECT * FROM tblUser where fldUserId='" + Account.UserId + "' ").FirstOrDefault ();

				if (result != null) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/users/saveUser", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						//var resp =  client.PostAsync (url,content).Result;

						var resp = await client.PostAsync (url, content);

						if (resp.IsSuccessStatusCode) {

						}
						result.IsSync = true;
						database.Update (result);
//					foreach (var item in result) {
//						item.IsSync = true;
//						database.Update (item);
//					}
				

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/users/getUser/" + Account.UserId + "", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
//					var	resultList = JsonConvert.DeserializeObject<List<tblUser>> (resp.Content.ReadAsStringAsync ().Result);
//					foreach (var item in resultList) {
//						var resultCheck = database.Query<tblUser> ("SELECT fldUserId FROM tblPhotos WHERE fldUserId='"+item.fldUserId+"'").FirstOrDefault ();
//						if (resultCheck == null) {
//							item.IsSync = true;
//							database.Insert (item);
//						}
//
//					}
					}
				}
			} catch (Exception ex) {

			}
			return;
		}

		#endregion

		#region Continents

		public void ManageContinents ()
		{
			try {
				
			
				var result = database.Query<tblContinents> ("SELECT * FROM tblContinents WHERE IsSync is null or IsSync =''").ToList ();

				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/continents/saveContinent", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;
	
						if (resp.IsSuccessStatusCode) {
						
						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}
					
					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/continents/getContinent", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblContinents>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblContinents> ("SELECT fldContinentID FROM tblContinents WHERE fldContinentID='" + item.fldContinentID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}
						}
					}


				}
			} catch (Exception ex) {

			}
		}

		#endregion

		#region Countries

		public void ManageCountries ()
		{
			try {
				
			
				var result = database.Query<tblCountries> ("SELECT * FROM tblCountries WHERE IsSync is null or IsSync =''").ToList ();

				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/countries/saveCountries", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/countries/getCountries", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblCountries>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblCountries> ("SELECT fldCountryID FROM tblCountries WHERE fldCountryID='" + item.fldCountryID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					}


				}
			} catch (Exception ex) {

			}
		}

		#endregion

		#region AirForces

		public void ManageAirForces ()
		{
			try {
				
			
				var result = database.Query<tblAirForces> ("SELECT * FROM tblAirForces WHERE  (IsSync is null or IsSync ='') limit 50 ").ToList ();

				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/airforces/saveAirForces", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/airforces/getAirForces", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblAirForces>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblAirForces> ("SELECT fldAirForceID FROM tblAirForces WHERE fldAirForceID='" + item.fldAirForceID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					}


				}
			} catch (Exception ex) {

			}
		}

		#endregion

		#region Airlines

		public void ManageAirlines ()
		{
			try {

				var result = database.Query<tblAirlines> ("SELECT * FROM tblAirlines WHERE (IsSync is null or IsSync ='' or IsSync =0) limit 50 ").ToList ();
				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/airlines/saveAirlines", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/airlines/getAirlines", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblAirlines>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblAirlines> ("SELECT fldAirlineID FROM tblAirlines WHERE fldAirlineID='" + item.fldAirlineID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					}


				}

			} catch (Exception ex) {

			}
		}

		#endregion


		#region Airports

		public void ManageAirports ()
		{
			try {
				
			
				var result = database.Query<tblAirports> ("SELECT * FROM tblAirports WHERE (IsSync is null or IsSync ='' or IsSync =0) limit 50 ").ToList ();
				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/airports/saveAirport", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/airports/getAirports", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblAirports>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblAirports> ("SELECT fldAirportID FROM tblAirports WHERE fldAirportID='" + item.fldAirportID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					}


				}
			} catch (Exception ex) {

			}

		}

		#endregion


		#region Manufacturers

		public void ManageManufacturers ()
		{
			try {
				
			
				var result = database.Query<tblManufacturers> ("SELECT * FROM tblManufacturers WHERE (IsSync is null or IsSync ='' or IsSync =0) ").ToList ();
				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/manufacturers/saveManufacturer", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/manufacturers/getManufacturers", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblManufacturers>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblManufacturers> ("SELECT fldManufacturerID FROM tblManufacturers WHERE fldManufacturerID='" + item.fldManufacturerID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					}


				}
			} catch (Exception ex) {

			}
		}

		#endregion

		#region Aircraft

		public void ManageAircraft ()
		{
			try {
				
			
				var result = database.Query<tblAircraft> ("SELECT * FROM tblAircraft WHERE (IsSync is null or IsSync ='' or IsSync =0) ").ToList ();
				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/aircraft/saveAircrafts", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}


					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/aircraft/getAircrafts", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblAircraft>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblAircraft> ("SELECT fldAircraftID FROM tblAircraft WHERE fldAircraftID='" + item.fldAircraftID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					}


				}
			} catch (Exception ex) {

			}
		}

		#endregion

		#region Units

		public void ManageUnits ()
		{
			try {
				
			
				var result = database.Query<tblUnits> ("SELECT * FROM tblUnits WHERE (IsSync is null or IsSync ='' or IsSync =0) ").ToList ();
				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/units/saveUnits", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = client.PostAsync (url, content).Result;

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							item.IsSync = true;
							database.Update (item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var url = string.Format ("{0}/units/getUnits", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = client.GetAsync (url).Result;

					if (resp.IsSuccessStatusCode) {
						var	resultList = JsonConvert.DeserializeObject<List<tblUnits>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							var resultCheck = database.Query<tblUnits> ("SELECT fldUnitID FROM tblUnits WHERE fldUnitID='" + item.fldUnitID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					}


				}
			} catch (Exception ex) {

			}
		}

		#endregion


		#region Registrations

		public async Task ManageRegistrationsDelete ()
		{

			try {
				
			
				var result = database.Query<tblDeletedReg> ("SELECT * FROM  tblDeletedReg WHERE  fldUserId='" + Account.UserId + "' ").ToList ();

				if (result.Count > 0) {
					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/registrations/deleteRegistrations", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = await client.PostAsync (url, content);

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {

							database.Delete (item);
						}

					}


				}
			} catch (Exception ex) {

			}
			return;
		}

		public async Task ManageRegistrations ()
		{
			try {
				
			

				var result = database.Query<tblRegistrations> ("SELECT * FROM  tblRegistrations WHERE  IsSync =0 and fldUserId='" + Account.UserId + "' ").ToList ();
				//var result = database.Query<tblPhotos> ("SELECT * FROM tblPhotos where (IsSync is null or IsSync ='' or IsSync =0 )  limit 100 ").ToList ();
				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/registrations/saveRegistrations", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
//					var resp = client.PostAsync (url,content).Result;
						var resp = await client.PostAsync (url, content);
						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							System.Diagnostics.Debug.WriteLine ("getRegistrations POST" + item.fldRegistrationID);
							item.IsSync = true;
							database.Update (item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var updateTime = database.Query<tblSyncDetail>("SELECT * FROM tblSyncDetail WHERE fldSyncTableName='"+CommonHelper.TabelName.tblRegistrations.ToString()+"' AND fldUserId='"+Account.UserId+"'").FirstOrDefault();
					var dtTemp =Convert.ToString(updateTime.fldSyncTime);
					var url = string.Format ("{0}/registrations/getRegistrations/" + Account.UserId + "/'"+dtTemp+"'", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
						var response = resp.Content.ReadAsStringAsync ().Result;
						var syncData =  JsonConvert.DeserializeObject<ParseData<tblRegistrations>> (response);
						var	resultList = syncData.DataList;
						foreach (var item in resultList) {
							System.Diagnostics.Debug.WriteLine ("getRegistrations GET" + item.fldRegistrationID);
							var resultCheck = database.Query<tblRegistrations> ("SELECT fldRegistrationID FROM tblRegistrations WHERE fldRegistrationID='" + item.fldRegistrationID + "' and fldUserId='" + Account.UserId + "'").FirstOrDefault ();
							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}
					
						if(updateTime!=null)
						{
							updateTime.fldSyncTime = syncData.SyncDate;
							database.Update(updateTime);
						}
					}


				}
			} catch (Exception ex) {

			}
			return;
		}

		#endregion

		#region Visits

		public async Task ManageVisits ()
		{
			try {
				
			
				//var result = database.Query<tblVisits> ("SELECT * FROM tblVisits WHERE IsSync is null or IsSync ='' or IsSync =0 limit 100 ").ToList ();
				var result = database.Query<tblVisits> ("SELECT * FROM tblVisits WHERE (IsSync is null or IsSync ='' or IsSync =0) and fldAirportVisitID in (select fldAirportVisitID from tblRegistrations where fldUserId='" + Account.UserId + "')  ").ToList ();
				if (result.Count > 0) {

					//POST lastest data
					using (var client = new HttpClient ()) {
						var url = string.Format ("{0}/visits/saveVisits", baseUrl);
						var jsonResult = JsonConvert.SerializeObject (result);
						HttpContent content = new StringContent (jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
						var resp = await client.PostAsync (url, content);

						if (resp.IsSuccessStatusCode) {

						}
						foreach (var item in result) {
							System.Diagnostics.Debug.WriteLine ("ManageVisits POST" + item.fldAirportVisitID);
							item.IsSync = true;
							database.Update (item);
						}
						System.Diagnostics.Debug.WriteLine ("ManageVisits POST");
					}


				}
				//GET lastest data
				using (var client = new HttpClient ()) {
					var updateTime = database.Query<tblSyncDetail>("SELECT * FROM tblSyncDetail WHERE fldSyncTableName='"+CommonHelper.TabelName.tblVisits.ToString()+"' AND fldUserId='"+Account.UserId+"'").FirstOrDefault();
					var dtTemp =Convert.ToString(updateTime.fldSyncTime);

					var url = string.Format ("{0}/visits/getVisits/" + Account.UserId + "/'"+dtTemp+"'", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {

						var response = resp.Content.ReadAsStringAsync ().Result;
						var syncData =  JsonConvert.DeserializeObject<ParseData<tblVisits>> (response);
						var	resultList = syncData.DataList;

						//var	resultList = JsonConvert.DeserializeObject<List<tblVisits>> (resp.Content.ReadAsStringAsync ().Result);
						foreach (var item in resultList) {
							System.Diagnostics.Debug.WriteLine ("ManageVisits GET" + item.fldAirportVisitID);
							var resultCheck = database.Query<tblVisits> ("SELECT fldAirportVisitID FROM tblVisits WHERE fldAirportVisitID='" + item.fldAirportVisitID + "'").FirstOrDefault ();

							if (resultCheck == null) {
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}

						if(updateTime!=null)
						{
							updateTime.fldSyncTime = syncData.SyncDate;
							database.Update(updateTime);
						}


					}


				}
			} catch (Exception ex) {

			}
			return;
		}

		#endregion

		#region Photos

		public async Task  ManagePhotos ()
		{
			try {
				
			
				var result = database.Query<tblPhotos> ("SELECT * FROM tblPhotos where (IsSync is null or IsSync ='' or IsSync =0) and fldRegistrationID in (select fldRegistrationID from tblRegistrations where fldUserId='" + Account.UserId + "') limit 1 ").ToList ();
				string imageBase64 = string.Empty;

				Device.BeginInvokeOnMainThread (() => {      
					IPicture picture = DependencyService.Get<IPicture> ();
					//result[0].fldPhotoFilename="885e54dc-930b-412f-9124-07903d579879";
			
					if (result.Count > 0) {
						//POST lastest data
						System.Diagnostics.Debug.WriteLine ("ManagePhotos POST");
						imageBase64 = picture.GetPictureBase64 (result [0].fldPhotoFilename);
						using (var client = new HttpClient ()) {
							var url = string.Format ("{0}/photos/savePhotoStream", baseUrl);
							result [0].fldFileStream = imageBase64;
							var jsonResult = JsonConvert.SerializeObject (result);
							HttpContent content = new StringContent (jsonResult);
							content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
							var resp = client.PostAsync (url, content).Result;

							if (resp.IsSuccessStatusCode) {

							}
							foreach (var item in result) {
								System.Diagnostics.Debug.WriteLine("ManagePhotos POST" + item.fldPhotoID);
								item.fldFileStream = "N/A";
								item.IsSync = true;
								database.Update (item);
							}

						}
					}
				});


		
				//GET lastest data
				using (var client = new HttpClient ()) {
					var updateTime = database.Query<tblSyncDetail>("SELECT * FROM tblSyncDetail WHERE fldSyncTableName='"+CommonHelper.TabelName.tblPhotos.ToString()+"' AND fldUserId='"+Account.UserId+"'").FirstOrDefault();
					var dtTemp =Convert.ToString(updateTime.fldSyncTime);

					var url = string.Format ("{0}/photos/getPhotos/" + Account.UserId + "/'"+dtTemp+"'", baseUrl);
					var jsonResult = JsonConvert.SerializeObject (result);
					HttpContent content = new StringContent (jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue ("application/json");
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
						//var	resultList = JsonConvert.DeserializeObject<List<tblPhotos>> (resp.Content.ReadAsStringAsync ().Result);
						var response = resp.Content.ReadAsStringAsync ().Result;
						var syncData =  JsonConvert.DeserializeObject<ParseData<tblPhotos>> (response);
						var	resultList = syncData.DataList;

						foreach (var item in resultList) {
							System.Diagnostics.Debug.WriteLine ("ManagePhotos GET" + item.fldPhotoID);
							var resultCheck = database.Query<tblPhotos> ("SELECT fldPhotoID FROM tblPhotos WHERE fldPhotoID='" + item.fldPhotoID + "'").FirstOrDefault ();
							if (resultCheck == null) {
								var _Picture = DependencyService.Get<IPicture> ();
								await _Picture.DownloadFileFromUrl (item.fldPhotoFilename, CommonHelper.BaseUrl);
								item.IsSync = true;
								database.Insert (item);
							} else {
								item.IsSync = true;
								database.Update (item);
							}

						}

						if(updateTime!=null)
						{
							updateTime.fldSyncTime = syncData.SyncDate;
							database.Update(updateTime);
						}



					}


				}

			} catch (Exception ex) {

			}

			return;
		}

		#endregion

	}

	public class ParseData<T> where T : new()
	{
		public List<T> DataList;
		public string SyncDate;
	}
}

