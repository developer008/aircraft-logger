﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

namespace Aircraft_Logger
{
	public class RealTimeDBSyncDatabase
	{
		private SQLiteConnection database;
		static object locker = new object();
		string baseUrl = CommonHelper.BaseUrl;

		public RealTimeDBSyncDatabase()
		{
			database = DependencyService.Get<ISQLite>().GetConnection();
		}

		#region Registrations

		public async void ManageRegistrationsRealTime(tblRegistrations objTblReg)
		{
			try
			{

				var result = new List<tblRegistrations>();
				result.Add(objTblReg);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/registrations/saveRegistrations", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);
						if (resp.IsSuccessStatusCode)
						{
							foreach (var item in result)
							{
								item.IsSync = true;
								database.Update(item);
							}
						}
					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/registrations/getRegistrations/" + Account.UserId + "", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblRegistrations>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblRegistrations>("SELECT fldRegistrationID FROM tblRegistrations WHERE fldRegistrationID='" + item.fldRegistrationID + "' and fldUserId='" + Account.UserId + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		public async void ManageRegistrationsDeleteRealTime(tblRegistrations objTblReg)
		{
			try
			{


				var result = new List<tblRegistrations>();
				result.Add(objTblReg);
				if (result.Count > 0)
				{
					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/registrations/deleteRegistrations", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}

			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Aircraft

		public async void ManageAircraftRealTime(tblAircraft objTblAircraft)
		{
			try
			{


				var result = new List<tblAircraft>();
				result.Add(objTblAircraft);
				//var result = database.Query<tblAircraft> ("SELECT * FROM tblAircraft WHERE IsSync is null or IsSync ='' or IsSync =0 limit 100 ").ToList ();
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/aircraft/saveAircrafts", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/aircraft/getAircrafts", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblAircraft>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblAircraft>("SELECT fldAircraftID FROM tblAircraft WHERE fldAircraftID='" + item.fldAircraftID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region AirForces

		public async void ManageAirForcesRealTime(tblAirForces objTblAirforce)
		{
			try
			{


				//var result = database.Query<tblAirForces> ("SELECT * FROM tblAirForces WHERE IsSync is null or IsSync =''").ToList ();
				var result = new List<tblAirForces>();
				result.Add(objTblAirforce);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/airforces/saveAirForces", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/airforces/getAirForces", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblAirForces>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblAirForces>("SELECT fldAirForceID FROM tblAirForces WHERE fldAirForceID='" + item.fldAirForceID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Airlines

		public async void ManageAirlinesRealTime(tblAirlines objTblAirline)
		{
			try
			{


				//var result = database.Query<tblAirlines> ("SELECT * FROM tblAirlines WHERE IsSync is null or IsSync ='' or IsSync =0 limit 100 ").ToList ();
				var result = new List<tblAirlines>();
				result.Add(objTblAirline);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/airlines/saveAirlines", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/airlines/getAirlines", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblAirlines>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblAirlines>("SELECT fldAirlineID FROM tblAirlines WHERE fldAirlineID='" + item.fldAirlineID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Airports

		public async void ManageAirportsRealTime(tblAirports objTblAirports)
		{
			try
			{


				//var result = database.Query<tblAirports> ("SELECT * FROM tblAirports WHERE IsSync is null or IsSync ='' or IsSync =0 limit 100 ").ToList ();
				var result = new List<tblAirports>();
				result.Add(objTblAirports);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/airports/saveAirport", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/airports/getAirports", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblAirports>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblAirports>("SELECT fldAirportID FROM tblAirports WHERE fldAirportID='" + item.fldAirportID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Manufacturers

		public async void ManageManufacturersRealTime(tblManufacturers objTblManu)
		{
			try
			{


				//var result = database.Query<tblManufacturers> ("SELECT * FROM tblManufacturers WHERE IsSync is null or IsSync ='' or IsSync =0 limit 100 ").ToList ();
				var result = new List<tblManufacturers>();
				result.Add(objTblManu);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/manufacturers/saveManufacturer", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/manufacturers/getManufacturers", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblManufacturers>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblManufacturers>("SELECT fldManufacturerID FROM tblManufacturers WHERE fldManufacturerID='" + item.fldManufacturerID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Continents

		public async void ManageContinentsRealTime(tblContinents objTblContinents)
		{
			try
			{


				//var result = database.Query<tblContinents> ("SELECT * FROM tblContinents WHERE IsSync is null or IsSync =''").ToList ();
				var result = new List<tblContinents>();
				result.Add(objTblContinents);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/continents/saveContinent", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/continents/getContinent", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblContinents>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblContinents>("SELECT fldContinentID FROM tblContinents WHERE fldContinentID='" + item.fldContinentID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}
						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Countries

		public async void ManageCountriesRealTime(tblCountries objTblCountry)
		{
			try
			{


				//var result = database.Query<tblCountries> ("SELECT * FROM tblCountries WHERE IsSync is null or IsSync =''").ToList ();
				var result = new List<tblCountries>();
				result.Add(objTblCountry);
				if (result.Count > 0)
				{
					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/countries/saveCountries", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}
					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/countries/getCountries", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblCountries>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblCountries>("SELECT fldCountryID FROM tblCountries WHERE fldCountryID='" + item.fldCountryID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}


		public List<tblCountries> GetCountries()
		{
			using (var client = new HttpClient())
			{
				var result = new List<tblCountries>();
				var url = string.Format("{0}/countries/getCountries", baseUrl);
				var jsonResult = JsonConvert.SerializeObject(result);
				HttpContent content = new StringContent(jsonResult);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				var resp = client.GetAsync(url).Result;

				if (resp.IsSuccessStatusCode)
				{
					var resultList = JsonConvert.DeserializeObject<List<tblCountries>>(resp.Content.ReadAsStringAsync().Result);
					result = resultList;
					foreach (var item in resultList)
					{
						var resultCheck = database.Query<tblCountries>("SELECT fldCountryID FROM tblCountries WHERE fldCountryID='" + item.fldCountryID + "'").FirstOrDefault();
						if (resultCheck == null)
						{
							item.IsSync = true;
							database.Insert(item);
						}
						else {
							item.IsSync = true;
							database.Update(item);
						}

					}
					return resultList;
				}

				return result;
			}
		}

		#endregion

		#region Photos

		public async void ManagePhotosRealTime(tblPhotos objTblPhotos)
		{
			try
			{


				var result = new List<tblPhotos>();
				result.Add(objTblPhotos);
				string imageBase64 = string.Empty;

				//Device.BeginInvokeOnMainThread (() => {      
				IPicture picture = DependencyService.Get<IPicture>();
				//result[0].fldPhotoFilename="885e54dc-930b-412f-9124-07903d579879";

				if (result.Count > 0)
				{
					//POST lastest data
					imageBase64 = picture.GetPictureBase64(result[0].fldPhotoFilename);
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/photos/savePhotoStream", baseUrl);
						result[0].fldFileStream = imageBase64;
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.fldFileStream = "N/A";
							item.IsSync = true;
							database.Update(item);
						}

					}
				}
				//});


				/*
			//GET lastest data
			using (var client = new HttpClient ()) {
				var url =string.Format("{0}/photos/getPhotos/"+Account.UserId+"",baseUrl);
				var jsonResult= JsonConvert.SerializeObject (result);
				HttpContent content = new StringContent(jsonResult);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				var resp = client.GetAsync (url).Result;

				if (resp.IsSuccessStatusCode) {
					var	resultList = JsonConvert.DeserializeObject<List<tblPhotos>> (resp.Content.ReadAsStringAsync ().Result);
					foreach (var item in resultList) {
						var resultCheck = database.Query<tblPhotos> ("SELECT fldPhotoID FROM tblPhotos WHERE fldPhotoID='"+item.fldPhotoID+"'").FirstOrDefault ();
						if (resultCheck == null) {
							item.IsSync = true;
							database.Insert (item);
						}
						else {
							item.IsSync = true;
							database.Update (item);
						}

					}
				}


			}
			*/
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Units

		public async void ManageUnitsRealTime(tblUnits objTblUnit)
		{
			try
			{


				//var result = database.Query<tblUnits> ("SELECT * FROM tblUnits WHERE IsSync is null or IsSync ='' or IsSync =0 limit 100 ").ToList ();
				var result = new List<tblUnits>();
				result.Add(objTblUnit);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/units/saveUnits", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/units/getUnits", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblUnits>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblUnits>("SELECT fldUnitID FROM tblUnits WHERE fldUnitID='" + item.fldUnitID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Visits

		public async void ManageVisitsRealTime(tblVisits objTblVisit)
		{
			try
			{


				var result = new List<tblVisits>();
				result.Add(objTblVisit);
				if (result.Count > 0)
				{

					//POST lastest data
					using (var client = new HttpClient())
					{
						var url = string.Format("{0}/visits/saveVisits", baseUrl);
						var jsonResult = JsonConvert.SerializeObject(result);
						HttpContent content = new StringContent(jsonResult);
						content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
						var resp = await client.PostAsync(url, content);

						if (resp.IsSuccessStatusCode)
						{

						}
						foreach (var item in result)
						{
							item.IsSync = true;
							database.Update(item);
						}

					}


				}
				//GET lastest data
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/visits/getVisits/" + Account.UserId + "", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp =await client.GetAsync(url);

					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<List<tblVisits>>(await resp.Content.ReadAsStringAsync());
						foreach (var item in resultList)
						{
							var resultCheck = database.Query<tblVisits>("SELECT fldAirportVisitID FROM tblVisits WHERE fldAirportVisitID='" + item.fldAirportVisitID + "'").FirstOrDefault();
							if (resultCheck == null)
							{
								item.IsSync = true;
								database.Insert(item);
							}
							else {
								item.IsSync = true;
								database.Update(item);
							}

						}
					}


				}
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

	}
}

