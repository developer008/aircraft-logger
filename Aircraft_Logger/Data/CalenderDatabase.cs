﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SQLite;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;


namespace Aircraft_Logger
{
	public class CalenderDatabase
	{
		List<YearMonthAirportId> objlstYearMonthAirportId = new List<YearMonthAirportId>();
		private SQLiteConnection database;
		public CalenderDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
		}

		public List<YearMonthAirportId> GetYearMonthByAirportVisitByYear()
		{
			List<YearMonthAirportId> q = database.Query<YearMonthAirportId> (
				" Select  tv.fldVisitDate, tv.fldAirportVisitID, tv.fldAirportID"
				+ " From tblVisits tv where tv.fldAirportVisitID in (Select fldAirportVisitID from tblRegistrations where fldUserId='"+Account.UserId+"')"
				+ " order by fldAirportVisitID desc"
			);

			var result = q;

			foreach (var item in result) {
				try {
					DateTime date = DateTime.ParseExact (item.fldVisitDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
					item.VisitDate = date;
					item.strMonth = item.VisitDate.ToString ("MMMM");
					item.Month = date.Month;
					item.Year = date.Year;
				} catch (Exception ex) {
					DateTime TestVal = DateTime.ParseExact (item.fldVisitDate, "dd-MM-yyyy", null);
					item.VisitDate = TestVal;
					item.strMonth = TestVal.ToString ("MMMM");;
					item.Month =TestVal.Month;
					item.Year =TestVal.Year;
				}
			}

			var grouped = (from p in result
				group p by new { year = p.VisitDate.Year} into d
				select new YearMonthAirportId {  Year = d.Key.year}).OrderByDescending(x=>x.Year).ToList ();	
		
			return grouped;

		}
		public List<YearMonthAirportId> GetYearMonthByAirportVisitByYearMonth()
		{
			List<YearMonthAirportId> q = database.Query<YearMonthAirportId> (
				" Select  tv.fldVisitDate, tv.fldAirportVisitID, tv.fldAirportID"
				+ " From tblVisits tv where tv.fldAirportVisitID in (Select fldAirportVisitID from tblRegistrations where fldUserId='"+Account.UserId+"')"
				+ " order by fldAirportVisitID desc"
			);

			var result = q;

			foreach (var item in result) {
				try {
					DateTime date = DateTime.ParseExact (item.fldVisitDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
					item.VisitDate = date;
					item.strMonth = item.VisitDate.ToString ("MMMM");
					item.Month = date.Month;
					item.Year = date.Year;
				} catch (Exception ex) {
					DateTime TestVal = DateTime.ParseExact (item.fldVisitDate, "dd-MM-yyyy", null);
					item.VisitDate = TestVal;
					item.strMonth = TestVal.ToString ("MMMM");;
					item.Month =TestVal.Month;
					item.Year =TestVal.Year;
				}
			}

			objlstYearMonthAirportId = result;
			var grouped = (from p in result
				group p by new { month = p.VisitDate.Month,year = p.VisitDate.Year,stMonth = p.strMonth } into d
				select new YearMonthAirportId { Month = d.Key.month, Year = d.Key.year, strMonth = d.Key.stMonth }).ToList ();	
			/*
			foreach (var item in grouped) {
				item.lstRegistration= this.GetRegistrationsByYear (item.Year, grouped);
			}
			*/
			return grouped;

		}

		public List<Registrations> GetRegistrationsByYear (int year, List<YearMonthAirportId> lstYearMonth)
		{

			var result2 = objlstYearMonthAirportId.Where (x => x.Year == year ).ToList ();
			List<Registrations> objReg = new List<Registrations> ();
			foreach (var item in result2) {
				var reg = new Registrations ();
				reg = (new CommonDatabase ()).GetRegistrationByAirportId (item.fldAirportVisitID);
				try {
					reg.VisitDate = item.VisitDate;
				} catch (Exception ex) {
				}
				objReg.Add (reg);
			}
			return objReg;
		}
		public List<TimelineRegistration> GetRegistrationsByYear (int year,int month, List<YearMonthAirportId> lstYearMonth)
		{

			var result2 = objlstYearMonthAirportId.Where (x => x.Year == year  && x.Month==month).ToList ();
			List<Registrations> objReg = new List<Registrations> ();
			foreach (var item in result2) {
				var reg = new Registrations ();
				reg = (new CommonDatabase ()).GetRegistrationByAirportId (item.fldAirportVisitID);
				try {
					reg.VisitDate = item.VisitDate;
				} catch (Exception ex) {
				}
				objReg.Add (reg);
			}

			var grouped = new List<TimelineRegistration> ();
			try {
				grouped = (from p in objReg.Where (x => x.VisitDate != null)
					group p by new { day = p.VisitDate.Day,airportId = p.AirportId,airportName = p.AirportName } into d
					select new TimelineRegistration { Day = d.Key.day, AirportId = d.Key.airportId, AirportName = d.Key.airportName }).ToList ();	

				foreach (var item in grouped) {
					item.lstRegistration = objReg.Where (x => x.VisitDate.Year ==year && x.VisitDate.Day ==item.Day && x.AirportId == item.AirportId).Skip(0).Take(3).ToList ();
				}
			} catch (Exception ex) {
			}

			return grouped;
		}
	}
}

