﻿using System;
using System.Collections.Generic;
using SQLite;
using Xamarin.Forms;
using System.Linq;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Aircraft_Logger
{
	public class CommonDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();

		public CommonDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();

		}

		public TotalRegistrationCounts GetTotalRegistrationCounts ()
		{
			List<TotalRegistrationCounts> q = database.Query<TotalRegistrationCounts> (
				                                  " select (select count(*) from tblRegistrations where fldUserId='" + Account.UserId + "') as [TotalRegistrations]  "
				                                  + " , (select count(*) from tblRegistrations WHERE fldUserId='" + Account.UserId + "' and fldUnitID IS NOT NULL ) as [TotalUniqueRegistrations] "
				                                  + " , (select count(*) from tblRegistrations WHERE fldUserId='" + Account.UserId + "' and fldAircraftType = 'C' ) as [TotalCivilRegistrations] "
				                                  + " , (select count(*) from tblRegistrations WHERE fldUserId='" + Account.UserId + "' and fldAircraftType = 'M' ) as [TotalMilitaryRegistrations] "
				                                  + " ,(select count(*) from tblRegistrations where fldUserId='" + Account.UserId + "' ) as [ThisYearTotalRegistrations] ");
			return q.FirstOrDefault ();
		}

		public List<YearMonthAirportId> GetYearMonth ()
		{
			List<YearMonthAirportId> q = database.Query<YearMonthAirportId> (
				                             " Select  tv.fldVisitDate, tv.fldAirportVisitID, tv.fldAirportID" +
				                             " From tblVisits tv where tv.fldAirportVisitID in (Select fldAirportVisitID from tblRegistrations where fldUserId='" + Account.UserId + "')" +
				                             " order by rowid desc"
			                             );
			return q.ToList ();
		}


		public Registrations GetRegistrationByAirportId (string airportVisitId)
		{
			try {
				string Query = " SELECT Tp.fldPhotoFilename as [PhotoName], Ta.fldAirportName as AirportName, Av.fldAirportID as AirportId, Reg.fldRegistrationID as [RegistrationID] ,Reg.fldRegistration as [Registration],Reg.fldCn as [Cn], "
				               + " Reg.fldAirlineID AS [AirlineID], Aln.fldAirlineName as [AirlineName], "
				               + " Reg.fldAircraftID AS [AircraftID], Acf.fldAircraftName as [AircraftName] , "
				               + " Reg.fldUnitID AS [UnitID] , fldUnitName1 AS [UnitName1] , fldUnitName2 AS [UnitName2], fldCode AS [Code],"
				               + " Av.fldAirportVisitID AS [AirportVisitID] ,Av.fldLocationGPS AS [LocationGPS] ,Av.fldVisitDate AS [VisitDate],fldAircraftType AS [AircraftType],fldNew AS [New],fldRemarks AS [Remarks] "
				               + " FROM tblRegistrations Reg "
				               + " LEFT JOIN tblAircraft Acf ON Acf.fldAircraftID = Reg.fldAircraftID "
				               + " LEFT JOIN tblAirlines Aln ON Aln.fldAirlineID = Reg.fldAirlineID "
				               + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID "
				               + " LEFT JOIN tblUnits U ON U.fldUnitID = Reg.fldUnitID "
				               + " LEFT JOIN tblAirports Ta ON Ta.fldAirportID = Av.fldAirportID"
				               + " LEFT JOIN tblPhotos Tp ON Tp.fldRegistrationID = Reg.fldRegistrationID"
				               + " WHERE Av.fldAirportVisitID = '" + airportVisitId + "'";
				var q = database.Query<Registrations> (Query).FirstOrDefault ();
				return q;
			} catch (Exception ex) {
				return new Registrations ();
			}
		}
		public List<Registrations> GetRegistrationByAirportId1(string airportVisitId)
		{
			try
			{
				string Query = " SELECT Tp.fldPhotoFilename as [PhotoName], Ta.fldAirportName as AirportName, Av.fldAirportID as AirportId, Reg.fldRegistrationID as [RegistrationID] ,Reg.fldRegistration as [Registration],Reg.fldCn as [Cn], "
							   + " Reg.fldAirlineID AS [AirlineID], Aln.fldAirlineName as [AirlineName], "
							   + " Reg.fldAircraftID AS [AircraftID], Acf.fldAircraftName as [AircraftName] , "
							   + " Reg.fldUnitID AS [UnitID] , fldUnitName1 AS [UnitName1] , fldUnitName2 AS [UnitName2], fldCode AS [Code],"
							   + " Av.fldAirportVisitID AS [AirportVisitID] ,Av.fldLocationGPS AS [LocationGPS] ,Av.fldVisitDate AS [VisitDate],fldAircraftType AS [AircraftType],fldNew AS [New],fldRemarks AS [Remarks] "
							   + " FROM tblRegistrations Reg "
							   + " LEFT JOIN tblAircraft Acf ON Acf.fldAircraftID = Reg.fldAircraftID "
							   + " LEFT JOIN tblAirlines Aln ON Aln.fldAirlineID = Reg.fldAirlineID "
							   + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID "
							   + " LEFT JOIN tblUnits U ON U.fldUnitID = Reg.fldUnitID "
							   + " LEFT JOIN tblAirports Ta ON Ta.fldAirportID = Av.fldAirportID"
							   + " LEFT JOIN tblPhotos Tp ON Tp.fldRegistrationID = Reg.fldRegistrationID"
							   + " WHERE Av.fldAirportVisitID = '" + airportVisitId + "'";
				var q = database.Query<Registrations>(Query).ToList();
				return q;
			}
			catch (Exception ex)
			{
				return new List<Registrations>();
			}
		}

		public List<Registrations> GetAllRegistrations ()
		{
			try {
				string Query = " SELECT Tf.fldAirForceID as AirForceID, Tf.fldAirForceName as AirForceName, Tc.fldCountryID as CountryId, Tc.fldCountryName as CountryName, Tc.fldCountryAviationCode as CountryCode, Tp.fldPhotoFilename as [PhotoName], Ta.fldAirportName as AirportName, Av.fldAirportID as AirportId, Reg.fldRegistrationID as [RegistrationID] ,Reg.fldRegistration as [Registration],Reg.fldCn as [Cn], "
				               + " Reg.fldAirlineID AS [AirlineID], Aln.fldAirlineName as [AirlineName], "
				               + " Reg.fldAircraftID AS [AircraftID], Acf.fldAircraftName as [AircraftName] , "
				               + " Reg.fldUnitID AS [UnitID] , fldUnitName1 AS [UnitName1] , fldUnitName2 AS [UnitName2], fldCode AS [Code],"
				               + " Av.fldAirportVisitID AS [AirportVisitID] ,Av.fldLocationGPS AS [LocationGPS] ,Av.fldVisitDate, fldAircraftType AS [AircraftType],fldNew AS [New],fldRemarks AS [Remarks] "
				               + " FROM tblRegistrations Reg "
				               + " LEFT JOIN tblAircraft Acf ON Acf.fldAircraftID = Reg.fldAircraftID "
				               + " LEFT JOIN tblAirlines Aln ON Aln.fldAirlineID = Reg.fldAirlineID "
				               + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID "
				               + " LEFT JOIN tblUnits U ON U.fldUnitID = Reg.fldUnitID "
				               + " LEFT JOIN tblAirports Ta ON Ta.fldAirportID = Av.fldAirportID "
				               + " LEFT JOIN tblAirForces Tf ON Tf.fldAirForceID = U.fldAirForceID "
				               + " LEFT JOIN tblPhotos Tp ON Tp.fldRegistrationID = Reg.fldRegistrationID "
				               + " LEFT JOIN tblCountries Tc ON Tc.fldCountryID = Ta.fldCountryID "
				               + " WHERE  Reg.fldUserId='" + Account.UserId + "' ";
			
				var q = database.Query<Registrations> (Query).ToList ();
				foreach (var item in q) {
					try {
						
						DateTime date = DateTime.ParseExact (item.fldVisitDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
						item.VisitDate = date;


					} catch (Exception ex) {

					}
				}
				return q;
			} catch (Exception ex) {
				return new List<Registrations> ();
			}
		}

		public List<Registrations> GetAllAirportVisit ()
		{
			try {
				
				string	Query = "SELECT Av.fldVisitDate, Reg.fldRegistrationID as [RegistrationID] ,Reg.fldRegistration as [Registration],Reg.fldCn as [Cn] "
				               + "FROM tblRegistrations Reg"
				               + " LEFT JOIN tblVisits Av ON Av.fldAirportVisitID = Reg.fldAirportVisitID "
				               + " WHERE  Reg.fldUserId='" + Account.UserId + "' ";
				var q = database.Query<Registrations> (Query).ToList ();
				foreach (var item in q) {
					try {

						DateTime date = DateTime.ParseExact (item.fldVisitDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
						item.VisitDate = date;


					} catch (Exception ex) {

					}
				}
				return q;
			} catch (Exception ex) {
				return new List<Registrations> ();
			}
		}

		public static DateTime FirstDayOfWeek (DateTime date)
		{
			DayOfWeek fdow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
			int offset = fdow - date.DayOfWeek;
			DateTime fdowDate = date.AddDays (offset);
			return fdowDate;
		}

		public static DateTime LastDayOfWeek (DateTime date)
		{
			DateTime ldowDate = FirstDayOfWeek (date).AddDays (6);
			return ldowDate;
		}

		public TotalRegistrationCountsMainMenu GetTotalRegistrationCountsMainMenu ()
		{
			List<TotalRegistrationCountsMainMenu> q = database.Query<TotalRegistrationCountsMainMenu> (
				                                          " select (select count(*) from tblRegistrations where fldUserId='" + Account.UserId + "') as [TotalRegistrations]  "
				                                          + " ,(select  fldDownloadedApp  from tblUser where fldUserId='" + Account.UserId + "') as [DownloadAppDate] "
				                                          + " ,0 as [TotalDays] "
				                                          + " ,(select count(*) from tblRegistrations Reg inner join tblVisits Tv on Tv.fldAirportVisitID = Reg.fldAirportVisitID "
				                                          + " where Reg.fldUserId='" + Account.UserId + "' and datetime(substr(Tv.fldVisitDate, 7, 4) || '-' || substr(Tv.fldVisitDate, 4, 2) || '-' || substr(Tv.fldVisitDate, 1, 2))  Between '" + FirstDayOfWeek (DateTime.Now).ToString ("yyyy-MM-dd") + "' and '" + LastDayOfWeek (DateTime.Now).ToString ("yyyy-MM-dd") + "' ) as [ThisWeek] "
				                                          + " ,(select count(*) from tblRegistrations Reg inner join tblVisits Tv on Tv.fldAirportVisitID = Reg.fldAirportVisitID "
				                                          + "  where  Reg.fldUserId='" + Account.UserId + "' and datetime(substr(Tv.fldVisitDate, 7, 4) || '-' || substr(Tv.fldVisitDate, 4, 2) || '-' || substr(Tv.fldVisitDate, 1, 2)) Between '" + DateTime.Now.AddDays (-1).ToString ("yyyy-MM-dd") + "' and  '" + DateTime.Now.AddDays (1).ToString ("yyyy-MM-dd") + "' ) as [TotalToday] "
				                                          + " ,(select count(*) from tblphotos where fldRegistrationID in (Select fldRegistrationID from tblRegistrations where  fldUserId='" + Account.UserId + "')) as [TotalPhotos]"
				                                          + " ,(select count(*) from tblVisits where fldAirportVisitID in (Select fldAirportVisitID  from tblRegistrations where  fldUserId='" + Account.UserId + "')) as [TotalVisit] ");
				                                          
			return q.FirstOrDefault ();
		}

		public List<KeyValuePair<String,int>> GetManufacturersWiseRegistrations (int Top)
		{
			List<KeyValuePair<String,int>> q = database.Query<KeyValuePair<String,int>> (
				                                   " SELECT  fldManufacturerName AS [Key], Count (*) AS [Value] FROM tblRegistrations Reg  "
				                                   + " INNER JOIN tblAircraft Ac ON Ac.fldAircraftID = Reg.fldAircraftID "
				                                   + " INNER JOIN tblManufacturers Man on Ac.fldManufacturerID = Man.fldManufacturerID  "
				                                   + " where Reg.fldUserId='" + Account.UserId + "'"
				                                   + " GROUP BY Ac.fldManufacturerID  ORDER BY Count (*)  desc " + (Top == 0 ? "" : " LIMIT 5 "));
			return q;
		}

		public List<KeyValuePair<String,int>> GetAircraftsWiseRegistrations (int Top)
		{
			List<KeyValuePair<String,int>> q = database.Query<KeyValuePair<String,int>> (
				                                   " SELECT  fldAircraftName As [Key] , Count (*) AS [Value] FROM tblRegistrations Reg  "
				                                   + " INNER JOIN tblAircraft Ac ON Ac.fldAircraftID = Reg.fldAircraftID "
				                                   + " where Reg.fldUserId='" + Account.UserId + "'"
				                                   + " GROUP BY Ac.fldAircraftID  ORDER BY Count (*)  desc " + (Top == 0 ? "" : " LIMIT 5 "));
			return q;
		}

		public List<KeyValuePair<String,int>> GetAirlinesWiseRegistrations (int Top)
		{
			List<KeyValuePair<String,int>> q = database.Query<KeyValuePair<String,int>> (
				                                   " SELECT  fldAirlineName As [Key] , Count (*) AS [Value] FROM tblRegistrations Reg  "
				                                   + " INNER JOIN tblAirlines Al ON Al.fldAirlineID = Reg.fldAirlineID "
				                                   + " where Reg.fldUserId='" + Account.UserId + "'"
				                                   + " GROUP BY Al.fldAirlineID  ORDER BY Count (*)  desc " + (Top == 0 ? "" : " LIMIT 5 "));
			return q;
		}

		public List<KeyValuePair<String,int>> GetAirportWiseRegistrations (int Top)
		{
			List<KeyValuePair<String,int>> q = database.Query<KeyValuePair<String,int>> (
				                                   " SELECT  fldAirportName As [Key] , Count (*) AS [Value] FROM tblRegistrations Reg  "
				                                   + " INNER JOIN tblVisits Av on Av.fldAirportVisitID = Reg.fldAirportVisitID "
				                                   + " INNER JOIN tblAirports Ap ON Ap.fldAirportID = Av.fldAirportID "
				                                   + " where Reg.fldUserId='" + Account.UserId + "'"
				                                   + " GROUP BY Ap.fldAirportID  ORDER BY Count (*)  desc " + (Top == 0 ? "" : " LIMIT 5 "));
			return q;
		}

		public List<KeyValuePair<String,int>> GetAirportVisitWiseRegistrations (int Top)
		{
			List<KeyValuePair<String,int>> q = database.Query<KeyValuePair<String,int>> (
				                                   " SELECT  fldAirportName As [Key] , Count (*) AS [Value] FROM tblRegistrations Reg "
				                                   + " INNER JOIN tblVisits Av on Av.fldAirportVisitID = Reg.fldAirportVisitID "
				                                   + " INNER JOIN tblAirports Ap ON Ap.fldAirportID = Av.fldAirportID "
				                                   + " where Reg.fldUserId='" + Account.UserId + "'"
				                                   + " GROUP BY Av.fldAirportVisitID  ORDER BY Count (*)  desc " + (Top == 0 ? "" : " LIMIT 5 "));
			return q;
		}

		public List<KeyValuePair<String,int>> GetContrieWiseRegistrations (int Top)
		{
			List<KeyValuePair<String,int>> q = database.Query<KeyValuePair<String,int>> (
				                                   " SELECT  fldCountryName As [Key] , Count (*) AS [Value] FROM tblRegistrations Reg "
				                                   + " INNER JOIN tblVisits Av on Av.fldAirportVisitID = Reg.fldAirportVisitID "
				                                   + " INNER JOIN tblAirports Ap ON Ap.fldAirportID = Av.fldAirportID "
				                                   + " INNER JOIN tblCountries C On C.fldCountryID = Ap.fldCountryID "
				                                   + " where Reg.fldUserId='" + Account.UserId + "'"
				                                   + " GROUP BY C.fldCountryID  ORDER BY Count (*)  desc " + (Top == 0 ? "" : " LIMIT 5 "));
			return q;
		}

		public List<KeyValuePair<String,int>> GetAirforceWiseRegistrations (int Top)
		{
			List<KeyValuePair<String,int>> q = database.Query<KeyValuePair<String,int>> (
				                                   " SELECT  fldAirForceName As [Key], Count (*) AS [Value] FROM tblRegistrations Reg "
				                                   + " INNER JOIN tblUnits U on Reg.fldUnitID = U.fldUnitID "
				                                   + " INNER JOIN tblAirForces Af ON Af.fldAirForceID = U.fldAirForceID "
				                                   + " where Reg.fldUserId='" + Account.UserId + "'"
				                                   + " GROUP BY Af.fldAirForceID  ORDER BY Count (*)  desc " + (Top == 0 ? "" : " LIMIT 5 "));
			return q;
		}

		public List<CountryWiseCount> GetTotalRegistrationCountsByCountry ()
		{
			List<CountryWiseCount> q = database.Query<CountryWiseCount> (
				                           "  select count(tblregistrations.fldUserId) as Count,tblairports.fldAirportName as CountryName from tblregistrations "
				                           + " left join tblvisits on tblvisits.fldAirportVisitID = tblregistrations.fldAirportVisitID "
				                           + " left join tblairports on tblairports.fldAirportID = tblvisits.fldAirportID "
				                           + " left join tblcountries on tblcountries.fldCountryID = tblairports.fldCountryID "
				                           + " where tblregistrations.fldUserId='" + Account.UserId + "' and tblcountries.fldCountryID is not null "
				                           + " group by tblairports.fldAirportID ");
			return q.ToList ();
		}

	}
}

