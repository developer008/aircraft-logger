﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Aircraft_Logger
{
	public class UserDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		string baseUrl = CommonHelper.BaseUrl;

		public UserDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			database.CreateTable<tblUser> ();
		}

		public tblUser GetUser (string UserID)
		{
			lock (locker) {
				return database.Table<tblUser> ().Where (c => c.fldUserId == UserID).FirstOrDefault ();
			}
		}

		public bool CheckAuthntication (string userName, string password)
		{
			bool result = false;
			lock (locker) {
				var data = database.Table<tblUser> ().Where	(x => x.fldEmail.ToLower () == userName.ToLower () && x.fldPassword.ToLower () == password.ToLower ()).FirstOrDefault ();
				if (data != null) {
					Account.UserId = data.fldUserId;
					Account.DefaultAirportId = data.fldDefaultAirportID;
					userdefault us;
					us = DependencyService.Get<userdefault>();
					us.setmyCountry(data.fldCountryID);
					result = true;
				} else {
					if (CommonHelper.IsConnected) {

						result = CheckAuthnticationLive (userName, password);
					} else {
						return result;
					}
				}
			}
			return result;

//			Account.UserId = "abc123";
//			Account.DefaultAirportId = "";
//
//			return true;
		}

		public bool CheckAuthnticationLive(string userName, string password)
		{
			bool result = false;
			//GET lastest data
			try
			{
				using (var client = new HttpClient())
				{
					var url = string.Format("{0}/users/checkUser/" + userName + "/" + password + "", baseUrl);
					var jsonResult = JsonConvert.SerializeObject(result);
					HttpContent content = new StringContent(jsonResult);
					content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
					var resp = client.GetAsync(url).Result;
					if (resp.IsSuccessStatusCode)
					{
						var resultList = JsonConvert.DeserializeObject<tblUser>(resp.Content.ReadAsStringAsync().Result);
						if (resultList == null)
						{
							return false;
						}
						var resultCheck = database.Query<tblUser>("SELECT fldUserId FROM tblUser WHERE fldUserId='" + resultList.fldUserId + "'").FirstOrDefault();
						if (resultCheck == null)
						{
							resultList.IsSync = true;
							CommonHelper.FirstTImeCome = 1;
							Account.UserId = resultList.fldUserId;
							Account.DefaultAirportId = resultList.fldDefaultAirportID;
							database.Insert(resultList);
							CommonHelper.FirstTImeCome = 1;

							SetupSyncData();

							result = true;
						}
					}
				}
			}
			catch (Exception ex)
			{

			}
			return result;

		}
		public void SetupSyncData()
		{
			
			var tableList =Enum.GetValues(typeof(CommonHelper.TabelName)).Cast<CommonHelper.TabelName>().Select(v => v.ToString()).ToList();
			var tblSync = new tblSyncDetail ();
			foreach (var item in tableList) {
				tblSync = new tblSyncDetail ();
				tblSync.fldSyncTableId = Guid.NewGuid ().ToString ();
				tblSync.fldSyncTableName = item.ToString ();
				tblSync.fldUserId = Account.UserId;
				tblSync.fldSyncTime = null;
				database.Insert (tblSync);
			}
		}
		public List<tblUser> GetUserList ()
		{
			lock (locker) {
				var MyList = (from c in database.Table<tblUser> ()
				              select c).ToList ();
				return MyList;
			}
		}

		public string SaveUser (tblUser User)
		{
			//var resr = GetUserList ();
			User.IsSync = false;
			lock (locker) {
				if (!string.IsNullOrEmpty (User.fldUserId)) {
					database.Update (User);
					return User.fldUserId;
				} else {
					User.fldUserId = Guid.NewGuid ().ToString ();
					database.Insert (User);
					return User.fldUserId;
				}
			}
		}

		public async void UploadRegistrationDataToServer(tblUser User)
		{
			//POST lastest data
			using (var client = new HttpClient())
			{
				User.fldFirstSync = true;
				var url = string.Format("{0}/users/saveUser", baseUrl);
				var jsonResult = JsonConvert.SerializeObject(User);
				HttpContent content = new StringContent(jsonResult);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
				var resp = await client.PostAsync(url, content);

				if (resp.IsSuccessStatusCode)
				{
					var result = database.Table<tblUser>().Where(x => x.fldEmail == User.fldEmail).FirstOrDefault();
					if (result == null)
					{
						database.Insert(User);
						SetupSyncData();
					}
				}
			}
		}

		public bool CheckUserSyncStatus (string userId)
		{
			lock (locker) {
				var data = database.Table<tblUser> ().Where	(x => x.fldUserId == userId).FirstOrDefault ();
				if (data != null) {
					return data.fldFirstSync.Value;
				} 
				return true;
			}
		}

		public void UpdateUserStatus (string userId)
		{
			var resultCheck = database.Query<tblUser> ("SELECT * FROM tblUser WHERE fldUserId='" + userId + "'").FirstOrDefault ();
			if (resultCheck != null) {
				resultCheck.fldFirstSync = false;
				database.Update (resultCheck);
			}
		}
	}
}

