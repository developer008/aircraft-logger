﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Aircraft_Logger
{
	public class SearchesDatabase
	{
		private SQLiteConnection database;
		static object locker = new object();

		public SearchesDatabase ()
		{
			database = DependencyService.Get<ISQLite>().GetUserDBConnection();
			database.CreateTable<tblSearches>();
		}

		public tblSearches GetSearches(string SearchesID){
			//lock (locker) {
				return database.Table<tblSearches> ().Where (c => c.fldSearchesId == SearchesID).FirstOrDefault();
			//}
		}

		public List<tblSearches> GetSearches(){
			//lock (locker) {
				var MyList = (from c in database.Table<tblSearches> ()
					select c).ToList ();
				return MyList;
			//}
		}

		public string SaveSearches(tblSearches Searches){
			Searches.IsSync = false;
			//lock (locker) {
				if (!string.IsNullOrEmpty(Searches.fldSearchesId)) {
					database.Update (Searches);
					return Searches.fldSearchesId;
				} else {
					 database.Insert (Searches);
					return Searches.fldSearchesId;
				}
			//}
		}
	}
}

