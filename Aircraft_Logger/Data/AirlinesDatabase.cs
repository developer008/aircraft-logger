﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Aircraft_Logger
{
	public class AirlinesDatabase
	{
		private SQLiteConnection database;
		static object locker = new object();

		public AirlinesDatabase ()
		{
			database = DependencyService.Get<ISQLite>().GetConnection();
			database.CreateTable<tblAirlines>();
		}

		public tblAirlines GetAirlinesById(string id){
			lock (locker) {
				return database.Table<tblAirlines> ().Where (c => c.fldAirlineID == id).FirstOrDefault();
			}
		}

		public tblAirlines GetAirlines(string airlineName){
			lock (locker) {
				return database.Table<tblAirlines> ().Where (c => c.fldAirlineName == airlineName).FirstOrDefault();
			}
		}

		public List<tblAirlines> GetAirlinesList(){
			lock (locker) {
				var MyList = (from c in database.Table<tblAirlines> ()
					select c).ToList ();
				return MyList;
			}
		}

		public string SaveAirlines(tblAirlines Airline){
			

				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Airline.IsSync = true;
					if (!string.IsNullOrEmpty(Airline.fldAirlineID)) {
					lock (locker)
					{
						database.Update(Airline);
					}
						objSync.ManageAirlinesRealTime (Airline);
						return Airline.fldAirlineID;
					} else {
						Airline.fldAirlineID = Guid.NewGuid ().ToString ();
					lock (locker)
					{
						database.Insert(Airline);
					}
						objSync.ManageAirlinesRealTime (Airline);
						return Airline.fldAirlineID;
					}
				} else {
					Airline.IsSync = false;
					if (!string.IsNullOrEmpty(Airline.fldAirlineID)) {
					lock (locker)
					{
						database.Update(Airline);
					}
						return Airline.fldAirlineID;
					} else {
						Airline.fldAirlineID = Guid.NewGuid ().ToString ();
					lock (locker)
					{
						database.Insert(Airline);
					}
						return Airline.fldAirlineID;
					}
				}


		}

		public List<Airlines> SearchAirLines (int Top , int LastCount,string AirlineName)
		{
			var q = database.Query<Airlines> (
				  " SELECT fldAirlineID as [AirlineID] , fldAirlineName AS [AirlineName], fldIATAcode AS [IATAcode],fldICAOcode AS [ICAOcode] "
				+ " FROM tblAirlines WHERE 1 = 1 "
				+ (LastCount == 0?"":" AND CAST(fldAirlineID as INTEGER) > " + LastCount + " ")
				+ (Convert.ToString( AirlineName) == string.Empty?"":" AND fldAirlineName like '%" + Convert.ToString(AirlineName) + "%' " +  " ")
				+ (Top == 0?"0":" LIMIT " + Top + " ")
				).ToList ();
			return q;
		}
	}
}

