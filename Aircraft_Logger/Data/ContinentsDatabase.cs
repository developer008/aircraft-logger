﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Aircraft_Logger
{
	public class ContinentsDatabase
	{
		private SQLiteConnection database;
		static object locker = new object ();
		//tblAirForces

		public ContinentsDatabase ()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			database.CreateTable<tblContinents> ();
		}

		public tblContinents GetContinent (string ContinentId)
		{
			lock (locker) {
				return database.Table<tblContinents> ().Where (c => c.fldContinentID == ContinentId).FirstOrDefault ();
			}
		}

		public List<tblContinents> GetContinentsList ()
		{
			lock (locker) {
				var MyList = (from c in database.Table<tblContinents> ()
				              select c).ToList ();
				return MyList;
			}
		}

		public string SaveContinent (tblContinents Continent)
		{
			
			lock (locker) {
				if (CommonHelper.IsConnected) {
					RealTimeDBSyncDatabase objSync = new RealTimeDBSyncDatabase ();
					Continent.IsSync = true;
					if (!string.IsNullOrEmpty (Continent.fldContinentID)) {
						database.Update (Continent);
						objSync.ManageContinentsRealTime (Continent);
						return Continent.fldContinentID;
					} else {
						Continent.fldContinentID = Guid.NewGuid ().ToString ();
						database.Insert (Continent);
						objSync.ManageContinentsRealTime (Continent);
						return Continent.fldContinentID;
					}
				} else {

					Continent.IsSync = false;
					if (!string.IsNullOrEmpty (Continent.fldContinentID)) {
						database.Update (Continent);
						return Continent.fldContinentID;
					} else {
						Continent.fldContinentID = Guid.NewGuid ().ToString ();
						database.Insert (Continent);
						return Continent.fldContinentID;
					}
				}
			}
		}
	}
}

