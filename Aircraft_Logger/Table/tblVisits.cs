﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblVisits
	{
		[PrimaryKeyAttribute]
		public string fldAirportVisitID{
			get;
			set;
		}

		public string fldAirportID{
			get;
			set;
		}

		public string fldVisitDate{
			get;
			set;
		}

		public string fldLocationGPS {
			get;
			set;
		}

		public bool IsSync {
			get;
			set;
		}
		public string tmpId{ get; set;}
	}
}

