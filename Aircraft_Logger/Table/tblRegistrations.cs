﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblRegistrations
	{
		[PrimaryKeyAttribute]
		public string fldRegistrationID {
			get;
			set;
		}
		public string fldRegistration {
			get;
			set;
		}
		public string fldCn {
			get;
			set;
		}
		public string fldAircraftID {
			get;
			set;
		}
		public string fldAirlineID {
			get;
			set;
		}
		public string fldUnitID {
			get;
			set;
		}
		public string fldAircraftType {
			get;
			set;
		}
		public string fldNew {
			get;
			set;
		}
		public string fldAirportVisitID {
			get;
			set;
		}
		public string fldRemarks {
			get;
			set;
		}

		public string fldCode {
			get;
			set;
		}

		public bool IsSync { 
			get; 
			set; 
		}
		public string tmpId{ get; set;}

		public string fldUserId{ get; set;}

		public bool IsDeleted{ get; set;}

		public Nullable<DateTime>  fldSyncDateTime{ get; set;}
	
	}
}

