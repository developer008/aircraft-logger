﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblSyncDetail
	{
		[PrimaryKeyAttribute]
		public string fldSyncTableId {
			get;
			set;
		}

		public string fldSyncTableName {
			get;
			set;
		}
		public string fldSyncTime {
			get;
			set;
		}
		public string fldUserId {
			get;
			set;
		}
	}
}

