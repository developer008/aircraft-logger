﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblAirports
	{
		[PrimaryKeyAttribute]
		public string fldAirportID {
			get;
			set;
		}

		public string fldAirportName {
			get;
			set;
		}

		public string fldAirportCity {
			get;
			set;
		}

		public string fldIATACode {
			get;
			set;
		}

		public string fldICAOCode{
			get;
			set;
		}

		public string fldCountryID{
			get;
			set;
		}

		public string fldGPSCoordinates{
			get;
			set;
		}

		public bool IsSync {
			get;
			set;
		}
		public string tmpId{ get; set;}
	}
}

