﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblManufacturers
	{
		[PrimaryKeyAttribute]
		public string fldManufacturerID{
			get;
			set;
		}

		public string fldManufacturerName{
			get;
			set;
		}
		public bool IsSync {
			get;
			set;
		}
		public string tmpId{ get; set;}
		public string fldCountryID{
			get;
			set;
		}

	}
}

