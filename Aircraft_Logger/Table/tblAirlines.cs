﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblAirlines
	{
		[PrimaryKeyAttribute]
		public string fldAirlineID{
			get;
			set;
		}

		public string fldAirlineName {
			get;
			set;
		}

		public string fldIATAcode {
			get;
			set;
		}

		public string fldICAOcode {
			get;
			set;
		}

		public bool IsSync {
			get;
			set;
		}

		public string tmpId{ get; set;}

		public string fldCountryID{
			get;
			set;
		}

	}
}

