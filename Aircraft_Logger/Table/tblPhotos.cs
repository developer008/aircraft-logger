﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblPhotos
	{
		[PrimaryKeyAttribute]
		public string fldPhotoID{
			get;
			set;
		}

		public string fldRegistrationID{
			get;
			set;
		}

		public string fldPhotoFilename {
			get;
			set;
		}

		public bool IsSync { 
			get; 
			set; 
		}

		public bool fldIsPrimary {
			get;
			set;
		}

		public string fldFileStream{ get; set;}

		public string tmpId{ get; set;}
	}
}

