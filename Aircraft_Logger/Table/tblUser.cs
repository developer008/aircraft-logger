﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblUser
	{
		[PrimaryKeyAttribute]
		public string fldUserId{ get; set;}

		public string fldFirstName{ get; set;}

		public string fldLastName{ get; set;}

		public string fldUserName{ get; set;}

		public string fldCountryID{ get; set;}

		public string fldEmail{ get; set;}

		public string fldPassword{ get; set;}

		public string fldDefaultAirportID{ get; set;}

		public string fldAircraftPreferance{ get; set;}

		public string fldDownloadedApp{ get; set;}

		public string fldEndofDataSub{ get; set;}

		public bool IsSync { 
			get; 
			set; 
		}

		public Nullable<bool> fldFirstSync {
			get;
			set;
		}

//		public string fldUserId { get; set; }
//
//		public string fldUserName { get; set; }
//
//		public string fldFirstName { get; set; }
//
//		public string fldLastName { get; set; }
//
//		public int fldCountryId { get; set; }
//
//		public string fldCity { get; set; }
//
//		public int fldYearBorn { get; set; }
//
//		public DateTime fldStartDateApp { get; set; }
//
//		public int fldSubscriptionPackage { get; set; }
//
//		public int fldStartDateSubscription { get; set; }
//
//		public int fldPrivacySettings { get; set; }
//
//		public bool IsSync { get; set; }
	}
}

