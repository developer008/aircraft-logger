﻿using System;

namespace Aircraft_Logger
{
	public class tblSearches
	{
		public string fldSearchesId { get; set; }

		public string fldUserID { get; set; }

		public DateTime fldDate { get; set; }

		public TimeSpan fldTime { get; set; }

		public string fldQueuery { get; set; }

		public bool IsSync { get; set; }
	}
}

