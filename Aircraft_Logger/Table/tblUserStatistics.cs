﻿using System;

namespace Aircraft_Logger
{
	public class tblUserStatistics
	{
		public string fldUserStatistics { get; set; }

		public int fldUserID { get; set; }

		public DateTime fldDate { get; set; }

		public int fldNumberOfRegistrationsCivil  { get; set; }

		public int fldNumberOfRegistrationsMilitary { get; set; }

		public int fldPrivacySettings { get; set; }

		public bool IsSync { get; set; }
	}
}

