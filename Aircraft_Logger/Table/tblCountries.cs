﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblCountries
	{
		[PrimaryKeyAttribute]
		public string fldCountryID{
			get;
			set;
		}

		public string fldCountryName{
			get;
			set;
		}

		public string fldCountryAviationCode {
			get;
			set;
		}

		public string fldContinentID {
			get;
			set;
		}

		public bool IsSync {
			get;
			set;
		}
		public string tmpId{ get; set;}

		public string fldScrambleMilitaryCode{get;set;}

		public string fldCountryCode{ get; set;}


	}
}

