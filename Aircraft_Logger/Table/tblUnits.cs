﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblUnits
	{
		[PrimaryKeyAttribute]
		public string fldUnitID{
			get;
			set;
		}

		public string fldUnitName1{
			get;
			set;
		}

		public string fldUnitName2 {
			get;
			set;
		}

		public string fldTailcode{
			get;
			set;
		}

		public string fldAirForceID{
			get;
			set;
		}

		public bool IsSync { 
			get; 
			set; 
		}
		public string tmpId{ get; set;}
	}
}