﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblAirForces
	{
		[PrimaryKeyAttribute]
		public string fldAirForceID{
			get;
			set;
		}
		public string fldAirForceNameShort{
			get;
			set;
		}
		public string fldAirForceName{
			get;
			set;
		}
		public string fldCountryID{
			get;
			set;
		}

		public string fldAirForceNameLocal{
			get;
			set;
		}

		public bool IsSync {
			get;
			set;
		}
		public string tmpId{ get; set;}
	}
}

