﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblAircraft
	{
		[PrimaryKeyAttribute]
		public string fldAircraftID {
			get;
			set;
		}
		public string fldAircraftName {
			get;
			set;
		}
		public string fldAircraftNameGenericy {
			get;
			set;
		}

		public string fldManufacturerID {
			get;
			set;
		}

		public bool IsSync {
			get;
			set;
		}
		public string tmpId{ get; set;}
	}
}

