﻿using System;
using SQLite;

namespace Aircraft_Logger
{
	public class tblContinents
	{
		[PrimaryKeyAttribute]
		public string fldContinentID{
			get;
			set;
		}

		public string fldContinentName {
			get;
			set;
		}

		public bool IsSync {
			get;
			set;
		}

		public string tmpId{ get; set;}
	}
}

