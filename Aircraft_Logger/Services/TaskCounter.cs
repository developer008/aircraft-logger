﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Threading;

namespace Aircraft_Logger
{
	public class TaskCounter
	{
		DBSyncDatabase dbSync = new DBSyncDatabase ();
		public async Task RunCounter(CancellationToken token)
		{
			await Task.Run (async () => {

				for (long i = 0; i < long.MaxValue; i++) {
					try {
						token.ThrowIfCancellationRequested ();
//						await Task.Delay(30000);
						var message = new TickedMessage { 
							Message = i.ToString()
						};
						//dbSync.ManageDatabase();
						//CommonHelper.IsConnected = DependencyService.Get<INetworkConnection>().CheckNetworkConnection();
						await Task.Delay(30000);
//						Device.BeginInvokeOnMainThread(() => {
//							MessagingCenter.Send<TickedMessage>(message, "TickedMessage");
//						});
					} catch (Exception ex) {
						
					}
				}
			}, token);
		}


	}

	public class TickedMessage
	{
		public string Message { get; set; }
	}

	public class CancelledMessage
	{
		public CancelledMessage ()
		{
		}
	}

	public class StopLongRunningTaskMessage
	{
		public StopLongRunningTaskMessage ()
		{
		}
	}

	public class StartLongRunningTaskMessage
	{
		public StartLongRunningTaskMessage ()
		{
		}
	}
}

