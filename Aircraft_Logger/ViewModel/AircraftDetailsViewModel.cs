﻿using System;
using System.Collections.Generic;

namespace Aircraft_Logger
{
	public class AircraftDetailsViewModel
	{
		public AircraftDetailsViewModel ()
		{
		}

		public tblAircraft GetAircraftIfExist(string aircraftName)
		{
			try {
				return  (new AircraftDatabase ()).GetAircraft (aircraftName);
			} catch (Exception ex) {
				return null;
			}
		}

		public tblAirlines GetAirlinIfExist(string airlineName)
		{
			
			try {
				return  (new AirlinesDatabase ()).GetAirlines (airlineName);
			} catch (Exception ex) {
				return null;
			}
		}

		public tblUnits GetUnitsIfExist(string UnitName)
		{
			try {
				return  (new UnitsDatabase ()).GetUnits (UnitName);
			} catch (Exception ex) {
				return null;
			}
		}

		public bool DeleteRegistration (tblRegistrations Registration)
		{
			return  (new RegistrationsDatabase ()).DeleteRegistration (Registration);
		}

		public Registrations GetRegistrationWithDetail (string registrationId)
		{
			return  (new RegistrationsDatabase ()).GetRegistrationWithDetail (registrationId);
		}

		public List<tblPhotos> GetPhotosByRegistrationId (string registrationId)
		{
			return  (new PhotosDatabase ()).GetPhotosByRegistarId (registrationId);
		}

		public List<Aircraft> SearchAircraft(int Top , int LastCount,string AircraftName)
		{
			return (new AircraftDatabase ()).SearchAircraft (Top,LastCount,AircraftName);
		}

		public List<Airlines> SearchAirline(int Top , int LastCount,string AirlineName)
		{
			return (new AirlinesDatabase ()).SearchAirLines(Top,LastCount,AirlineName);
		}

		public List<Units> SearchUnits(int Top , int LastCount,string UnitName)
		{
			return (new UnitsDatabase ()).SearchUnits (Top,LastCount,UnitName);
		}

		public List<AirForces> SearcAirForces(int Top , int LastCount,string AirforceName)
		{
			return (new AirForcesDatabase ()).SearchAirForces (Top,LastCount,AirforceName);
		}

		public List<Manufacturers> SearcManufacturers(int Top , int LastCount,string Manufacturer)
		{
			return (new ManufacturersDatabase ()).GetManufacturerList (Top,LastCount, Manufacturer);
		}

		public List<Countries> SearcCountries(int Top , int LastCount,string CountryName)
		{
			return (new CountriesDatabase ()).SearchCountrie(Top,LastCount,CountryName);
		}

		public string AddRegistration(tblRegistrations registrations)
		{
			return (new RegistrationsDatabase ()).SaveRegistrations(registrations);
		}

		public string AddAircraft(tblAircraft Aircraft )
		{
			try {
				return (new AircraftDatabase ()).SaveAircraft (Aircraft );
				//return true;
			} catch (Exception ex) {
				return string.Empty;
			}
		}

		public string AddAirline(tblAirlines Airlines )
		{
			try {
				return (new AirlinesDatabase ()).SaveAirlines (Airlines );
			} catch (Exception ex) {
				return string.Empty;	
			}
		}

		public string AddUnits(tblUnits Units )
		{
			try {
				return (new UnitsDatabase ()).SaveUnit (Units );
			} catch (Exception ex) {
				return string.Empty;
			}
		}

		public string AddAirforce(tblAirForces Airforce )
		{
			try {
				string ab =(new AirForcesDatabase ()).SaveAirforce (Airforce );
				return ab;
			} catch (Exception ex) {
				return "";
			}
		}

		public bool AddManufacturer(tblManufacturers Manufacturer )
		{
			try {
				(new ManufacturersDatabase ()).SaveManufacturer(Manufacturer );
				return true;
			} catch (Exception ex) {
				return false;
			}
		}

		public string AddPhoto(tblPhotos Photos )
		{
			try {
				return (new PhotosDatabase ()).SavePhotos(Photos );
			} catch (Exception ex) {
				return string.Empty;
			}
		}
	}
}

