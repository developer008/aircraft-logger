﻿using System;
using System.Collections.Generic;
using SQLite;
using Xamarin.Forms;
using System.Linq;

namespace Aircraft_Logger
{
	public class StatisticsViewModel
	{
		private SQLiteConnection database;
		static object locker = new object();
		public StatisticsViewModel ()
		{
			database = DependencyService.Get<ISQLite>().GetUserDBConnection();
		}

		public TotalRegistrationCounts GetTotaoCounts()
		{
			return (new CommonDatabase()).GetTotalRegistrationCounts();
		}
		public List<KeyValuePair<String,int>> GetManufacturersWiseRegistrations(int Top = 0)
		{
			return (new CommonDatabase ()).GetManufacturersWiseRegistrations (Top);
		}

		public List<KeyValuePair<String,int>> GetAircraftsWiseRegistrations(int Top = 0)
		{
			return (new CommonDatabase ()).GetAircraftsWiseRegistrations (Top);
		}

		public List<KeyValuePair<String,int>> GetAirlinesWiseRegistrations(int Top = 0)
		{
			return (new CommonDatabase ()).GetAirlinesWiseRegistrations (Top);
		}

		public List<KeyValuePair<String,int>> GetAirportWiseRegistrations(int Top = 0)
		{
			return (new CommonDatabase ()).GetAirportWiseRegistrations (Top);
		}

		public List<KeyValuePair<String,int>> GetAirportVisitWiseRegistrations(int Top = 0)
		{
			return (new CommonDatabase ()).GetAirportVisitWiseRegistrations (Top);
		}

		public List<KeyValuePair<String,int>> GetContrieWiseRegistrations(int Top = 0)
		{
			return (new CommonDatabase ()).GetContrieWiseRegistrations (Top);
		}

		public List<KeyValuePair<String,int>> GetAirforceWiseRegistrations(int Top = 0)
		{
			return (new CommonDatabase ()).GetAirforceWiseRegistrations (Top);
		}
		public List<Registrations> GetAllRegistrations()
		{
			return (new CommonDatabase ()).GetAllRegistrations();
		}
		public List<Registrations> GetAllAirportVisits()
		{
			return (new CommonDatabase ()).GetAllAirportVisit();
		}
		public List<ChartData> GetAllRegistrationsInYearRange(List<Registrations> lsRegistrations,List<ChartData> objChartData)
		{
			
			foreach (var item in objChartData) {
				item.dataCount = lsRegistrations.Where (x => x.VisitDate.Year == item.chartKey).ToList ().Count;
			}
			return objChartData;
		}
		public List<ChartData> GetAllAirportVisitsInYearRange(List<Registrations> lsRegistrations,List<ChartData> objChartData)
		{

			foreach (var item in objChartData) {
				item.dataCount = lsRegistrations.Where (x => x.VisitDate.Year == item.chartKey).ToList ().Count;
			}
			return objChartData;
		}

	}
}

