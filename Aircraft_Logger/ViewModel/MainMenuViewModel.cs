﻿using System;

namespace Aircraft_Logger
{
	public class MainMenuViewModel
	{
		public MainMenuViewModel ()
		{
		}

		public TotalRegistrationCountsMainMenu GetTotalRegistrationCountsMainMenu ()
		{
			return (new CommonDatabase()).GetTotalRegistrationCountsMainMenu();
		}
	}
}