﻿using System;
using System.Collections.Generic;

namespace Aircraft_Logger
{
	public class AddRegistrationViewModel
	{
		public AddRegistrationViewModel ()
		{
		}

		public bool GetIfDuplicate (string Registration)
		{
			try {
				return (new RegistrationsDatabase ()).IsRegistrationExists (Registration);
			} catch (Exception ex) {
				return false;
			}
		}

		public List<tblRegistrations> GetIfDuplicate (string Registration, string AirportId)
		{
			try {
				return (new RegistrationsDatabase ()).IsRegistrationExists (Registration, AirportId);
			} catch (Exception ex) {
				return new List<tblRegistrations> ();
			}
		}

		public List<AddRegistrationSearchList> SearchRegistrationDetailList (int top, int LastCount, DateTime registerDate, string Registration, char IsCivilOrMilitary, string orderBy,string airportId)
		{
			return (new RegistrationsDatabase ()).SearchRegistrationDetailList (top, LastCount, registerDate, Registration, IsCivilOrMilitary, orderBy,airportId);
		}

		public string SaveRegistration (tblRegistrations Registrations)
		{
			return (new RegistrationsDatabase ()).SaveRegistrations (Registrations);
		}

		public string SaveVisits (tblVisits Visits)
		{
			return (new VisitsDatabase ()).SaveVisits (Visits);
		}

		public string SavePhoto (tblPhotos Photo)
		{
			return (new PhotosDatabase ()).SavePhotos (Photo);
		}

		public tblVisits GetIfVisitExists (string AirportId, string GpsLocation, DateTime visitDate)
		{
			return (new VisitsDatabase ()).GetIfVisitExists (AirportId, GpsLocation, visitDate);
		}

		public List<Airports> SearchAirports (int Top, int LastCount, string Manufacturer)
		{
			return (new AirportsDatabase ()).GetAirportsList (Top, LastCount, Manufacturer);
		}

		public string AddAirport (tblAirports Airports)
		{
			try {
				return (new AirportsDatabase ()).SaveAirports (Airports);
			} catch (Exception ex) {
				return string.Empty;
			}
		}

		public List<Countries> SearcCountries (int Top, int LastCount, string CountryName)
		{
			return (new CountriesDatabase ()).SearchCountrie (Top, LastCount, CountryName);
		}

		public string AddUpdateManufacturer(tblManufacturers manufacturer)
		{
			return (new ManufacturersDatabase ()).SaveManufacturer (manufacturer);
		}
		public tblManufacturers GetManufacturer(string manufacturerName)
		{
			return (new ManufacturersDatabase ()).GetManufacturers (manufacturerName);
		}
		public string AddUpdateAircraft(tblAircraft aircraft)
		{
			return (new AircraftDatabase ()).SaveAircraft (aircraft);
		}
		public string AddUpdateUnit(tblUnits unit)
		{
			return (new UnitsDatabase ()).SaveUnit (unit);
		}

		public tblAircraft GetAircraft(string aircraftName)
		{
			return (new AircraftDatabase ()).GetAircraft (aircraftName);
		}
		public string AddUpdateAirline(tblAirlines airlines)
		{
			return (new AirlinesDatabase ()).SaveAirlines (airlines);
		}
		public tblAirlines GetAirline(string airlineName)
		{
			return (new AirlinesDatabase ()).GetAirlines (airlineName);
		}
		public tblUnits GetUnit(string unitName)
		{
			return (new UnitsDatabase ()).GetUnits (unitName);
		}
		public tblUnits GetUnitbyid(string unitid)
		{
			return (new UnitsDatabase()).GetUnitsById(unitid);
		}
	}
}

