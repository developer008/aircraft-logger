﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace Aircraft_Logger
{
	public class TimelineModel
	{

		List<YearMonthAirportId> objlstYearMonthAirportId = new List<YearMonthAirportId>();
		public List<YearMonthAirportId> GetYearMonth ()
		{
			var result = (new CommonDatabase ()).GetYearMonth ();

			foreach (var item in result) {
				try {
					DateTime date = DateTime.ParseExact (item.fldVisitDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
					item.VisitDate = date;
					item.strMonth = item.VisitDate.ToString ("MMMM");
					item.Month = date.Month;
					item.Year = date.Year;
				} catch (Exception ex) {
					DateTime TestVal = DateTime.ParseExact (item.fldVisitDate, "dd-MM-yyyy", null);
					item.VisitDate = TestVal;
					item.strMonth = TestVal.ToString ("MMMM");;
					item.Month =TestVal.Month;
					item.Year =TestVal.Year;
					
				}
			}
			objlstYearMonthAirportId = result;
			var grouped = (from p in result
			               group p by new { month = p.VisitDate.Month,year = p.VisitDate.Year,stMonth = p.strMonth } into d
			               select new YearMonthAirportId { Month = d.Key.month, Year = d.Key.year, strMonth = d.Key.stMonth }).OrderByDescending(x=>x.Month).ToList ();	
			
			return grouped;

		}


		public List<TimelineRegistration> GetRegistrationsByYearMonth (int year, int month, List<YearMonthAirportId> lstYearMonth)
		{
			
			var result2 = objlstYearMonthAirportId.Where (x => x.VisitDate.Year ==year && x.VisitDate.Month ==month ).ToList ();
			//var result2 = objlstYearMonthAirportId.Where (x => x.VisitDate.Year == 2016 && x.VisitDate.Month == 3).ToList ();
			List<Registrations> objReg = new List<Registrations> ();
			objReg.Sort();
			foreach (var item in result2) {
				var reg = new List< Registrations> ();
				reg = (new CommonDatabase ()).GetRegistrationByAirportId1 (item.fldAirportVisitID);
				try {
					foreach (var a in reg)
					{
						a.VisitDate = item.VisitDate;
						objReg.Add(a);
					}
				} catch (Exception ex) {
				}
				//objReg.Add (reg);
			}

			var grouped = new List<TimelineRegistration> ();
			try {
				grouped = (from p in objReg.Where (x => x.VisitDate != null)
				          group p by new { day = p.VisitDate.Day,airportId = p.AirportId,airportName = p.AirportName } into d
				          select new TimelineRegistration { Day = d.Key.day, AirportId = d.Key.airportId, AirportName = d.Key.airportName }).ToList ();	
			
				foreach (var item in grouped) {
					item.lstRegistration = objReg.Where (x => x.VisitDate.Day == item.Day && x.VisitDate.Year ==year && x.AirportId == item.AirportId).Skip(0).Take(3).ToList ();

				}
			} catch (Exception ex) {
			}
	
			return grouped;
		}
	}



}

