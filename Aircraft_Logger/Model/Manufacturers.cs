﻿using System;

namespace Aircraft_Logger
{
	public class Manufacturers
	{
		public Manufacturers ()
		{
		}

		public string ManufacturerID{
			get;
			set;
		}

		public string ManufacturerName{
			get;
			set;
		}
	}
}

