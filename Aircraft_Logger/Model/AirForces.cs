﻿using System;

namespace Aircraft_Logger
{
	public class AirForces
	{
		public AirForces ()
		{
		}

		public string AirForceID{
			get;
			set;
		}
		public string AirForceNameShort{
			get;
			set;
		}
		public string AirForceName{
			get;
			set;
		}
		public string CountryID{
			get;
			set;
		}

		public string CountryName{
			get;
			set;
		}

		public string AirForceNameLocal{
			get;
			set;
		}
	}
}

