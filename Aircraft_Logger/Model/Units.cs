﻿using System;

namespace Aircraft_Logger
{
	public class Units
	{
		public Units ()
		{
		}

		public string UnitID{
			get;
			set;
		}

		public string UnitName1{
			get;
			set;
		}

		public string UnitName2 {
			get;
			set;
		}

		public string Tailcode{
			get;
			set;
		}

		public string AirForceID{
			get;
			set;
		}

		public string AirForceNameShort{
			get;
			set;
		}
		public string AirForceName{
			get;
			set;
		}
	}
}

