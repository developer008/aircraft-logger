﻿using System;
using System.Collections.Generic;

namespace Aircraft_Logger
{
	public class Timeline
	{
		public List<YearMonthAirportId> lstYear{ get; set;}
	}
	public class YearMonthAirportId
	{
		public string fldVisitDate{ get; set;}
		public DateTime VisitDate{ get; set;}
		public int Month{ get; set;}
		public string strMonth{ get; set;}
		public int Year{ get; set;}
		public string fldAirportVisitID{ get; set;}
		public string fldAirportID{get;set;}
		public int Day{ get;set;}
		public List<TimelineRegistration> lsttimeRegistration{ get; set;}
		public List<Registrations> lstRegistration{ get; set;}
		public List<YearMonthAirportId> lstYearMonth{ get; set;}
	}
	public class TimelineRegistration{
		public string AirportName{get;set;}
		public string AirportId{ get; set;}
		public int Day{ get; set;}
		public int Year{ get; set;}
		public int Month{ get; set;}
		public string RegistrationNo{ get; set;}
		public List<Registrations> lstRegistration{ get; set;}

	}
}

