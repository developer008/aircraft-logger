﻿using System;
using System.Collections.Generic;

namespace Aircraft_Logger
{
	public class Registrations
	{
		public Registrations ()
		{
		}
		public int RecordNumber{ get; set;}
		public string RegistrationID {
			get;
			set;
		}
		public string Registration {
			get;
			set;
		}
		public string Cn {
			get;
			set;
		}
		public string AircraftID {
			get;
			set;
		}
		public string AircraftName {
			get;
			set;
		}
		public string AirlineID {
			get;
			set;
		}
		public string AirlineName {
			get;
			set;
		}
		public string UnitID {
			get;
			set;
		}
		public string UnitName1{
			get;
			set;
		}

		public string UnitName2 {
			get;
			set;
		}
		public string AircraftType {
			get;
			set;
		}
		public string New {
			get;
			set;
		}
		public string AirportVisitID {
			get;
			set;
		}
		public DateTime VisitDate{
			get;
			set;
		}
		public string fldVisitDate{
			get;
			set;
		}
		public string LocationGPS {
			get;
			set;
		}
		public string Remarks {
			get;
			set;
		}
		public string Code {
			get;
			set;
		}
		public string AirportId{get;set;}
		public string AirportName{ get; set;}
		public string PhotoName{ get; set;}
		public string CountryId{ get; set;}
		public string CountryName{ get; set;}
		public string CountryCode{get;set;}
		public string AirForceID{ get; set;}
		public string AirForceName{ get; set;}
		public string UserId{ get; set;}
	}
	public class GroupByCountry
	{
		//: System.Collections.ObjectModel.ObservableCollection<Registrations>
		public string CountryId{get;set;}
		public string CountryName{ get;set;}
		public string CountryCode{ get; set;}
		public List<Registrations> lsRegistrationCountry{get;set;}
	}
	public class GroupByAircraft
	{
		
		public string AircraftId{get;set;}
		public string AircraftName{ get;set;}
		public List<Registrations> lsRegistrationAircraft{get;set;}
	}
	public class GroupByAirline
	{

		public string AirlineID{get;set;}
		public string AirlineName{ get;set;}
		public List<Registrations> lsRegistrationAirline{get;set;}
	}
	public class GroupByAirforce
	{

		public string AirForceID{get;set;}
		public string AirForceName{ get;set;}
		public List<Registrations> lsRegistrationAirforce{get;set;}
	}
}

