﻿using System;

namespace Aircraft_Logger
{
	public class Airports
	{
		public Airports ()
		{
		}

		public string AirportID {
			get;
			set;
		}

		public string AirportName {
			get;
			set;
		}

		public string AirportCity {
			get;
			set;
		}

		public string IATACode {
			get;
			set;
		}

		public string ICAOCode{
			get;
			set;
		}

		public string CountryID{
			get;
			set;
		}

		public string CountryName{
			get;
			set;
		}

		public string GPSCoordinates{
			get;
			set;
		}
	}
}

