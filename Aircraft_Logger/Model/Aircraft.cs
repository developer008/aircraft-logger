﻿using System;

namespace Aircraft_Logger
{
	public class Aircraft
	{
		public Aircraft ()
		{
		}

		public string AircraftID {
			get;
			set;
		}
		public string AircraftName {
			get;
			set;
		}
		public string AircraftNameGenericy {
			get;
			set;
		}

		public string ManufacturerID {
			get;
			set;
		}

		public string ManufacturerName{
			get;
			set;
		}
	}
}

