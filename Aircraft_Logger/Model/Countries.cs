﻿using System;

namespace Aircraft_Logger
{
	public class Countries
	{
		public Countries ()
		{
		}

		public string CountryID{
			get;
			set;
		}

		public string CountryName{
			get;
			set;
		}

		public string CountryAviationCode {
			get;
			set;
		}

		public string ContinentID {
			get;
			set;
		}

		public string ContinentName {
			get;
			set;
		}
	}
}

