﻿using System;

namespace Aircraft_Logger
{
	public class Visits
	{
		public Visits ()
		{
		}

		public string AirportVisitID{
			get;
			set;
		}

		public string AirportID{
			get;
			set;
		}

		public string AirportName {
			get;
			set;
		}

		public DateTime VisitDate{
			get;
			set;
		}

		public string LocationGPS {
			get;
			set;
		}
	}
}