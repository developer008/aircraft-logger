﻿using System;

namespace Aircraft_Logger
{
	public class Continents
	{
		public Continents ()
		{
		}

		public string ContinentID{
			get;
			set;
		}

		public string ContinentName {
			get;
			set;
		}
	}
}

