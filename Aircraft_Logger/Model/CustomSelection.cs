﻿using System;

namespace Aircraft_Logger
{
	//	public class CustomSelection
	//	{
	//		public CustomSelection ()
	//		{
	//		}
	//	}

	public class AddRegistrationSearchList
	{
		public int RecordNumber { get; set; }

		public string RegistrationID { get; set; }

		public string Registration { get; set; }

		public string AircraftName { get; set; }

		public string CN { get; set; }

		public string AirlineName { get; set; }

		public string UnitName1 { get; set; }

		public DateTime VisitDate{ get; set;}

		public string New{ get; set;}

		public string Code{ get; set;}

		public bool IsVisibleRed{ get; set;}

		public bool IsSync{ get; set;}
	}

	public class TotalRegistrationCountsMainMenu
	{
		public int TotalRegistrations{ get; set; }

		public int TotalDays{ get; set; }

		public int ThisWeek{ get; set; }

		public int TotalToday{ get; set; }

		public int TotalPhotos{ get; set;}

		public int 	TotalVisit{ get; set;}

		public string DownloadAppDate{ get; set;}
	}

	public class TotalRegistrationCounts
	{
		public int TotalRegistrations{ get; set; }

		public int TotalUniqueRegistrations{ get; set; }

		public int TotalCivilRegistrations{ get; set; }

		public int TotalMilitaryRegistrations{ get; set; }

		public int ThisYearTotalRegistrations{ get; set; }
	}

	public class CountryWiseCount{
		public string CountryName{ get; set;}
		public int Count{ get; set;}
	}

	public class KeyValuePair<T,V>{
		public T Key{ get; set; }
		public V Value{ get; set; }
	}
}

