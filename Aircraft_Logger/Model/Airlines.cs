﻿using System;

namespace Aircraft_Logger
{
	public class Airlines
	{
		public Airlines ()
		{
		}

		public string AirlineID{
			get;
			set;
		}

		public string AirlineName {
			get;
			set;
		}

		public string IATAcode {
			get;
			set;
		}

		public string ICAOcode {
			get;
			set;
		}
	}
}

