﻿using System;

namespace Aircraft_Logger
{
	public class Photos
	{
		public Photos ()
		{
		}

		public string PhotoID{
			get;
			set;
		}

		public object PhotoFilename {
			get;
			set;
		}

		public string RegistrationID{
			get;
			set;
		}

	}
}

