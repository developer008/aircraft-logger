﻿using System;

using Xamarin.Forms;

namespace Aircraft_Logger
{
	public class App : Application
	{
		public static IDropboxSync DbSync = DependencyService.Get<IDropboxSync>();
		public App ()
		{

			// The root page of your application
			MainPage = new Login();
			//MainPage = new MainMenu();
			//MainPage = new AircraftDetails();

		}
		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

		void HandleReceivedMessages ()
		{
			MessagingCenter.Subscribe<TickedMessage> (this, "TickedMessage", message => {
				Device.BeginInvokeOnMainThread(() => {
					//ticker.Text = message.Message;
				});
			});

			MessagingCenter.Subscribe<CancelledMessage> (this, "CancelledMessage", message => {
				Device.BeginInvokeOnMainThread(() => {
					//ticker.Text = "Cancelled";
				});
			});
		}


	}
}

