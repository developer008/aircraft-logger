﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Aircraft_Logger
{
	public partial class Settings : ContentPage
	{
		public Settings ()
		{
			InitializeComponent ();
			NavigationPage.SetHasNavigationBar (this, false);
			Bind_ClickEvents ();
		}
		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s,e)=>NavigateBackCommand(s,e);
			stkDone.GestureRecognizers.Add (tapGestureRecognizerCalendar);
		}

		protected void NavigateBackCommand(object sender , EventArgs e)
		{
			Navigation.PopModalAsync (true);
		}
	}
}

