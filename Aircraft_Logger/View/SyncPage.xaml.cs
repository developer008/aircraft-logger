﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Aircraft_Logger
{
	public partial class SyncPage : ContentPage
	{
		public SyncPage ()
		{
			InitializeComponent ();
			THreadStart();

		}
		public async void THreadStart()
		{
			int i = 0;
			DIsplayLoader (true);
			await CommonHelper.SetSyncSetting ();
			i = 1;
			DIsplayLoader (false);
			Navigation.PushModalAsync (new MainMenu ());
		}

		public void DIsplayLoader (bool IsDiaplay)
		{
			loader.IsVisible = IsDiaplay;
		}
	}
}

