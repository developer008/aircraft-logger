﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Aircraft_Logger
{
	public partial class AddManufacturer : ContentPage
	{
		public AddManufacturer ()
		{
			InitializeComponent ();
			Bind_ClickEvents ();
		}

		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s,e)=>NavigateBackCommand(s,e);
			lblBackMenu.GestureRecognizers.Add (tapGestureRecognizerCalendar);
			lblDone.GestureRecognizers.Add (tapGestureRecognizerCalendar);
		}

		protected void NavigateBackCommand(object sender , EventArgs e)
		{
			Navigation.PopModalAsync (true);
		}
	}
}

