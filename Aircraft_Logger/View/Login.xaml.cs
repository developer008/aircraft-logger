﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Aircraft_Logger
{
	public partial class Login : ContentPage
	{
		public Login ()
		{
			InitializeComponent ();
			//UserName.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeSentence);
			//Password.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeSentence);
			LoadEvent ();

		}
		public void LoadEvent()
		{
			SetUI ();

			var tapGestureRecognizer = new TapGestureRecognizer ();
			tapGestureRecognizer.Tapped += (s, e) => {
				Navigation.PushModalAsync (new UserRegistration ());
			};
			lblSignUp.GestureRecognizers.Add (tapGestureRecognizer);
		}
		public void SetUI()
		{
			
			UserName.TextColor = Color.White;
			Password.TextColor = Color.White;
		}

		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			//Entry entry = sender as Entry;
			//string val = entry.Text;
			//entry.Text= val.ToUpper();

		}

		public void OnbtnLogin(object o, EventArgs e)
		{
			if (!string.IsNullOrEmpty (UserName.Text.Trim()) && !string.IsNullOrEmpty (Password.Text.Trim())) {

				UserDatabase ud = new UserDatabase ();
				if (ud.CheckAuthntication (UserName.Text, Password.Text)) {
					//Navigation.PushModalAsync (new MainMenu ());
					Navigation.PushModalAsync (new SyncPage ());
					//var message = new StartLongRunningTaskMessage ();
					//MessagingCenter.Send (message, "StartLongRunningTaskMessage");
				} else {
					DisplayAlert ("Authentication Failed", "Invalid Username/Password or check your internet is connected", "Cancel");
				}
			} else {
				DisplayAlert ("Authentication", "Please enter email and password", "Cancel");
			}

		}


	}
}

