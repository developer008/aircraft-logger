﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms.Maps;

namespace Aircraft_Logger
{
	public partial class AircraftDetails : ContentPage
	{
		bool isSearchableRecord = false;
		AircraftDetailsViewModel viewModel;
		userdefault user;
		APICivilSearch selectedCivil = new APICivilSearch();
		tblVisits currentVisit;
		Position position = new Position();
		AddPhoto addPhoto;
		string selectedaction;
		userdefault us;
		tblRegistrations registration;
		tblManufacturers manufacturer;
		tblAircraft aircraft;
		tblAirlines airlines;
		AirForcesDatabase airforcedb = new AirForcesDatabase();
		tblUnits units;
		string Airforce_id;
		APImilitarydbSearch selectedMilitary = new APImilitarydbSearch();
		APICountrydbSearch selectedCountry = new APICountrydbSearch();
		enum LocationSelectionType
		{
			CurrentLocation,
			SelectedAirport
		}
		AddRegistrationViewModel viewModel1;
		LocationSelectionType LocationType;
		tblAirports SelectedAirport;

		ObservableCollection<Aircraft> AircraftList = new ObservableCollection<Aircraft>();
		bool isAircraftLoading;
		int lastAircraftCount = 0;

		ObservableCollection<Airlines> AirlineList = new ObservableCollection<Airlines>();
		bool isAirlineLoading;
		int lastAirlineCount = 0;

		ObservableCollection<Units> UnitsList = new ObservableCollection<Units>();
		bool isUnitsLoading;
		int lastUnitsCount = 0;

		ObservableCollection<AirForces> AirForcesList = new ObservableCollection<AirForces>();
		bool isAirForcesLoading;
		int lastAirForcesCount = 0;

		ObservableCollection<Manufacturers> ManufacturersList = new ObservableCollection<Manufacturers>();
		bool isManufacturersLoading;
		int lastManufacturersCount = 0;

		ObservableCollection<Countries> CountriesList = new ObservableCollection<Countries>();
		bool isCountriesLoading;
		int lastCountriesCount = 0;

		string RegistrationId = string.Empty;

		Registrations CurrentRegistration;
		List<tblPhotos> Photos;

		public AircraftDetails()
		{
			init();
		}

		public AircraftDetails(string registrationId)
		{
			RegistrationId = registrationId;
			init();
		}



		private void init()
		{
			InitializeComponent();
			Bind_ClickEvents();
			viewModel = new AircraftDetailsViewModel();
			stkAircraftPopup.BackgroundColor = new Color(0, 0, 0, 0.5);
			stkAirLinePopup.BackgroundColor = new Color(0, 0, 0, 0.5);
			stkSelectUnitsPopup.BackgroundColor = new Color(0, 0, 0, 0.5);
			stkAddManufacturerPopup.BackgroundColor = new Color(0, 0, 0, 0.5);
			stkAddAirforcePopup.BackgroundColor = new Color(0, 0, 0, 0.5);

			if (RegistrationId != null)
				LoadRegistration();
			else
				CurrentRegistration = new Registrations();
		}

		protected override void OnAppearing()
		{

		}

		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand(s, e);
			stkBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => stkDone_Click(s, e);
			stkDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblNavAircraft_Click(s, e);
			stkNavAircraft.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			txtAircraft.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblNavAirline_Click(s, e);
			stkNavAirline.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			txtAirline.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblNavUnit_Click(s, e);
			stkNavUnit.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			txtUnit.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAircraftDone_Click(s, e);
			stkAircraftDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAircraftBackMenu_Click(s, e);
			stkAircraftBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAirLineDone_Click(s, e);
			stkAirlineDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAirLineBackMenu_Click(s, e);
			stkAirLineBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblNavUnit_Click(s, e);
			lblNavUnit.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblNavAirforce_Click(s, e);
			lblNavAirforce.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAddManufacturer_Click(s, e);
			stkAddManufacturer.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAddAirforce_Click(s, e);
			stkAddAirforce.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblSelectUnitsBackMenu_Click(s, e);
			stkSelectUnitsBac.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblSelectUnitsDone_Click(s, e);
			stkSelectUnitsDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAddAirforceBackMenu_Click(s, e);
			stkAddAirforceBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAddAirforceDone_Click(s, e);
			stkAddAirforceDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAddManufacturerBackMenu_Click(s, e);
			stkAddManufacturerBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAddManufacturerDone_Click(s, e);
			stkAddManufacturerDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblSearchAirforceBackMenu_Click(s, e);
			stkSelectAirforceBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblSearchAirforceDone_Click(s, e);
			stkSelectAirforceDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => imgCamera_Click(s, e);
			imgCamera.GestureRecognizers.Add(tapGestureRecognizerCalendar);
		}


		#region CommonEvens

		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			//Close ();
			//Navigation.PushModalAsync(new AddRegistration());
			Navigation.PopModalAsync(true);
		}

		protected async void btnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				var ans = await DisplayAlert("Delete ", "Do you want to delete registration?", "Ok", "Cancel");
				if (ans == true)
				{
					tblRegistrations registration = new tblRegistrations()
					{
						fldRegistrationID = CurrentRegistration.RegistrationID,
						//fldRegistration = txtRegistration.Text,
						//fldCn = txtCN.Text,
						//fldAircraftID = CurrentRegistration.AircraftID,
						//fldAirlineID = CurrentRegistration.AirlineID,
						//fldUnitID = CurrentRegistration.UnitID,
						//fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M",
						//fldNew = CurrentRegistration.New,
						//fldCode = CurrentRegistration.Code,
						//fldAirportVisitID = CurrentRegistration.AirportVisitID,
						//fldRemarks = txtRemark.Text
					};
					viewModel.DeleteRegistration(registration);
					Close();
				}
			}
			catch
			{
			}
		}

		protected async void btnSearch_Click(object sender, EventArgs e)
		{
			try
			{
				AirportsDatabase ad = new AirportsDatabase();
				var result = ad.GetAirports(Account.DefaultAirportId);
				if (result == null)
					return;
				LocationType = LocationSelectionType.SelectedAirport;
				SelectedAirport = new tblAirports()
				{
					fldAirportID = result.fldAirportID,
					fldAirportName = result.fldAirportName,
					fldCountryID = result.fldCountryID,
					fldGPSCoordinates = result.fldGPSCoordinates
				};

				if (SelectedAirport != null)
				{

					//				Searchlist.Clear ();
					//				lastCount = 0;
					//				LoadGrid();
				}
				viewModel1 = new AddRegistrationViewModel();
				isSearchableRecord = true;
				SearchAPI api = new SearchAPI();
				if (sagIsCivil.SelectedValue)
				{
					string airportId = string.Empty;
					if (SelectedAirport.fldAirportID != null)
					{
						airportId = SelectedAirport.fldAirportID;
					}

					List<tblRegistrations> isExist = viewModel1.GetIfDuplicate(Convert.ToString(txtRegistration.Text), airportId);
					if (isExist.Count > 0)
					{
						//var answer = await DisplayAlert("Question?", "This registration is already in the database", "Continue", "Cancel");
						//if (answer)
						//{
						sagIsCivil.IsVisible = true;
						loader.IsVisible = true;
						LstSearchCivilDB.ItemsSource = await api.SearchCivilDB();
						stkCivilSearch.IsVisible = true;
						loader.IsVisible = false;
						//}
						//else {

						//}
					}
					else {
						sagIsCivil.IsVisible = true;
						loader.IsVisible = true;
						LstSearchCivilDB.ItemsSource = await api.SearchCivilDB(txtRegistration.Text);

						loader.IsVisible = false;
					}
				}
				else {
					user = DependencyService.Get<userdefault>();
					APICountrydbSearch ap = new APICountrydbSearch();

					string lscountry = user.getUserlastcountry();
					string myconid = user.getmycountry();
					ap.Country = lscountry;
					stkCountrySearch.IsVisible = true;
					loader.IsVisible = true;
					List<APICountrydbSearch> listcountry = new List<APICountrydbSearch>();

					List<APICountrydbSearch> temp = new List<APICountrydbSearch>();

					string myconname;
					temp = api.SearchCountryDb().Result;
					foreach (var b in temp)
					{
						if (b.CountryID == myconid)
						{
							listcountry.Add(b);
						}
						if (b.Country == lscountry)
						{
							ap.Code = b.Code;
						}
					}
					listcountry.Add(ap);
					temp.Sort((x, y) => x.Country.CompareTo(y.Country));
					foreach (var a in temp)
					{
						if (a.Country != ap.Country)
						{
							if (a.CountryID != myconid)
							{
								if (a.Code.Length == 2)
								{
									listcountry.Add(a);
								}
							}
						}


					}



					LstSearchCountryDB.ItemsSource = listcountry;
					loader.IsVisible = false;
					/*
                    #region Old Code
                    var action = await DisplayActionSheet (null, "Cancel", null, "africanairforce", "americaairforce","asianairforce","eurairforce");
                    //stkCivilSearch.IsVisible = true;
                    stkMilitarySearch.IsVisible = true;
                    loader.IsVisible = true;
                    LstSearchCivilDB.ItemsSource = await api.SearchMilitaryDatabase (action);
                    LstSearchMilitaryDB.ItemsSource = await api.SearchMilitaryDb (action);
                    loader.IsVisible = false;
                    #endregion 
                    */
				}

				//            var list1 = await api.SearchAircrafttype ();
				//
				//            var list3 = await api.SearchMilitaryDatabase ("");
				//            var list4 = await api.SearchMilitaryDb ();
				//var result = await api.CallApi ("http://192.168.200.72:83/api/values/5");
				//ReloadGrid();
			}
			catch (Exception ex)
			{
				int i = 0;
			}
		}

		protected async void stkDone_Click(object sender, EventArgs e)
		{
			loader.IsVisible = true;
			try
			{


				demo();
				if (!ValidateRegistration())
					return;
				loader.IsVisible = true;
				registration = new tblRegistrations();
				registration.fldRegistrationID = CurrentRegistration.RegistrationID;
				registration.fldRegistration = CurrentRegistration.Registration;
				registration.fldCn = CurrentRegistration.Cn;
				registration.fldAircraftID = CurrentRegistration.AircraftID;
				registration.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
				registration.fldNew = CurrentRegistration.New;
				registration.fldAirportVisitID = CurrentRegistration.AirportVisitID;
				registration.fldRemarks = txtRemark.Text;
				registration.fldCode = CurrentRegistration.Code;
				if (sagIsCivil.SelectedValue)
					registration.fldAirlineID = CurrentRegistration.AirlineID;
				else {
					registration.fldUnitID = CurrentRegistration.UnitID;
					registration.fldCode = CurrentRegistration.Code;
				}
				viewModel.AddRegistration(registration);

				tblPhotos photo;
				IPicture _Picture = DependencyService.Get<IPicture>();
				foreach (var item in stkRegistrationPhotos.Children)
				{

					var tmpStackLayout = ((StackLayout)item).Children;
					CImage _photo = (CImage)tmpStackLayout[0];
					string imageName = _photo.ImageID.Replace("img_", "");
					if (_photo.Tag.ToString() == "0")
					{
						await _Picture.SavePictureToDisk(_photo.Source, imageName, 300, 200);
						App.DbSync.UploadFile(imageName + ".jpg", "AircraftLogger", "/Photos/");
					}
					photo = new tblPhotos();
					photo.fldPhotoFilename = imageName;
					//photo.fldPhotoID = Convert.ToInt32 (_photo.Tag);
					photo.fldPhotoID = Convert.ToString(_photo.Tag);
					photo.fldRegistrationID = CurrentRegistration.RegistrationID;
					photo.fldIsPrimary = _photo.IsPrimary;
					viewModel.AddPhoto(photo);
				}
				//				foreach (CImage _photo in stkRegistrationPhotos.Children) {
				//					string imageName = _photo.ImageID.Replace ("img_", "");
				//					await _Picture.SavePictureToDisk (_photo.Source, imageName, 300, 200);
				//					photo = new tblPhotos ();
				//					photo.fldPhotoFilename = imageName;
				//					photo.fldPhotoID = Convert.ToInt32 (_photo.Tag);
				//					photo.fldRegistrationID = CurrentRegistration.RegistrationID;
				//					photo.fldIsPrimary = _photo.IsPrimary;
				//					viewModel.AddPhoto (photo);
				//				}

				Close();



				//
				//---------------------------------------------
				loader.IsVisible = false;
			}
			catch (Exception ex)
			{
			}
			loader.IsVisible = false;
		}



		#region registration

		public async void demo()
		{
			try
			{
				loader.IsVisible = true;
				tblRegistrations Registrations = new tblRegistrations();
				if (isSearchableRecord)
				{





					string tmpMfName = "TestM52";
					string aircraftId = string.Empty;
					string manufacturerId = string.Empty;
					string airlineId = string.Empty;
					string unitId = string.Empty;
					manufacturer = new tblManufacturers();
					var manufacturerResult = GetManufacturer(tmpMfName);
					if (manufacturerResult == null)
					{
						manufacturer.fldManufacturerName = tmpMfName;
						manufacturerId = AddUpdateManufacturer();
					}
					else {
						manufacturer = manufacturerResult;
						manufacturerId = manufacturer.fldManufacturerID;
					}





					//					if (SelectedAirport == null) {
					//						DisplayAlert ("Airport", "Please select airport", "Ok");
					//						return;
					//					}
					if (!IsVisitExists())
						SaveVisits();

					if (sagIsCivil.SelectedValue)
					{
						aircraft = new tblAircraft();
						var aircraftResult = GetAircraft(selectedCivil.FullType);
						if (aircraftResult == null)
						{
							aircraft.fldAircraftName = selectedCivil.FullType;
							aircraft.fldManufacturerID = manufacturerId;
							aircraftId = AddUpdateAircraft();
						}
						else {
							aircraft = aircraftResult;
							aircraftId = aircraft.fldAircraftID;
						}

						airlines = new tblAirlines();
						var airlinesResult = GetAirline(selectedCivil.Owner);
						if (airlinesResult == null)
						{
							airlines.fldAirlineName = selectedCivil.Owner;
							airlineId = AddUpdateAirline();
						}
						else {
							airlines = airlinesResult;
							airlineId = airlines.fldAirlineID;
						}

						Registrations.fldRegistration = txtRegistration.Text;
						Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
						Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
						Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";
						Registrations.fldCn = selectedCivil.Cnr;
						Registrations.fldAircraftID = aircraftId;
						Registrations.fldAirlineID = airlineId;
						Registrations.fldRemarks = selectedCivil.Remarks;

					}
					else
					{
						aircraft = new tblAircraft();
						var aircraftResult = GetAircraft(selectedMilitary.Type);
						if (aircraftResult == null)
						{
							aircraft.fldAircraftName = selectedMilitary.Type;
							aircraft.fldManufacturerID = manufacturerId;
							aircraftId = AddUpdateAircraft();
						}
						else {
							aircraft = aircraftResult;
							aircraftId = aircraft.fldAircraftID;
						}

						units = new tblUnits();
						var unitResult = GetUnit(selectedMilitary.Unit);
						if (unitResult == null)
						{
							units.fldUnitName1 = selectedMilitary.Unit;
							unitId = AddUpdateUnit();
						}
						else {
							units = unitResult;
							unitId = units.fldUnitID;
						}

						Registrations.fldRegistration = txtRegistration.Text;
						Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
						Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
						Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";
						Registrations.fldCn = selectedMilitary.CN;
						Registrations.fldCode = selectedMilitary.Code;
						Registrations.fldAircraftID = aircraftId;
						Registrations.fldUnitID = unitId;
						//Registrations.fldAirlineID = airlineId;
						Registrations.fldRemarks = selectedMilitary.Comment;
					}

					SaveRegistration(Registrations);

				}
				else {




					//					if (SelectedAirport == null) {
					//						DisplayAlert ("Airport", "Please select airport", "Ok");
					//						return;
					//					}
					if (!IsVisitExists())
						SaveVisits();


					Registrations.fldRegistration = txtRegistration.Text;
					Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
					Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
					Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";

					SaveRegistration(Registrations);

				}
				loader.IsVisible = false;
			}
			catch (Exception ex)
			{

			}

		}
		private bool IsRegistrationExists()
		{
			try
			{
				return viewModel1.GetIfDuplicate(Convert.ToString(txtRegistration.Text), (SelectedAirport.fldAirportID)).Count > 0;
			}
			catch (Exception ex)
			{
				return false;
			}
		}
		private void SaveRegistration(tblRegistrations Registrations)
		{

			try
			{

				viewModel1.SaveRegistration(Registrations);
			}
			catch (Exception ex)
			{
				Registrations = null;
			}
		}

		private bool IsVisitExists()
		{
			try
			{
				tblVisits Visits = viewModel1.GetIfVisitExists(SelectedAirport == null ? "" : SelectedAirport.fldAirportID, CommonHelper.ConvertLocationToDMS(position), DateTime.Now.Date);
				if (Visits != null)
					currentVisit = Visits;
				return Visits != null;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private void SaveVisits()
		{
			
			tblVisits Visits = new tblVisits();
			try
			{
				loader.IsVisible = true;
				if (LocationType == LocationSelectionType.SelectedAirport && SelectedAirport != null)
					Visits.fldAirportID = SelectedAirport.fldAirportID;
				else
					position = new Position(0, 0);
				Visits.fldLocationGPS = CommonHelper.ConvertLocationToDMS(position);
				Visits.fldVisitDate = DateTime.Now.DefaultDateFormate();
				Visits.fldAirportVisitID = viewModel1.SaveVisits(Visits);
				currentVisit = Visits;
				loader.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
			finally
			{
				Visits = null;
			}

		}

		private string AddUpdateManufacturer()
		{
			try
			{
				return viewModel1.AddUpdateManufacturer(manufacturer);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		private tblManufacturers GetManufacturer(string manufacturerName)
		{
			try
			{
				return viewModel1.GetManufacturer(manufacturerName);
			}
			catch (Exception ex)
			{
				return manufacturer;
			}
		}

		private tblAircraft GetAircraft(string aircraftName)
		{
			try
			{
				return viewModel1.GetAircraft(aircraftName);
			}
			catch (Exception ex)
			{
				return aircraft;
			}
		}

		private tblAirlines GetAirline(string airlineName)
		{
			try
			{
				return viewModel1.GetAirline(airlineName);
			}
			catch (Exception ex)
			{
				return airlines;
			}
		}

		private tblUnits GetUnit(string unitName)
		{
			try
			{
				return viewModel1.GetUnit(unitName);
			}
			catch (Exception ex)
			{
				return units;
			}
		}

		private string AddUpdateAircraft()
		{
			try
			{
				return viewModel1.AddUpdateAircraft(aircraft);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		private string AddUpdateUnit()
		{
			try
			{
				return viewModel1.AddUpdateUnit(units);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		private string AddUpdateAirline()
		{
			try
			{
				return viewModel1.AddUpdateAirline(airlines);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		#endregion
		private bool ValidateRegistration()
		{
			if (Convert.ToString(txtAircraft.Text) != "" && CurrentRegistration.AircraftID == string.Empty)
			{
				DisplayAlert("Warning", "This Aircraft Is Not Exists.", "Ok");
				txtAircraft.Focus();
				return false;
			}
			else if (sagIsCivil.SelectedValue && Convert.ToString(txtAirline.Text) != "" && CurrentRegistration.AirlineID == string.Empty)
			{
				DisplayAlert("Warning", "This Airline Is Not Exists.", "Ok");
				txtAirline.Focus();
				return false;
			}
			else if (!sagIsCivil.SelectedValue && Convert.ToString(txtUnit.Text) != "" && CurrentRegistration.UnitID == string.Empty)
			{
				DisplayAlert("Warning", "This Unit Is Not Exists.", "Ok");
				txtUnit.Focus();
				return false;
			}
			else
				return true;
		}
		ImageSource LastImageSource = null;

		protected async void imgCamera_Click(object sender, EventArgs e)
		{
			
			CameraViewModel cameraOps = new CameraViewModel();
			//var action = await DisplayActionSheet(null, "Cancel", null, "Last Photo Taken", "Take Photo", "Choose From Library");
			//switch (action)
			//{
			//	case "Last Photo Taken":
			//		userdefault us;
			//		us = DependencyService.Get<userdefault>();
			//		ImageSource sor = us.getlastimage();

			//		cameraOps.lastimg(sor);
			//		break;
			//	case "Take Photo":
			//		await cameraOps.TakePicture();
			//		break;
			//	case "Choose From Library":
			//		cameraOps.SelectPicture();
			//		break;
			//}

			ActionSheet.IsVisible = true;
			if (cameraOps.ImageSource == null)
			{
				//cameraOps.ImageSource = LastImageSource;
			}
			//LastImageSource = cameraOps.ImageSource;
			//BindImages(LastImageSource);
		}
		async void chooselastphoto()
		{
			try
			{
				userdefault us;
				us = DependencyService.Get<userdefault>();
				ImageSource sor = us.getlastimage();
				CameraViewModel cameraOps = new CameraViewModel();
				cameraOps.lastimg(sor);
				if (cameraOps.ImageSource == null)
				{
					//cameraOps.ImageSource = LastImageSource;
				}
				LastImageSource = cameraOps.ImageSource;
				BindImages(LastImageSource);
			}
			catch (Exception ex)
			{

			}

		}
		protected void Lasttaken(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			chooselastphoto();
		}

		protected void Picture(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			TakeNewPicture();
		}

		protected void Choosegallery(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			PicPicture();
		}
		async void PicPicture()
		{
			//PicCamera.ChoosePhoto ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.SelectPicture();
			if (cameraOps.ImageSource == null)
			{
				//cameraOps.ImageSource = LastImageSource;
			}
			LastImageSource = cameraOps.ImageSource;
			BindImages(LastImageSource);
			//CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
		}

		protected void Cancelgal(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
		}
		private async void TakeNewPicture()
		{
			//PicCamera.TakePicture ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.TakePicture();
			//CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
			if (cameraOps.ImageSource == null)
			{
				//cameraOps.ImageSource = LastImageSource;
			}
			LastImageSource = cameraOps.ImageSource;
			BindImages(LastImageSource);
		}
		protected void sagIsCivil_OnChange(object sender, EventArgs e)
		{
			CurrentRegistration.AircraftType = sagIsCivil.SelectedValue ? "C" : "M";
			SetVisibility(sagIsCivil.SelectedValue);
		}

		protected void lblNavAircraft_Click(object sender, EventArgs e)
		{
			ShowAircraftPopup();
		}

		protected void lblNavAirline_Click(object sender, EventArgs e)
		{
			ShowAirlinePopup();
		}

		protected void lblNavUnit_Click(object sender, EventArgs e)
		{
			ShowSelectUnitPopup();
		}
		protected void lblNavAirforce_Click(object sender, EventArgs e)
		{
			ShowSearchAirforcePopup();
		}

		protected void lblAddManufacturer_Click(object sender, EventArgs e)
		{
			ShowAddManufacturerPopup();
		}

		protected void lblAddAirforce_Click(object sender, EventArgs e)
		{
			ShowSearchAirforcePopup1();
		}

		protected void txtTailNo_Unfocused(object sender, EventArgs e)
		{
			CurrentRegistration.Code = txtTailNo.Text;
		}

		protected void txtCN_Unfocused(object sender, EventArgs e)
		{
			CurrentRegistration.Cn = txtCN.Text;
		}

		protected void txtRemark_Unfocused(object sender, EventArgs e)
		{
			CurrentRegistration.Remarks = txtRemark.Text;
		}

		protected void txtAircraft_Unfocused(object sender, EventArgs e)
		{
			if (Convert.ToString(txtAircraft.Text) == "")
			{
				CurrentRegistration.AircraftID = string.Empty;
				return;
			}

			var aircraft = viewModel.GetAircraftIfExist(txtAircraft.Text);
			if (aircraft != null)
				CurrentRegistration.AircraftID = aircraft.fldAircraftID;
			else
				CurrentRegistration.AircraftID = string.Empty;
		}

		protected void txtAirline_Unfocused(object sender, EventArgs e)
		{
			if (Convert.ToString(txtAirline.Text) == "")
			{
				CurrentRegistration.AirlineID = string.Empty;
				return;
			}
			var airline = viewModel.GetAirlinIfExist(txtAirline.Text);
			if (airline != null)
				CurrentRegistration.AirlineID = airline.fldAirlineID;
			else
				CurrentRegistration.AirlineID = string.Empty;
		}

		protected void txtUnit_Unfocused(object sender, EventArgs e)
		{
			if (Convert.ToString(txtUnit.Text) == "")
			{
				CurrentRegistration.UnitID = string.Empty;
				return;
			}
			var unit = viewModel.GetUnitsIfExist(txtUnit.Text);
			if (unit != null)
				CurrentRegistration.UnitID = unit.fldUnitID;
			else
				CurrentRegistration.UnitID = string.Empty;
		}
		protected void txtAirforce_Unfocused(object sender, EventArgs e)
		{
			if (Convert.ToString(txtAirforce.Text) == "")
			{
				CurrentRegistration.AirForceID = string.Empty;
				return;
			}
			var unit = viewModel.GetUnitsIfExist(txtUnit.Text);
			if (unit != null)
				CurrentRegistration.AirForceID = unit.fldAirForceID;
			else
				CurrentRegistration.AirForceID = string.Empty;
		}

		#endregion

		#region CommonFunctions

		private void Close()
		{
			try
			{
				if (Navigation.ModalStack[Navigation.ModalStack.Count - 2] != null)
					(Navigation.ModalStack[Navigation.ModalStack.Count - 2] as AddRegistration).ReloadGrid();
			}
			catch (Exception ex)
			{
			}
			finally
			{
				Navigation.PopModalAsync(true);
			}
		}

		private async void LoadRegistration()
		{
			CurrentRegistration = viewModel.GetRegistrationWithDetail(RegistrationId);
			Photos = viewModel.GetPhotosByRegistrationId(RegistrationId);
			IPicture picture = DependencyService.Get<IPicture>();
			foreach (tblPhotos photo in Photos)
			{
				BindImagesFromFile(await picture.GetPictureFromDisk(photo.fldPhotoFilename), photo.fldPhotoFilename, photo.fldPhotoID, photo.fldIsPrimary);
			}

			ApplyValue();
		}

		private void ApplyValue()
		{
			try
			{
				txtRegistration.Text = string.Empty;
				txtCN.Text = string.Empty;
				txtTailNo.Text = string.Empty;
				txtAircraft.Text = string.Empty;
				txtAirline.Text = string.Empty;
				txtUnit.Text = string.Empty;
				txtAirforce.Text = string.Empty;
				txtRemark.Text = string.Empty;
				AirForcesDatabase airforcedb = new AirForcesDatabase();
				viewModel1 = new AddRegistrationViewModel();
				sagIsCivil.SelectedValue = CurrentRegistration.AircraftType == "C" ? true : false;
				SetVisibility(sagIsCivil.SelectedValue);
				txtRegistration.Text = CurrentRegistration.Registration;
				txtCN.Text = CurrentRegistration.Cn;
				txtTailNo.Text = CurrentRegistration.Code;//CurrentRegistration.
				txtAircraft.Text = CurrentRegistration.AircraftName;
				txtAirline.Text = CurrentRegistration.AirlineName;
				txtUnit.Text = CurrentRegistration.UnitName1;
				txtAirforce.Text = CurrentRegistration.AirForceName;
				txtRemark.Text = CurrentRegistration.Remarks;

				string temp = CurrentRegistration.UnitID;
				tblUnits un = viewModel1.GetUnitbyid(temp);
				tblAirForces ar = airforcedb.GetAirforce(un.fldAirForceID);
				if (ar.fldAirForceID != null)
				{
					CurrentRegistration.AirForceID = ar.fldAirForceID;
					CurrentRegistration.AirForceName = ar.fldAirForceName;
					txtAirforce.Text = CurrentRegistration.AirForceName;
				}


			}
			catch (Exception ex)
			{

			}

		}

		private void SetAircraftVisibility(bool IsSearch)
		{
			stkAddNewAircraft.IsVisible = !IsSearch;
			stkSearchAircraft.IsVisible = IsSearch;
		}

		private void SetAirLineVisibility(bool IsSearch)
		{
			stkAirLineAddNew.IsVisible = !IsSearch;
			stkAirLineSearch.IsVisible = IsSearch;
		}

		private void SetUnitsVisibility(bool IsSearch)
		{
			stkSelectUnitsAddNew.IsVisible = !IsSearch;
			stkSelectUnitsSearch.IsVisible = IsSearch;
		}
		private void SetAirforceVisibility(bool IsSearch)
		{
			//stkAddAirforcePopup.IsVisible = !IsSearch;
			stkSelectAirforceSearch.IsVisible = IsSearch;
		}
		private void SetVisibility(bool isCivil)
		{
			if (isCivil)
			{
				stkCode.IsVisible = false;
				stkUnit.IsVisible = false;
				stkAirForce.IsVisible = false;
				stkAirline.IsVisible = true;
			}
			else {
				stkCode.IsVisible = true;
				stkUnit.IsVisible = true;
				stkAirForce.IsVisible = true;
				stkAirline.IsVisible = false;
			}
		}

		private void BindImagesFromFile(string file, string fileName, string photoId, bool isPrimary)
		{
			CImage newImage = new CImage()
			{
				WidthRequest = 220,
				HeightRequest = 150,
				Source = ImageSource.FromFile(file),
				ImageID = "img_" + fileName,
				Tag = photoId,
				Aspect = Aspect.AspectFit,
				IsPrimary = isPrimary
			};
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.NumberOfTapsRequired = 2;
			tapGestureRecognizer.Tapped += (s, e) => Image_Tapped(s, e);
			//newImage.GestureRecognizers.Add (tapGestureRecognizer);

			StackLayout stkImage = new StackLayout()
			{
				Padding = 3

			};
			if (isPrimary)
			{
				stkImage.BackgroundColor = Color.FromRgb(36, 175, 227);
			}
			stkImage.Children.Add(newImage);
			stkImage.GestureRecognizers.Add(tapGestureRecognizer);
			stkRegistrationPhotos.Children.Add(stkImage);
			//			stkRegistrationPhotos.Children.Add (newImage);
		}

		private void Image_Tapped(object sender, EventArgs e)
		{
			//CImage image = sender as CImage;
			var children = stkRegistrationPhotos.Children;
			foreach (var item in children)
			{
				StackLayout stkTemp = (StackLayout)item;
				stkTemp.BackgroundColor = Color.Transparent;
				var tmpimageControl = stkTemp.Children[0];
				CImage tmpimage = (CImage)tmpimageControl;
				tmpimage.IsPrimary = false;

			}
			StackLayout stkImage = sender as StackLayout;
			var imageControl = stkImage.Children[0];
			CImage image = (CImage)imageControl;
			image.IsPrimary = true;
			stkImage.BackgroundColor = Color.FromRgb(36, 175, 227);
			SelectPic.IsVisible = true;
			selctedimg.Source = image.Source;
			//DisplayAlert("Test", image.ImageID, "Ok");
		}
		protected void btnClosePhoto_Clicked(object sender, EventArgs e)
		{
			SelectPic.IsVisible = false;
		}

		private void BindImages(ImageSource image)
		{
			string imageName = Guid.NewGuid().ToString();
			CImage newImage = new CImage()
			{
				WidthRequest = 220,
				HeightRequest = 150,
				Source = image,
				ImageID = "img_" + imageName,
				Tag = 0,
				Aspect = Aspect.AspectFit
			};
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.NumberOfTapsRequired = 2;
			tapGestureRecognizer.Tapped += (s, e) => Image_Tapped(s, e);
			//newImage.GestureRecognizers.Add (tapGestureRecognizer);

			StackLayout stkImage = new StackLayout()
			{
				Padding = 3
			};
			tblPhotos photo = new tblPhotos();
			photo.fldRegistrationID = CurrentRegistration.RegistrationID;
			photo.fldPhotoFilename = imageName;
			Photos.Add(photo);

			stkImage.Children.Add(newImage);
			stkImage.GestureRecognizers.Add(tapGestureRecognizer);
			stkRegistrationPhotos.Children.Add(stkImage);
			//stkRegistrationPhotos.Children.Add (newImage);
		}

		#endregion

		#region Aircraft

		private void ShowAircraftPopup()
		{
			try
			{
				stkAircraftPopup.IsVisible = true;
				SetAircraftVisibility(sagAircraft.SelectedValue);
				InitAircraftPopup();
			}
			catch (Exception ex)
			{

			}
		}

		protected void lblAircraftDone_Click(object sender, EventArgs e)
		{

			try
			{
				if (sagAircraft.SelectedValue)
				{
					if (!SelectAircraft())
						return;
				}
				else {
					if (!SaveAircraft())
					{
						LoadAircraft("");
						return;
					}
				}
			}
			catch (Exception ex)
			{
				return;
			}
			ApplyValue();
			CloseAircraftPopup();
		}

		protected void lblAircraftBackMenu_Click(object sender, EventArgs e)
		{
			try
			{
				CloseAircraftPopup();
			}
			catch (Exception ex)
			{

			}
		}

		protected void sagAircraft_OnChange(object sender, EventArgs e)
		{
			SetAircraftVisibility(sagAircraft.SelectedValue);
		}

		private void InitAircraftPopup()
		{
			try
			{
				FIllAircraftList();
				LoadManufacturer("");
				LstSearchAircraftManufacturerList.ItemsSource = ManufacturersList;
			}
			catch (Exception ex)
			{

			}
		}

		private void CloseAircraftPopup()
		{
			try
			{
				stkAircraftPopup.IsVisible = false;
				ClearAircraftPopup();
			}
			catch (Exception ex)
			{

			}
		}

		private void ClearAircraftPopup()
		{
			try
			{
				LstSearchAircraftManufacturerList.ItemsSource = null;
				LstSearchAircraftList.ItemsSource = null;
				UnLoadManufacturer();
				UnLoadAircraft();
				txtAddAircraft.Text = string.Empty;
				txtAircraftGeneric.Text = string.Empty;
				txtSearchAircraft.Text = string.Empty;
				//				txtAircraftManufacturer.Text = string.Empty;
			}
			catch (Exception ex)
			{

			}
		}

		private void FIllAircraftList()
		{
			try
			{
				LoadAircraft(txtSearchAircraft.Text);
				LstSearchAircraftList.ItemsSource = AircraftList;
			}
			catch (Exception ex)
			{

			}
		}

		private void LoadAircraft(string AircraftName)
		{
			try
			{
				var _searchList = viewModel.SearchAircraft(20, lastAircraftCount, AircraftName);
				var uni = _searchList.Distinct().ToList();
				AircraftList.Clear();
				for (int i = 0; i <= uni.Count - 1; i++)
				{
					AircraftList.Add(uni[i]);
					//if (i == _searchList.Count - 1)
					//	lastAircraftCount = _searchList [i].AircraftID;
				}
			}
			catch (Exception ex)
			{

			}
		}

		protected void LstSearchAircraftList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			try
			{
				if (isAircraftLoading || AircraftList.Count == 0)
					return;

				if (e.Item == AircraftList[AircraftList.Count - 1])
				{
					LoadAircraft(txtSearchAircraft.Text);
				}
			}
			catch (Exception ex)
			{

			}
		}
		protected void LstSearchAircraftList_tapped(object sender, ItemTappedEventArgs e)
		{
			var Airline = e.Item as Aircraft;
			txtSearchAircraft.Text = Airline.AircraftName;
			try
			{
				if (sagAircraft.SelectedValue)
				{
					if (!SelectAircraft())
						return;
				}
				else {
					if (!SaveAircraft())
					{
						LoadAircraft("");
						return;
					}
				}
			}
			catch (Exception ex)
			{
				return;
			}
			ApplyValue();
			CloseAircraftPopup();
		}

		protected void LstSearchAircraftManufacturerList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			try
			{
				if (isManufacturersLoading || ManufacturersList.Count == 0)
					return;

				if (e.Item == ManufacturersList[ManufacturersList.Count - 1])
				{
					LoadManufacturer("");
				}
			}
			catch (Exception ex)
			{

			}
		}

		private void LstSearchAircraftManufacturerList_Tap(object sender, ItemTappedEventArgs e)
		{
			try
			{
				var manufacturer = e.Item as Manufacturers;
				txtAircraftManufacturer.Text = manufacturer.ManufacturerName;
			}
			catch (Exception ex)
			{

			}
		}

		protected void txtSearchAircraft_Change(object sender, TextChangedEventArgs e)
		{
			UnLoadAircraft();
			LoadAircraft(txtSearchAircraft.Text);
		}

		private void UnLoadAircraft()
		{
			try
			{
				AircraftList.Clear();
				lastAircraftCount = 0;
			}
			catch (Exception ex)
			{

			}
		}

		private bool SaveAircraft()
		{
			if (!ValidateAircraftForSave())
				return false;
			
			tblAircraft Aircraft = new tblAircraft();
			try
			{
				loader.IsVisible = true;
				Aircraft.fldAircraftName = txtAddAircraft.Text;
				Aircraft.fldAircraftNameGenericy = txtAircraftGeneric.Text;
				Aircraft.fldManufacturerID = (LstSearchAircraftManufacturerList.SelectedItem as Manufacturers).ManufacturerID;
				Aircraft.fldAircraftID = viewModel.AddAircraft(Aircraft);
				CurrentRegistration.AircraftID = Aircraft.fldAircraftID;
				CurrentRegistration.AircraftName = Aircraft.fldAircraftName;
				loader.IsVisible = false;
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			finally
			{
				Aircraft = null;
			}

		}

		private bool ValidateAircraftForSave()
		{
			try
			{
				if (txtAddAircraft.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Aircraft", "Ok");
					txtAddAircraft.Focus();
					return false;
				}
				else if (txtAircraftGeneric.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Aircraft Genericy", "Ok");
					txtAircraftGeneric.Focus();
					return false;
				}
				else if (LstSearchAircraftManufacturerList.SelectedItem == null)
				{
					DisplayAlert("Required", "Please Insert Aircraft", "Ok");
					LstSearchAircraftManufacturerList.Focus();
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}

		}

		private bool SelectAircraft()
		{
			try
			{
				if (ValidateAircraftForSelect())
				{
					//loader.IsVisible = true;
					Aircraft aircraft = LstSearchAircraftList.SelectedItem as Aircraft;
					CurrentRegistration.AircraftID = aircraft.AircraftID;
					CurrentRegistration.AircraftName = aircraft.AircraftName;
					//loader.IsVisible = false;
					return true;
				}
				else
					return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool ValidateAircraftForSelect()
		{
			try
			{
				if (LstSearchAircraftList.SelectedItem == null)
				{
					DisplayAlert("Required", "Please Select Aircraft First", "Ok");
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		#endregion

		#region Airlines

		private void ShowAirlinePopup()
		{
			stkAirLinePopup.IsVisible = true;
			InitAirlinePopup();
		}

		protected void lblAirLineDone_Click(object sender, EventArgs e)
		{
			try
			{
				if (sagAirLine.SelectedValue)
				{
					if (!SelectAirline())
						return;
				}
				else {
					if (!SaveAirline())
					{
						LoadAirline("");
						return;
					}
				}
			}
			catch (Exception ex)
			{
				return;
			}
			ApplyValue();
			CloseAirlinePopup();
		}

		protected void lblAirLineBackMenu_Click(object sender, EventArgs e)
		{
			CloseAirlinePopup();
		}

		protected void sagAirLine_OnChange(object sender, EventArgs e)
		{
			SetAirLineVisibility(sagAirLine.SelectedValue);
		}

		private void InitAirlinePopup()
		{
			SetAirLineVisibility(sagAirLine.SelectedValue);
			FIllAirLineList();
			LoadCountry("");
			LstSearchAirlineCountryList.ItemsSource = CountriesList;
		}

		private void CloseAirlinePopup()
		{
			stkAirLinePopup.IsVisible = false;
			ClearAirlinePopup();
		}

		private void ClearAirlinePopup()
		{
			LstSearchAirlineCountryList.ItemsSource = null;
			LstSearchAirlineList.ItemsSource = null;
			UnLoadCountry();
			UnLoadAirline();
			TxtAddAirline.Text = string.Empty;
			TxtAirlineIataCode.Text = string.Empty;
			TxtAirlineIacoCode.Text = string.Empty;
			TxtAirlineCountry.Text = string.Empty;
			txtSearchAirline.Text = string.Empty;
		}

		private void FIllAirLineList()
		{
			LoadAirline(txtSearchAirline.Text);
			LstSearchAirlineList.ItemsSource = AirlineList;
		}

		private void LoadAirline(string AirlineName)
		{
			var _searchList = viewModel.SearchAirline(20, lastAirlineCount, AirlineName);
			var uni = _searchList.Distinct().ToList();
			AirlineList.Clear();
			for (int i = 0; i <= uni.Count - 1; i++)
			{

				AirlineList.Add(uni[i]);
				//if (i == _searchList.Count - 1)
				//	lastAirlineCount = _searchList [i].AirlineID;
			}
		}

		protected void LstSearchAirlineList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			if (isAirlineLoading || AirlineList.Count == 0)
				return;

			//hit bottom!
			if (e.Item == AirlineList[AirlineList.Count - 1])
			{
				LoadAirline(txtSearchAirline.Text);
			}
		}
		protected void LstSearchAirlineList_tapped(object sender, ItemTappedEventArgs e)
		{
			var Airline = e.Item as Airlines;
			txtSearchAirline.Text = Airline.AirlineName;

			try
			{
				if (sagAirLine.SelectedValue)
				{
					if (!SelectAirline())
						return;
				}
				else {
					if (!SaveAirline())
					{
						LoadAirline("");
						return;
					}
				}
			}
			catch (Exception ex)
			{
				return;
			}
			ApplyValue();
			CloseAirlinePopup();
		}


		protected void LstSearchAirlineCountryList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			if (isCountriesLoading || CountriesList.Count == 0)
				return;

			//hit bottom!
			if (e.Item == CountriesList[CountriesList.Count - 1])
			{
				LoadCountry("");
			}
		}

		private void LstSearchAirlineCountryList_Tap(object sender, ItemTappedEventArgs e)
		{
			var countries = e.Item as Countries;
			TxtAirlineCountry.Text = countries.CountryName;
		}

		protected void txtSearchAirline_Change(object sender, TextChangedEventArgs e)
		{
			UnLoadAirline();
			LoadAirline(txtSearchAirline.Text);
		}

		private void UnLoadAirline()
		{
			AirlineList.Clear();
			lastAirlineCount = 0;
		}

		private bool SaveAirline()
		{
			if (!ValidateAirlineForSave())
				return false;
			
			tblAirlines airlines = new tblAirlines();
			try
			{
				loader.IsVisible = true;
				airlines.fldAirlineName = TxtAddAirline.Text;
				airlines.fldIATAcode = TxtAirlineIataCode.Text;
				airlines.fldICAOcode = TxtAirlineIacoCode.Text;
				airlines.fldCountryID = (LstSearchAirlineCountryList.SelectedItem as Countries).CountryID;

				//airlines. = TxtAirlineCountry.Text;
				airlines.fldAirlineID = viewModel.AddAirline(airlines);
				CurrentRegistration.AirlineID = airlines.fldAirlineID;
				CurrentRegistration.AirlineName = airlines.fldAirlineName;
				loader.IsVisible = false;
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}

		}

		private bool ValidateAirlineForSave()
		{
			try
			{
				if (TxtAddAirline.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Aircraft", "Ok");
					TxtAddAirline.Focus();
					return false;
				}
				//else if (TxtAirlineIataCode.Text.Trim() == string.Empty)
				//{
				//	DisplayAlert("Required", "Please Insert IATA Code", "Ok");
				//	TxtAirlineIataCode.Focus();
				//	return false;
				//}
				//else if (TxtAirlineIacoCode.Text.Trim() == string.Empty)
				//{
				//	DisplayAlert("Required", "Please Insert IACO Code", "Ok");
				//	TxtAirlineIacoCode.Focus();
				//	return false;
				//}
				//else if (TxtAirlineCountry.Text.Trim() == string.Empty)
				//{
				//	DisplayAlert("Required", "Please Insert Country", "Ok");
				//	TxtAirlineCountry.Focus();
				//	return false;
				//}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			return true;
		}

		private bool SelectAirline()
		{
			try
			{
				if (ValidateAirlineForSelect())
				{
					//loader.IsVisible = true;
					Airlines airline = LstSearchAirlineList.SelectedItem as Airlines;
					CurrentRegistration.AirlineID = airline.AirlineID;
					CurrentRegistration.AirlineName = airline.AirlineName;
					return true;
					//loader.IsVisible = false;
				}
				else
					return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool ValidateAirlineForSelect()
		{
			try
			{
				if (LstSearchAirlineList.SelectedItem == null)
				{
					DisplayAlert("Required", "Please Select Airline First", "Ok");
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		#endregion

		#region Airforce

		private void ShowAddAirforcePopup()
		{

			//stkSelectAirforcePopup.IsVisible = true;

			stkAddAirforcePopup.IsVisible = true;
			InitAddAirforcePopup();
		}

		protected void lblAddAirforceBackMenu_Click(object sender, EventArgs e)
		{
			CloseAddAirforcePopupp();
		}

		protected void lblAddAirforceDone_Click(object sender, EventArgs e)
		{
			if (SaveAirforce())
			{
				LoadAirforce("");
				CloseAddAirforcePopupp();
			}
		}

		private void InitAddAirforcePopup()
		{
			FIllAirForceList();
		}

		private void CloseAddAirforcePopupp()
		{
			stkAddAirforcePopup.IsVisible = false;
			ClearAddAirforcePopup();
		}

		private void ClearAddAirforcePopup()
		{
			LstSearchAirforceCountryList.ItemsSource = null;
			UnLoadCountry();
			TxtAirforce.Text = string.Empty;
			txtAirforceLocarlName.Text = string.Empty;
			txtAirforceShortcut.Text = string.Empty;
			txtAirforceCountry.Text = string.Empty;
		}

		private void FIllAirForceList()
		{
			LoadCountry("");
			LstSearchAirforceCountryList.ItemsSource = CountriesList;
		}

		private void LoadAirforce(string AirforceName)
		{
			var _searchList = viewModel.SearcAirForces(1000, lastAirForcesCount, AirforceName);
			var uni = _searchList.Distinct().ToList();
			AirForcesList.Clear();
			for (int i = 0; i <= uni.Count - 1; i++)
			{
				AirForcesList.Add(uni[i]);
				//if (i == _searchList.Count - 1)
				//	lastAirForcesCount = _searchList [i].AirForceID;
			}
		}

		private void UnLoadAirforce()
		{
			AirForcesList.Clear();
			lastAirForcesCount = 0;
		}

		protected void LstSearchAirforceCountryList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			if (isCountriesLoading || CountriesList.Count == 0)
				return;

			if (e.Item == CountriesList[CountriesList.Count - 1])
			{
				LoadCountry("");
			}
		}

		private void LstSearchAirforceCountryList_Tap(object sender, ItemTappedEventArgs e)
		{
			var countries = e.Item as Countries;
			txtAirforceCountry.Text = countries.CountryName;
		}

		private bool SaveAirforce()
		{
			if (!ValidateAirforceForSave())
				return false;
			
			tblAirForces airforce = new tblAirForces();
			try
			{
				loader.IsVisible = true;
				airforce.fldAirForceName = TxtAirforce.Text;
				airforce.fldAirForceNameLocal = txtAirforceLocarlName.Text;
				airforce.fldAirForceNameShort = txtAirforceShortcut.Text;
				Airforce_id = viewModel.AddAirforce(airforce);
				txtAirforce.Text = txtAirforceShortcut.Text;
				txtUnitAirforce.Text = TxtAirforce.Text;
				loader.IsVisible = false;
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}

		}

		private bool ValidateAirforceForSave()
		{
			try
			{
				if (TxtAirforce.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Airforce", "Ok");
					TxtAirforce.Focus();
					return false;
				}
				else if (txtAirforceLocarlName.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Local Name", "Ok");
					txtAirforceLocarlName.Focus();
					return false;
				}
				else if (txtAirforceShortcut.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Shortcut", "Ok");
					txtAirforceShortcut.Focus();

					return false;
				}
				else if (txtAirforceCountry.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Country", "Ok");
					txtAirforceCountry.Focus();
					return false;
				}
				else

					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		#endregion

		#region Units

		private void ShowSelectUnitPopup()
		{
			stkSelectUnitsPopup.IsVisible = true;
			InitSelectUnitPopup();
		}

		protected void lblSelectUnitsDone_Click(object sender, EventArgs e)
		{
			try
			{
				if (sagSelectUnits.SelectedValue)
				{
					if (!SelectUnits())
						return;
				}
				else {
					if (!SaveUnits())
					{
						LoadUnits("");
						return;
					}
				}
			}
			catch (Exception ex)
			{
				return;
			}
			ApplyValue();
			CloseSelectUnitPopup();
			loader.IsVisible = false;
		}

		protected void lblSelectUnitsBackMenu_Click(object sender, EventArgs e)
		{
			CloseSelectUnitPopup();
		}

		protected void sagSelectUnits_OnChange(object sender, EventArgs e)
		{
			SetUnitsVisibility(sagSelectUnits.SelectedValue);
		}

		private void InitSelectUnitPopup()
		{
			FIllUnitsList();
		}

		private void CloseSelectUnitPopup()
		{
			stkSelectUnitsPopup.IsVisible = false;
			ClearstkSelectUnitsPopup();
		}

		private void ClearstkSelectUnitsPopup()
		{
			LstSearchUnitList.ItemsSource = null;
			LstSearchUnitAirforceList.ItemsSource = null;
			UnLoadUnits();
			UnLoadAirforce();
			txtUnitName.Text = string.Empty;
			txtUnitAirforce.Text = string.Empty;
			txtUnitsSearch.Text = String.Empty;
		}

		private void FIllUnitsList()
		{
			LoadUnits(txtUnitsSearch.Text);
			LoadAirforce("");
			LstSearchUnitList.ItemsSource = UnitsList;
			LstSearchUnitAirforceList.ItemsSource = AirForcesList;
		}

		private void LoadUnits(string UnitName)
		{
			var _searchList = viewModel.SearchUnits(50, lastUnitsCount, UnitName);
			var uni = _searchList.Distinct().ToList();
			UnitsList.Clear();
			for (int i = 0; i <= uni.Count - 1; i++)
			{
				UnitsList.Add(uni[i]);
				//if (i == _searchList.Count - 1)
				//	lastUnitsCount = _searchList [i].UnitID;
			}
		}

		protected void LstUnitsList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			if (isUnitsLoading || UnitsList.Count == 0)
				return;

			//hit bottom!
			if (e.Item == UnitsList[UnitsList.Count - 1])
			{
				LoadUnits(txtUnitsSearch.Text);
			}
		}
		protected void LstSearchUnitList_tapped(object sender, SelectedItemChangedEventArgs e)
		{
			var unit = e.SelectedItem as Units;
			txtUnitsSearch.Text = unit.UnitName1;
		}

		protected void LstSearchUnitAirforceList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			if (isAirForcesLoading || AirForcesList.Count == 0)
				return;

			//hit bottom!
			if (e.Item == AirForcesList[AirForcesList.Count - 1])
			{
				LoadAirforce("");
			}
		}

		private void LstSearchUnitAirforceList_Tap(object sender, ItemTappedEventArgs e)
		{
			var airforce = e.Item as AirForces;
			txtUnitAirforce.Text = airforce.AirForceName;
		}

		protected void txtUnitsSearch_Change(object sender, TextChangedEventArgs e)
		{
			UnLoadUnits();
			LoadUnits(txtUnitsSearch.Text);
		}

		private void UnLoadUnits()
		{
			UnitsList.Clear();
			lastUnitsCount = 0;
		}

		private bool SaveUnits()
		{
			if (!ValidateUnitsForSave())
				return false;
			
			tblUnits units = new tblUnits();
			try
			{
				loader.IsVisible = true;
				units.fldUnitName1 = txtUnitName.Text;
				if (LstSearchUnitAirforceList.SelectedItem == null)
				{
					tblAirForces airforcels = airforcedb.GetAirforce(Airforce_id);
					units.fldAirForceID = airforcels.fldAirForceID;
					CurrentRegistration.AirForceName = airforcels.fldAirForceName;
				}
				else
				{
					units.fldAirForceID = (LstSearchUnitAirforceList.SelectedItem as AirForces).AirForceID;
					CurrentRegistration.AirForceName = (LstSearchUnitAirforceList.SelectedItem as AirForces).AirForceName;
				}

				units.fldUnitID = viewModel.AddUnits(units);

				CurrentRegistration.UnitID = units.fldUnitID;
				CurrentRegistration.Code = units.fldTailcode;
				CurrentRegistration.UnitName1 = units.fldUnitName1;
				CurrentRegistration.AirForceID = units.fldAirForceID;
				//CurrentRegistration.AirForceName = airforcels.fldAirForceName;
				//CurrentRegistration.AirForceName = (LstSearchUnitAirforceList.SelectedItem as AirForces).AirForceName;
				LstSearchUnitAirforceList.SelectedItem = null;
				loader.IsVisible = false;
				return true;
			}
			catch (Exception ex)
			{
				
				loader.IsVisible = false;
				return false;
			}
			loader.IsVisible = false;
		}

		private bool ValidateUnitsForSave()
		{
			try
			{
				if (txtUnitName.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Unit Name", "Ok");
					txtUnitName.Focus();
					return false;
				}
				else if (txtUnitAirforce.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Select Airforce", "Ok");
					txtUnitAirforce.Focus();
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool SelectUnits()
		{
			try
			{
				if (ValidateUnitsForSelect())
				{
					//loader.IsVisible = true;
					Units unit = LstSearchUnitList.SelectedItem as Units;
					CurrentRegistration.UnitID = unit.UnitID;
					CurrentRegistration.UnitName1 = unit.UnitName1;
					CurrentRegistration.AirForceID = unit.AirForceID;
					CurrentRegistration.AirForceName = unit.AirForceName;
					CurrentRegistration.Code = unit.Tailcode;
					//loader.IsVisible = false;
					return true;
				}
				else
					return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool ValidateUnitsForSelect()
		{
			try
			{
				if (LstSearchUnitList.SelectedItem == null)
				{
					DisplayAlert("Required", "Please Select Unit First", "Ok");
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		#endregion

		#region Manufacturer

		private void ShowAddManufacturerPopup()
		{
			stkAddManufacturerPopup.IsVisible = true;
			InitManufacturerPopup();
		}

		protected void lblAddManufacturerBackMenu_Click(object sender, EventArgs e)
		{
			CloseManufacturerPopup();
		}

		protected void lblAddManufacturerDone_Click(object sender, EventArgs e)
		{
			if (SaveManufacturer())
			{
				LoadManufacturer("");
				CloseManufacturerPopup();
			}
		}

		private void InitManufacturerPopup()
		{
			LoadCountry("");
			LstSearchManufacturerCountryList.ItemsSource = CountriesList;
		}

		private void CloseManufacturerPopup()
		{
			stkAddManufacturerPopup.IsVisible = false;
			ClearManufacturerPopup();
		}

		private void ClearManufacturerPopup()
		{
			LstSearchManufacturerCountryList.ItemsSource = null;
			UnLoadCountry();
			txtManufacturer.Text = string.Empty;
			txtManufacturerCountry.Text = string.Empty;
		}

		protected void LstSearchManufacturerCountryList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			if (isCountriesLoading || CountriesList.Count == 0)
				return;

			if (e.Item == CountriesList[CountriesList.Count - 1])
			{
				LoadCountry("");
			}
		}

		private void LstSearchManufacturerCountryList_Tap(object sender, ItemTappedEventArgs e)
		{
			var countries = e.Item as Countries;
			txtManufacturerCountry.Text = countries.CountryName;
		}

		private void LoadManufacturer(string Manufacturer)
		{
			var _searchList = viewModel.SearcManufacturers(20, lastCountriesCount, Manufacturer);
			for (int i = 0; i <= _searchList.Count - 1; i++)
			{
				ManufacturersList.Add(_searchList[i]);
				//if (i == _searchList.Count - 1)
				//	lastManufacturersCount = _searchList [i].ManufacturerID;
			}
		}

		private void UnLoadManufacturer()
		{
			ManufacturersList.Clear();
			lastManufacturersCount = 0;
		}

		private bool SaveManufacturer()
		{
			if (!ValidateManufacturerForSave())
				return false;
			
			tblManufacturers manufacturers = new tblManufacturers();
			try
			{
				loader.IsVisible = true;
				manufacturers.fldManufacturerName = txtManufacturer.Text;
				manufacturers.fldCountryID = (LstSearchManufacturerCountryList.SelectedItem as Countries).CountryID;
				//manufacturers.
				viewModel.AddManufacturer(manufacturers);
				loader.IsVisible = false;
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			finally
			{
				manufacturers = null;
			}

		}

		private bool ValidateManufacturerForSave()
		{
			try
			{
				if (txtManufacturer.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Manufacturer", "Ok");
					txtManufacturer.Focus();
					return false;
				}
				else if (LstSearchManufacturerCountryList.SelectedItem == null)
				{

					DisplayAlert("Required", "Please select country", "Ok");
					txtManufacturerCountry.Focus();
					return false;
				}
				else
				{
					return true;
				}


			}
			catch (Exception ex)
			{
				return false;
			}

		}

		#endregion

		#region Country

		private void LoadCountry(string CountryName)
		{
			var _searchList = viewModel.SearcCountries(150, lastCountriesCount, CountryName);
			for (int i = 0; i <= _searchList.Count - 1; i++)
			{
				CountriesList.Add(_searchList[i]);
				//if (i == _searchList.Count - 1)
				//	lastCountriesCount = _searchList [i].CountryID;
			}
		}

		private void UnLoadCountry()
		{
			CountriesList.Clear();
			lastCountriesCount = 0;
		}

		#endregion

		#region Civil DB Seaqrch

		protected void btnCloseCivilSearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkCivilSearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		protected void LstSearchCivilDB_Tap(object o, ItemTappedEventArgs e)
		{
			var civil = e.Item as APICivilSearch;
			selectedCivil = civil;
			txtRegistration.Text = civil.Registration;
			//lblSelectedRegistration.Text = civil.Owner + " " + civil.FullType; 
			lblSelectedRegistration.Text = civil.FullType + "," + civil.Owner;
			txtCN.Text = civil.Cnr;
			txtAirline.Text = civil.Owner;
			txtAircraft.Text = civil.FullType;
			stkCivilSearch.IsVisible = false;
			stkRegistrationPhotos.Children.Clear();
		}

		#endregion

		#region Country DB Search

		protected void btnCloseCountrySearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkCountrySearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		protected async void LstSearchCountryDB_Tap(object o, ItemTappedEventArgs e)
		{
			try
			{

				SearchAPI api = new SearchAPI();
				var country = e.Item as APICountrydbSearch;
				selectedCountry = country;
				user.setuserCountry(country.Country);
				stkCountrySearch.IsVisible = false;

				loader.IsVisible = true;
				List<APImilitarydbSearch> list = new List<APImilitarydbSearch>();
				list = await api.SearchMilitaryDbByCountry(country.Code);
				if (list.Count != 0)
				{
					LstSearchMilitaryDB.ItemsSource = list;
					stkMilitarySearch.IsVisible = true;
				}
				else {
					DisplayAlert("Registration", "No registration Found", "ok");

				}
				loader.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region Military DB Seaqrch

		protected void btnCloseMilitarySearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkMilitarySearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		protected void LstSearchMilitaryDB_Tap(object o, ItemTappedEventArgs e)
		{
			//			var military = e.Item as APIMilitaryDatabaseSearch;
			var military = e.Item as APImilitarydbSearch;
			selectedMilitary = military;
			//txtRegister.Text = military.;
			//			lblSelectedRegistration.Text = military.CountryCode + " " + military.AirForceName; 
			lblSelectedRegistration.Text = military.Type + " " + military.Unit;
			txtCN.Text = military.CN;
			txtUnit.Text = military.Unit;
			txtAircraft.Text = military.Type;
			txtRegistration.Text = military.Serial;
			stkMilitarySearch.IsVisible = false;
			stkRegistrationPhotos.Children.Clear();
		}
		#endregion


		#region search airforce

		private void ShowSearchAirforcePopup()
		{
			try
			{
				stkSelectAirforcePopup.IsVisible = true;
				//	SetAirforceVisibility(sagSelectAirforce.SelectedValue);
				InitAirforcePopup();
			}
			catch (Exception ex)
			{

			}
		}
		private void ShowSearchAirforcePopup1()
		{
			try
			{
				stkAddAirforcePopup.IsVisible = true;
				//	SetAirforceVisibility(sagSelectAirforce.SelectedValue);
				InitAirforcePopup();
			}
			catch (Exception ex)
			{

			}
		}

		protected void sagSelectAirforce_OnChange(object sender, EventArgs e)
		{
			//		SetAirforceVisibility(sagSelectAirforce.SelectedValue);
		}
		private void LstSearchAirforceList_Tap(object sender, ItemTappedEventArgs e)
		{
			var airforce = e.Item as AirForces;
			txtAirforceSearch.Text = airforce.AirForceName;
		}
		protected void LstSearchAirforceList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			//if (isAirForcesLoading || AirForcesList.Count == 0)
			//	return;

			////hit bottom!
			//if (e.Item == AirForcesList[AirForcesList.Count - 1])
			//{
			//	LoadAirforce("");
			//}
		}

		private void InitAirforcePopup()
		{
			try
			{
				FIllAirForceList();
				LoadAirforce("");
				LstSearchAirforceList.ItemsSource = AirForcesList;
			}
			catch (Exception ex)
			{

			}
		}


		void lblSearchAirforceBackMenu_Click(object s, EventArgs e)
		{
			try
			{
				CloseSearchAirforcePopup();
			}
			catch (Exception ex)
			{

			}
		}
		protected void txtSearchAirforce_Change(object sender, TextChangedEventArgs e)
		{
			UnLoadAirforce();
			LoadAirforce(txtAirforceSearch.Text);
		}
		void CloseSearchAirforcePopup()
		{
			try
			{
				stkSelectAirforcePopup.IsVisible = false;
				//ClearAircraftPopup();
			}
			catch (Exception ex)
			{

			}
		}

		void lblSearchAirforceDone_Click(object s, EventArgs e)
		{
			SelectAirforce();
			ApplyValue();
			CloseSearchAirforcePopup();
		}
		bool SelectAirforce()
		{
			if (ValidateAirforceForSelect())
			{
				AirForces AirForce = LstSearchAirforceList.SelectedItem as AirForces;
				CurrentRegistration.AirForceID = AirForce.AirForceID;
				CurrentRegistration.AirForceName = AirForce.AirForceName;
				txtAirforce.Text = AirForce.AirForceName;
				return true;
			}
			else
				return false;
		}
		private bool ValidateAirforceForSelect()
		{
			try
			{
				if (LstSearchAirforceList.SelectedItem == null)
				{
					DisplayAlert("Required", "Please Select Airforce First", "Ok");
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		#endregion

		#region selectpic

		#endregion

	}
}


