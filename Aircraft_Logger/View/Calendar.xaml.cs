﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Linq;

namespace Aircraft_Logger
{
	public partial class Calendar : ContentPage
	{
		List<YearMonthAirportId> lsByYear = new List<YearMonthAirportId>();
		List<YearMonthAirportId> lsByYearMonth = new List<YearMonthAirportId>();
		List<YearMonthAirportId> lsMonths = new List<YearMonthAirportId>();
		List<YearMonthAirportId> lsBindableResult = new List<YearMonthAirportId>();
		public int cnt;
		CalenderDatabase calenderDb = new CalenderDatabase();
		int arrayIndex = 0;
		int arrayIndexByYearMonth = 0;
		int arrayIndexByMonth = 0;

		public Calendar()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			loader.IsRunning = true;
			loader.IsVisible = true;

			Bind_ClickEvents();

			lsBindableResult = calenderDb.GetYearMonthByAirportVisitByYearMonth();
			List<YearMonthAirportId> distinctPeople = lsBindableResult.GroupBy(p => p.Year).Select(g => g.First()).ToList();
			cnt = distinctPeople.Count;
			BindByYear();
			BindByMonth();
			loader.IsRunning = false;
			loader.IsVisible = false;

		}
		public void BindByMonth()
		{
			lsByYearMonth = calenderDb.GetYearMonthByAirportVisitByYear();
			BindMonthName();
			BindDataByYearMonth(0, 0);
		}
		public void BindByYear()
		{
			lsByYear = calenderDb.GetYearMonthByAirportVisitByYear();
			BindDataByYear(0);
		}

		public void BindMonthName()
		{
			string[] monthNames = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
			int i = 1;
			foreach (var item in monthNames)
			{
				if (!string.IsNullOrEmpty(item))
				{

					lsMonths.Add(new YearMonthAirportId() { Month = i, strMonth = item });
					i++;
				}
			}
		}
		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand(s, e);
			var tapGestureRecognizerCalendar1 = new TapGestureRecognizer();
			tapGestureRecognizerCalendar1.Tapped += (s, e) => NavigatePicCommand(s, e);
			stkBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			stkDone.GestureRecognizers.Add(tapGestureRecognizerCalendar1);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => StkMonth_Click(s, e);
			stk_Month.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => StkYear_Click(s, e);
			stk_Year.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			var tapGestureRecognizerCalendarPrev = new TapGestureRecognizer();
			tapGestureRecognizerCalendarPrev.Tapped += (s, e) => btnPrev_Clicked(s, e);
			lblPrev.GestureRecognizers.Add(tapGestureRecognizerCalendarPrev);

			var tapGestureRecognizerCalendarNext = new TapGestureRecognizer();
			tapGestureRecognizerCalendarNext.Tapped += (s, e) => btnNext_Clicked(s, e);
			lblNext.GestureRecognizers.Add(tapGestureRecognizerCalendarNext);

			var tapGestureRecognizerCalendarMonthPrev = new TapGestureRecognizer();
			tapGestureRecognizerCalendarMonthPrev.Tapped += (s, e) => btnMonthNext_Clicked(s, e);
			lblMonthPrev.GestureRecognizers.Add(tapGestureRecognizerCalendarMonthPrev);

			var tapGestureRecognizerCalendarMonthNext = new TapGestureRecognizer();
			tapGestureRecognizerCalendarMonthNext.Tapped += (s, e) => btnMonthPrev_Clicked(s, e);
			lblMonthNext.GestureRecognizers.Add(tapGestureRecognizerCalendarMonthNext);

			var tapGestureRecognizerCalendarMonthYearPrev = new TapGestureRecognizer();
			tapGestureRecognizerCalendarMonthYearPrev.Tapped += (s, e) => btnMonthYearPrev_Clicked(s, e);
			lblMonthYearPrev.GestureRecognizers.Add(tapGestureRecognizerCalendarMonthYearPrev);

			var tapGestureRecognizerCalendarMonthYearNext = new TapGestureRecognizer();
			tapGestureRecognizerCalendarMonthYearNext.Tapped += (s, e) => btnMonthYearNext_Clicked(s, e);
			lblMonthYearNext.GestureRecognizers.Add(tapGestureRecognizerCalendarMonthYearNext);

		}

		public string Truncate(string value, int maxChars)
		{
			if (!string.IsNullOrEmpty(value))
			{
				return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
			}
			else {
				return string.Empty;
			}

		}

		public void ReSetButtonBackColor()
		{
			btn1.BackgroundColor = Color.White;
			btn2.BackgroundColor = Color.White;
			btn3.BackgroundColor = Color.White;
			btn4.BackgroundColor = Color.White;
			btn5.BackgroundColor = Color.White;
			btn6.BackgroundColor = Color.White;
			btn7.BackgroundColor = Color.White;
			btn8.BackgroundColor = Color.White;
			btn9.BackgroundColor = Color.White;
			btn10.BackgroundColor = Color.White;
			btn11.BackgroundColor = Color.White;
			btn12.BackgroundColor = Color.White;

		}

		public void SetMonthButtonBackColor(int month)
		{

			if (month == 1)
			{
				btn1.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 2)
			{
				btn2.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 3)
			{
				btn3.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 4)
			{
				btn4.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 5)
			{
				btn5.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 6)
			{
				btn6.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 7)
			{
				btn7.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 8)
			{
				btn8.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 9)
			{
				btn9.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 10)
			{
				btn10.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 11)
			{
				btn11.BackgroundColor = Color.FromHex("#24AFE3");
			}
			if (month == 12)
			{
				btn12.BackgroundColor = Color.FromHex("#24AFE3");
			}


		}

		public void BindDataByYear(int arrayIndex)
		{

			try
			{

				loader.IsVisible = true;
				loader.IsRunning = true;

				stkCollectionByYear.Children.Clear();

				//List<YearMonthAirportId> result = calenderDb.GetYearMonthByAirportVisitByYearMonth ().FindAll(x=>x.Year==lsByYear[arrayIndex].Year);
				List<YearMonthAirportId> result = lsBindableResult.FindAll(x => x.Year == lsByYear[arrayIndex].Year);
				lblYear.Text = lsByYear[arrayIndex].Year.ToString();
				TimelineModel objTimelineModel = new TimelineModel();
				List<YearMonthAirportId> result1 = objTimelineModel.GetYearMonth();
				StackLayout stkOuterLayout = new StackLayout();

				foreach (var item in result)
				{
					item.lsttimeRegistration = objTimelineModel.GetRegistrationsByYearMonth(item.Year, item.Month, result);
					StackLayout stkItemCollections = new StackLayout()
					{
						Padding = new Thickness(1, 1, 1, 1)
					};
					SetMonthButtonBackColor(item.Month);
					#region Year Month 
					StackLayout stkYearMonth = new StackLayout()
					{
						HorizontalOptions = LayoutOptions.FillAndExpand,
						BackgroundColor = Color.FromHex("#24AFE3"),
						HeightRequest = 20,

						Spacing = 0
					};
					Label lblYear = new Label()
					{
						TextColor = Color.White,
						XAlign = TextAlignment.Center,
						YAlign = TextAlignment.Center,
						FontSize = 13,
						FontAttributes = FontAttributes.Bold
					};

					lblYear.Text = item.strMonth + " " + item.Year.ToString();
					stkYearMonth.Children.Add(lblYear);
					#endregion


					#region Display Registrations
					foreach (var regItem in item.lsttimeRegistration)
					{

						if (regItem.AirportName != null)
						{
							StackLayout stkItems = new StackLayout()
							{
								HeightRequest = 70,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								Orientation = StackOrientation.Horizontal,
								Spacing = 0,
								Padding = new Thickness(5, 0, 5, 0)
							};

							StackLayout stkImage = new StackLayout()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								VerticalOptions = LayoutOptions.Start,
								HorizontalOptions = LayoutOptions.Start,
								Spacing = 0,
								Padding = new Thickness(0, 0, 5, 0)
							};

							var tapGestureRecognizerAirportVisit = new TapGestureRecognizer();
							tapGestureRecognizerAirportVisit.Tapped += (s, e) =>
							{
								NavigateToAirportVisit(regItem.AirportId);
							};
							stkImage.GestureRecognizers.Add(tapGestureRecognizerAirportVisit);

							StackLayout temp = new StackLayout()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								HorizontalOptions = LayoutOptions.Start,
								Spacing = 0,
								Padding = new Thickness(1, 1, 1, 1),
								BackgroundColor = Color.FromHex("#24AFE3")

							};

							CImage imgTmp = new CImage()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								Source = "tmpImg1.jpg",
								Aspect = Aspect.AspectFill
							};
							temp.Children.Add(imgTmp);
							stkImage.Children.Add(temp);
							StackLayout stkRegistrations = new StackLayout()
							{
								HeightRequest = 30,
								VerticalOptions = LayoutOptions.Start,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								Spacing = 0
							};

							StackLayout stkAirportName = new StackLayout()
							{
								Orientation = StackOrientation.Horizontal,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								Spacing = 0
							};
							Label lblAirportName = new Label()
							{
								Text = Truncate(regItem.AirportName, 10),
								FontFamily = "Roboto Condensed",
								FontAttributes = FontAttributes.Bold,
								FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
								HorizontalOptions = LayoutOptions.FillAndExpand,
								//WidthRequest = 180,
								XAlign = TextAlignment.Start
							};
							Label lblDay = new Label()
							{
								Text = regItem.Day.ToString(),
								FontFamily = "Roboto Condensed",
								FontAttributes = FontAttributes.Bold,
								FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
								WidthRequest = 38,
								XAlign = TextAlignment.End,
								HorizontalOptions = LayoutOptions.CenterAndExpand
							};

							Grid grid = new Grid
							{
								VerticalOptions = LayoutOptions.FillAndExpand,
								RowDefinitions =
						{

					new RowDefinition { Height =  GridLength.Auto},

						},
								ColumnDefinitions =
						{
					new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
							new ColumnDefinition { Width = new GridLength(40,GridUnitType.Absolute) },
						}
							};
							grid.Children.Add(lblAirportName, 0, 0);
							grid.Children.Add(lblDay, 1, 0);
							//stkAirportName.Children.Add (lblAirportName);
							//stkAirportName.Children.Add (lblDay);
							if (lblAirportName.Text != "" && lblDay.Text != "")
							{
								stkRegistrations.Children.Add(grid);
							}





							//stkAirportName.Children.Add(lblAirportName);
							//stkAirportName.Children.Add(lblDay);

							//stkRegistrations.Children.Add(stkAirportName);

							StackLayout stkRegistrationDetail = new StackLayout()
							{
								VerticalOptions=LayoutOptions.StartAndExpand,
								Spacing = 0
							};
							foreach (var reg in regItem.lstRegistration)
							{
								if (reg.PhotoName != null)
								{
									imgTmp.Source = LoadImageFromDisk(reg.PhotoName);
								}
								StackLayout stkRegistrationItems = new StackLayout()
								{
									Orientation = StackOrientation.Horizontal,
									Spacing = 0
								};
								Label lblRegNo = new Label()
								{
									Text = reg.Registration,
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
									WidthRequest = 60
								};
								Label lblAircraft = new Label()
								{
									Text = Truncate(reg.AircraftName, 7),
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),

								};
								Label lblAirline = new Label()
								{
									Text = Truncate(reg.AirlineName, 7),
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),

								};
								Grid grid1 = new Grid
								{
									VerticalOptions = LayoutOptions.FillAndExpand,
									RowDefinitions =
						{

										new RowDefinition { Height = new GridLength(20, GridUnitType.Absolute)},

						},
									ColumnDefinitions =
						{
					new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
								new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
							new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
						}
								};
								grid1.Children.Add(lblRegNo, 0, 0);
								grid1.Children.Add(lblAircraft, 1, 0);
								grid1.Children.Add(lblAirline, 2, 0);
								//stkRegistrationItems.Children.Add(lblRegNo);
								//stkRegistrationItems.Children.Add(lblAircraft);
								//stkRegistrationItems.Children.Add(lblAirline);

								stkRegistrationDetail.Children.Add(grid1);



							}
							stkRegistrations.Children.Add(stkRegistrationDetail);




							stkItems.Children.Add(stkImage);
							stkItems.Children.Add(stkRegistrations);

							stkItemCollections.Children.Add(stkYearMonth);
							stkItemCollections.Children.Add(stkItems);

							StackLayout stkHorizontalLine = new StackLayout()
							{
								Spacing = 0,
								BackgroundColor = Color.Silver,
								HeightRequest = 1,
								HorizontalOptions = LayoutOptions.FillAndExpand,

							};
							stkItemCollections.Children.Add(stkHorizontalLine);
						}
					}
					#endregion

					stkOuterLayout.Children.Add(stkItemCollections);
				}

				stkCollectionByYear.Children.Add(stkOuterLayout);
				loader.IsVisible = false;
				loader.IsRunning = false;

			}
			catch (Exception ex)
			{
				loader.IsVisible = false;
				loader.IsRunning = false;
				DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
			}

		}

		public void BindDataByYearMonth(int arrayIndexyearMonth, int arrayMonthIndex)
		{

			try
			{

				loader.IsVisible = true;
				loader.IsRunning = true;
				stkCalender.Children.Clear();
				stkCollectionByMonth.Children.Clear();
				//TimelineModel objTimelineModel = new TimelineModel();
				//List<YearMonthAirportId> result1 = objTimelineModel.GetYearMonth();

				//List<YearMonthAirportId> result = calenderDb.GetYearMonthByAirportVisitByYearMonth ().FindAll(x=>x.Year==lsByYearMonth[arrayIndexyearMonth].Year && x.Month==lsMonths[arrayMonthIndex].Month);
				List<YearMonthAirportId> result = lsBindableResult.FindAll(x => x.Year == lsByYearMonth[arrayIndexyearMonth].Year && x.Month == lsMonths[arrayMonthIndex].Month);

				var getallDataYearWiseForMonthSelection = result;
				BindCalender(lsByYearMonth[arrayIndexyearMonth].Year, lsMonths[arrayMonthIndex].Month, result);
				TimelineModel objTimelineModel = new TimelineModel();
				lblMonthYear.Text = lsByYearMonth[arrayIndexyearMonth].Year.ToString();
				lblMonth.Text = lsMonths[arrayMonthIndex].strMonth.ToString();
				List<YearMonthAirportId> result1 = objTimelineModel.GetYearMonth();
				StackLayout stkOuterLayout = new StackLayout();

				foreach (var item in result)
				{
					item.lsttimeRegistration = objTimelineModel.GetRegistrationsByYearMonth(item.Year, item.Month, result);
					StackLayout stkItemCollections = new StackLayout()
					{

					};
					SetMonthButtonBackColor(item.Month);
					#region Year Month 
					StackLayout stkYearMonth = new StackLayout()
					{
						HorizontalOptions = LayoutOptions.FillAndExpand,
						BackgroundColor = Color.FromHex("#24AFE3"),
						HeightRequest = 20,
						Spacing = 0
					};
					Label lblYear = new Label()
					{
						TextColor = Color.White,
						XAlign = TextAlignment.Center,
						YAlign = TextAlignment.Center,
						FontSize = 13,
						FontAttributes = FontAttributes.Bold
					};

					lblYear.Text = item.strMonth + " " + item.Year.ToString();
					stkYearMonth.Children.Add(lblYear);
					#endregion


					#region Display Registrations
					foreach (var regItem in item.lsttimeRegistration)
					{
						if (regItem.AirportName != null)
						{
							StackLayout stkItems = new StackLayout()
							{
								HeightRequest = 70,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								Orientation = StackOrientation.Horizontal,
								Spacing = 0,
								Padding = new Thickness(5, 0, 5, 5)
							};

							StackLayout stkImage = new StackLayout()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								VerticalOptions = LayoutOptions.Start,
								HorizontalOptions = LayoutOptions.Start,
								Spacing = 0,
								Padding = new Thickness(0, 0, 5, 0)
							};

							var tapGestureRecognizerAirportVisit = new TapGestureRecognizer();
							tapGestureRecognizerAirportVisit.Tapped += (s, e) =>
							{
								NavigateToAirportVisit(regItem.AirportId);
							};
							stkImage.GestureRecognizers.Add(tapGestureRecognizerAirportVisit);

							StackLayout temp = new StackLayout()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								HorizontalOptions = LayoutOptions.Start,
								Spacing = 0,
								Padding = new Thickness(1, 1, 1, 1),
								BackgroundColor = Color.FromHex("#24AFE3")

							};

							CImage imgTmp = new CImage()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								Source = "tmpImg1.jpg",
								Aspect = Aspect.AspectFill
							};
							temp.Children.Add(imgTmp);
							stkImage.Children.Add(temp);
							StackLayout stkRegistrations = new StackLayout()
							{
								HeightRequest = 30,
								VerticalOptions = LayoutOptions.Start,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								Spacing = 0
							};

							StackLayout stkAirportName = new StackLayout()
							{
								Orientation = StackOrientation.Horizontal,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								Spacing = 0
							};
							Label lblAirportName = new Label()
							{
								Text = Truncate(regItem.AirportName, 10),
								FontFamily = "Roboto Condensed",
								FontAttributes = FontAttributes.Bold,
								FontSize = 16,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								//WidthRequest = 180,
								XAlign = TextAlignment.Start

							};
							Label lblDay = new Label()
							{
								Text = regItem.Day.ToString(),
								FontFamily = "Roboto Condensed",
								FontAttributes = FontAttributes.Bold,
								FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
								WidthRequest = 38,
								XAlign = TextAlignment.End,
								HorizontalOptions = LayoutOptions.CenterAndExpand
							};
							Grid grid = new Grid
							{
								VerticalOptions = LayoutOptions.FillAndExpand,
								RowDefinitions =
						{

					new RowDefinition { Height =  GridLength.Auto},

						},
								ColumnDefinitions =
						{
					new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
							new ColumnDefinition { Width = new GridLength(40,GridUnitType.Absolute) },
						}
							};
							grid.Children.Add(lblAirportName, 0, 0);
							grid.Children.Add(lblDay, 1, 0);
							//stkAirportName.Children.Add (lblAirportName);
							//stkAirportName.Children.Add (lblDay);
							if (lblAirportName.Text != "" && lblDay.Text != "")
							{
								stkRegistrations.Children.Add(grid);
							}





							//stkAirportName.Children.Add(lblAirportName);
							//stkAirportName.Children.Add(lblDay);

							//stkRegistrations.Children.Add(stkAirportName);

							StackLayout stkRegistrationDetail = new StackLayout()
							{
								VerticalOptions=LayoutOptions.StartAndExpand,
								Spacing = 0
							};
							foreach (var reg in regItem.lstRegistration)
							{
								if (reg.PhotoName != null)
								{
									imgTmp.Source = LoadImageFromDisk(reg.PhotoName);
								}
								StackLayout stkRegistrationItems = new StackLayout()
								{
									Orientation = StackOrientation.Horizontal,

									Spacing = 0
								};
								Label lblRegNo = new Label()
								{
									Text = reg.Registration,
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
									WidthRequest = 60
								};
								Label lblAircraft = new Label()
								{
									Text = Truncate(reg.AircraftName, 7),
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),

								};
								Label lblAirline = new Label()
								{
									Text = Truncate(reg.AirlineName, 7),
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),

								};
								Grid grid1 = new Grid
								{
									VerticalOptions = LayoutOptions.FillAndExpand,
									RowDefinitions =
						{

					new RowDefinition { Height = new GridLength(20, GridUnitType.Absolute)},

						},
									ColumnDefinitions =
						{
					new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
								new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
							new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
						}
								};
								grid1.Children.Add(lblRegNo, 0, 0);
								grid1.Children.Add(lblAircraft, 1, 0);
								grid1.Children.Add(lblAirline, 2, 0);
								//stkRegistrationItems.Children.Add(lblRegNo);
								//stkRegistrationItems.Children.Add(lblAircraft);
								//stkRegistrationItems.Children.Add(lblAirline);

								stkRegistrationDetail.Children.Add(grid1);



							}
							stkRegistrations.Children.Add(stkRegistrationDetail);


							stkItems.Children.Add(stkImage);
							stkItems.Children.Add(stkRegistrations);

							stkItemCollections.Children.Add(stkYearMonth);
							stkItemCollections.Children.Add(stkItems);

							StackLayout stkHorizontalLine = new StackLayout()
							{
								Spacing = 0,
								BackgroundColor = Color.Silver,
								HeightRequest = 1,
								HorizontalOptions = LayoutOptions.FillAndExpand,

							};
							stkItemCollections.Children.Add(stkHorizontalLine);

						}


						stkOuterLayout.Children.Add(stkItemCollections);
					}
					#endregion
				}

				stkCollectionByMonth.Children.Add(stkOuterLayout);
				loader.IsVisible = false;
				loader.IsRunning = false;
			}
			catch (Exception ex)
			{
				loader.IsVisible = false;
				loader.IsRunning = false;
				DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
			}

		}

		public static List<DateTime> GetDates(int year, int month)
		{
			return Enumerable.Range(1, DateTime.DaysInMonth(year, month))  // Days: 1, 2 ... 31 etc.
				.Select(day => new DateTime(year, month, day)) // Map each day to a date
				.ToList(); // Load dates into a list
		}
		public void BindCalender(int year, int month, List<YearMonthAirportId> lsSelectionForMonth)
		{
			List<int> lsDays = new List<int>();
			var dateList = GetDates(year, month);
			foreach (var item in lsSelectionForMonth)
			{
				var registartions = calenderDb.GetRegistrationsByYear(item.Year, item.Month, lsSelectionForMonth);
				lsDays = registartions.Select(x => x.Day).ToList();

			}
			//var dayOrderBy = dateList.OrderBy (x => (int)x.DayOfWeek).ToList ();
			var dateListGroup = (from e in dateList
								 group e by e.DayOfWeek into dayList
								 select dayList.ToList()
			).ToList();


			var stkCollection = new StackLayout()
			{
				//Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand

			};
			for (var i = 0; i < dateListGroup.Count; i++)
			{

				var stkDayCollection = new StackLayout()
				{
					//Spacing = 10,

					HorizontalOptions = LayoutOptions.FillAndExpand
				};
				var stkDayName = new StackLayout()
				{
					//Spacing = 0,
					HorizontalOptions = LayoutOptions.FillAndExpand
				};
				var lblDayName = new Label()
				{
					FontSize = 10,
					HeightRequest = 20,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center

				};

				stkDayName.Children.Add(lblDayName);
				foreach (var item in dateListGroup[i])
				{
					lblDayName.Text = item.DayOfWeek.ToString().Substring(0, 1);
					var lblDay = new Label()
					{
						Text = item.Day.ToString(),
						FontSize = 10,
						//HeightRequest=20,
						XAlign = TextAlignment.Center,
						YAlign = TextAlignment.Center
					};
					var dayExixt = lsDays.Where(x => x == item.Day).ToList();
					if (dayExixt.Count > 0)
					{
						lblDay.BackgroundColor = Color.FromHex("#24AFE3");

					}
					else {
						lblDay.BackgroundColor = Color.White;
					}

					stkDayCollection.Children.Add(lblDayName);
					stkDayCollection.Children.Add(lblDay);
				}
				stkCollection.Spacing = 0;
				stkCollection.Children.Add(stkDayCollection);
			}


			stkCalender.Children.Add(stkCollection);

		}
		protected void btnNext_Clicked(object sender, EventArgs e)
		{
			ReSetButtonBackColor();
			arrayIndex--;
			if (arrayIndex >= 0)
			{
				BindDataByYear(arrayIndex);
			}
			else
			{
				DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
				arrayIndex++;
				BindDataByYear(arrayIndex);
			}
		}

		protected void btnPrev_Clicked(object sender, EventArgs e)
		{
			ReSetButtonBackColor();
			arrayIndex++;
			if (arrayIndex < cnt)
			{
				BindDataByYear(arrayIndex);
			}
			else
			{
				DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
				arrayIndex--;
				BindDataByYear(arrayIndex);
			}
		}

		protected void btnMonthPrev_Clicked(object sender, EventArgs e)
		{

			arrayIndexByMonth++;
			if (arrayIndexByMonth < 12)
			{
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
			else
			{
				//DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
				arrayIndexByMonth--;
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
			//ReSetButtonBackColor ();

		}
		protected void btnMonthNext_Clicked(object sender, EventArgs e)
		{
			//ReSetButtonBackColor ();
			arrayIndexByMonth--;
			if (arrayIndexByMonth >= 0)
			{
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
			else
			{
				//DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
				arrayIndexByMonth++;
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
		}
		protected void btnMonthYearPrev_Clicked(object sender, EventArgs e)
		{
			arrayIndexByYearMonth++;
			//ReSetButtonBackColor ();
			if (arrayIndexByYearMonth < cnt)
			{
				arrayIndexByMonth = 0;
				//arrayIndexByYearMonth++;
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
			else
			{
				DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
				arrayIndexByMonth = 0;
				arrayIndexByYearMonth--;
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
		}
		protected void btnMonthYearNext_Clicked(object sender, EventArgs e)
		{
			arrayIndexByYearMonth--;
			if (arrayIndexByYearMonth >= 0)
			{
				//ReSetButtonBackColor ();

				arrayIndexByMonth = 0;
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
			else
			{
				DisplayAlert("Airport Visit", "No more Airport visits.", "Cancel");
				arrayIndexByYearMonth++;
				arrayIndexByMonth = 0;
				BindDataByYearMonth(arrayIndexByYearMonth, arrayIndexByMonth);
			}
		}




		private string LoadImageFromDisk(string fileName)
		{
			IPicture picture = DependencyService.Get<IPicture>();
			return picture.GetPicture(fileName);
		}

		protected void NavigateToAirportVisit(string airportId)
		{
			Navigation.PushModalAsync(new AirportVisit(airportId));
		}

		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
		protected void NavigatePicCommand(object sender, EventArgs e)
		{
			loader.IsVisible = true;
			AddRegistration addRegistration = new AddRegistration();
			Navigation.PushModalAsync(addRegistration);
			loader.IsVisible = false;
		}
		protected void StkMonth_Click(object sender, EventArgs e)
		{
			stkByMonth.IsVisible = true;
			stkByYear.IsVisible = false;
			imgMonth.IsVisible = true;
			imgYear.IsVisible = false;
		}

		protected void StkYear_Click(object sender, EventArgs e)
		{
			stkByMonth.IsVisible = false;
			stkByYear.IsVisible = true;
			imgMonth.IsVisible = false;
			imgYear.IsVisible = true;
		}
	}
}

