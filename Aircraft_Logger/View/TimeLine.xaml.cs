﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;

namespace Aircraft_Logger
{
	public partial class TimeLine : ContentPage
	{
		string glbAirport;
		string glbday;
		public TimeLine()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			loader.IsRunning = true;
			loader.IsVisible = true;

			Bind_ClickEvents();
			BindData();

			loader.IsRunning = false;
			loader.IsVisible = false;
		}


		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand(s, e);

			stkBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			var tapGestureRecognizerCalendar1 = new TapGestureRecognizer();
			tapGestureRecognizerCalendar1.Tapped += (s, e) => NavigateAddRegisCommand(s, e);
			stkDone.GestureRecognizers.Add(tapGestureRecognizerCalendar1);

			//lblDone.GestureRecognizers.Add (tapGestureRecognizerCalendar);

		}

		public string Truncate(string value, int maxChars)
		{
			if (!string.IsNullOrEmpty(value))
			{
				return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
			}
			else {
				return string.Empty;
			}

		}

		private void BindData()
		{
			TimelineModel objTimelineModel = new TimelineModel();
			List<YearMonthAirportId> result = objTimelineModel.GetYearMonth();

			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{
				item.lsttimeRegistration = objTimelineModel.GetRegistrationsByYearMonth(item.Year, item.Month, result);
				StackLayout stkItemCollections = new StackLayout()
				{

				};

				#region Year Month 
				StackLayout stkYearMonth = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblYear = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblYear.Text = item.strMonth + " " + item.Year.ToString();
				stkYearMonth.Children.Add(lblYear);
				#endregion


				#region Display Registrations

				foreach (var regItem in item.lsttimeRegistration)
				{

					StackLayout stkItems = new StackLayout()
					{
						HeightRequest = 70,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions=LayoutOptions.CenterAndExpand,
						Orientation = StackOrientation.Horizontal,
						Spacing = 0,
						Padding = new Thickness(5, 0, 5, 0)
					};

					StackLayout stkImage = new StackLayout()
					{
						HeightRequest = 70,
						WidthRequest = 100,
						VerticalOptions = LayoutOptions.CenterAndExpand,
						HorizontalOptions = LayoutOptions.Start,
						Spacing = 0,
						Padding = new Thickness(0, 0, 5, 0),

					};

					var tapGestureRecognizerAirportVisit = new TapGestureRecognizer();
					tapGestureRecognizerAirportVisit.Tapped += (s, e) =>
					{
						NavigateToAirportVisit(regItem.AirportId);
					};
					//stkImage.GestureRecognizers.Add (tapGestureRecognizerAirportVisit);
					stkItems.GestureRecognizers.Add(tapGestureRecognizerAirportVisit);
					StackLayout temp = new StackLayout()
					{
						HeightRequest = 70,
						WidthRequest = 100,
						VerticalOptions = LayoutOptions.CenterAndExpand,
						HorizontalOptions = LayoutOptions.Start,
						Spacing = 0,
						Padding = new Thickness(1, 1, 1, 1),
						BackgroundColor=Color.FromHex("#24AFE3")

					};

					CImage imgTmp = new CImage()
					{
						HeightRequest = 70,
						WidthRequest = 100,
						VerticalOptions=LayoutOptions.CenterAndExpand,
						Source = "tmpImg1.jpg",
						Aspect=Aspect.AspectFill
					};
					temp.Children.Add(imgTmp);
					stkImage.Children.Add(temp);
					StackLayout stkRegistrations = new StackLayout()
					{
						HeightRequest = 30,
						Padding = new Thickness(0, 0, 5, 0),
						VerticalOptions = LayoutOptions.Start,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Spacing = 0
					};

					StackLayout stkAirportName = new StackLayout()
					{
						Orientation = StackOrientation.Horizontal,
						Padding = new Thickness(0, 0, 5, 0),
						Spacing = 0
					};
					Label lblAirportName = new Label()
					{
						Text = Truncate(regItem.AirportName, 10),
						FontFamily = "Roboto Condensed",
						FontAttributes = FontAttributes.Bold,
						FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
						HorizontalOptions = LayoutOptions.FillAndExpand,
						//WidthRequest = 180,
						XAlign = TextAlignment.Start

					};
					Label lblDay = new Label()
					{
						
						Text = regItem.Day.ToString(),
						FontFamily = "Roboto Condensed",
						FontAttributes = FontAttributes.Bold,
						FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
						WidthRequest = 38,
						XAlign = TextAlignment.End,
						HorizontalOptions = LayoutOptions.CenterAndExpand
					};

					Grid grid = new Grid
					{
						VerticalOptions = LayoutOptions.FillAndExpand,
						RowDefinitions =
						{

					new RowDefinition { Height =  GridLength.Auto},

						},
						ColumnDefinitions =
						{
					new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
							new ColumnDefinition { Width = new GridLength(40,GridUnitType.Absolute) },
						}
					};
					grid.Children.Add(lblAirportName, 0, 0);
					grid.Children.Add(lblDay, 1, 0);
					//stkAirportName.Children.Add (lblAirportName);
					//stkAirportName.Children.Add (lblDay);
					if (lblAirportName.Text != "" && lblDay.Text != "")
					{
						stkRegistrations.Children.Add(grid);
					}
					glbday = lblDay.Text;
					glbAirport = lblAirportName.Text;
					//stkRegistrations.Children.Add(grid);
					StackLayout stkRegistrationDetail = new StackLayout()
					{
						Spacing = 0,
						VerticalOptions=LayoutOptions.StartAndExpand
						//WidthRequest = 240
					};
					IPicture picture = DependencyService.Get<IPicture>();
					foreach (var reg in regItem.lstRegistration)
					{
						if (reg.PhotoName != null)
						{
							imgTmp.Source = LoadImageFromDisk(reg.PhotoName);
						}

						StackLayout stkRegistrationItems = new StackLayout()
						{
							Orientation = StackOrientation.Horizontal,
							HorizontalOptions=LayoutOptions.CenterAndExpand,
							Spacing = 0
						};
						Label lblRegNo = new Label()
						{
							Text = reg.Registration,
							FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
							WidthRequest = 60
						};
						Label lblAircraft = new Label()
						{
							Text = Truncate(reg.AircraftName, 7),
							FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),

						};
						Label lblAirline = new Label()
						{
							Text = Truncate(reg.AirlineName, 7),
							FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),

						};
						Grid grid1 = new Grid
						{
							VerticalOptions = LayoutOptions.FillAndExpand,
							RowDefinitions =
						{

					new RowDefinition { Height = new GridLength(20, GridUnitType.Absolute)},

						},
							ColumnDefinitions =
						{
					new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
								new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
							new ColumnDefinition { Width = new GridLength(1520, GridUnitType.Star) },
						}
						};
						grid1.Children.Add(lblRegNo, 0, 0);
						grid1.Children.Add(lblAircraft, 1, 0);
						grid1.Children.Add(lblAirline, 2, 0);
						//stkRegistrationItems.Children.Add(lblRegNo);
						//stkRegistrationItems.Children.Add(lblAircraft);
						//stkRegistrationItems.Children.Add(lblAirline);

						stkRegistrationDetail.Children.Add(grid1);


					}
					stkRegistrations.Children.Add(stkRegistrationDetail);


					stkItems.Children.Add(stkImage);
					stkItems.Children.Add(stkRegistrations);

					stkItemCollections.Children.Add(stkYearMonth);



					StackLayout stkHorizontalLine = new StackLayout()
					{
						Spacing = 0,
						BackgroundColor = Color.Silver,
						HeightRequest = 1,
						HorizontalOptions = LayoutOptions.FillAndExpand,

					};

					if (glbday != "" && glbAirport != "")
					{
						stkItemCollections.Children.Add(stkItems);
						stkItemCollections.Children.Add(stkHorizontalLine);
					}


				}
				#endregion

				stkOuterLayout.Children.Add(stkItemCollections);
			}

			stkCollection.Children.Add(stkOuterLayout);


		}
		private string LoadImageFromDisk(string fileName)
		{

			IPicture picture = DependencyService.Get<IPicture>();

			return picture.GetPicture(fileName);

		}

		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
		protected void NavigateAddRegisCommand(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new AddRegistration());
		}
		protected void NavigateToAirportVisit(string airportId)
		{
			Navigation.PushModalAsync(new AirportVisit(airportId));
		}
	}
}

