﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace Aircraft_Logger
{
	public partial class PhotosList : ContentPage
	{
		List<Registrations> lsRegistrations = new List<Registrations>();
		AddPhoto addPhoto;
		string selectedaction;
		userdefault us;
		public PhotosList()
		{
			InitializeComponent();

			loader.IsRunning = true;
			loader.IsVisible = true;

			GetData();
			Bind_ClickEvents();

			loader.IsRunning = false;
			loader.IsVisible = false;
			NavigationPage.SetHasNavigationBar(this, false);
		}

		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand(s, e);
			var tapGestureRecognizerCalendar1 = new TapGestureRecognizer();
			tapGestureRecognizerCalendar1.Tapped += (s, e) => NavigatePicCommand(s, e);
			stkBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			stkDone.GestureRecognizers.Add(tapGestureRecognizerCalendar1);

			//tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			//tapGestureRecognizerCalendar.Tapped += (s,e)=>stkDetails_Click(s,e);
			//stkDetails.GestureRecognizers.Add (tapGestureRecognizerCalendar);
		}
		public void GetData()
		{
			BindGroupByPhotosData();
			lsRegistrations = new RegistrationsDatabase().GetRegistrationsByGroup();

		}

		private void BindGroupByPhotosData()
		{
			tblAirports resultair;
			try
			{
				PhotosDatabase objPhotosDatabaseModel = new PhotosDatabase();
				List<YearMonthAirportId> result = objPhotosDatabaseModel.GetYearMonth();

				StackLayout stkOuterLayout = new StackLayout()
				{
					Spacing = 5,
					Padding = new Thickness(0, 4, 0, 4)
				};
				foreach (var item in result)
				{
					item.lsttimeRegistration = objPhotosDatabaseModel.GetRegistrationsByYearMonth(item.Year, result);
					StackLayout stkItemCollections = new StackLayout()
					{
						Spacing = 5,

					};
					#region Year Month 
					StackLayout stkYearMonth = new StackLayout()
					{
						HorizontalOptions = LayoutOptions.FillAndExpand,
						BackgroundColor = Color.FromHex("#24AFE3"),
						HeightRequest = 20,
						Spacing = 0,

					};
					Label lblYear = new Label()
					{
						TextColor = Color.White,
						XAlign = TextAlignment.Center,
						YAlign = TextAlignment.Center,
						FontSize = 13,
						FontAttributes = FontAttributes.Bold
					};

					lblYear.Text = item.Year.ToString();
					stkYearMonth.Children.Add(lblYear);
					#endregion
					#region Display Registrations
					WrapLayout wrpImageCollection = new WrapLayout
					{
						Spacing = 5,

						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.Center,
						Padding = new Thickness(2, 2, 2, 2)
					};
					foreach (var regItem in item.lsttimeRegistration)
					{
						List<Registrations> temp = regItem.lstRegistration.GroupBy(p => p.VisitDate.Day).Select(g => g.First()).ToList();
						List<List<Registrations>> temp1 = regItem.lstRegistration.GroupBy(p => p.VisitDate.Day).Select(g => g.ToList()).ToList();
						foreach (var reg in temp)
						{
							
							StackLayout stkcombo = new StackLayout()
							{
								VerticalOptions = LayoutOptions.Center,
								HorizontalOptions=LayoutOptions.Center,
								HeightRequest=65,
								WidthRequest=95,
								Padding=new Thickness(1,1,2,1),
								BackgroundColor=Color.FromHex("#24AFE3")
									
							};
							RelativeLayout stkImage = new RelativeLayout()
							{
								VerticalOptions = LayoutOptions.Center,
								HorizontalOptions = LayoutOptions.Center,
								HeightRequest = 65,
								WidthRequest = 95,
								//Spacing = 0,
								//Padding = new Thickness(2, 0, 2, 2)
							};
							var tapGestureRecognizerCalendar = new TapGestureRecognizer();
							tapGestureRecognizerCalendar.Tapped += (s, e) =>
							{
								Navigation.PushModalAsync(new DayWisePhotoList(temp1, reg.VisitDate));
							};
							stkImage.GestureRecognizers.Add(tapGestureRecognizerCalendar);
							CImage imgTmp = new CImage()
							{
								VerticalOptions = LayoutOptions.Center,
								HorizontalOptions = LayoutOptions.Center,
								HeightRequest = 65,
								WidthRequest = 95,
								Source = "tmpImg1.jpg",
								Aspect=Aspect.AspectFit
							};
							AirportsDatabase ad = new AirportsDatabase();
							resultair = new tblAirports();
							resultair = ad.GetAirports(reg.AirportId);
							Label lblDate = new Label()
							{
								Text = reg.VisitDate.Day + "/" + reg.VisitDate.Month+"-"+ resultair.fldIATACode,
								FontSize = 9,
								TextColor = Color.White,
								FontAttributes=FontAttributes.Bold
							};

							imgTmp.Source = LoadImageFromDisk(reg.PhotoName);
							stkImage.Children.Add(imgTmp, Constraint.Constant(0),
															Constraint.Constant(0),
															Constraint.RelativeToParent((parent) => { return parent.Width; }),
															Constraint.RelativeToParent((parent) => { return parent.Height; }));
							stkImage.Children.Add(lblDate, Constraint.Constant(45),
															Constraint.Constant(50),
															Constraint.RelativeToParent((parent) => { return parent.Width; }),
															Constraint.RelativeToParent((parent) => { return parent.Height; }));
							stkcombo.Children.Add(stkImage);
							wrpImageCollection.Children.Add(stkcombo);

						}
					}
					BoxView box = new BoxView()
					{
						BackgroundColor = Color.Transparent,
						HeightRequest = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand
					};
					stkItemCollections.Children.Add(stkYearMonth);
					stkItemCollections.Children.Add(wrpImageCollection);
					stkItemCollections.Children.Add(box);

					#endregion

					stkOuterLayout.Children.Add(stkItemCollections);
				}

				stkDetails.Children.Add(stkOuterLayout);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private async void btnOptions_Clicked(object sender, EventArgs e)
		{
			var action = await DisplayActionSheet("Group", "Cancel", null, "Country", "Aircraft", "Airline", "Air force");
			switch (action)
			{
				case "Country":
					BindData(1);
					break;
				case "Aircraft":
					BindData(2);
					break;
				case "Airline":

					BindData(3);
					break;
				case "Air force":
					BindData(4);
					break;
			}
		}


		public void BindData(int groupByType)
		{

			if (groupByType == 1)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;

				BindGroupByCountryData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}
			if (groupByType == 2)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;

				BindGroupByAircraftData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}
			if (groupByType == 3)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;

				BindGroupByAirlineData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}
			if (groupByType == 4)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;

				BindGroupByAirforceData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}

		}



		private void BindGroupByCountryData()
		{
			var result = new RegistrationsDatabase().GroupByCountryData(lsRegistrations);
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};
				lblName.Text = item.CountryCode + " " + item.CountryName;
				if (item.CountryId != null && item.CountryName != null && item.CountryName != "null")
				{
					//stkName.Children.Add(lblName);


					#endregion
					#region Display Registrations
					WrapLayout wrpImageCollection = new WrapLayout
					{
						Spacing = 5,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center,
						VerticalOptions = LayoutOptions.Center,

					};
					//List<Registrations> temp = new List<Registrations>();
					//temp = item.lsRegistrationCountry;
					foreach (var regItem in item.lsRegistrationCountry)
					{
						if (!string.IsNullOrEmpty(regItem.PhotoName))
						{
							StackLayout stkImage = new StackLayout()
							{
								HeightRequest = 65,
								WidthRequest = 95,
								Spacing = 0,
								Padding = new Thickness(2, 4, 0, 4)
							};
							CImage imgTmp = new CImage()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								Source = "tmpImg1.jpg"

							};

							imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
							stkImage.Children.Add(imgTmp);

							wrpImageCollection.Children.Add(stkImage);
						}
					}

					#endregion
					if (wrpImageCollection.Children.Count > 0)
					{
						stkName.Children.Add(lblName);
						stkOuterLayout.Children.Add(stkName);
						stkOuterLayout.Children.Add(wrpImageCollection);
					}
				}
				stkDetails.Children.Clear();
				stkDetails.Children.Add(stkOuterLayout);
			}
		}
		public void BindGroupByAircraftData()
		{
			var result = new RegistrationsDatabase().GroupByAircraftData(lsRegistrations);
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0,
					//Padding = 5
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item.AircraftName;
				//stkName.Children.Add(lblName);
				if (item.AircraftId != null && item.AircraftName != null && item.AircraftName != "null" ) 				{

					#endregion
					#region Display Registrations
					WrapLayout wrpImageCollection = new WrapLayout
					{
						Spacing = 5,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center,
						VerticalOptions = LayoutOptions.Center,

					};
					//List<Registrations> temp = new List<Registrations>(); 					//temp = item.lsRegistrationAircraft;
					foreach (var regItem in item.lsRegistrationAircraft)
					{
						if (!string.IsNullOrEmpty(regItem.PhotoName))
						{
							StackLayout stkImage = new StackLayout()
							{
								HeightRequest = 65,
								WidthRequest = 95,
								Spacing = 0,
								Padding = new Thickness(2, 4, 0, 4)
							};
							CImage imgTmp = new CImage()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								Source = "tmpImg1.jpg"

							};

							imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
							stkImage.Children.Add(imgTmp);

							wrpImageCollection.Children.Add(stkImage);
						}
					}

					#endregion
					if (wrpImageCollection.Children.Count > 0) 					{ 						stkName.Children.Add(lblName); 						stkOuterLayout.Children.Add(stkName); 						stkOuterLayout.Children.Add(wrpImageCollection); 					}
				}
				stkDetails.Children.Clear();
				stkDetails.Children.Add(stkOuterLayout);
			}
		}

		public void BindGroupByAirlineData()
		{
			var result = new RegistrationsDatabase().GroupByAirelineData(lsRegistrations);
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item.AirlineName;
				//stkName.Children.Add(lblName);

				if (item.AirlineID != null && item.AirlineName!=null && item.AirlineName!="null") 				{
					#endregion
					#region Display Registrations
					WrapLayout wrpImageCollection = new WrapLayout
					{
						Spacing = 5,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center,
						VerticalOptions = LayoutOptions.Center,

					};

					foreach (var regItem in item.lsRegistrationAirline)
					{
						if (!string.IsNullOrEmpty(regItem.PhotoName))
						{
							StackLayout stkImage = new StackLayout()
							{
								HeightRequest = 65,
								WidthRequest = 95,
								Spacing = 0,
								Padding = new Thickness(2, 4, 0, 4)
							};
							CImage imgTmp = new CImage()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								Source = "tmpImg1.jpg"

							};

							imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
							stkImage.Children.Add(imgTmp);

							wrpImageCollection.Children.Add(stkImage);
						}
					}

					#endregion
					if (wrpImageCollection.Children.Count > 0) 					{ 						stkName.Children.Add(lblName); 						stkOuterLayout.Children.Add(stkName); 						stkOuterLayout.Children.Add(wrpImageCollection); 					}
				}
				stkDetails.Children.Clear();
				stkDetails.Children.Add(stkOuterLayout);
			}
		}
		public void BindGroupByAirforceData()
		{
			var result = new RegistrationsDatabase().GroupByAirforceData(lsRegistrations);
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item.AirForceName;
				if (item.AirForceID != null && item.AirForceName != null && item.AirForceName != "null") 				{

					//stkName.Children.Add(lblName);
					#endregion
					#region Display Registrations
					WrapLayout wrpImageCollection = new WrapLayout
					{
						Spacing = 5,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center,
						VerticalOptions = LayoutOptions.Center,

					};
					foreach (var regItem in item.lsRegistrationAirforce)
					{

						if (!string.IsNullOrEmpty(regItem.PhotoName))
						{
							StackLayout stkImage = new StackLayout()
							{
								HeightRequest = 65,
								WidthRequest = 95,
								Spacing = 0,
								Padding = new Thickness(2, 4, 0, 4)
							};
							CImage imgTmp = new CImage()
							{
								HeightRequest = 70,
								WidthRequest = 100,
								Source = "tmpImg1.jpg"

							};

							imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
							stkImage.Children.Add(imgTmp);

							wrpImageCollection.Children.Add(stkImage);
						}
					}

					#endregion

					if (wrpImageCollection.Children.Count > 0) 					{ 						stkName.Children.Add(lblName); 						stkOuterLayout.Children.Add(stkName); 						stkOuterLayout.Children.Add(wrpImageCollection); 					}

				}
				stkDetails.Children.Clear();
				stkDetails.Children.Add(stkOuterLayout);

			}
		}

		private string LoadImageFromDisk(string fileName)
		{

			IPicture picture = DependencyService.Get<IPicture>();

			return picture.GetPicture(fileName);

		}
		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
		protected void NavigatePicCommand(object sender, EventArgs e)
		{

			Stkoption.IsVisible = false;
			ActionSheet.IsVisible = true;
		}
		protected void Lasttaken(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			Stkoption.IsVisible = true;
			chooselastphoto();
		}

		protected void Picture(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			Stkoption.IsVisible = true;
			TakeNewPicture();
		}

		protected void Choosegallery(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			Stkoption.IsVisible = true;
			PicPicture();
		}
		async void PicPicture()
		{
			//PicCamera.ChoosePhoto ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.SelectPicture();
			if (cameraOps.ImageSource != null)
			{
				CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
			}
		}

		async void chooselastphoto()
		{
			try
			{
				us = DependencyService.Get<userdefault>();
				ImageSource sor = us.getlastimage();
				CameraViewModel cameraOps = new CameraViewModel();
				cameraOps.lastimg(sor);
				if (cameraOps.ImageSource != null)
				{
					CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(sor, 300, 200));
				}
			}
			catch (Exception ex)
			{

			}

		}

		private async void TakeNewPicture()
		{
			//PicCamera.TakePicture ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.TakePicture();
			if (cameraOps.ImageSource != null)
			{
				CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
			}
		}

		private void CallAddPhoto(ImageSource source)
		{
			try
			{
				addPhoto = null;
				addPhoto = new AddPhoto();
				addPhoto.imgDisplaySource = source;
				Navigation.PushModalAsync(addPhoto);
			}
			catch (Exception ex)
			{

			}

		}
		protected void Cancelgal(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
		}
		protected void stkDetails_Click(object sender, EventArgs e)
		{
			//			DayWisePhotoList photoList = new DayWisePhotoList ();
			//			Navigation.PushModalAsync (photoList);
		}
	}
}

