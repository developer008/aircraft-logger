﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Linq;

namespace Aircraft_Logger
{
	public partial class UserRegistration : ContentPage
	{
		List<tblCountries> countries = new List<tblCountries>();
		List<tblAirports> airports = new List<tblAirports>();

		public UserRegistration()
		{
			InitializeComponent();
			LoadEvent();

		}

		public void LoadEvent()
		{
			SetUI();

			BindCountry();
			BindAirport();
			BindPreference();
		}

		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			//var entry = (Entry)sender;
			//if (entry.Text.Length > 8)
			//{
			//	string entryText = entry.Text;
			//	entryText = entryText.Remove(entryText.Length - 1); 
			//	entry.Text = entryText;
			//}
		}

		public void SetUI()
		{

			FirstName.TextColor = Color.White;

			LastName.TextColor = Color.White;

			UserName.TextColor = Color.White;
			Email.TextColor = Color.White;
			Password.TextColor = Color.White;
			countryId.BackgroundColor = Color.Transparent;

			DefaultAirportId.BackgroundColor = Color.Transparent;
			Picker pic;
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				Navigation.PushModalAsync(new Login());
			};
			lblLogin.GestureRecognizers.Add(tapGestureRecognizer);
		}

		public void BindCountry()
		{
			CountriesDatabase countryDb = new CountriesDatabase();
			countries = countryDb.GetCountriesList();
			var sortedList = countries.OrderBy(si => si.fldCountryName).ToList();
			foreach (var country in sortedList)
			{
				countryId.Items.Add(country.fldCountryName);
			}
		}
		public void BindPreference()
		{
			Preference.Items.Add("Military");
			Preference.Items.Add("Civil");
			Preference.Items.Add("Civil & Military");
		}

		public void BindAirport()
		{
			AirportsDatabase airportDb = new AirportsDatabase();
			airports = airportDb.GetAirportsList();
			foreach (var airport in airports)
			{
				DefaultAirportId.Items.Add(airport.fldAirportName);
			}
		}

		public void OnbtnRegistration(object o, EventArgs e)
		{
			if (CommonHelper.IsConnected)
			{

				try
				{

					if (string.IsNullOrEmpty(FirstName.Text))
					{
						DisplayAlert("Validation", "Please enter first name", "Cancel");
						return;
					}
					if (string.IsNullOrEmpty(LastName.Text))
					{
						DisplayAlert("Validation", "Please enter last name", "Cancel");
						return;
					}
					if (string.IsNullOrEmpty(UserName.Text))
					{
						DisplayAlert("Validation", "Please enter user name", "Cancel");
						return;
					}
					if (countryId.SelectedIndex == -1)
					{
						DisplayAlert("Validation", "Please select country.", "Cancel");
						return;
					}
					if (string.IsNullOrEmpty(Email.Text))
					{
						DisplayAlert("Validation", "Please enter email", "Cancel");
						return;
					}
					if (string.IsNullOrEmpty(Password.Text))
					{
						DisplayAlert("Validation", "Please enter password name", "Cancel");
						return;
					}
					if (Password.Text.Length<8)
					{
						DisplayAlert("Validation", "Please enter password more then 8 character.", "Cancel");
						return;
					}
					if (DefaultAirportId.SelectedIndex == -1)
					{
						DisplayAlert("Validation", "Please select default airport.", "Cancel");
						return;
					}


					var selectedCountryId = countries.Where(x => x.fldCountryName == countryId.Items[countryId.SelectedIndex]).Select(x => x.fldCountryID).FirstOrDefault();
					var selectedAirportId = airports.Where(x => x.fldAirportName == DefaultAirportId.Items[DefaultAirportId.SelectedIndex]).Select(x => x.fldAirportID).FirstOrDefault();
					tblUser objTblUser = new tblUser();
					objTblUser.IsSync = true;
					objTblUser.fldUserId = Guid.NewGuid().ToString();
					objTblUser.fldFirstName = FirstName.Text.Trim();
					objTblUser.fldLastName = LastName.Text.Trim();
					objTblUser.fldUserName = UserName.Text.Trim();
					objTblUser.fldCountryID = selectedCountryId;
					objTblUser.fldEmail = Email.Text.Trim();
					objTblUser.fldPassword = Password.Text.Trim();
					objTblUser.fldDefaultAirportID = selectedAirportId;
					//objTblUser.fldAircraftPreferance = AircraftPreferance.Text;
					const string FMT = "yyyy-MM-dd HH:mm:ss tt";
					DateTime now1 = DateTime.UtcNow;
					string strDate = now1.ToString(FMT);

					objTblUser.fldDownloadedApp = strDate;
					objTblUser.fldEndofDataSub = DateTime.UtcNow.AddYears(1).ToString();
					objTblUser.fldFirstSync = true;
					UserDatabase userDb = new UserDatabase();
					//userDb.SaveUser (objTblUser);
					userDb.UploadRegistrationDataToServer(objTblUser);

					Navigation.PushModalAsync(new Login());
					//var message = new StartLongRunningTaskMessage ();
					//MessagingCenter.Send (message, "StartLongRunningTaskMessage");
				}
				catch (Exception ex)
				{

				}

			}
			else
			{
				DisplayAlert("Internet", "Please check your internet", "Cancel");
			}
		}

	}
}

