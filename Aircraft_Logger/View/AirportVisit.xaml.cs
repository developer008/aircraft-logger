﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace Aircraft_Logger
{

	public partial class AirportVisit : ContentPage
	{
		public string _airportId { get; set; }
		public int _currentDay { get; set; }
		public int _currentYear { get; set; }
		public int _currentMonth { get; set; }
		public int count_lscount;
		public int counter;
		WrapLayout wrpImageCollection = new WrapLayout
		{
			Spacing = 5,
			Orientation = StackOrientation.Horizontal,
			HorizontalOptions = LayoutOptions.Center,
			VerticalOptions = LayoutOptions.Center,

		};
		TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
		TapGestureRecognizer tapGestureRecognizer2 = new TapGestureRecognizer();
		List<Registrations> lsRegistrations = new List<Registrations>();
		List<YearMonthAirportId> lsYearMonth = new List<YearMonthAirportId>();
		List<Registrations> distinctPeople = new List<Registrations>();
		int arrayIndex = 0;

		public AirportVisit()
		{
			init();

		}
		public AirportVisit(string airportId)
		{
			_airportId = airportId;
			_currentDay = 9;
			_currentYear = 2016;
			_currentMonth = 1;
			init();
		}
		private void init()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			scrlDetails.IsVisible = false;
			tapGestureRecognizer.NumberOfTapsRequired = 1;
			tapGestureRecognizer.Tapped += (s, e) => Image_Tapped(s, e);
			tapGestureRecognizer2.NumberOfTapsRequired = 1;
			tapGestureRecognizer2.Tapped += (s, e) => Image_Tapped1(s, e);
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			loader.IsRunning = true;
			loader.IsVisible = true;

			Bind_ClickEvents();
			GetData();

			loader.IsRunning = false;
			loader.IsVisible = false;
		}
		public void GetData()
		{
			lsYearMonth = new AirportsDatabase().GetYearMonthByAirportId(_airportId);
			counter = lsYearMonth.Count;
			BindData(0);
		}
		public void BindData(int arrayIndex)
		{
			try
			{

				loader.IsVisible = true;
				loader.IsRunning = true;
				count_lscount = lsYearMonth.Count;
				lsRegistrations = new List<Registrations>();
				int rowNumber = 0;
				foreach (var regItem in lsYearMonth[arrayIndex].lstRegistration)
				{

					rowNumber++;
					lblAriportName.Text = regItem.AirportName;
					lblDate.Text = regItem.VisitDate.Day.ToString() + " " + regItem.VisitDate.ToString("MMMM") + " " + regItem.VisitDate.Year.ToString();
					regItem.RecordNumber = rowNumber;
					lsRegistrations.Add(regItem);

					arrayIndex++;

				}

				distinctPeople = lsRegistrations.GroupBy(p => p.Registration).Select(g => g.First()).ToList();
				rowNumber = 0;
				foreach (var i in distinctPeople)
				{
					rowNumber++;
					i.RecordNumber = rowNumber;
					if (i.New == "N")
					{
						i.New = "false";
					}
					else { i.New = "true"; }
				}
				//	var regi = lsRegistrations.Distinct(p => p.);
				LstRegistrationList.ItemsSource = null;
				LstRegistrationList.ItemsSource = distinctPeople;
				BindBottomImages(lsRegistrations);

				loader.IsVisible = false;
				loader.IsRunning = false;
			}
			catch (Exception ex)
			{
				loader.IsVisible = false;
				loader.IsRunning = false;
				DisplayAlert("n", "You have reached maximum photo limit.", "Ok");
				arrayIndex = 0;
				BindData(arrayIndex);
			}
		}
		public void BindBottomImages(List<Registrations> ObjlsRegistrations)
		{
			#region Bind Photos
			picup.IsVisible = false;
			stkimg1.IsVisible = true;
			stkimg2.IsVisible = true;
			wrpImageCollection.Children.Clear();
			stkBottomImages.Children.Clear();
			stkBottomImages.Children.Clear();
			img1.Source = null;
			img2.Source = null;
			int counts = 1;

			foreach (var regItem in ObjlsRegistrations)
			{
				if (!string.IsNullOrEmpty(regItem.PhotoName))
				{
					picup.IsVisible = true;
					//counts ++;
					if (counts == 1)
					{
						img1.Source = LoadImageFromDisk(regItem.PhotoName); ;
						counts++;
						img1.GestureRecognizers.Add(tapGestureRecognizer2);
					}
					else if (counts == 2)
					{
						img2.Source = LoadImageFromDisk(regItem.PhotoName); ;
						counts++;
						img2.GestureRecognizers.Add(tapGestureRecognizer2);
					}
					else
					{
						StackLayout stkImage = new StackLayout()
						{
							HeightRequest = 65,
							WidthRequest = 95,
							Spacing = 0,
							Padding = new Thickness(1, 0.5, 1, 0.5),
							BackgroundColor = Color.FromHex("24AFE3")
						};

						CImage imgTmp = new CImage()
						{
							HeightRequest = 64,
							WidthRequest = 94,

						};

						imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
						stkImage.Children.Add(imgTmp);
						stkImage.GestureRecognizers.Add(tapGestureRecognizer);
						wrpImageCollection.Children.Add(stkImage);
						counts++;
					}

				}
			}

			stkBottomImages.Children.Add(wrpImageCollection);
			if (img2.Source == null)
			{
				stkimg2.IsVisible = false;
			}
			if (img1.Source == null)
			{
				stkimg2.IsVisible = false;
			}
			#endregion
		}
		private void Image_Tapped(object sender, EventArgs e)
		{
			//CImage image = sender as CImage;
			var children = wrpImageCollection.Children;
			foreach (var item in children)
			{
				StackLayout stkTemp = (StackLayout)item;
				stkTemp.BackgroundColor = Color.Transparent;
				var tmpimageControl = stkTemp.Children[0];
				CImage tmpimage = (CImage)tmpimageControl;
				tmpimage.IsPrimary = false;

			}
			StackLayout stkImage = sender as StackLayout;
			var imageControl = stkImage.Children[0];
			CImage image = (CImage)imageControl;
			image.IsPrimary = true;
			stkImage.BackgroundColor = Color.FromRgb(36, 175, 227);
			SelectPic.IsVisible = true;
			selctedimg.Source = image.Source;
			//DisplayAlert("Test", image.ImageID, "Ok");
		}
		private void Image_Tapped1(object sender, EventArgs e)
		{
			Image image = sender as Image;



			SelectPic.IsVisible = true;
			selctedimg.Source = image.Source;
			//DisplayAlert("Test", image.ImageID, "Ok");
		}
		protected void btnClosePhoto_Clicked(object sender, EventArgs e)
		{
			SelectPic.IsVisible = false;
		}
		private string LoadImageFromDisk(string fileName)
		{

			IPicture picture = DependencyService.Get<IPicture>();

			return picture.GetPicture(fileName);

		}
		protected void OnChangeEvent(object sender, EventArgs e)
		{

		}
		protected void btnNext_Clicked(object sender, EventArgs e)
		{

			arrayIndex++;
			if (arrayIndex <= count_lscount - 1)
			{
				BindData(arrayIndex);
				stkDetails.IsVisible = false;
				scrlDetails.IsVisible = false;
				//picup.IsVisible = true;
				listdata.IsVisible = true;
				stkBottomImages.IsVisible = true;
			}
			else
			{
				DisplayAlert("Visit", "You have reached to limit.", "Ok");
				arrayIndex--;
				BindData(arrayIndex);
				stkDetails.IsVisible = false;
				scrlDetails.IsVisible = false;
				//picup.IsVisible = true;
				listdata.IsVisible = true;
				stkBottomImages.IsVisible = true;
			}
		}
		protected void btnPrev_Clicked(object sender, EventArgs e)
		{
			arrayIndex--;
			if (arrayIndex >= 0)
			{
				BindData(arrayIndex);
				stkDetails.IsVisible = false;
				scrlDetails.IsVisible = false;
				//picup.IsVisible = true;
				listdata.IsVisible = true;
				stkBottomImages.IsVisible = true;
			}
			else
			{
				DisplayAlert("Visit", "You have reached to limit.", "Ok");
				arrayIndex++;
				BindData(arrayIndex);
				stkDetails.IsVisible = false;
				scrlDetails.IsVisible = false;
				//picup.IsVisible = true;
				listdata.IsVisible = true;
				stkBottomImages.IsVisible = true;
			}
		}
		protected void LstRegistrationList_Tapped(object o, ItemTappedEventArgs e)
		{
			/*
			var Registration = e.Item as Registrations;
		
			Navigation.PushModalAsync (new AircraftDetails (Registration.RegistrationID));
			*/
		}
		private async void btnOptions_Clicked(object sender, EventArgs e)
		{
			var action = await DisplayActionSheet("Group", "Cancel", null, "Registration", "Aircraft", "Unit", "Airline");
			switch (action)
			{
				case "Registration":
					BindData1(1);
					break;
				case "Aircraft":
					BindData1(2);
					break;
				case "Unit":
					BindData1(3);
					break;
				case "Airline":
					BindData1(4);
					break;
			}
		}

		public void BindData1(int groupByType)
		{

			if (groupByType == 1)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;
				listdata.IsVisible = false;
				BindGroupByRegistrationData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}
			if (groupByType == 2)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;

				BindGroupByAircraftData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}
			if (groupByType == 3)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;

				BindGroupByUnitData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}
			if (groupByType == 4)
			{
				loader.IsRunning = true;
				loader.IsVisible = true;

				BindGroupByAirlineData();

				loader.IsRunning = false;
				loader.IsVisible = false;
			}

		}
		void registrationselected(object s, SelectedItemChangedEventArgs e)
		{
			loader.IsVisible = true;
			loader.IsRunning = true;
			if (e.SelectedItem == null)
			{
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
			}
			var temp = e.SelectedItem as Registrations;
			Navigation.PushModalAsync(new AircraftDetails(temp.RegistrationID));
			loader.IsVisible = false;
		}
		void BindGroupByAirlineData()
		{
			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			picup.IsVisible = false;
			listdata.IsVisible = false;
			stkBottomImages.IsVisible = false;
			var groupedCustomerList = lsRegistrations.GroupBy(u => u.AirlineName).Select(grp => grp.ToList()).ToList();
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in groupedCustomerList)
			{

				#region Airline Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item[0].AirlineName;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Airline
				WrapLayout wrpImageCollection = new WrapLayout
				{
					//Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					//VerticalOptions = LayoutOptions.FillAndExpand,

				};
				//foreach (var regItem in item)
				//{
				//	if (!string.IsNullOrEmpty(regItem.AirlineName))
				//	{
				StackLayout stkImage1 = new StackLayout()
				{
					//HeightRequest = 20,
					Spacing = 0,
					Padding = new Thickness(2, 0, 0, 0)
				};
				var registrationslist = new ListView
				{
					Header="",
					Footer="",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = item,
					RowHeight = 13,
					SeparatorColor = Color.FromHex("#DDD"),
				};
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);
				//imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
				stkImage1.Children.Add(registrationslist);

				wrpImageCollection.Children.Add(stkImage1);
				//	}
				//}

				#endregion
				#region pics
				StackLayout stkImage2 = new StackLayout()
				{
					Orientation = StackOrientation.Vertical,
					Padding = new Thickness(5,0,5,0)
					//Spacing = 0,

				};

				foreach (var regItem in item)
				{
					if (!string.IsNullOrEmpty(regItem.PhotoName))
					{


						StackLayout stkImage = new StackLayout()
						{
							HeightRequest = 65,
							WidthRequest = 95,
							Spacing = 0,
							Padding = new Thickness(1, 0.5, 1, 0.5),
							BackgroundColor = Color.FromHex("24AFE3")
						};

						CImage imgTmp = new CImage()
						{
							HeightRequest = 64,
							WidthRequest = 94,

						};

						imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
						stkImage.Children.Add(imgTmp);
						stkImage.GestureRecognizers.Add(tapGestureRecognizer);
						//wrpImageCollection.Children.Add(stkImage);
						stkImage2.Children.Add(stkImage);

					}

				}

				#endregion
				wrpImageCollection.Children.Add(stkImage2);
				stkOuterLayout.Children.Add(stkName);
				stkOuterLayout.Children.Add(wrpImageCollection);
			}


			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			stkDetails.Children.Clear();
			stkDetails.Children.Add(stkOuterLayout);
		}


		void BindGroupByUnitData()
		{
			
			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			picup.IsVisible = false;
			listdata.IsVisible = false;
			stkBottomImages.IsVisible = false;
			var groupedCustomerList = lsRegistrations.GroupBy(u => u.UnitName1).Select(grp => grp.ToList()).ToList();
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in groupedCustomerList)
			{

				#region Unit Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item[0].Registration;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Unit
				WrapLayout wrpImageCollection = new WrapLayout
				{
					//Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					//VerticalOptions = LayoutOptions.FillAndExpand,

				};
				//foreach (var regItem in item)
				//{
				//	if (!string.IsNullOrEmpty(regItem.UnitName1))
				//	{
				StackLayout stkImage1 = new StackLayout()
				{
					//HeightRequest = 20,
					Spacing = 0,
					Padding = new Thickness(2, 0, 0, 0)
				};
				var registrationslist = new ListView
				{
					Header="",
					Footer="",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = item,
					RowHeight = 13,
					SeparatorColor = Color.FromHex("#DDD"),
				};
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);
				//imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
				stkImage1.Children.Add(registrationslist);

				wrpImageCollection.Children.Add(stkImage1);
				//	}
				//}

				#endregion

				#region pics
				StackLayout stkImage2 = new StackLayout()
				{
					Orientation = StackOrientation.Vertical,
					//Spacing = 0,

				};
				foreach (var regItem in item)
				{
					if (!string.IsNullOrEmpty(regItem.PhotoName))
					{


						StackLayout stkImage = new StackLayout()
						{
							HeightRequest = 65,
							WidthRequest = 95,
							Spacing = 0,
							Padding = new Thickness(1, 0.5, 1, 0.5),
							BackgroundColor = Color.FromHex("24AFE3")
						};

						CImage imgTmp = new CImage()
						{
							HeightRequest = 64,
							WidthRequest = 94,

						};

						imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
						stkImage.Children.Add(imgTmp);
						stkImage.GestureRecognizers.Add(tapGestureRecognizer);
						//wrpImageCollection.Children.Add(stkImage);
						stkImage2.Children.Add(stkImage);

					}

				}

				#endregion
				wrpImageCollection.Children.Add(stkImage2);
				stkOuterLayout.Children.Add(stkName);
				stkOuterLayout.Children.Add(wrpImageCollection);

			}
			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			stkDetails.Children.Clear();
			stkDetails.Children.Add(stkOuterLayout);
		}

		void BindGroupByAircraftData()
		{
			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			picup.IsVisible = false;
			listdata.IsVisible = false;
			stkBottomImages.IsVisible = false;
			var groupedCustomerList = lsRegistrations.GroupBy(u => u.AircraftName).Select(grp => grp.ToList()).ToList();
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in groupedCustomerList)
			{

				#region Aircraft Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item[0].AircraftName;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Aircrafts
				WrapLayout wrpImageCollection = new WrapLayout
				{
					//Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					//VerticalOptions = LayoutOptions.FillAndExpand,

				};
				//foreach (var regItem in item)
				//{
				//	if (!string.IsNullOrEmpty(regItem.AircraftName))
				//	{
				StackLayout stkImage1 = new StackLayout()
				{
					//HeightRequest = 20,
					Spacing = 0,
					Padding = new Thickness(2, 0, 0, 0)
				};
				var registrationslist = new ListView
				{
					Header="",
					Footer="",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = item,
					RowHeight = 13,
					SeparatorColor = Color.FromHex("#DDD"),
				};
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);
				//imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
				stkImage1.Children.Add(registrationslist);

				wrpImageCollection.Children.Add(stkImage1);
				//	}
				//}

				#endregion

				#region pics
				StackLayout stkImage2 = new StackLayout()
				{
					Orientation = StackOrientation.Vertical,
					//Spacing = 0,

				};
				foreach (var regItem in item)
				{
					
					if (!string.IsNullOrEmpty(regItem.PhotoName))
					{


						StackLayout stkImage = new StackLayout()
						{
							HeightRequest = 65,
							WidthRequest = 95,
							Spacing = 0,
							Padding = new Thickness(1, 0.5, 1, 0.5),
							BackgroundColor = Color.FromHex("24AFE3")
						};

						CImage imgTmp = new CImage()
						{
							HeightRequest = 64,
							WidthRequest = 94,

						};

						imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
						stkImage.Children.Add(imgTmp);
						stkImage.GestureRecognizers.Add(tapGestureRecognizer);
						//wrpImageCollection.Children.Add(stkImage);
						stkImage2.Children.Add(stkImage);
					}

				}

				#endregion
				wrpImageCollection.Children.Add(stkImage2);
				stkOuterLayout.Children.Add(stkName);
				stkOuterLayout.Children.Add(wrpImageCollection);

			}
			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			stkDetails.Children.Clear();
			stkDetails.Children.Add(stkOuterLayout);
		}

		void BindGroupByRegistrationData()
		{
			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			picup.IsVisible = false;
			listdata.IsVisible = false;
			stkBottomImages.IsVisible = false;
			var groupedCustomerList = lsRegistrations.GroupBy(u => u.Registration).Select(grp => grp.ToList()).ToList();
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in groupedCustomerList)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0,
					Padding = new Thickness(0, 5, 0, 0)
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					FontSize = 13,
					FontAttributes = FontAttributes.Bold,

				};

				lblName.Text = item[0].Registration;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Registrations
				WrapLayout wrpImageCollection = new WrapLayout
				{
					//Orientation = StackOrientation.Horizontal,

					//BackgroundColor = Color.Transparent,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};
				StackLayout stkImage1 = new StackLayout()
				{
					//HeightRequest = 40,
					Spacing = 0,
					Padding = new Thickness(2, 0, 0, 0),
				};
			var registrationslist = new ListView
				{
					Header = "",
					Footer="",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = item,
					SeparatorColor = Color.FromHex("#DDD"),
					RowHeight = 13,

				};
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);
				stkImage1.Children.Add(registrationslist);
				wrpImageCollection.Children.Add(stkImage1);

				#endregion
				#region pics
				StackLayout stkImage2 = new StackLayout()
				{
					Orientation = StackOrientation.Vertical,
					//Spacing = 0,

				};
				foreach (var regItem in item)
				{
					if (!string.IsNullOrEmpty(regItem.PhotoName))
					{


						StackLayout stkImage = new StackLayout()
						{
							HeightRequest = 65,
							WidthRequest = 95,
							Spacing = 0,
							Padding = new Thickness(1, 0.5, 1, 0.5),
							BackgroundColor = Color.FromHex("24AFE3"),
							HorizontalOptions = LayoutOptions.CenterAndExpand
						};

						CImage imgTmp = new CImage()
						{
							HeightRequest = 64,
							WidthRequest = 94,
						};

						imgTmp.Source = LoadImageFromDisk(regItem.PhotoName);
						stkImage.Children.Add(imgTmp);
						stkImage.GestureRecognizers.Add(tapGestureRecognizer);
						//wrpImageCollection.Children.Add(stkImage);
						stkImage2.Children.Add(stkImage);
					}

				}

				#endregion
				wrpImageCollection.Children.Add(stkImage2);
				stkOuterLayout.Children.Add(stkName);
				stkOuterLayout.Children.Add(wrpImageCollection);

			}
			scrlDetails.IsVisible = true;
			stkDetails.IsVisible = true;
			stkDetails.Children.Clear();
			stkDetails.Children.Add(stkOuterLayout);
		}


		protected void LstRegistrationList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{

		}
		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand(s, e);
			stkBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			stkDone.GestureRecognizers.Add(tapGestureRecognizerCalendar);
		}
		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
	}
}

