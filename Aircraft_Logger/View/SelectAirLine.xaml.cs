﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Aircraft_Logger
{
	public partial class SelectAirLine : ContentPage
	{
		public SelectAirLine ()
		{
			InitializeComponent ();
			SetVisibility (true);
			Bind_ClickEvents ();
		}

		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s,e)=>NavigateBackCommand(s,e);
			lblBackMenu.GestureRecognizers.Add (tapGestureRecognizerCalendar);
			lblDone.GestureRecognizers.Add (tapGestureRecognizerCalendar);
		}

		protected void NavigateBackCommand(object sender , EventArgs e)
		{
			Navigation.PopModalAsync (true);
		}

		protected void OnChangeEvent(object sender, EventArgs e)
		{
			SetVisibility (sagIsCivil.SelectedValue);
		}

		private void SetVisibility(bool IsSearch)
		{
			stkAddNew.IsVisible = !IsSearch;
			stkSearch.IsVisible = IsSearch;

		}
	}
}

