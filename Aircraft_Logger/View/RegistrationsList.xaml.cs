﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;

namespace Aircraft_Logger
{

	public partial class RegistrationsList : ContentPage
	{
		List<Registrations> lsRegistrations = new List<Registrations>();

		public RegistrationsList()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			loader.IsRunning = true;
			loader.IsVisible = true;

			Bind_ClickEvents();
			GetData();
			BindData(1);

			loader.IsRunning = false;
			loader.IsVisible = false;

		}

		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand(s, e);
			stkBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			var tapGestureRecognizerCalendar1 = new TapGestureRecognizer();
			tapGestureRecognizerCalendar1.Tapped += (s, e) => NavigateAddRegisCommand(s, e);
			stkDone.GestureRecognizers.Add(tapGestureRecognizerCalendar1);

			var tapGestureRecognizerCountry = new TapGestureRecognizer();
			tapGestureRecognizerCountry.Tapped += (s, e) => lblCountry_Clicked(s, e);
			stk_Country.GestureRecognizers.Add(tapGestureRecognizerCountry);

			var tapGestureRecognizerAircraft = new TapGestureRecognizer();
			tapGestureRecognizerAircraft.Tapped += (s, e) => lblAircraft_Clicked(s, e);
			stk_Aircraft.GestureRecognizers.Add(tapGestureRecognizerAircraft);

			var tapGestureRecognizerAirline = new TapGestureRecognizer();
			tapGestureRecognizerAirline.Tapped += (s, e) => lblAirline_Clicked(s, e);
			stk_Airline.GestureRecognizers.Add(tapGestureRecognizerAirline);

			var tapGestureRecognizerAirforce = new TapGestureRecognizer();
			tapGestureRecognizerAirforce.Tapped += (s, e) => lblAirforce_Clicked(s, e);
			stk_Airforce.GestureRecognizers.Add(tapGestureRecognizerAirforce);
		}

		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
		protected void NavigateAddRegisCommand(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new AddRegistration());
		}

		public void lblCountry_Clicked(object sender, EventArgs e)
		{
			stkContry.IsVisible = true;
			stkAirCraft.IsVisible = false;
			stkAirline.IsVisible = false;
			stkAirForce.IsVisible = false;

			imgCountry.IsVisible = true;
			imgAircraft.IsVisible = false;
			imgAirline.IsVisible = false;
			imgAirforce.IsVisible = false;

			BindData(1);
		}
		public void lblAircraft_Clicked(object sender, EventArgs e)
		{
			stkContry.IsVisible = false;
			stkAirCraft.IsVisible = true;
			stkAirline.IsVisible = false;
			stkAirForce.IsVisible = false;

			imgCountry.IsVisible = false;
			imgAircraft.IsVisible = true;
			imgAirline.IsVisible = false;
			imgAirforce.IsVisible = false;

			BindData(2);

		}
		public void lblAirline_Clicked(object sender, EventArgs e)
		{
			stkContry.IsVisible = false;
			stkAirCraft.IsVisible = false;
			stkAirline.IsVisible = true;
			stkAirForce.IsVisible = false;

			imgCountry.IsVisible = false;
			imgAircraft.IsVisible = false;
			imgAirline.IsVisible = true;
			imgAirforce.IsVisible = false;

			BindData(3);

		}
		public void lblAirforce_Clicked(object sender, EventArgs e)
		{
			stkContry.IsVisible = false;
			stkAirCraft.IsVisible = false;
			stkAirline.IsVisible = false;
			stkAirForce.IsVisible = true;

			imgCountry.IsVisible = false;
			imgAircraft.IsVisible = false;
			imgAirline.IsVisible = false;
			imgAirforce.IsVisible = true;

			BindData(4);

		}
		public void GetData()
		{
			lsRegistrations = new RegistrationsDatabase().GetRegistrationsByGroup();
		}
		public void BindData(int groupByType)
		{
			if (groupByType == 1)
			{
				stkContry.Children.Clear();
				BindGroupByCountryData();
			}
			if (groupByType == 2)
			{

				stkAirCraft.Children.Clear();

				BindGroupByAircraftData();
			}
			if (groupByType == 3)
			{
				stkAirline.Children.Clear();
				BindGroupByAirlineData();
			}
			if (groupByType == 4)
			{
				stkAirForce.Children.Clear();
				BindGroupByAirforceData();
			}
		}

		public void BindGroupByCountryData()
		{
			ListView registrationslist;
			var result = lsRegistrations.GroupBy(u => u.CountryName).Select(grp => grp.ToList()).Distinct().ToList();
			//var result = new RegistrationsDatabase ().GroupByCountryData (lsRegistrations);
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					//	FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item[0].CountryCode + "-" + item[0].CountryName;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Registrations
				StackLayout stkRegistrationDetail = new StackLayout()
				{
					Spacing = 0,
					Padding = new Thickness(10, 0, 10, 0),
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					//HeightRequest = 150
				};
				var result1 = item.GroupBy(u => u.RegistrationID).Select(grp => grp.FirstOrDefault()).Distinct().ToList();
				registrationslist = new ListView
				{
					RowHeight = 15,
					Footer="",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = result1,
					SeparatorColor = Color.FromHex("#DDD"),
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);
				StackLayout temp = new StackLayout() { HorizontalOptions = LayoutOptions.CenterAndExpand };
				temp.Children.Add(registrationslist);
				stkRegistrationDetail.Children.Add(temp);
				#endregion
				if (lblName.Text != null && lblName.Text != "" && lblName.Text != "-")
				{
					stkOuterLayout.Children.Add(stkName);
					stkOuterLayout.Children.Add(stkRegistrationDetail);
				}

			}

			stkContry.Children.Add(stkOuterLayout);

		}

		void registrationselected(object s, SelectedItemChangedEventArgs e)
		{
			loader.IsVisible = true;
			if (e.SelectedItem == null)
			{
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
			}
			var temp = e.SelectedItem as Registrations;
			Navigation.PushModalAsync(new AircraftDetails(temp.RegistrationID));
			loader.IsVisible = false;
		}

		public void BindGroupByAircraftData()
		{
			var result = lsRegistrations.GroupBy(u => u.AircraftName).Select(grp => grp.ToList()).ToList();
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					//FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item[0].AircraftName;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Registrations

				StackLayout stkRegistrationDetail = new StackLayout()
				{
					Spacing = 0,
					Padding = new Thickness(10, 0, 10, 0),
					//HeightRequest = 200

				};
				var result1 = item.GroupBy(u => u.RegistrationID).Select(grp => grp.FirstOrDefault()).Distinct().ToList();
				var registrationslist = new ListView
				{
					RowHeight = 15,
					Footer = "",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = result1,
					SeparatorColor = Color.FromHex("#DDD"),
				};
				stkRegistrationDetail.Children.Add(registrationslist);
				#endregion
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);


				if (lblName.Text != null && lblName.Text!="null" && lblName.Text != "" && lblName.Text != "-")
				{
					stkOuterLayout.Children.Add(stkName);
					stkOuterLayout.Children.Add(stkRegistrationDetail);
				}

			}
			stkAirCraft.Children.Add(stkOuterLayout);
		}
		public void BindGroupByAirlineData()
		{
			var result = lsRegistrations.GroupBy(u => u.AirlineName).Select(grp => grp.ToList()).ToList();
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					//FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item[0].AirlineName;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Registrations
				StackLayout stkRegistrationDetail = new StackLayout()
				{
					Spacing = 0,
					Padding = new Thickness(10, 0, 10, 0),
					//HeightRequest = 200

				};
				var result1 = item.GroupBy(u => u.RegistrationID).Select(grp => grp.FirstOrDefault()).Distinct().ToList();
				var registrationslist = new ListView
				{
					RowHeight = 15,
					Footer = "",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = result1,
					SeparatorColor = Color.FromHex("#DDD"),
				};
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);
				stkRegistrationDetail.Children.Add(registrationslist);
				#endregion

				if (lblName.Text != null && lblName.Text != "" && lblName.Text != "-")
				{
					stkOuterLayout.Children.Add(stkName);
					stkOuterLayout.Children.Add(stkRegistrationDetail);
				}

			}
			stkAirline.Children.Add(stkOuterLayout);
		}
		public void BindGroupByAirforceData()
		{
			var result = lsRegistrations.GroupBy(u => u.AirForceName).Select(grp => grp.ToList()).ToList();
			StackLayout stkOuterLayout = new StackLayout();
			foreach (var item in result)
			{

				#region Country Title
				StackLayout stkName = new StackLayout()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					BackgroundColor = Color.FromHex("#24AFE3"),
					HeightRequest = 20,
					Spacing = 0
				};
				Label lblName = new Label()
				{
					TextColor = Color.White,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center,
					//FontSize = 13,
					FontAttributes = FontAttributes.Bold
				};

				lblName.Text = item[0].AirForceName;
				stkName.Children.Add(lblName);
				#endregion
				#region Display Registrations
				StackLayout stkRegistrationDetail = new StackLayout()
				{
					Spacing = 0,
					Padding = new Thickness(10, 0, 10, 0),
					//HeightRequest = 200

				};
				var result1 = item.GroupBy(u => u.RegistrationID).Select(grp => grp.FirstOrDefault()).Distinct().ToList();
				var registrationslist = new ListView
				{

					RowHeight = 15,
					Footer = "",
					ItemTemplate = new DataTemplate(typeof(RegistrationDetailViewCell)),
					ItemsSource = result1,
					SeparatorColor = Color.FromHex("#DDD"),
				};
				registrationslist.ItemSelected += (s, e) => registrationselected(s, e);
				stkRegistrationDetail.Children.Add(registrationslist);
				#endregion

				if (lblName.Text != null && lblName.Text != "" && lblName.Text != "-")
				{
					stkOuterLayout.Children.Add(stkName);
					stkOuterLayout.Children.Add(stkRegistrationDetail);
				}
			}
			stkAirForce.Children.Add(stkOuterLayout);
		}
	}
}
