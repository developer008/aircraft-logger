﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace Aircraft_Logger
{
	public partial class MainMenu : ContentPage
	{
		//		IPicture PicCamera;
		MainMenuViewModel viewModel;
		AddPhoto addPhoto;
		//string selectedaction;
		userdefault us;
		public MainMenu()
		{
			//			SetSyncSetting ();

			InitializeComponent();
			viewModel = new MainMenuViewModel();
			NavigationPage.SetHasNavigationBar(this, false);
			BindClickEvent();
			LoadTotals();

			//			if (!App.DbSync.CheckAuthentication ())
			//				App.DbSync.SyncDopbox ();

			//CommonHelper.SetSyncSetting ();

		}



		public void btnChooseClick(object o, EventArgs e)
		{
			//			if (CommonHelper.IsConnected) {
			//				CommonHelper.IsConnected = false;	
			//
			//			} else {
			//				CommonHelper.IsConnected = true;	
			//			}
			//App.DbSync.ChooseDropBoxPhoto ();
			//DependencyService.Get<IDropboxSync> ().ChooseDropBoxPhoto ("/Photos/");
			//			var result =  App.DbSync.ChooseDropBoxPhoto ("/Photos/");
			//			if (result !=null) {
			//				DisplayAlert ("Path", result.ToString(), "Can");
			//			}
		}

		private void BindClickEvent()
		{
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) => imgCamera_Click(s, e);
			imgCamera.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) => imgAdd_Click(s, e);
			imgAdd.GestureRecognizers.Add(tapGestureRecognizer);

			tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) => lblNavRegistration_Click(s, e);
			//stkNavRegistration.GestureRecognizers.Add (tapGestureRecognizer);
			imgRegistration.GestureRecognizers.Add(tapGestureRecognizer);
			lblregistration.GestureRecognizers.Add(tapGestureRecognizer);
			lblTotalRegCount.GestureRecognizers.Add(tapGestureRecognizer);
			lblNavRegistration.GestureRecognizers.Add(tapGestureRecognizer);

			var tapGestureRecognizerPhotos = new TapGestureRecognizer();
			tapGestureRecognizerPhotos.Tapped += (s, e) => lblNavPhotos_Click(s, e);
			//stkNavPhotos.GestureRecognizers.Add (tapGestureRecognizerPhotos);
			imgPhotos.GestureRecognizers.Add(tapGestureRecognizerPhotos);
			lblPhoto.GestureRecognizers.Add(tapGestureRecognizerPhotos);
			lblTotalPhotos.GestureRecognizers.Add(tapGestureRecognizerPhotos);
			lblNavPhotos.GestureRecognizers.Add(tapGestureRecognizerPhotos);

			var tapGestureRecognizerTimeLine = new TapGestureRecognizer();
			tapGestureRecognizerTimeLine.Tapped += (s, e) => lblNavTimeLine_Click(s, e);
			//stkNavTimeLine.GestureRecognizers.Add (tapGestureRecognizerTimeLine);
			imgTimeline.GestureRecognizers.Add(tapGestureRecognizerTimeLine);
			lblTimeline.GestureRecognizers.Add(tapGestureRecognizerTimeLine);
			lblNavTimeLine.GestureRecognizers.Add(tapGestureRecognizerTimeLine);
			lblTImeLineCount.GestureRecognizers.Add(tapGestureRecognizerTimeLine);

			var tapGestureRecognizerTags = new TapGestureRecognizer();
			tapGestureRecognizerTags.Tapped += (s, e) => lblNavTags_Click(s, e);
			//stkNavTags.GestureRecognizers.Add (tapGestureRecognizerTags);
			lbltags.GestureRecognizers.Add(tapGestureRecognizerTags);
			imgtags.GestureRecognizers.Add(tapGestureRecognizerTags);
			lblNavTags.GestureRecognizers.Add(tapGestureRecognizerTags);
			lbltotalTags.GestureRecognizers.Add(tapGestureRecognizerTags);


			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblNavCalendar_Click(s, e);
			//stkNavCalendar.GestureRecognizers.Add (tapGestureRecognizerCalendar);
			imgCalender.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			lblCalender.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			lblNavCalendar.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			lbltotalcalender.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			var tapGestureRecognizerStatistic = new TapGestureRecognizer();
			tapGestureRecognizerStatistic.Tapped += (s, e) => lblNavStatistic_Click(s, e);
			//stkNavStatistic.GestureRecognizers.Add (tapGestureRecognizerStatistic);
			imgstatistics.GestureRecognizers.Add(tapGestureRecognizerStatistic);
			lblstatistic.GestureRecognizers.Add(tapGestureRecognizerStatistic);
			lblNavStatistic.GestureRecognizers.Add(tapGestureRecognizerStatistic);
			lblcountstatistic.GestureRecognizers.Add(tapGestureRecognizerStatistic);

			var tapGestureRecognizerStatisticSetting = new TapGestureRecognizer();
			tapGestureRecognizerStatisticSetting.Tapped += (s, e) => lblNavSetting_Click(s, e);
			//stkNavSetting.GestureRecognizers.Add (tapGestureRecognizerStatisticSetting);
			imgsetting.GestureRecognizers.Add(tapGestureRecognizerStatisticSetting);
			lblsettings.GestureRecognizers.Add(tapGestureRecognizerStatisticSetting);
			lblNavSetting.GestureRecognizers.Add(tapGestureRecognizerStatisticSetting);
			lblcountsettings.GestureRecognizers.Add(tapGestureRecognizerStatisticSetting);
		}

		private async void imgCamera_Click(object sender, EventArgs e)
		{

			//var action = await DisplayActionSheet(null ,null,null, "Last Photo Taken", "Take Photo", "Choose From Library");
			//selectedaction = action;
			ActionSheet.IsVisible = true;
			//call_library();
		}
		void call_library( )
		{ 

					

		}

		private void LoadTotals()
		{
			try
			{
				TotalRegistrationCountsMainMenu counts = viewModel.GetTotalRegistrationCountsMainMenu();
				//var differanceInDay = (DateTime.Now.Date - Convert.ToDateTime(counts.DownloadAppDate).Date).TotalDays;
				DateTime today = DateTime.Now.Date;
				//DateTime old = Convert.ToDateTime(counts.DownloadAppDate);
				DateTime oDate = DateTime.ParseExact(counts.DownloadAppDate,"yyyy-MM-dd HH:mm:ss tt",null);
				var differanceInDay = (today - oDate.Date).TotalDays;
				lblTotalReg.Text = counts.TotalRegistrations.ToString();
				lblTotalDays.Text = differanceInDay.ToString();
				lblTotalThisWeek.Text = counts.ThisWeek.ToString();
				lblTotalToday.Text = counts.TotalToday.ToString();
				lblTImeLineCount.Text = counts.TotalVisit.ToString();
				lblTotalRegCount.Text = counts.TotalRegistrations.ToString();
				lblTotalPhotos.Text = counts.TotalPhotos.ToString();
			}
			catch (Exception ex)
			{

			}
		}

		 async void PicPicture()
		{
			loader.IsVisible = true;
			//PicCamera.ChoosePhoto ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.SelectPicture();
			if (cameraOps.ImageSource != null)
			{
				CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
			}
			loader.IsVisible = false;
		}

		async void chooselastphoto()
		{
			loader.IsVisible = true;
			try
			{
				us = DependencyService.Get<userdefault>();
				ImageSource sor = us.getlastimage();
				CameraViewModel cameraOps = new CameraViewModel();
				cameraOps.lastimg(sor);
				if (cameraOps.ImageSource != null)
				{
					CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(sor, 300, 200));
				}
			}
			catch (Exception ex)
			{

			}
			loader.IsVisible = false;
		}

		private async void TakeNewPicture()
		{
			loader.IsVisible = true;
			//PicCamera.TakePicture ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.TakePicture();
			if (cameraOps.ImageSource != null)
			{
				CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
			}
			loader.IsVisible = false;
		}

		private void CallAddPhoto(ImageSource source)
		{
			try
			{
				addPhoto = null;
				addPhoto = new AddPhoto();
				addPhoto.imgDisplaySource = source;
				Navigation.PushModalAsync(addPhoto);
			}
			catch (Exception ex)
			{

			}

		}
		protected void Lasttaken(object sender,EventArgs e) 
		{ 
		ActionSheet.IsVisible = false;
			chooselastphoto();
		}

		protected void Picture(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			TakeNewPicture();
		}

		protected void Choosegallery(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
			PicPicture();
		}

		protected void Cancelgal(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
		}
		private void imgAdd_Click(object sender, EventArgs e)
		{
			AddRegistration addRegistration = new AddRegistration();
			Navigation.PushModalAsync(addRegistration);
		}

		private void lblNavRegistration_Click(object sender, EventArgs e)
		{
			RegistrationsList registration = new RegistrationsList();
			Navigation.PushModalAsync(registration);
		}

		private void lblNavPhotos_Click(object sender, EventArgs e)
		{
			PhotosList photos = new PhotosList();
			Navigation.PushModalAsync(photos);
		}

		private void lblNavTimeLine_Click(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new TimeLine());
		}

		private void lblNavTags_Click(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new AirportVisit());

		}

		private void lblNavCalendar_Click(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new Calendar());
		}

		private void lblNavStatistic_Click(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new Statistics());
		}

		private void lblNavSetting_Click(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new Settings());
		}


	}
}

