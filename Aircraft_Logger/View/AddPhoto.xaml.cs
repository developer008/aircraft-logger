﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using Xamarin.Forms.Maps;
using System.Globalization;
using System.Linq;

namespace Aircraft_Logger
{
	public partial class AddPhoto : ContentPage,IDisposable
	{
		ObservableCollection<AddRegistrationSearchList> Searchlist = new ObservableCollection<AddRegistrationSearchList> ();
		bool isLoading;
		int lastCount = 0;

		ObservableCollection<Countries> CountriesList = new ObservableCollection<Countries> ();
		bool isCountriesLoading;
		int lastCountriesCount = 0;

		ObservableCollection<Airports> AirportsList = new ObservableCollection<Airports> ();
		bool isAirportsLoading;
		int lastAirportsCount = 0;

		public ImageSource imgDisplaySource;
		public System.IO.Stream imgsource;
		ILocationPicker LPicker;
		Position position = new Position ();
		IPicture _Picture;
		APICivilSearch selectedCivil = new APICivilSearch();
		APImilitarydbSearch selectedMilitary = new APImilitarydbSearch();
		APICountrydbSearch selectedCountry = new APICountrydbSearch();
		userdefault user;
		string imageName;
		AddRegistrationViewModel viewModel;
		bool isSearchableRecord = false;
		enum LocationSelectionType
		{
			CurrentLocation,
			SelectedAirport
		}

		tblAirports SelectedAirport;
		LocationSelectionType LocationType;
		tblVisits currentVisit;
		Registrations CurrentRegistration;
		tblManufacturers manufacturer;
		tblAircraft aircraft;
		tblAirlines airlines;
		tblUnits units;

		public AddPhoto ()
		{
			InitializeComponent ();
			viewModel = new AddRegistrationViewModel ();

			CurrentRegistration = new Registrations();
			_Picture = DependencyService.Get<IPicture> ();
			_Picture.SavePictureComplete += Image_SaveComplete;
			txtRegister.Focus ();
			NavigationPage.SetHasNavigationBar (this, false);
			Bind_ClickEvents ();
			SetUserDefaultLocation ();
			BindRegistration ();
			LoadGrid ();
		}
		public void SetUserDefaultLocation ()
		{
			AirportsDatabase ad = new AirportsDatabase ();
			var result = ad.GetAirports (Account.DefaultAirportId);
			if (result == null)
				return;
			LocationType = LocationSelectionType.SelectedAirport;
			SelectedAirport = new tblAirports () {
				fldAirportID = result.fldAirportID,
				fldAirportName = result.fldAirportName,
				fldCountryID = result.fldCountryID,
				fldGPSCoordinates = result.fldGPSCoordinates
			};
			position = new Position (0, 0);
			lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString ("dd MMM yyyy");
			if (SelectedAirport != null) {

				//				Searchlist.Clear ();
				//				lastCount = 0;
				//				LoadGrid();
			}

		}

		public async void Image_SaveComplete (bool IsSAveComplete, string ImageName)
		{
			App.DbSync.UploadFile (imageName + ".jpg", "AircraftLogger", "/Photos/");

			//string fileName = await DependencyService.Get<IPicture> ().GetPictureFromDisk (imageName);
			//imgDisplay.Source = ImageSource.FromFile (fileName);
		}

		protected override void OnAppearing ()
		{
			if (imgDisplaySource != null)
				imgDisplay.Source = imgDisplaySource;
		}

		private void Bind_ClickEvents ()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand (s, e);
			stkBack.GestureRecognizers.Add (tapGestureRecognizerCalendar);
			stkDone.GestureRecognizers.Add (tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s, e) => imgfilterDate_Clicked (s, e);
			imgfilterDate.GestureRecognizers.Add (tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s, e) => imgPicLocation_Click (s, e);
			imgPicLocation.GestureRecognizers.Add (tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblSelectAirportDone_Click (s, e);
			lblSelectAirportDone.GestureRecognizers.Add (tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblSelectAirportBackMenu_Click (s, e);
			lblSelectAirportBackMenu.GestureRecognizers.Add (tapGestureRecognizerCalendar);
		
		tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAZ1_Clicked(s, e);
			stk_AZ1.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lbl09_Clicked(s, e);
			stk_09.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAZ2_Clicked(s, e);
			stk_AZ2.GestureRecognizers.Add(tapGestureRecognizerCalendar);
		
		}


		public void lblAZ1_Clicked(object sender, EventArgs e)
		{
			imgAZ1.IsVisible = true;
			img09.IsVisible = false;
			imgAZ2.IsVisible = false;
			ReloadGrid();

		}

		public void lbl09_Clicked(object sender, EventArgs e)
		{
			imgAZ1.IsVisible = false;
			img09.IsVisible = true;
			imgAZ2.IsVisible = false;
			ReloadGrid();
		}

		public void lblAZ2_Clicked(object sender, EventArgs e)
		{
			imgAZ1.IsVisible = false;
			img09.IsVisible = false;
			imgAZ2.IsVisible = true;
			ReloadGrid();
		}


		protected void LstRegistrationList_Tapped (object o, ItemTappedEventArgs e)
		{
			loader.IsVisible = true;
			var Registration = e.Item as AddRegistrationSearchList;
			//DisplayAlert ("Choosen!",Registration.Registration + " Was Selected!","OK");
			Navigation.PushModalAsync (new AircraftDetails (Registration.RegistrationID));
			loader.IsVisible = false;
		}
		private void imgfilterDate_Clicked (object o, EventArgs e)
		{
			datePick.Focus ();
		}

		protected async void btnSearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				isSearchableRecord = true;
				SearchAPI api = new SearchAPI();
				if (sagIsCivil.SelectedValue)
				{
					string airportId = string.Empty;
					if (SelectedAirport.fldAirportID != null)
					{
						airportId = SelectedAirport.fldAirportID;
					}

					List<tblRegistrations> isExist = viewModel.GetIfDuplicate(Convert.ToString(txtRegister.Text), airportId);
					if (isExist.Count > 0)
					{
						//var answer = await DisplayAlert("Question?", "This registration is already in the database", "Continue", "Cancel");
						//if (answer)
						//{
							//stkCivilSearch.IsVisible = true;
							loader.IsVisible = true;
							LstSearchCivilDB.ItemsSource = await api.SearchCivilDB(txtRegister.Text);
							loader.IsVisible = false;
						stkCivilSearch.IsVisible = true;
						//}
						//else {

						//}
					}
					else {
						//stkCivilSearch.IsVisible = true;
						loader.IsVisible = true;
						LstSearchCivilDB.ItemsSource = await api.SearchCivilDB(txtRegister.Text);

						loader.IsVisible = false;
						stkCivilSearch.IsVisible = true;
					}
				}
				else {


					loader.IsVisible = true;


					user = DependencyService.Get<userdefault>();
					APICountrydbSearch ap = new APICountrydbSearch();

					string lscountry = user.getUserlastcountry();
					string myconid = user.getmycountry();
					ap.Country = lscountry;

					//loader.IsVisible = true;
					List<APICountrydbSearch> listcountry = new List<APICountrydbSearch>();

					List<APICountrydbSearch> temp = new List<APICountrydbSearch>();

					string myconname;
					temp = api.SearchCountryDb().Result;
					foreach (var b in temp)
					{
						if (b.CountryID == myconid)
						{
							listcountry.Add(b);
						}
						if (b.Country == lscountry)
						{
							ap.Code = b.Code;
						}
					}
					listcountry.Add(ap);
					temp.Sort((x, y) => x.Country.CompareTo(y.Country));
					foreach (var a in temp)
					{
						if (a.Country != ap.Country)
						{
							if (a.CountryID != myconid)
							{
								if (a.Code.Length == 2)
								{
									listcountry.Add(a);
								}
							}
						}


					}


					LstSearchCountryDB.ItemsSource =listcountry;
					loader.IsVisible = false;
					stkCountrySearch.IsVisible = true;
				}

			
				ReloadGrid();
			}
			catch (Exception ex)
			{
				int i = 0;
			}
		}
		protected void LstSearchCivilDB_Tap(object o, ItemTappedEventArgs e)
		{
			loader.IsVisible = true;
			var civil = e.Item as APICivilSearch;
			selectedCivil = civil;
			txtRegister.Text = civil.Registration;
			CurrentRegistration.Cn = civil.Cnr;
			CurrentRegistration.Registration = civil.Registration;
			CurrentRegistration.AircraftName = civil.FullType;
			CurrentRegistration.AirlineName = civil.Owner;
			CurrentRegistration.Remarks = civil.Remarks;
			//lblSelectedRegistration.Text = civil.Owner + " " + civil.FullType; 
			lblSelectedRegistration.Text = civil.FullType + "," + civil.Owner;

			savelog();

			loader.IsVisible = false;
			stkCivilSearch.IsVisible = false;
		}
		protected void btnCloseCivilSearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkCivilSearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}
		protected void LstSearchMilitaryDB_Tap(object o, ItemTappedEventArgs e)
		{
			//			var military = e.Item as APIMilitaryDatabaseSearch;
			loader.IsVisible = true;
			var military = e.Item as APImilitarydbSearch;
			selectedMilitary = military;
			//txtRegister.Text = military.;
			//			lblSelectedRegistration.Text = military.CountryCode + " " + military.AirForceName; 
			txtRegister.Text = military.Serial;
			lblSelectedRegistration.Text = military.Type + " " + military.Unit;

			savelog();
			stkMilitarySearch.IsVisible = false;
			loader.IsVisible = false;
		}


		protected async void LstSearchCountryDB_Tap(object o, ItemTappedEventArgs e)
		{
			try
			{
				loader.IsVisible = true;
				SearchAPI api = new SearchAPI();
				var country = e.Item as APICountrydbSearch;
				selectedCountry = country;


				LstSearchMilitaryDB.ItemsSource = await api.SearchMilitaryDbByCountry(country.Code, txtRegister.Text);
				stkCountrySearch.IsVisible = false;
				stkMilitarySearch.IsVisible = true;
				loader.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}
		protected void btnCloseMilitarySearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkMilitarySearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}
		protected void btnCloseCountrySearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkCountrySearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}


		private void btnLogs_Clicked (object o, EventArgs e)
		{
			loader.IsVisible = true;
			try {
				

				//if (!IsRegistrationExists ())
				//{
				savelog();
				//}
			} catch (Exception ex) {
				
			}
			finally{
				loader.IsVisible = false;
			}

		}
		private bool ValidateLogs()
		{
			try
			{
				if (txtRegister.Text.Trim() == string.Empty)
				{
					DisplayAlert("Warning", "Please Enter Registration", "Ok");
					txtRegister.Focus();
					loader.IsVisible = false;
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			finally
			{
			}
		}
		private void ClearLog()
		{
			try
			{
				txtRegister.Text = string.Empty;
				lblSelectedRegistration.Text = string.Empty;
				selectedCivil = new APICivilSearch();
				selectedMilitary = new APImilitarydbSearch();
				ReloadGrid();

			}
			catch (Exception ex)
			{

			}
		}
		async void savelog()
		{
			if (!ValidateLogs())
				return;
			imageName = Guid.NewGuid().ToString();
			await _Picture.SavePictureToDisk(imgDisplay.Source, imageName, 300, 200);
			App.DbSync.UploadFile(imageName + ".jpg", "AircraftLogger", "/Photos/");
		if (!IsVisitExists())
				SaveVisits();
			SaveRegistration();
			ReloadGrid();

			//imgDisplay.Source = "";
			lblSelectedRegistration.Text = string.Empty;
			txtRegister.Text = string.Empty;
			ClearLog();
		}

		private async void imgPicLocation_Click (object sender, EventArgs e)
		{
			AddPhoto addPhoto = new AddPhoto ();
			var action = await DisplayActionSheet (null, "Cancel", null, "Current Location", "Select Airport");
			switch (action) {
			case "Current Location":
				LPicker = DependencyService.Get<ILocationPicker> ();
				LPicker.ChangeLoation += LPicker_OnChangeLoation;
				LPicker.GetCurrentLocation ();
				SelectedAirport = null;
				LocationType = LocationSelectionType.CurrentLocation;
				lblLocation.Text = datePick.Date.ToString ("dd MMM yyyy");
				break;
			case "Select Airport":
				ShowAirportPopup ();
				break;
			}
		}

		public void LPicker_OnChangeLoation (Position _position)
		{
			position = _position;
			LocationType = LocationSelectionType.SelectedAirport;
			lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString ("dd MMM yyyy");
			LPicker.StopListening ();
			LPicker = null;
		}

		protected void NavigateBackCommand (object sender, EventArgs e)
		{
			Navigation.PopModalAsync (true);
			this.Dispose ();
		}

		protected void OnChangeEvent (object sender, EventArgs e)
		{

		}

		public void ReloadGrid ()
		{
			try {
				Searchlist.Clear ();
				lastCount = 0;
				LoadGrid ();
			} catch (Exception ex) {

			}
		}

		protected void LstRegistrationList_Appearing (object Sender, ItemVisibilityEventArgs e)
		{
			if (isLoading || Searchlist.Count == 0)
				return;
			if (e.Item == Searchlist [Searchlist.Count - 1]) {
				LoadGrid ();
			}
		}

		private void LoadGrid ()
		{
			loader.IsVisible = true;
			try {

				string orderby = string.Empty;
				if (imgAZ1.IsVisible)
					orderby = "Registration";
				else if (img09.IsVisible)
					orderby = "Aircraft";
				else if (imgAZ2.IsVisible)
				{
					orderby = sagIsCivil.SelectedValue ? "Airline" : "Units";

				}
				string airportId = string.Empty;

				if (SelectedAirport != null)
				{
					airportId = SelectedAirport.fldAirportID;
				}
				var _searchList = viewModel.SearchRegistrationDetailList (20, Searchlist.Count, datePick.Date, "", 'C', "",SelectedAirport.fldAirportID);
				var _searchList1 = viewModel.SearchRegistrationDetailList(20, Searchlist.Count, datePick.Date, "", 'M', "", SelectedAirport.fldAirportID);
				foreach (var item in _searchList1)
				{
					_searchList.Add(item);
				}
				foreach (var item in _searchList)
				{
					if (item.New == "Y")
					{
						item.IsVisibleRed = true;
					}
					else {
						item.IsVisibleRed = false;
					}

				}
				if (imgAZ1.IsVisible)
					_searchList = _searchList.OrderBy(x => x.Registration).ToList();
				else if (img09.IsVisible)
					_searchList = _searchList.OrderBy(x => x.AircraftName).ToList();
				else if (imgAZ2.IsVisible)
				{
					string a = sagIsCivil.SelectedValue ? "Airline" : "Units";
					if (a == "Airline")
					{
						_searchList = _searchList.OrderBy(x => x.AirlineName).ToList();
					}
					else
					{
						_searchList = _searchList.OrderBy(x => x.UnitName1).ToList();
					}

				}
				for (int i = 0; i <= _searchList.Count - 1; i++) 
				{
					_searchList [i].RecordNumber = lastCount + i + 1;
					Searchlist.Add (_searchList [i]);
				}
				lastCount = lastCount + _searchList.Count;
			} catch (Exception ex) {
				
			}
			finally {
				loader.IsVisible = false;
			}
		}

		private void BindRegistration ()
		{
			//RegistrationsDatabase Data = new RegistrationsDatabase ();
			LstRegistrationList.ItemsSource = Searchlist;
		}

		protected void datePick_DateSelected (object sender, DateChangedEventArgs e)
		{
			try {
				ReloadGrid ();
				if (LocationType == LocationSelectionType.CurrentLocation)
					lblLocation.Text = datePick.Date.ToString ("dd MMM yyyy");
				else
					lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString ("dd MMM yyyy");
			} catch (Exception ex) {

			}
		}

		private bool IsRegistrationExists ()
		{
			try {
				return viewModel.GetIfDuplicate (Convert.ToString (txtRegister.Text));
			} 
			catch (Exception ex) {
				return false;
			}
		}

		private bool IsVisitExists ()
		{
			try {
				tblVisits Visits = viewModel.GetIfVisitExists (SelectedAirport.fldAirportID, position.ToString (), datePick.Date);
				if (Visits != null)
					currentVisit = Visits;
				return Visits != null;
			} catch (Exception ex) {
				return false;
			}
		}

		private void SaveVisits ()
		{
			tblVisits Visits = new tblVisits ();
			try {
				Visits.fldAirportID = SelectedAirport.fldAirportID;
				Visits.fldLocationGPS = position.ToString ();
				//DateTime date = DateTime.ParseExact ( datePick.Date.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
				//Visits.fldVisitDate = datePick.Date.ToString("dd/MM/yyyy");
				//Visits.fldVisitDate = date.ToString();
				Visits.fldVisitDate = datePick.Date.DefaultDateFormate();
				Visits.fldAirportVisitID = viewModel.SaveVisits (Visits);
				currentVisit = Visits;
			} catch (Exception ex) {

			} finally {
				Visits = null;
			}
		}

		private void SaveRegistration ()
		{
			///////////////////////
			loader.IsVisible = true;
			string tmpMfName = "TestM52";
			string aircraftId = string.Empty;
			string manufacturerId = string.Empty;
			string airlineId = string.Empty;
			string unitId = string.Empty;
			manufacturer = new tblManufacturers();
			var manufacturerResult = GetManufacturer(tmpMfName);
			if (manufacturerResult == null)
			{
				manufacturer.fldManufacturerName = tmpMfName;
				manufacturerId = AddUpdateManufacturer();
			}
			else {
				manufacturer = manufacturerResult;
				manufacturerId = manufacturer.fldManufacturerID;
			}


			tblRegistrations Registrations = new tblRegistrations ();
			tblPhotos Photo = new tblPhotos ();
			try {
				Registrations.fldRegistration = txtRegister.Text;
				Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
				Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
				Registrations.fldNew = "Y";



				if (sagIsCivil.SelectedValue)
				{
					aircraft = new tblAircraft();
					var aircraftResult = GetAircraft(selectedCivil.FullType);
					if (aircraftResult == null)
					{
						aircraft.fldAircraftName = selectedCivil.FullType;
						aircraft.fldManufacturerID = manufacturerId;
						aircraftId = AddUpdateAircraft();
					}
					else {
						aircraft = aircraftResult;
						aircraftId = aircraft.fldAircraftID;
					}

					airlines = new tblAirlines();
					var airlinesResult = GetAirline(selectedCivil.Owner);
					if (airlinesResult == null)
					{
						airlines.fldAirlineName = selectedCivil.Owner;
						airlineId = AddUpdateAirline();
					}
					else {
						airlines = airlinesResult;
						airlineId = airlines.fldAirlineID;
					}

					Registrations.fldRegistration = txtRegister.Text;
					Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
					Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
					Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";
					Registrations.fldCn = selectedCivil.Cnr;
					Registrations.fldAircraftID = aircraftId;
					Registrations.fldAirlineID = airlineId;
					Registrations.fldRemarks = selectedCivil.Remarks;

				}
				else
				{
					aircraft = new tblAircraft();
					var aircraftResult = GetAircraft(selectedMilitary.Type);
					if (aircraftResult == null)
					{
						aircraft.fldAircraftName = selectedMilitary.Type;
						aircraft.fldManufacturerID = manufacturerId;
						aircraftId = AddUpdateAircraft();
					}
					else {
						aircraft = aircraftResult;
						aircraftId = aircraft.fldAircraftID;
					}

					units = new tblUnits();
					var unitResult = GetUnit(selectedMilitary.Unit);
					if (unitResult == null)
					{
						units.fldUnitName1 = selectedMilitary.Unit;
						unitId = AddUpdateUnit();
					}
					else {
						units = unitResult;
						unitId = units.fldUnitID;
					}

					Registrations.fldRegistration = txtRegister.Text;
					Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
					Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
					Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";
					Registrations.fldCn = selectedMilitary.CN;
					Registrations.fldCode = selectedMilitary.Code;
					Registrations.fldAircraftID = aircraftId;
					Registrations.fldUnitID = unitId;
					//Registrations.fldAirlineID = airlineId;
					Registrations.fldRemarks = selectedMilitary.Comment;
				}





				Registrations.fldRegistrationID = viewModel.SaveRegistration(Registrations);
				Photo.fldPhotoID="0";
				Photo.fldPhotoFilename = imageName;
				Photo.fldRegistrationID = Registrations.fldRegistrationID;
				viewModel.SavePhoto(Photo);
			} catch (Exception ex) {
				Registrations = null;
			}
			finally {
				loader.IsVisible = false;
			}
		}


		private string AddUpdateManufacturer()
		{
			try
			{
				return viewModel.AddUpdateManufacturer(manufacturer);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		private tblManufacturers GetManufacturer(string manufacturerName)
		{
			try
			{
				return viewModel.GetManufacturer(manufacturerName);
			}
			catch (Exception ex)
			{
				return manufacturer;
			}
		}

		private tblAircraft GetAircraft(string aircraftName)
		{
			try
			{
				return viewModel.GetAircraft(aircraftName);
			}
			catch (Exception ex)
			{
				return aircraft;
			}
		}

		private tblAirlines GetAirline(string airlineName)
		{
			try
			{
				return viewModel.GetAirline(airlineName);
			}
			catch (Exception ex)
			{
				return airlines;
			}
		}

		private tblUnits GetUnit(string unitName)
		{
			try
			{
				return viewModel.GetUnit(unitName);
			}
			catch (Exception ex)
			{
				return units;
			}
		}

		private string AddUpdateAircraft()
		{
			try
			{
				return viewModel.AddUpdateAircraft(aircraft);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		private string AddUpdateUnit()
		{
			try
			{
				return viewModel.AddUpdateUnit(units);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		private string AddUpdateAirline()
		{
			try
			{
				return viewModel.AddUpdateAirline(airlines);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		#region Airport

		private void ShowAirportPopup ()
		{
			try {
				stkSelectAirport.IsVisible = true;
				SetAirportVisibility (sagSelectAirport.SelectedValue);
				InitAirportPopup ();
			} catch (Exception ex) {

			}
			finally{
				
			}
		}

		private void SetAirportVisibility (bool IsSearch)
		{
			stkSelectAirportAddNew.IsVisible = !IsSearch;
			stkSelectAirportSearch.IsVisible = IsSearch;
		}

		protected void lblSelectAirportDone_Click (object sender, EventArgs e)
		{
			try {
				if (sagSelectAirport.SelectedValue) {
					if (!SelectAirport ())
						return;
				} else {
					if (!SaveAirport ())
						return;
				}
			} catch (Exception ex) {
				return;
			}
			CloseAirportPopup ();
		}

		protected void lblSelectAirportBackMenu_Click (object sender, EventArgs e)
		{
			try {
				CloseAirportPopup ();
			} catch (Exception ex) {

			}
		}

		protected void sagSelectAirport_OnChange (object sender, EventArgs e)
		{
			SetAirportVisibility (sagSelectAirport.SelectedValue);
		}

		private void InitAirportPopup ()
		{
			try {
				FIllAirportList ();
				LoadCountry ("");
				LstSearchAirportCountry.ItemsSource = CountriesList;
			} catch (Exception ex) {

			}
		}

		private void CloseAirportPopup ()
		{
			try {
				stkSelectAirport.IsVisible = false;
				ClearAirportPopup ();
			} catch (Exception ex) {

			}
		}

		private void ClearAirportPopup ()
		{
			try {
				LstSearchAirportCountry.ItemsSource = null;
				LstSearchAirportist.ItemsSource = null;
				UnLoadCountry ();
				UnLoadAirport ();
				txtAirportName.Text = string.Empty;
				txtAirportCity.Text = string.Empty;
				txtAirportIataCode.Text = string.Empty;
				txtAirportIcaoCode.Text = string.Empty;
				txtAirportGpsCordinates.Text = string.Empty;
			} catch (Exception ex) {
			}
		}

		private void FIllAirportList ()
		{
			try {
				LoadAirports (txtAirportSearch.Text);
				LstSearchAirportist.ItemsSource = AirportsList;
			} catch (Exception ex) {
			}
		}

		private void LoadAirports (string AirportName)
		{
			loader.IsVisible = true;
			try {
				var _searchList = viewModel.SearchAirports (20, lastAirportsCount, AirportName);
				var uni = _searchList.Distinct().ToList();
				for (int i = 0; i <= uni.Count - 1; i++) {
					AirportsList.Add (uni [i]);
					//if (i == _searchList.Count - 1)
					//	lastAirportsCount = _searchList [i].AirportID;
				}

			} catch (Exception ex) {
			}
			finally{
				loader.IsVisible = false;
			}
		}

		protected void LstSearchAirportList_Appearing (object Sender, ItemVisibilityEventArgs e)
		{
			try {
				if (isAirportsLoading || AirportsList.Count == 0)
					return;

				if (e.Item == AirportsList [AirportsList.Count - 1]) {
					LoadAirports (txtAirportSearch.Text);
				}
			} catch (Exception ex) {
			}
		}
		protected void LstSearchAirportList_selected(object Sender,SelectedItemChangedEventArgs e)
		{
			try
			{
				if (isAirportsLoading || AirportsList.Count == 0)
					return;

				if (e.SelectedItem !=null)
				{
					var airport = e.SelectedItem as Airports;
					txtAirportSearch.Text = airport.AirportName;
					LoadAirports(txtAirportSearch.Text);

				}
			}
			catch (Exception ex)
			{
			}
		}
		private void LstSearchAirportCountry_Tap (object sender, ItemTappedEventArgs e)
		{
			try {
				var countrie = e.Item as Countries;
				txtAirportCountry.Text = countrie.CountryName;
			} catch (Exception ex) {
			}
		}

		protected void LstSearchAirportCountry_Appearing (object Sender, ItemVisibilityEventArgs e)
		{
			try {
				if (isCountriesLoading || CountriesList.Count == 0)
					return;

				if (e.Item == CountriesList [CountriesList.Count - 1]) {
					LoadCountry ("");
				}
			} catch (Exception ex) {
			}
		}

		protected void txtSearchAircraft_Change (object sender, TextChangedEventArgs e)
		{
			UnLoadAirport ();
			LoadAirports (txtAirportSearch.Text);
		}

		private void UnLoadAirport ()
		{
			try {
				AirportsList.Clear ();
				lastAirportsCount = 0;
			} catch (Exception ex) {
			}
		}

		private bool SaveAirport ()
		{
			loader.IsVisible = true;
			if (!ValidateAirportForSave ())
				return false;
			tblAirports Airport = new tblAirports ();
			try {
				Airport.fldAirportName = txtAirportName.Text;
				Airport.fldAirportCity = txtAirportCity.Text;
				Airport.fldIATACode = txtAirportIataCode.Text;
				Airport.fldICAOCode = txtAirportIcaoCode.Text;
				Airport.fldGPSCoordinates = txtAirportGpsCordinates.Text;
				Airport.fldCountryID = (LstSearchAirportCountry.SelectedItem as Countries).CountryID;
				Airport.fldAirportID = viewModel.AddAirport (Airport);

				LocationType = LocationSelectionType.SelectedAirport;
				SelectedAirport = Airport;
				position = new Position (0, 0);
				lblLocation.Text = Airport.fldAirportName + ", " + datePick.Date.ToString ("dd MMM yyyy");
				return true;
			} catch (Exception ex) {
				return false;
			} finally {
				Airport = null;
				loader.IsVisible = false;
			}
		}

		private bool ValidateAirportForSave ()
		{
			try {
				if (txtAirportName.Text.Trim () == string.Empty) {
					DisplayAlert ("Required", "Please Insert Airport", "Ok");
					txtAirportName.Focus ();
					return false;
				} else if (txtAirportCity.Text.Trim () == string.Empty) {
					DisplayAlert ("Required", "Please Insert City", "Ok");
					txtAirportCity.Focus ();
					return false;
				} else if (txtAirportIataCode.Text.Trim () == string.Empty) {
					DisplayAlert ("Required", "Please Insert IATA Code", "Ok");
					txtAirportIataCode.Focus ();
					return false;
				} else if (txtAirportIcaoCode.Text.Trim () == string.Empty) {
					DisplayAlert ("Required", "Please Insert ICAO Code", "Ok");
					txtAirportIcaoCode.Focus ();
					return false;
				} else if (LstSearchAirportCountry.SelectedItem == null) {
					DisplayAlert ("Required", "Please Select Country", "Ok");
					LstSearchAirportCountry.Focus ();
					return false;
				} else
					return true;
			} catch (Exception ex) {
				return false;
			}
		}

		private bool SelectAirport ()
		{
			loader.IsVisible = true;
			try {
				if (ValidateAirportForSelect ()) {
					Airports airport = LstSearchAirportist.SelectedItem as Airports;
					LocationType = LocationSelectionType.SelectedAirport;
					SelectedAirport = new tblAirports () {
						fldAirportID = airport.AirportID ,
						fldAirportName = airport.AirportName ,
						fldCountryID = airport.CountryID,
						fldGPSCoordinates = airport.GPSCoordinates
					};
					position = new Position (0, 0);
					lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString ("dd MMM yyyy");
					return true;
				} else
					return false;
			} catch (Exception ex) {
				return false;
			}finally {
				loader.IsVisible = false;
			}
		}

		private bool ValidateAirportForSelect ()
		{
			try {
				if (LstSearchAirportist.SelectedItem == null) {
					DisplayAlert ("Required", "Please Select Airport First", "Ok");
					return false;
				} else
					return true;
			} catch (Exception ex) {
				return false;
			}
		}

		#endregion

		#region Country

		private void LoadCountry (string CountryName)
		{
			loader.IsVisible = true;
			try {
				var _searchList = viewModel.SearcCountries (20, lastCountriesCount, CountryName);
				for (int i = 0; i <= _searchList.Count - 1; i++) {
					CountriesList.Add (_searchList [i]);
					//if (i == _searchList.Count - 1)
					//	lastCountriesCount = _searchList [i].CountryID;
				}
			} catch (Exception ex) {
				
			}
			finally{
				loader.IsVisible = false;
			}
		}

		private void UnLoadCountry ()
		{
			CountriesList.Clear ();
			lastCountriesCount = 0;
		}

		#endregion

		#region IDisposable implementation

		public void Dispose ()
		{
			imageName = string.Empty;
			_Picture = null;
		}

		#endregion
	}
}

