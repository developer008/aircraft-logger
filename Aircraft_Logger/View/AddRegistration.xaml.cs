﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Xamarin.Forms.Maps;
using XLabs.Enums;
using System.Threading.Tasks;
using Plugin.Geolocator;
using System.Diagnostics;
using System.Linq;

namespace Aircraft_Logger
{
	public partial class AddRegistration : ContentPage
	{
		ObservableCollection<AddRegistrationSearchList> Searchlist = new ObservableCollection<AddRegistrationSearchList>();

		ObservableCollection<Airports> AirportsList = new ObservableCollection<Airports>();
		bool isAirportsLoading;
		int lastAirportsCount = 0;

		ObservableCollection<Countries> CountriesList = new ObservableCollection<Countries>();
		bool isCountriesLoading;
		int lastCountriesCount = 0;

		bool isLoading;
		int lastCount = 0;
		bool isSearchableRecord = false;

		Position position = new Position();
		ILocationPicker LPicker;
		userdefault user;
		AddRegistrationViewModel viewModel;
		APICivilSearch selectedCivil = new APICivilSearch();
		APImilitarydbSearch selectedMilitary = new APImilitarydbSearch();
		APICountrydbSearch selectedCountry = new APICountrydbSearch();
		enum LocationSelectionType
		{
			CurrentLocation,
			SelectedAirport
		}

		LocationSelectionType LocationType;
		tblAirports SelectedAirport;
		tblVisits currentVisit;
		tblManufacturers manufacturer;
		tblAircraft aircraft;
		tblAirlines airlines;
		tblUnits units;

		public AddRegistration()
		{
			InitializeComponent();
			stkSelectAirport.BackgroundColor = new Color(0, 0, 0, 0.5);
			stkCivilSearch.BackgroundColor = new Color(0, 0, 0, 0.5);
			stkMilitarySearch.BackgroundColor = new Color(0, 0, 0, 0.5);
			//LocationType = LocationSelectionType.CurrentLocation;
			txtRegister.Focus();
			PicLocation();
			//lblLocation.Text = datePick.Date.ToString ("dd MMM yyyy");
			viewModel = new AddRegistrationViewModel();
			NavigationPage.SetHasNavigationBar(this, false);

			Bind_ClickEvents();
			SetUserDefaultLocation();
			BindRegistration();
			LoadGrid();
			//txtRegister.Text.ToUpper ();

		}

		public void SetUserDefaultLocation()
		{
			AirportsDatabase ad = new AirportsDatabase();
			var result = ad.GetAirports(Account.DefaultAirportId);
			if (result == null)
				return;
			LocationType = LocationSelectionType.SelectedAirport;
			SelectedAirport = new tblAirports()
			{
				fldAirportID = result.fldAirportID,
				fldAirportName = result.fldAirportName,
				fldCountryID = result.fldCountryID,
				fldGPSCoordinates = result.fldGPSCoordinates
			};
			position = new Position(0, 0);
			lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString("dd MMM yyyy");
			if (SelectedAirport != null)
			{

				//				Searchlist.Clear ();
				//				lastCount = 0;
				//				LoadGrid();
			}

		}

		private void Bind_ClickEvents()
		{
			var TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => NavigateBackCommand(s, e);
			stkBack.GestureRecognizers.Add(TapEventRecognizer);
			stkDone.GestureRecognizers.Add(TapEventRecognizer);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => CallAircraftDetails(s, e);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => imgfilterDate_Clicked(s, e);
			imgfilterDate.GestureRecognizers.Add(TapEventRecognizer);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => imgPicLocation_Click(s, e);
			imgPicLocation.GestureRecognizers.Add(TapEventRecognizer);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => lblAZ1_Clicked(s, e);
			stk_AZ1.GestureRecognizers.Add(TapEventRecognizer);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => lbl09_Clicked(s, e);
			stk_09.GestureRecognizers.Add(TapEventRecognizer);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => lblAZ2_Clicked(s, e);
			stk_AZ2.GestureRecognizers.Add(TapEventRecognizer);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => lblSelectAirportDone_Click(s, e);
			lblSelectAirportDone.GestureRecognizers.Add(TapEventRecognizer);

			TapEventRecognizer = new TapGestureRecognizer();
			TapEventRecognizer.Tapped += (s, e) => lblSelectAirportBackMenu_Click(s, e);
			lblSelectAirportBackMenu.GestureRecognizers.Add(TapEventRecognizer);
		}

		public void lblAZ1_Clicked(object sender, EventArgs e)
		{
			imgAZ1.IsVisible = true;
			img09.IsVisible = false;
			imgAZ2.IsVisible = false;
			ReloadGrid();

		}

		public void lbl09_Clicked(object sender, EventArgs e)
		{
			imgAZ1.IsVisible = false;
			img09.IsVisible = true;
			imgAZ2.IsVisible = false;
			ReloadGrid();
		}

		public void lblAZ2_Clicked(object sender, EventArgs e)
		{
			imgAZ1.IsVisible = false;
			img09.IsVisible = false;
			imgAZ2.IsVisible = true;
			ReloadGrid();
		}

		private void imgfilterDate_Clicked(object o, EventArgs e)
		{
			//btnSearch.Focus ();
			datePick.Unfocus();
			datePick.Focus();
			//ImageOrientation.ImageOnTop
		}

		private async void imgPicLocation_Click(object sender, EventArgs e)
		{
			AddPhoto addPhoto = new AddPhoto();
			var action = await DisplayActionSheet(null, "Cancel", null, "Current Location", "Select Airport");
			switch (action)
			{
				case "Current Location":
					PicLocation();
					break;
				case "Select Airport":
					ShowAirportPopup();
					break;
			}
		}

		public async void PicLocation()
		{
			lblLocation.Text = "Off Airport" + ", " + datePick.Date.ToString("dd MMM yyyy"); ;

			LPicker = DependencyService.Get<ILocationPicker>();
			LPicker.ChangeLoation += LPicker_OnChangeLoation;
			LPicker.GetCurrentLocation();
			SelectedAirport = null;
			//try
			//{
			//	var locator = CrossGeolocator.Current;
			//	locator.DesiredAccuracy = 50;
			//	var position = locator.GetPositionAsync(timeoutMilliseconds: 10000).Result;
			//	Debug.WriteLine("Position Status: {0}", position.Timestamp);
			//	Debug.WriteLine("Position Latitude: {0}", position.Latitude);
			//	Debug.WriteLine("Position Longitude: {0}", position.Longitude);
			//}
			//catch (Exception ex)
			//{
			//	Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			//}


		}

		public void LPicker_OnChangeLoation(Position _position)
		{
			try
			{
				position = _position;
				//LocationType = LocationSelectionType.CurrentLocation;
				LocationType = LocationSelectionType.SelectedAirport;
				lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString("dd MMM yyyy");
				//lblLocation.Text = datePick.Date.ToString ("dd MMM yyyy");
				LPicker.StopListening();
				LPicker = null;
			}
			catch (Exception ex)
			{

			}

		}

		private void BindRegistration()
		{
			LstRegistrationList.ItemsSource = Searchlist;
		}

		protected void LstRegistrationList_Tapped(object o, ItemTappedEventArgs e)
		{
			loader.IsVisible = true;
			var Registration = e.Item as AddRegistrationSearchList;
			//DisplayAlert ("Choosen!",Registration.Registration + " Was Selected!","OK");
			Navigation.PushModalAsync(new AircraftDetails(Registration.RegistrationID));
			loader.IsVisible = false;
		}

		protected void LstRegistrationList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			if (isLoading || Searchlist.Count == 0)
				return;

			//hit bottom!
			if (e.Item == Searchlist[Searchlist.Count - 1])
			{
				LoadGrid();
			}
		}

		private void LoadGrid()
		{
			string orderby = string.Empty;
			if (imgAZ1.IsVisible)
				orderby = "Registration";
			else if (img09.IsVisible)
				orderby = "Aircraft";
			else if (imgAZ2.IsVisible)
			{
				orderby = sagIsCivil.SelectedValue ? "Airline" : "Units";

			}
			string airportId = string.Empty;

			if (SelectedAirport != null)
			{
				airportId = SelectedAirport.fldAirportID;
			}
			var _searchList = viewModel.SearchRegistrationDetailList(200, Searchlist.Count, datePick.Date, "", 'M', orderby, airportId);
			var _searchList1 = viewModel.SearchRegistrationDetailList(200, Searchlist.Count, datePick.Date, "", 'C', orderby, airportId);
			foreach (var item in _searchList1)
			{
				//if (item.UnitName1 == null) {
				//	item.UnitName1 = "";
				//}
				//if (item.AirlineName == null) {
				//	item.AirlineName = "";
				//}
				_searchList.Add(item);
			}
			foreach (var item in _searchList)
			{
				if (item.New == "Y")
				{
					item.IsVisibleRed = true;
				}
				else
				{
					item.IsVisibleRed = false;
				}

			}
			int recordNumber = Searchlist.Count;

			if (_searchList.Count > 0)
			{
				stkHeader.IsVisible = true;
			}
			if (imgAZ1.IsVisible)
				_searchList = _searchList.OrderBy(x => x.Registration).ToList();
			else if (img09.IsVisible)
				_searchList = _searchList.OrderBy(x => x.AircraftName).ToList();
			else if (imgAZ2.IsVisible)
			{
				string a = sagIsCivil.SelectedValue ? "Airline" : "Units";
				if (a == "Airline")
				{
					_searchList = _searchList.OrderBy(x => x.AirlineName).ToList();
				}
				else
				{
					_searchList = _searchList.OrderBy(x => x.UnitName1).ToList();
				}

			}



			foreach (var item in _searchList)
			{
				recordNumber = recordNumber + 1;
				item.RecordNumber = recordNumber;
				if (item.UnitName1 == null || item.UnitName1=="" || item.UnitName1=="null") {
					item.UnitName1 = "";
				} 
				if (item.AirlineName == null||item.AirlineName=="" || item.AirlineName=="null") {
					item.AirlineName = "";
				}
				if (item.AircraftName == null||item.AircraftName=="" || item.AircraftName=="null") {
					item.AircraftName = "";
				}
				Searchlist.Add(item);
			}

			//foreach (var item in _searchList1)
			//{
			//	if (item.New == "Y")
			//	{
			//		item.IsVisibleRed = true;
			//	}
			//	else {
			//		item.IsVisibleRed = false;
			//	}

			//}
			////int recordNumber = Searchlist.Count;
			//if (_searchList.Count > 0)
			//{
			//	stkHeader.IsVisible = true;
			//}

			//foreach (var item in _searchList1)
			//{
			//	recordNumber = recordNumber + 1;
			//	item.RecordNumber = recordNumber;
			//	//				item.Registration = item.Registration;
			//	//				item.AircraftName = recordNumber.ToString();
			//	//				item.AirlineName = recordNumber.ToString();
			//	//				item.UnitName1 = recordNumber.ToString();
			//	Searchlist.Add(item);
			//}
			/*
			//--old code start
			for (int i = 0; i <= _searchList.Count - 1; i++) {
				_searchList [i].RecordNumber = recordNumber + i + 1;
				Searchlist.Add (_searchList [i]);
//				if (i == _searchList.Count - 1)
//					lastCount =  _searchList[i].RegistrationID;
			}
			//lastCount = lastCount + _searchList.Count;
			//Searchlist.Add ();
			//old code end
			*/
			if (recordNumber != 0) { Stkpartition.IsVisible = true; }
		}

		protected void OnChangeEvent(object sender, EventArgs e)
		{
			//ReloadGrid();
		}

		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			//Navigation.PopModalAsync (true);
			Navigation.PushModalAsync(new MainMenu());
		}

		protected void CallAircraftDetails(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new AircraftDetails());
		}

		public void ReloadGrid()
		{
			try
			{
				Searchlist.Clear();
				lastCount = 0;
				LoadGrid();
			}
			catch (Exception ex)
			{

			}
		}

		protected void datePick_DateSelected(object sender, DateChangedEventArgs e)
		{
			try
			{
				ReloadGrid();
				if (LocationType == LocationSelectionType.CurrentLocation)
					lblLocation.Text = datePick.Date.ToString("dd MMM yyyy");
				else
					lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString("dd MMM yyyy");
			}
			catch (Exception ex)
			{

			}
		}

		protected void btnLog_Clicked(object sender, EventArgs e)
		{
			savelog();

		}


		public void savelog()
		{
			try
			{
				loader.IsVisible = true;
				tblRegistrations Registrations = new tblRegistrations();
				if (isSearchableRecord)
				{
					if (!ValidateLogs())
						return;
					string tmpMfName = "TestM52";
					string aircraftId = string.Empty;
					string manufacturerId = string.Empty;
					string airlineId = string.Empty;
					string unitId = string.Empty;
					manufacturer = new tblManufacturers();
					var manufacturerResult = GetManufacturer(tmpMfName);
					if (manufacturerResult == null)
					{
						manufacturer.fldManufacturerName = tmpMfName;
						manufacturerId = AddUpdateManufacturer();
					}
					else
					{
						manufacturer = manufacturerResult;
						manufacturerId = manufacturer.fldManufacturerID;
					}
					//					if (SelectedAirport == null) {
					//						DisplayAlert ("Airport", "Please select airport", "Ok");
					//						return;
					//					}
					if (!IsVisitExists())
						SaveVisits();
					if (sagIsCivil.SelectedValue)
					{
						aircraft = new tblAircraft();
						var aircraftResult = GetAircraft(selectedCivil.FullType);
						if (aircraftResult == null)
						{
							aircraft.fldAircraftName = selectedCivil.FullType;
							aircraft.fldManufacturerID = manufacturerId;
							aircraftId = AddUpdateAircraft();
						}
						else
						{
							aircraft = aircraftResult;
							aircraftId = aircraft.fldAircraftID;
						}

						airlines = new tblAirlines();
						var airlinesResult = GetAirline(selectedCivil.Owner);
						if (airlinesResult == null)
						{
							airlines.fldAirlineName = selectedCivil.Owner;
							airlineId = AddUpdateAirline();
						}
						else
						{
							airlines = airlinesResult;
							airlineId = airlines.fldAirlineID;
						}

						Registrations.fldRegistration = txtRegister.Text;
						Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
						Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
						Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";
						Registrations.fldCn = selectedCivil.Cnr;
						Registrations.fldAircraftID = aircraftId;
						Registrations.fldAirlineID = airlineId;
						Registrations.fldRemarks = selectedCivil.Remarks;

					}
					else
					{
						aircraft = new tblAircraft();
						var aircraftResult = GetAircraft(selectedMilitary.Type);
						if (aircraftResult == null)
						{
							aircraft.fldAircraftName = selectedMilitary.Type;
							aircraft.fldManufacturerID = manufacturerId;
							aircraftId = AddUpdateAircraft();
						}
						else
						{
							aircraft = aircraftResult;
							aircraftId = aircraft.fldAircraftID;
						}

						units = new tblUnits();
						var unitResult = GetUnit(selectedMilitary.Unit);
						if (unitResult == null)
						{
							units.fldUnitName1 = selectedMilitary.Unit;
							unitId = AddUpdateUnit();
						}
						else
						{
							units = unitResult;
							unitId = units.fldUnitID;
						}

						Registrations.fldRegistration = txtRegister.Text;
						Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
						Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
						Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";
						Registrations.fldCn = selectedMilitary.CN;
						Registrations.fldCode = selectedMilitary.Code;
						Registrations.fldAircraftID = aircraftId;
						Registrations.fldUnitID = unitId;

						//Registrations.fldAirlineID = airlineId;
						Registrations.fldRemarks = selectedMilitary.Comment;
					}

					SaveRegistration(Registrations);
					ClearLog();
				}
				else
				{


					if (!ValidateLogs())
						return;

					//					if (SelectedAirport == null) {
					//						DisplayAlert ("Airport", "Please select airport", "Ok");
					//						return;
					//					}
					if (!IsVisitExists())
						SaveVisits();


					Registrations.fldRegistration = txtRegister.Text;
					Registrations.fldAirportVisitID = currentVisit.fldAirportVisitID;
					Registrations.fldAircraftType = sagIsCivil.SelectedValue ? "C" : "M";
					Registrations.fldNew = IsRegistrationExists() ? "N" : "Y";

					SaveRegistration(Registrations);
					ClearLog();
				}
				loader.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		void txtRegister_OnTextChanged(object sender, EventArgs e)
		{
			//		txtRegister.CBorderColor = Color.FromHex("#24AFE3");
			//			if (!String.IsNullOrEmpty (txtRegister)) {
			//
			//		SelectedAirport.fldAirportID == null	String val = txtRegister.Text.ToUpper(); //Get Current Text
			//			txtRegister.Text =val;
			//			}

			Entry entry = sender as Entry;
			string val = entry.Text;
			entry.Text = val.ToUpper();


		}

		void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
		{
			txtRegister.CBorderColor = Color.Red;
		}

		protected async void btnSearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				isSearchableRecord = true;
				SearchAPI api = new SearchAPI();
				if (sagIsCivil.SelectedValue)
				{
					string airportId = string.Empty;
					if (SelectedAirport.fldAirportID != null)
					{
						airportId = SelectedAirport.fldAirportID;
					}

					List<tblRegistrations> isExist = viewModel.GetIfDuplicate(Convert.ToString(txtRegister.Text), airportId);
					if (isExist.Count > 0)
					{
						//var answer = await DisplayAlert("Question?", "This registration is already in the database", "Continue", "Cancel");
						//if (answer)
						//{

						loader.IsVisible = true;
						LstSearchCivilDB.ItemsSource = await api.SearchCivilDB(txtRegister.Text);
						loader.IsVisible = false;
						stkCivilSearch.IsVisible = true;
						//}
						//else {

						//}
					}
					else
					{

						loader.IsVisible = true;
						LstSearchCivilDB.ItemsSource = await api.SearchCivilDB(txtRegister.Text);

						loader.IsVisible = false;
						stkCivilSearch.IsVisible = true;
					}
				}
				else
				{
					user = DependencyService.Get<userdefault>();
					APICountrydbSearch ap = new APICountrydbSearch();

					string lscountry = user.getUserlastcountry();
					string myconid = user.getmycountry();
					ap.Country = lscountry;

					loader.IsVisible = true;
					List<APICountrydbSearch> listcountry = new List<APICountrydbSearch>();

					List<APICountrydbSearch> temp = new List<APICountrydbSearch>();

					string myconname;
					temp = api.SearchCountryDb().Result;
					foreach (var b in temp)
					{
						if (b.CountryID == myconid)
						{
							listcountry.Add(b);
						}
						if (b.Country == lscountry)
						{
							ap.Code = b.Code;
						}
					}
					listcountry.Add(ap);
					temp.Sort((x, y) => x.Country.CompareTo(y.Country));
					foreach (var a in temp)
					{
						if (a.Country != ap.Country)
						{
							if (a.CountryID != myconid)
							{
								if (a.Code.Length == 2)
								{
									listcountry.Add(a);
								}
							}
						}


					}



					LstSearchCountryDB.ItemsSource = listcountry;
					loader.IsVisible = false;
					stkCountrySearch.IsVisible = true;
					/*
					#region Old Code
					var action = await DisplayActionSheet (null, "Cancel", null, "africanairforce", "americaairforce","asianairforce","eurairforce");
					//stkCivilSearch.IsVisible = true;
					stkMilitarySearch.IsVisible = true;
					loader.IsVisible = true;
//					LstSearchCivilDB.ItemsSource = await api.SearchMilitaryDatabase (action);
					LstSearchMilitaryDB.ItemsSource = await api.SearchMilitaryDb (action);
					loader.IsVisible = false;
					#endregion 
					*/
				}

				//			var list1 = await api.SearchAircrafttype ();
				//
				//			var list3 = await api.SearchMilitaryDatabase ("");
				//			var list4 = await api.SearchMilitaryDb ();
				//var result = await api.CallApi ("http://192.168.200.72:83/api/values/5");
				ReloadGrid();
			}
			catch (Exception ex)
			{
				int i = 0;
			}
		}

		private bool IsRegistrationExists()
		{
			try
			{
				return viewModel.GetIfDuplicate(Convert.ToString(txtRegister.Text), (SelectedAirport.fldAirportID)).Count > 0;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private void SaveRegistration(tblRegistrations Registrations)
		{

			try
			{

				viewModel.SaveRegistration(Registrations);
			}
			catch (Exception ex)
			{
				Registrations = null;
			}
		}

		private bool IsVisitExists()
		{
			try
			{
				tblVisits Visits = viewModel.GetIfVisitExists(SelectedAirport == null ? "" : SelectedAirport.fldAirportID, CommonHelper.ConvertLocationToDMS(position), datePick.Date);
				if (Visits != null)
					currentVisit = Visits;
				return Visits != null;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private void SaveVisits()
		{
			tblVisits Visits = new tblVisits();
			try
			{
				if (LocationType == LocationSelectionType.SelectedAirport && SelectedAirport != null)
					Visits.fldAirportID = SelectedAirport.fldAirportID;
				else
					Visits.fldLocationGPS = CommonHelper.ConvertLocationToDMS(position);
				Visits.fldVisitDate = datePick.Date.DefaultDateFormate();
				Visits.fldAirportVisitID = viewModel.SaveVisits(Visits);
				currentVisit = Visits;
			}
			catch (Exception ex)
			{

			}
			finally
			{
				Visits = null;
			}
		}

		private string AddUpdateManufacturer()
		{
			try
			{
				return viewModel.AddUpdateManufacturer(manufacturer);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		private tblManufacturers GetManufacturer(string manufacturerName)
		{
			try
			{
				return viewModel.GetManufacturer(manufacturerName);
			}
			catch (Exception ex)
			{
				return manufacturer;
			}
		}

		private tblAircraft GetAircraft(string aircraftName)
		{
			try
			{
				return viewModel.GetAircraft(aircraftName);
			}
			catch (Exception ex)
			{
				return aircraft;
			}
		}

		private tblAirlines GetAirline(string airlineName)
		{
			try
			{
				return viewModel.GetAirline(airlineName);
			}
			catch (Exception ex)
			{
				return airlines;
			}
		}

		private tblUnits GetUnit(string unitName)
		{
			try
			{
				return viewModel.GetUnit(unitName);
			}
			catch (Exception ex)
			{
				return units;
			}
		}

		private string AddUpdateAircraft()
		{
			try
			{
				return viewModel.AddUpdateAircraft(aircraft);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		private string AddUpdateUnit()
		{
			try
			{
				return viewModel.AddUpdateUnit(units);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}
		private string AddUpdateAirline()
		{
			try
			{
				return viewModel.AddUpdateAirline(airlines);
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		private void ClearLog()
		{
			try
			{
				txtRegister.Text = string.Empty;
				lblSelectedRegistration.Text = string.Empty;
				selectedCivil = new APICivilSearch();
				selectedMilitary = new APImilitarydbSearch();
				ReloadGrid();

			}
			catch (Exception ex)
			{

			}
		}

		private bool ValidateLogs()
		{
			try
			{
				if (txtRegister.Text.Trim() == string.Empty)
				{
					DisplayAlert("Warning", "Please Enter Registration", "Ok");
					txtRegister.Focus();
					loader.IsVisible = false;
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			finally
			{
			}
		}

		#region Airport

		private void ShowAirportPopup()
		{
			try
			{
				stkSelectAirport.IsVisible = true;
				SetAirportVisibility(sagSelectAirport.SelectedValue);
				InitAirportPopup();
			}
			catch (Exception ex)
			{

			}
		}

		private void SetAirportVisibility(bool IsSearch)
		{
			stkSelectAirportAddNew.IsVisible = !IsSearch;
			stkSelectAirportSearch.IsVisible = IsSearch;
		}

		protected void lblSelectAirportDone_Click(object sender, EventArgs e)
		{
			try
			{
				if (sagSelectAirport.SelectedValue)
				{
					if (!SelectAirport())
						return;
				}
				else
				{
					if (!SaveAirport())
						return;
				}
			}
			catch (Exception ex)
			{
				return;
			}
			CloseAirportPopup();
		}

		protected void lblSelectAirportBackMenu_Click(object sender, EventArgs e)
		{
			try
			{
				CloseAirportPopup();
			}
			catch (Exception ex)
			{

			}
		}

		protected void sagSelectAirport_OnChange(object sender, EventArgs e)
		{
			SetAirportVisibility(sagSelectAirport.SelectedValue);
		}

		private void InitAirportPopup()
		{
			try
			{
				FIllAirportList();
				LoadCountry("");
				var list = CountriesList.OrderBy(o=>o.CountryName).ToList();
				LstSearchAirportCountry.ItemsSource = list;
			}
			catch (Exception ex)
			{

			}
		}

		private void CloseAirportPopup()
		{
			try
			{
				stkSelectAirport.IsVisible = false;
				ClearAirportPopup();
			}
			catch (Exception ex)
			{

			}
		}

		private void ClearAirportPopup()
		{
			try
			{
				LstSearchAirportCountry.ItemsSource = null;
				LstSearchAirportist.ItemsSource = null;
				UnLoadCountry();
				UnLoadAirport();
				txtAirportName.Text = string.Empty;
				txtAirportCity.Text = string.Empty;
				txtAirportIataCode.Text = string.Empty;
				txtAirportIcaoCode.Text = string.Empty;
			}
			catch (Exception ex)
			{
			}
		}

		private void FIllAirportList()
		{
			try
			{
				LoadAirports(txtAirportSearch.Text);
				LstSearchAirportist.ItemsSource = AirportsList;
			}
			catch (Exception ex)
			{
			}
		}

		private void LoadAirports(string AirportName)
		{
			try
			{
				AirportsList.Clear();
				var _searchList = viewModel.SearchAirports(10000, lastAirportsCount, AirportName);
				var uni = _searchList.Distinct().ToList();
				for (int i = 0; i <= uni.Count - 1; i++)
				{
					AirportsList.Add(uni[i]);
					//if (i == _searchList.Count - 1)
					//lastAirportsCount = _searchList [i].AirportID;
				}

			}
			catch (Exception ex)
			{
			}
		}

		protected void LstSearchAirportList_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			try
			{
				if (isAirportsLoading || AirportsList.Count == 0)
					return;

				if (e.Item == AirportsList[AirportsList.Count - 1])
				{
					LoadAirports(txtAirportSearch.Text);
				}
			}
			catch (Exception ex)
			{
			}
		}

		protected void LstSearchAirportList_Tapped(object Sender, ItemTappedEventArgs e)
		{
			try
			{
				var airpt = e.Item as Airports;
				txtAirportSearch.Text = airpt.AirportName;
				if (isAirportsLoading || AirportsList.Count == 0)
					return;

				if (e.Item == AirportsList[AirportsList.Count - 1])
				{
					LoadAirports(txtAirportSearch.Text);
				}

			}
			catch (Exception ex)
			{
			}
		}
		private void LstSearchAirportCountry_Tap(object sender, ItemTappedEventArgs e)
		{
			try
			{
				var countrie = e.Item as Countries;
				txtAirportCountry.Text = countrie.CountryName;

			}
			catch (Exception ex)
			{
			}
		}

		protected void LstSearchAirportCountry_Appearing(object Sender, ItemVisibilityEventArgs e)
		{
			try
			{
				if (isCountriesLoading || CountriesList.Count == 0)
					return;

				if (e.Item == CountriesList[CountriesList.Count - 1])
				{
					LoadCountry("");
				}
			}
			catch (Exception ex)
			{
			}
		}

		protected void txtSearchAircraft_Change(object sender, TextChangedEventArgs e)
		{
			UnLoadAirport();
			LoadAirports(txtAirportSearch.Text);
		}

		private void UnLoadAirport()
		{
			try
			{
				AirportsList.Clear();
				lastAirportsCount = 0;
			}
			catch (Exception ex)
			{
			}
		}

		private bool SaveAirport()
		{
			if (!ValidateAirportForSave())
				return false;
			tblAirports Airport = new tblAirports();
			try
			{
				Airport.fldAirportName = txtAirportName.Text;
				Airport.fldAirportCity = txtAirportCity.Text;
				Airport.fldIATACode = txtAirportIataCode.Text;
				Airport.fldICAOCode = txtAirportIcaoCode.Text;
				Airport.fldCountryID = (LstSearchAirportCountry.SelectedItem as Countries).CountryID;
				Airport.fldAirportID = viewModel.AddAirport(Airport);

				LocationType = LocationSelectionType.SelectedAirport;
				SelectedAirport = Airport;
				position = new Position(0, 0);
				lblLocation.Text = Airport.fldAirportName + ", " + datePick.Date.ToString("dd MMM yyyy");
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			finally
			{
				Airport = null;
			}
		}

		private bool ValidateAirportForSave()
		{
			try
			{
				if (txtAirportName.Text.Trim() == string.Empty)
				{
					DisplayAlert("Required", "Please Insert Airport", "Ok");
					txtAirportName.Focus();
					return false;
				}
				//else if (txtAirportCity.Text.Trim() == string.Empty)
				//{
				//	DisplayAlert("Required", "Please Insert City", "Ok");
				//	txtAirportCity.Focus();
				//	return false;
				//}
				/*
				else if (txtAirportIataCode.Text.Trim () == string.Empty) {
					DisplayAlert ("Required", "Please Insert IATA Code", "Ok");
					txtAirportIataCode.Focus ();
					return false;
				} else if (txtAirportIcaoCode.Text.Trim () == string.Empty) {
					DisplayAlert ("Required", "Please Insert ICAO Code", "Ok");
					txtAirportIcaoCode.Focus ();
					return false;
				}
				*/
				else if (LstSearchAirportCountry.SelectedItem == null)
				{
					DisplayAlert("Required", "Please Select Country", "Ok");
					LstSearchAirportCountry.Focus();
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool SelectAirport()
		{
			try
			{
				if (ValidateAirportForSelect())
				{
					Airports airport = LstSearchAirportist.SelectedItem as Airports;
					txtAirportSearch.Text = airport.AirportName;
					LocationType = LocationSelectionType.SelectedAirport;
					SelectedAirport = new tblAirports()
					{
						fldAirportID = airport.AirportID,
						fldAirportName = airport.AirportName,
						fldCountryID = airport.CountryID,
						fldGPSCoordinates = airport.GPSCoordinates
					};
					position = new Position(0, 0);
					lblLocation.Text = SelectedAirport.fldAirportName + ", " + datePick.Date.ToString("dd MMM yyyy");
					if (SelectedAirport != null)
					{

						Searchlist.Clear();
						lastCount = 0;
						LoadGrid();
					}
					return true;
				}
				else
					return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private bool ValidateAirportForSelect()
		{
			try
			{
				if (LstSearchAirportist.SelectedItem == null)
				{
					DisplayAlert("Required", "Please Select Airport First", "Ok");
					return false;
				}
				else
					return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		#endregion

		#region Country

		private void LoadCountry(string CountryName)
		{
			var _searchList = viewModel.SearcCountries(1000, lastCountriesCount, CountryName);
			for (int i = 0; i <= _searchList.Count - 1; i++)
			{
				CountriesList.Add(_searchList[i]);
				//if (i == _searchList.Count - 1)
				//	lastCountriesCount = _searchList [i].CountryID;
			}
			CountriesList.OrderBy(o => o.CountryName).ToList();

		}

		private void UnLoadCountry()
		{
			CountriesList.Clear();
			lastCountriesCount = 0;
		}

		#endregion

		#region Civil DB Seaqrch

		protected void btnCloseCivilSearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkCivilSearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		protected void LstSearchCivilDB_Tap(object o, ItemTappedEventArgs e)
		{
			var civil = e.Item as APICivilSearch;
			selectedCivil = civil;
			txtRegister.Text = civil.Registration;
			//lblSelectedRegistration.Text = civil.Owner + " " + civil.FullType; 
			lblSelectedRegistration.Text = civil.FullType + "," + civil.Owner;
			savelog();
			LstSearchCivilDB.SelectedItem = null;
			stkCivilSearch.IsVisible = false;
			//selectedCivil = null;



		}

		#endregion

		#region Military DB Seaqrch

		protected void btnCloseMilitarySearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkMilitarySearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		protected void LstSearchMilitaryDB_Tap(object o, ItemTappedEventArgs e)
		{
			//			var military = e.Item as APIMilitaryDatabaseSearch;
			var military = e.Item as APImilitarydbSearch;
			selectedMilitary = military;
			//txtRegister.Text = military.;
			//			lblSelectedRegistration.Text = military.CountryCode + " " + military.AirForceName; 
			lblSelectedRegistration.Text = military.Type + "," + military.Unit;
			txtRegister.Text = military.Serial;
			savelog();
			LstSearchMilitaryDB.SelectedItem = null;
			//selectedMilitary = null;
			stkMilitarySearch.IsVisible = false;
		}

		#endregion

		#region Country DB Search

		protected void btnCloseCountrySearch_Clicked(object sender, EventArgs e)
		{
			try
			{
				stkCountrySearch.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		protected async void LstSearchCountryDB_Tap(object o, ItemTappedEventArgs e)
		{
			try
			{

				SearchAPI api = new SearchAPI();
				var country = e.Item as APICountrydbSearch;
				selectedCountry = country;
				user.setuserCountry(country.Country);


				loader.IsVisible = true;
				List<APImilitarydbSearch> list = new List<APImilitarydbSearch>();
				list = await api.SearchMilitaryDbByCountry(country.Code, txtRegister.Text);
				if (list.Count != 0)
				{

					LstSearchMilitaryDB.ItemsSource = list;
					stkCountrySearch.IsVisible = false;
					stkMilitarySearch.IsVisible = true;
				}
				else
				{
					DisplayAlert("Registration", "No registration Found", "ok");

				}
				loader.IsVisible = false;
			}
			catch (Exception ex)
			{

			}
		}

		#endregion

		#region InternetChecking

		public async Task CheckInternetRunCounter()
		{

			DBSyncDatabase dbSync = new DBSyncDatabase();
			for (long i = 0; i < long.MaxValue; i++)
			{
				try
				{

					if (CommonHelper.IsConnected)
					{
						if (CommonHelper.LastTimeSync != 1)
						{
							CommonHelper.LastTimeSync = 1;
							dbSync.ManageDatabase();
							//DisplayAlert("Internet","Let's start sync","Cancel");
						}

					}
					else
					{
						CommonHelper.LastTimeSync = 0;
						//DisplayAlert("Internet","Internet Not Available","Cancel");
					}


					await Task.Delay(30000);
				}
				catch (Exception ex)
				{

				}
			}

		}

		#endregion
	}
}
