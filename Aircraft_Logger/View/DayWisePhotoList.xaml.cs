﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Aircraft_Logger
{
	public partial class DayWisePhotoList : ContentPage
	{
		public string _registrationId{ get; set;}
		public DateTime _regDate{ get; set;}
		List<Registrations> lsRegistrations;
		AddPhoto addPhoto;
		List<List<Registrations>> regis;
		string selectedaction;
		userdefault us;
		public DayWisePhotoList (List<List<Registrations>> lstregist,DateTime dtRegDate)
		{
			InitializeComponent ();
			NavigationPage.SetHasNavigationBar (this, false);
			Bind_ClickEvents ();
			regis = lstregist;
			//_registrationId = registrationId;
			_regDate = dtRegDate;
		   lblDt.Text=	String.Format("{0:d MMMM , yyyy}", dtRegDate);
			BindImages ();
		}
		public void BindImages()
		{
			lsRegistrations = new RegistrationsDatabase().GetRegistrationsByGroup();
			var result = (new PhotosDatabase ()).GetPhotosByRegistarId (_registrationId);

			VisitsDatabase vd = new VisitsDatabase();
			List<tblVisits> visitlist = new List<tblVisits>();
			visitlist = vd.GetVisits();



			StackLayout wrpImageCollection= new StackLayout {
				Spacing = 7,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,

			};
			foreach (var item in regis)
			{
				foreach (var item1 in item)
				{
					StackLayout stkImage = new StackLayout()
					{
						HeightRequest = 200,
						WidthRequest = 260,
						Spacing = 0,
						BackgroundColor = Color.FromHex("24AFE3"),
						Padding = new Thickness(2, 2, 2, 2),
						VerticalOptions = LayoutOptions.CenterAndExpand

					};
					CImage imgTmp = new CImage()
					{
						HeightRequest = 200,
						WidthRequest = 260,
						Source = "tmpImg1.jpg",
						VerticalOptions = LayoutOptions.CenterAndExpand,
						Aspect = Aspect.AspectFill
					};
					imgTmp.Source = LoadImageFromDisk(item1.PhotoName);
					stkImage.Children.Add(imgTmp);

					wrpImageCollection.Children.Add(stkImage);
				}
			}

			stkImageCollection.Children.Add (wrpImageCollection);
		}

		private string LoadImageFromDisk (string fileName)
		{

			IPicture picture = DependencyService.Get<IPicture> ();

			return picture.GetPicture (fileName);

		}
		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer ();
			tapGestureRecognizerCalendar.Tapped += (s,e)=>NavigateBackCommand(s,e);
			var tapGestureRecognizerCalendar1 = new TapGestureRecognizer();
			tapGestureRecognizerCalendar1.Tapped += (s, e) => NavigatePicCommand(s, e);
			stkBack.GestureRecognizers.Add (tapGestureRecognizerCalendar);
			stkDone.GestureRecognizers.Add (tapGestureRecognizerCalendar1);
		}


		protected void NavigateBackCommand(object sender , EventArgs e)
		{
			Navigation.PopModalAsync (true);
		}

		protected void NavigatePicCommand(object sender, EventArgs e)
		{


			ActionSheet.IsVisible = true;
		}
		protected void Lasttaken(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;

			chooselastphoto();
		}

		protected void Picture(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;

			TakeNewPicture();
		}

		protected void Choosegallery(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;

			PicPicture();
		}
		async void PicPicture()
		{
			//PicCamera.ChoosePhoto ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.SelectPicture();
			if (cameraOps.ImageSource != null)
			{
				CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
			}
		}

		async void chooselastphoto()
		{
			try
			{
				us = DependencyService.Get<userdefault>();
				ImageSource sor = us.getlastimage();
				CameraViewModel cameraOps = new CameraViewModel();
				cameraOps.lastimg(sor);
				if (cameraOps.ImageSource != null)
				{
					CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(sor, 300, 200));
				}
			}
			catch (Exception ex)
			{

			}

		}

		private async void TakeNewPicture()
		{
			//PicCamera.TakePicture ();
			CameraViewModel cameraOps = new CameraViewModel();
			await cameraOps.TakePicture();
			if (cameraOps.ImageSource != null)
			{
				CallAddPhoto(await DependencyService.Get<IPicture>().ResizePicture(cameraOps.ImageSource, 300, 200));
			}
		}

		private void CallAddPhoto(ImageSource source)
		{
			try
			{
				addPhoto = null;
				addPhoto = new AddPhoto();
				addPhoto.imgDisplaySource = source;
				Navigation.PushModalAsync(addPhoto);
			}
			catch (Exception ex)
			{

			}

		}
		protected void Cancelgal(object sender, EventArgs e)
		{
			ActionSheet.IsVisible = false;
		}
	}
}

