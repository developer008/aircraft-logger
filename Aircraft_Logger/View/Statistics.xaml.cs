﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using XLabs.Forms.Charting.Controls;
using Xamarin.Forms.Maps;
using OxyPlot.Xamarin.Forms;
using OxyPlot.Axes;
using OxyPlot;
using OxyPlot.Series;
using System.Net;
using System.IO;
using System.Text;

namespace Aircraft_Logger
{
	public class ChartData
	{
		public int chartKey { get; set; }
		public int dataCount { get; set; }
	}
	public partial class Statistics : ContentPage
	{
		public int startYear { get; set; }
		public int endYear { get; set; }
		public int yearInterval { get; set; }
		Map map;
		StatisticsViewModel viewModel;
		List<Registrations> lsRegistrations = new List<Registrations>();
		List<Registrations> lsAirportVisit = new List<Registrations>();

		public Statistics()
		{
			InitializeComponent();
			viewModel = new StatisticsViewModel();
			stkDetailPopup.BackgroundColor = new Color(0, 0, 0, 0.5);
			NavigationPage.SetHasNavigationBar(this, false);
			LoadMap();
			Bind_ClickEvents();
			LoadData();
		}

		private void Bind_ClickEvents()
		{
			var tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => NavigateBackCommand(s, e);
			var tapGestureRecognizerCalendar1 = new TapGestureRecognizer();
			tapGestureRecognizerCalendar1.Tapped += (s, e) => NavigatePicCommand(s, e);
			stkBack.GestureRecognizers.Add(tapGestureRecognizerCalendar);
			stkDone.GestureRecognizers.Add(tapGestureRecognizerCalendar1);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblMap_Click(s, e);
			stk_Map.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblGraph_Click(s, e);
			stk_Graph.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblStatistics_Click(s, e);
			stk_Statistics.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblShowManufacturerdetails_Click(s, e);
			lblShowManufacturerdetails.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblShowAircraftDetails_Click(s, e);
			lblShowAircraftDetails.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblShowAirlineDetails_Click(s, e);
			lblShowAirlineDetails.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblShowAirportDetails_Click(s, e);
			lblShowAirportDetails.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblShowAirportVisitDetails_Click(s, e);
			lblShowAirportVisitDetails.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblCountriesDetails_Click(s, e);
			lblCountriesDetails.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			tapGestureRecognizerCalendar = new TapGestureRecognizer();
			tapGestureRecognizerCalendar.Tapped += (s, e) => lblAirforceDetails_Click(s, e);
			lblAirforceDetails.GestureRecognizers.Add(tapGestureRecognizerCalendar);

			var tapGestureRecognizerCalendarPrev = new TapGestureRecognizer();
			tapGestureRecognizerCalendarPrev.Tapped += (s, e) => btnPrev_Clicked(s, e);
			lblPrev.GestureRecognizers.Add(tapGestureRecognizerCalendarPrev);

			var tapGestureRecognizerCalendarNext = new TapGestureRecognizer();
			tapGestureRecognizerCalendarNext.Tapped += (s, e) => btnNext_Clicked(s, e);
			lblNext.GestureRecognizers.Add(tapGestureRecognizerCalendarNext);
		}
		protected Tuple<double, double> FindCoordinatesFromCountryName(string countryName)
		{
			return DependencyService.Get<ILocationPicker>().FindCoordinatesFromCountryName(countryName);
		}
		private void LoadMap()
		{
			map = new Map
			{
				IsShowingUser = true,
				HeightRequest = 100,
				WidthRequest = 960,
				MapType = MapType.Satellite,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			// You can use MapSpan.FromCenterAndRadius 
			//map.MoveToRegion (MapSpan.FromCenterAndRadius (new Position (37, -122), Distance.FromMiles (0.3)));
			// or create a new MapSpan object directly
			map.MoveToRegion (new MapSpan(new Position(0,0), 360, 360) );
			//			map.MoveToRegion (MapSpan.FromCenterAndRadius (
			//				new Position (21.1473790, 72.7599300), Distance.FromMiles (3))); // Santa Cruz golf course
			var countryList = (new CommonDatabase()).GetTotalRegistrationCountsByCountry();
			foreach (var item in countryList)
			{
				var latplnagResult = FindCoordinatesFromCountryName(item.CountryName);
				var position = new Position(latplnagResult.Item1, latplnagResult.Item2); // Latitude, Longitude
				var pin = new Pin
				{
					Type = PinType.SearchResult,
					Position = position,
					Label = item.CountryName,
					Address = Convert.ToString(item.Count)
				};

				map.Pins.Add(pin);
			}
			var positionPin = new Position(20.5937, 78.9629); // Latitude, Longitude
			var pinobj = new Pin
			{
				Type = PinType.Generic,
			Position = positionPin,
			Label = "145",
			Address="India"
			};
			map.Pins.Add(pinobj);


			// create buttons
			/*
			var morePins = new Button { Text = "Add more pins" };
			morePins.Clicked += (sender, e) => {
				map.Pins.Add (new Pin {
					Position = new Position (36.9641949, -122.0177232),
					Label = "Boardwalk"
				});
				map.Pins.Add (new Pin {
					Position = new Position (36.9571571, -122.0173544),
					Label = "Wharf"
				});
				map.MoveToRegion (MapSpan.FromCenterAndRadius (
					new Position (36.9628066, -122.0194722), Distance.FromMiles (1.5)));
			};
			var reLocate = new Button { Text = "Re-center" };
			reLocate.Clicked += (sender, e) => {
				map.MoveToRegion (MapSpan.FromCenterAndRadius (
					new Position (21.1473790, 72.7599300), Distance.FromMiles (3)));
			};
			var buttons = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					morePins, reLocate
				}
			};
			*/
			// put the page together
			stkMap.Children.Add(map);
			//stkMap.Children.Add (buttons);
			//			Content = new StackLayout { 
			//				Spacing = 0,
			//				Children = {
			//					map,
			//					buttons
			//				}};
		}

		protected void NavigateBackCommand(object sender, EventArgs e)
		{
			Navigation.PopModalAsync(true);
		}
		protected void NavigatePicCommand(object sender, EventArgs e)
		{
			loader.IsVisible = true;
			AddRegistration addRegistration = new AddRegistration();
			Navigation.PushModalAsync(addRegistration);
			loader.IsVisible = false;
		}

		protected void lblMap_Click(object sender, EventArgs e)
		{
			

			stkMap.IsVisible = true;
			stkGraph.IsVisible = false;
			stkStatistics.IsVisible = false;

			imgMap.IsVisible = true;
			imgGraph.IsVisible = false;
			imgStatistics.IsVisible = false;
		}

		protected void lblGraph_Click(object sender, EventArgs e)
		{
			stkMap.IsVisible = false;
			stkGraph.IsVisible = true;
			stkStatistics.IsVisible = false;

			imgMap.IsVisible = false;
			imgGraph.IsVisible = true;
			imgStatistics.IsVisible = false;

			stkGraphDraw.Children.Clear();
			startYear = 2011;
			yearInterval = 10;
			endYear = startYear + yearInterval;
			lblYear.Text = startYear.ToString() + " - " + endYear.ToString();
			List<ChartData> objChartData = new List<ChartData>();
			for (int i = startYear; i <= endYear; i++)
			{

				objChartData.Add(new ChartData() { chartKey = i });
			}
			lsRegistrations = viewModel.GetAllRegistrations();
			lsAirportVisit = viewModel.GetAllAirportVisits();
			var result = viewModel.GetAllRegistrationsInYearRange(lsRegistrations, objChartData);
			var result1 = viewModel.GetAllAirportVisitsInYearRange(lsAirportVisit, objChartData);
			LoadOxyPlotChartBinding("Annual Registrations", result);
			LoadOxyPlotChartBinding("Annual Airport Visit", result1);
		}

		protected void btnNext_Clicked(object sender, EventArgs e)
		{
			stkGraphDraw.Children.Clear();
			startYear = startYear + yearInterval;
			endYear = endYear + yearInterval;
			lblYear.Text = startYear.ToString() + " - " + endYear.ToString();
			List<ChartData> objChartData = new List<ChartData>();
			for (int i = startYear; i <= endYear; i++)
			{

				objChartData.Add(new ChartData() { chartKey = i });
			}
			var result = viewModel.GetAllRegistrationsInYearRange(lsRegistrations, objChartData);
			var result1 = viewModel.GetAllAirportVisitsInYearRange(lsAirportVisit, objChartData);
			LoadOxyPlotChartBinding("Annual registrations", result);
			LoadOxyPlotChartBinding("Annual Airport Visit", result1);

		}

		protected void btnPrev_Clicked(object sender, EventArgs e)
		{
			stkGraphDraw.Children.Clear();
			startYear = startYear - yearInterval;
			endYear = endYear - yearInterval;
			lblYear.Text = startYear.ToString() + " - " + endYear.ToString();
			List<ChartData> objChartData = new List<ChartData>();
			for (int i = startYear; i <= endYear; i++)
			{

				objChartData.Add(new ChartData() { chartKey = i });
			}
			var result = viewModel.GetAllRegistrationsInYearRange(lsRegistrations, objChartData);
			var result1 = viewModel.GetAllAirportVisitsInYearRange(lsAirportVisit, objChartData);
			LoadOxyPlotChartBinding("Annual registrations", result);
			LoadOxyPlotChartBinding("Annual Airport Visit", result1);
		}
		private void LoadOxyPlotChartBinding(string title, List<ChartData> objChartData)
		{
			var view = new PlotView();
			view.HeightRequest = 200;
			//view.WidthRequest = 300;
			var plotModel = new PlotModel
			{
				Title = title,

			};
			var categoryAxis = new CategoryAxis
			{
				Position = AxisPosition.Bottom,
				TextColor = OxyColor.Parse("#cfcfcf"),
				IsPanEnabled = false,
				Angle = 90,
			};
			foreach (var item in objChartData)
			{
				categoryAxis.Labels.Add(item.chartKey.ToString());
			}
			/*
			for (int i = 2011; i < 2020; i++) {
				categoryAxis.Labels.Add (i.ToString());
			}
			*/
			var valueAxis = new LinearAxis
			{
				Position = AxisPosition.Left,
				TextColor = OxyColor.Parse("#cfcfcf"),
				IsPanEnabled = false,
				IntervalLength = 15,

			};
			//valueAxis.ExtraGridlines = new double[]{ 200, 400, 600, 800, 1000, 1200 };
			valueAxis.ExtraGridlineColor = OxyColor.Parse("#cfcfcf");

			plotModel.Axes.Add(categoryAxis);
			plotModel.Axes.Add(valueAxis);
			var series = new ColumnSeries();
			foreach (var item in objChartData)
			{
				series.Items.Add(new ColumnItem { Value = item.dataCount, Color = OxyColor.Parse("#24AFE3"), });
			}

			/*
			series.Items.Add (new ColumnItem { Value = 1200, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 400, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 650, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 100, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 880, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 1300, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 100, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 420, Color = OxyColor.Parse ("#24AFE3") });
			series.Items.Add (new ColumnItem { Value = 620, Color = OxyColor.Parse ("#24AFE3") });
			*/
			//series.Items.Add(new ColumnItem { Value = 0 });

			plotModel.Series.Add(series);
			plotModel.PlotAreaBorderColor = OxyColor.Parse("#24AFE3");
			view.Model = plotModel;
			stkGraphDraw.Children.Add(view);
		}


		private void LoadOxyPlotChart(string title)
		{
			var view = new PlotView();
			view.HeightRequest = 200;
			//view.WidthRequest = 300;
			var plotModel = new PlotModel
			{
				Title = title,

			};
			var categoryAxis = new CategoryAxis
			{
				Position = AxisPosition.Bottom,
				TextColor = OxyColor.Parse("#cfcfcf"),
				IsPanEnabled = false
			};
			for (int i = 2000; i < 2010; i++)
			{
				categoryAxis.Labels.Add(i.ToString());
			}
			/*
			categoryAxis.Labels.Add ("1988");
			categoryAxis.Labels.Add ("1989");
			categoryAxis.Labels.Add ("1990");
			categoryAxis.Labels.Add ("1991");
			categoryAxis.Labels.Add ("1992");
			categoryAxis.Labels.Add ("1993");
			categoryAxis.Labels.Add ("1991");
			categoryAxis.Labels.Add ("1992");
			categoryAxis.Labels.Add ("1993");
			*/
			var valueAxis = new LinearAxis
			{
				Position = AxisPosition.Left,
				TextColor = OxyColor.Parse("#cfcfcf"),
				IsPanEnabled = false,
				IntervalLength = 15
			};
			valueAxis.ExtraGridlines = new double[] { 200, 400, 600, 800, 1000, 1200 };
			valueAxis.ExtraGridlineColor = OxyColor.Parse("#cfcfcf");

			plotModel.Axes.Add(categoryAxis);
			plotModel.Axes.Add(valueAxis);
			var series = new ColumnSeries();
			series.Items.Add(new ColumnItem { Value = 1200, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 400, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 650, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 100, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 880, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 1300, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 100, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 420, Color = OxyColor.Parse("#24AFE3") });
			series.Items.Add(new ColumnItem { Value = 620, Color = OxyColor.Parse("#24AFE3") });

			//series.Items.Add(new ColumnItem { Value = 0 });

			plotModel.Series.Add(series);
			plotModel.PlotAreaBorderColor = OxyColor.Parse("#24AFE3");
			view.Model = plotModel;
			stkGraphDraw.Children.Add(view);
		}

		protected void lblStatistics_Click(object sender, EventArgs e)
		{
			stkMap.IsVisible = false;
			stkGraph.IsVisible = false;
			stkStatistics.IsVisible = true;

			imgMap.IsVisible = false;
			imgGraph.IsVisible = false;
			imgStatistics.IsVisible = true;
		}

		protected void lblShowManufacturerdetails_Click(object sender, EventArgs e)
		{
			FillDetailGrid("Manufacturer");
			ShowDetailPopup();
		}

		protected void lblShowAircraftDetails_Click(object sender, EventArgs e)
		{
			FillDetailGrid("Aircraft");
			ShowDetailPopup();
		}

		protected void lblShowAirlineDetails_Click(object sender, EventArgs e)
		{
			FillDetailGrid("Airline");
			ShowDetailPopup();
		}

		protected void lblShowAirportDetails_Click(object sender, EventArgs e)
		{
			FillDetailGrid("Airport");
			ShowDetailPopup();
		}

		protected void lblShowAirportVisitDetails_Click(object sender, EventArgs e)
		{
			FillDetailGrid("AirportVisit");
			ShowDetailPopup();
		}

		protected void lblCountriesDetails_Click(object sender, EventArgs e)
		{
			FillDetailGrid("Countries");
			ShowDetailPopup();
		}

		protected void lblAirforceDetails_Click(object sender, EventArgs e)
		{

			FillDetailGrid("Airforce");
			ShowDetailPopup();
		}

		protected void btnClosePopup_Click(object sender, EventArgs e)
		{

			stkDetailPopup.IsVisible = false;
		}

		private void ShowDetailPopup()
		{
			stkDetailPopup.IsVisible = true;
		}

		private void LoadData()
		{

			var Totalcount = viewModel.GetTotaoCounts();
			lblTotalRegistrations.Text = Totalcount.TotalRegistrations.ToString();
			lblTotalUniqueRegistration.Text = Totalcount.TotalUniqueRegistrations.ToString();
			lblTotalCivilAircraft.Text = Totalcount.TotalCivilRegistrations.ToString();
			lblTotalMilitayAircraft.Text = Totalcount.TotalMilitaryRegistrations.ToString();
			lblTotalThisYear.Text = Totalcount.ThisYearTotalRegistrations.ToString();
			BindList();
		}
		public class temp
		{
			public int lblId { get; set; }
			public string lblName { get; set; }
			public int lblCount { get; set; }
			public temp(int id, string name, int count)
			{
				this.lblId = id;
				this.lblName = name;
				this.lblCount = count;
			}
		}
		private void BindList()
		{

			int id = 0;

			List<temp> list1 = new List<temp>();
			List<KeyValuePair<string, int>> DetailList1 = new List<KeyValuePair<string, int>>();
			DetailList1 = viewModel.GetManufacturersWiseRegistrations(5);

			foreach (var a in DetailList1)
			{
				id++;
				list1.Add(new temp(id, a.Key, a.Value));
			}
			id = 0;
			LstManufacturerList.ItemsSource = list1;

			List<temp> list2 = new List<temp>();
			List<KeyValuePair<string, int>> DetailList2 = new List<KeyValuePair<string, int>>();
			DetailList2 = viewModel.GetAircraftsWiseRegistrations(5);
			foreach (var a in DetailList2)
			{
				id++;
				list2.Add(new temp(id, a.Key, a.Value));
			}
			id = 0;
			LstAircraftList.ItemsSource = list2;

			List<temp> list3 = new List<temp>();
			List<KeyValuePair<string, int>> DetailList3 = new List<KeyValuePair<string, int>>();
			DetailList3 = viewModel.GetAirlinesWiseRegistrations(5);
			foreach (var a in DetailList3)
			{
				id++;
				list3.Add(new temp(id, a.Key, a.Value));
			}
			id = 0;
			LstAirlineList.ItemsSource = list3;

			List<temp> list4 = new List<temp>();
			List<KeyValuePair<string, int>> DetailList4 = new List<KeyValuePair<string, int>>();
			DetailList4 = viewModel.GetAirportWiseRegistrations(5);
			foreach (var a in DetailList4)
			{
				id++;
				list4.Add(new temp(id, a.Key, a.Value));
			}
			id = 0;
			LstAirportList.ItemsSource = list4;

			List<temp> list5 = new List<temp>();
			List<KeyValuePair<string, int>> DetailList5 = new List<KeyValuePair<string, int>>();
			DetailList5 = viewModel.GetAirportVisitWiseRegistrations(5);
			foreach (var a in DetailList5)
			{
				id++;
				list5.Add(new temp(id, a.Key, a.Value));
			}
			id = 0;
			LstAirportVisitList.ItemsSource = list5;

			List<temp> list6 = new List<temp>();
			List<KeyValuePair<string, int>> DetailList6 = new List<KeyValuePair<string, int>>();
			DetailList6 = viewModel.GetContrieWiseRegistrations(5);
			foreach (var a in DetailList6)
			{
				id++;
				list6.Add(new temp(id, a.Key, a.Value));
			}
			id = 0;
			LstCountriesList.ItemsSource = list6;

			List<temp> list7 = new List<temp>();
			List<KeyValuePair<string, int>> DetailList7 = new List<KeyValuePair<string, int>>();
			DetailList7 = viewModel.GetAirforceWiseRegistrations(5);
			foreach (var a in DetailList7)
			{
				id++;
				list7.Add(new temp(id, a.Key, a.Value));
			}
			id = 0;
			LstAirforceList.ItemsSource = list7;

			//LstAirportList.ItemsSource = viewModel.GetAirportWiseRegistrations (5);
			//LstAirportVisitList.ItemsSource = viewModel.GetAirportVisitWiseRegistrations (5);
			//LstCountriesList.ItemsSource = viewModel.GetContrieWiseRegistrations (5);
			//LstAirforceList.ItemsSource = viewModel.GetAirforceWiseRegistrations (5);
		}

		private void FillDetailGrid(string DetailFor)
		{
			lblManufacturer1.IsVisible = false;
			lblAircraft1.IsVisible = false;
			lblAirline1.IsVisible = false;
			lblAirport1.IsVisible = false;
			lblAirportVisit1.IsVisible = false;
			lblCountries1.IsVisible = false;
			lblAirforce1.IsVisible = false;

			List<KeyValuePair<string, int>> DetailList = new List<KeyValuePair<string, int>>();
			int id = 0;
			switch (DetailFor)
			{
				case "Manufacturer":
					List<temp> list1 = new List<temp>();
					List<KeyValuePair<string, int>> DetailList1 = new List<KeyValuePair<string, int>>();
					DetailList1 = viewModel.GetManufacturersWiseRegistrations();

					foreach (var a in DetailList1)
					{
						id++;
						list1.Add(new temp(id, a.Key, a.Value));
					}
					id = 0;
					lblManufacturer1.IsVisible = true;
					LstShowAllList.ItemsSource = list1;
					break;
				case "Aircraft":
					List<temp> list2 = new List<temp>();
					List<KeyValuePair<string, int>> DetailList2 = new List<KeyValuePair<string, int>>();
					DetailList2 = viewModel.GetAircraftsWiseRegistrations();
					foreach (var a in DetailList2)
					{
						id++;
						list2.Add(new temp(id, a.Key, a.Value));
					}
					id = 0;
					LstShowAllList.ItemsSource = list2;
					lblAircraft1.IsVisible = true;
					break;
				case "Airline":
					List<temp> list3 = new List<temp>();
					List<KeyValuePair<string, int>> DetailList3 = new List<KeyValuePair<string, int>>();
					DetailList3 = viewModel.GetAirlinesWiseRegistrations();
					foreach (var a in DetailList3)
					{
						id++;
						list3.Add(new temp(id, a.Key, a.Value));
					}
					id = 0;
					lblAirline1.IsVisible = true;
					LstShowAllList.ItemsSource = list3;
					break;
				case "Airport":
					List<temp> list4 = new List<temp>();
					List<KeyValuePair<string, int>> DetailList4 = new List<KeyValuePair<string, int>>();
					DetailList4 = viewModel.GetAirportWiseRegistrations();
					foreach (var a in DetailList4)
					{
						id++;
						list4.Add(new temp(id, a.Key, a.Value));
					}
					id = 0;
					lblAirport1.IsVisible = true;
					LstShowAllList.ItemsSource = list4;
					break;
				case "AirportVisit":
					List<temp> list5 = new List<temp>();
					List<KeyValuePair<string, int>> DetailList5 = new List<KeyValuePair<string, int>>();
					DetailList5 = viewModel.GetAirportVisitWiseRegistrations();
					foreach (var a in DetailList5)
					{
						id++;
						list5.Add(new temp(id, a.Key, a.Value));
					}
					id = 0;
					lblAirportVisit1.IsVisible = true;
					LstShowAllList.ItemsSource = list5;

					break;
				case "Countries":
					List<temp> list6 = new List<temp>();
					List<KeyValuePair<string, int>> DetailList6 = new List<KeyValuePair<string, int>>();
					DetailList6 = viewModel.GetContrieWiseRegistrations();
					foreach (var a in DetailList6)
					{
						id++;
						list6.Add(new temp(id, a.Key, a.Value));
					}
					id = 0;
					lblCountries1.IsVisible = true;
					LstShowAllList.ItemsSource = list6;
					break;
				case "Airforce":
					
					List<temp> list7 = new List<temp>();
					List<KeyValuePair<string, int>> DetailList7 = new List<KeyValuePair<string, int>>();
					DetailList7 = viewModel.GetAirforceWiseRegistrations();
					foreach (var a in DetailList7)
					{
						id++;
						list7.Add(new temp(id, a.Key, a.Value));
					}
					id = 0;
					lblAirforce1.IsVisible = true;
					LstShowAllList.ItemsSource = list7;

					break;
			}
		}

	}
}

