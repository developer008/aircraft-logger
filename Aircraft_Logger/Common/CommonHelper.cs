﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Aircraft_Logger
{
	public static class CommonHelper
	{
		//public static string BaseUrl = "http://localhost:7777";

		public static string BaseUrl = "http://54.85.156.64";
	//	public static string BaseUrl = "http://lanetteam.com:8041/aircraftlogger/";

		public static bool IsConnected = true;

		public static int LastTimeSync = 0;

		public static int FirstTImeCome = 0;

		public static bool IsSyncWorking = false;

	
		#region InternetChecking & Sync


		public static async Task SetSyncSetting ()
		{
			var cancellationTokenSource = new CancellationTokenSource ();
			await Task.Run (() => FirstTimeDataSync ());
			return;
//			await Task.Factory.StartNew ((j) => FirstTimeDataSync ()
//				, TaskCreationOptions.LongRunning
//				, cancellationTokenSource.Token);  
			
//			var userStatus = new UserDatabase ();
//			var syncStatus = userStatus.CheckUserSyncStatus (Account.UserId);
//			if (syncStatus) {
//				//CommonHelper.FirstTImeCome = 0;
//
//				var cancellationTokenSource = new CancellationTokenSource ();
//				await Task.Factory.StartNew ((j) => FirstTimeDataSync ()
//					, TaskCreationOptions.LongRunning
//					, cancellationTokenSource.Token);  
//			} else {
//
//				var cancellationTokenSource = new CancellationTokenSource ();
//				await Task.Factory.StartNew ((j) =>  CheckInternetRunCounter ()
//					, TaskCreationOptions.LongRunning
//					, cancellationTokenSource.Token);  
//
//			}

		}

		//Continous check with internet and once get it sync with live data
		public static async Task CheckInternetRunCounter ()
		{

			DBSyncDatabase dbSync = new DBSyncDatabase ();

			try {
				if (CommonHelper.IsSyncWorking == false) {
					if (CommonHelper.IsConnected) {
						if (CommonHelper.LastTimeSync != 1) {
							CommonHelper.LastTimeSync = 1;
							CommonHelper.IsSyncWorking = true;
							dbSync.ManageDatabase ();
							CommonHelper.IsSyncWorking = false;
							//DisplayAlert("Internet","Let's start sync","Cancel");
						}

					} else {
						CommonHelper.LastTimeSync = 0;
						//DisplayAlert("Internet","Internet Not Available","Cancel");
					}


					await Task.Delay (30000);
				}
			} catch (Exception ex) {

			}


		}

		//If user first time install application in mobile then this menthod will call.
		public static async Task FirstTimeDataSync ()
		{
			
			DBSyncDatabase dbSync = new DBSyncDatabase ();
			if (CommonHelper.IsConnected) {
				if (CommonHelper.LastTimeSync != 1) {
					CommonHelper.LastTimeSync = 1;
					//loader.IsVisible = true;
				 	//await	Task.Delay(30000);
					await dbSync.ManageDatabase ();
					var userDb = new UserDatabase ();
					userDb.UpdateUserStatus (Account.UserId);
					//loader.IsVisible = false;
					//DisplayAlert("Internet","Let's start sync","Cancel");
				}

			} else {
				CommonHelper.LastTimeSync = 0;
				//DisplayAlert("Internet","Internet Not Available","Cancel");
			}
			return;
		}

		public static string ConvertLocationToDMS (Xamarin.Forms.Maps.Position location)
		{
			string dmsLocation = string.Empty;
			double latitide_degrees = location.Latitude; 
			double longitude_degrees = location.Longitude; 

			double minutes;
			double seconds;
			double tenths;

			// set decimal_degrees value here

			minutes = (latitide_degrees - Math.Floor (latitide_degrees)) * 60.0; 
			seconds = (minutes - Math.Floor (minutes)) * 60.0;
			tenths = (seconds - Math.Floor (seconds)) * 10.0;
			// get rid of fractional part
			minutes = Math.Floor (minutes);
			seconds = Math.Floor (seconds);
			tenths = Math.Floor (tenths);

			dmsLocation += ConvertLocationToDMS (minutes.ToString () + ":" + seconds.ToString () + ":" + tenths.ToString (), "la");

			minutes = (longitude_degrees - Math.Floor (longitude_degrees)) * 60.0; 
			seconds = (minutes - Math.Floor (minutes)) * 60.0;
			tenths = (seconds - Math.Floor (seconds)) * 10.0;
			// get rid of fractional part
			minutes = Math.Floor (minutes);
			seconds = Math.Floor (seconds);
			tenths = Math.Floor (tenths);

			dmsLocation += ":"+ConvertLocationToDMS (minutes.ToString () + ":" + seconds.ToString () + ":" + tenths.ToString (), "lo");

			return dmsLocation;

		}

		public static string ConvertLocationToDMS (string location, string locationType)
		{
			
			string[] dmsArray = location.Split (':');
			string DMSLocation = string.Empty;
			string ewns = string.Empty;

			if (locationType == "lo") {
				if (Convert.ToInt32 (dmsArray [0]) > 0)
					ewns = "E";
				else
					ewns = "W";
			} else {
				if (Convert.ToInt32 (dmsArray [0]) > 0)
					ewns = "N";
				else
					ewns = "S";
			}

			DMSLocation = string.Format ("{0}#{1}#{2}#{3}", dmsArray [0], dmsArray [1], dmsArray [2].Split ('.') [0], ewns);
			return DMSLocation;
		}

		#endregion

		public enum TabelName
		{
			tblAircraft,
			tblAirForces,
			tblAirlines,
			tblAirports,
			tblContinents,
			tblCountries,
			tblManufacturers,
			tblPhotos,
			tblRegistrations,
			tblUnits,
			tblUser,
			tblVisits
		}
	}
}

