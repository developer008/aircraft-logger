﻿using System;
using System.Collections.Generic;

namespace Aircraft_Logger
{
	public class DBSyncClass
	{
		public DBSyncClass()
		{
		}
		public List<tblAircraft> Aircraft {
			get;
			set;
		}

		public List<tblAirForces> AirForces {
			get;
			set;
		}

		public List<tblAirlines> Airlines {
			get;
			set;
		}

		public List<tblAirports> Airports {
			get;
			set;
		}

		public List<tblCountries> Countries {
			get;
			set;
		}

		public List<tblManufacturers> Manufacturers {
			get;
			set;
		}

		public List<tblPhotos> Photos {
			get;
			set;
		}

		public List<tblRegistrations> Registrations {
			get;
			set;
		}

		public List<tblSearches> Searches {
			get;
			set;
		}

		public List<tblUnits> Units {
			get;
			set;
		}

		public List<tblUser> User {
			get;
			set;
		}

		public List<tblUserStatistics> UserStatistics {
			get;
			set;
		}

		public List<tblVisits> Visits {
			get;
			set;
		}
	}
}

