﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace Aircraft_Logger
{
	public class SearchAPI
	{
		public SearchAPI ()
		{
		}

		public async Task<List<APIAircrafttypeSearch>> SearchAircrafttype (string basictype = ""
									  , string fulltype = "")
		{
			List<APIAircrafttypeSearch> MyList = new List<APIAircrafttypeSearch> ();
			try {
				string parameter = string.Empty;
				parameter += basictype == string.Empty ? "" : string.Format ("basictype/{0}/", basictype);
				parameter += fulltype == string.Empty ? "" : string.Format ("fulltype/{0}/", fulltype);

				using (var client = new HttpClient ()) {
					var url = string.Format ("http://www.scramble.nl/webservices_v2/index.php/api/aircrafttype/search/format/json/" + parameter);
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
						MyList = JsonConvert.DeserializeObject<List<APIAircrafttypeSearch>> (resp.Content.ReadAsStringAsync ().Result);
					}
					return MyList;
				}

			} catch (Exception ex) {
				return MyList;
			}
		}

		public async Task<List<APICivilSearch>> SearchCivilDB (string registration = ""
								 , string constrno = ""
								 , string Operator = ""
							     , string airtype = ""
								 , string currentaircraft = "")
		{
			List<APICivilSearch> MyList = new List<APICivilSearch> ();
			try {
				string parameter = string.Empty;
				parameter += registration == string.Empty ? "" : string.Format ("registration/{0}/", registration);
				parameter += constrno == string.Empty ? "" : string.Format ("constrno/{0}/", constrno);
				parameter += Operator == string.Empty ? "" : string.Format ("Operator/{0}/", Operator);
				parameter += airtype == string.Empty ? "" : string.Format ("airtype/{0}/", airtype);
				parameter += currentaircraft == string.Empty ? "" : string.Format ("currentaircraft/{0}/", currentaircraft);

				using (var client = new HttpClient ()) {
					var url = string.Format ("http://www.scramble.nl/webservices_v2/index.php/api/civildb/search/format/json/" + parameter);
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
						MyList = JsonConvert.DeserializeObject<List<APICivilSearch>> (resp.Content.ReadAsStringAsync ().Result);
					}
					return MyList;
				}

			} catch (Exception ex) {
				return MyList;
			}
		}

		public async Task<List<APIMilitaryDatabaseSearch>> SearchMilitaryDatabase (string dbName
										  ,	string CountryCode = ""
										  , string AirforceCode = ""
										  , string AirForceName = ""
										  , string continent = "")
		{
			List<APIMilitaryDatabaseSearch> MyList = new List<APIMilitaryDatabaseSearch> ();
			try {
				string parameter = string.Empty;
				parameter += CountryCode == string.Empty ? "" : string.Format ("CountryCode/{0}/", CountryCode);
				parameter += AirforceCode == string.Empty ? "" : string.Format ("AirforceCode/{0}/", AirforceCode);
				parameter += AirForceName == string.Empty ? "" : string.Format ("AirForceName/{0}/", AirForceName);
				parameter += continent == string.Empty ? "" : string.Format ("continent/{0}/", continent);

				using (var client = new HttpClient ()) {
					//http://www.scramble.nl/webservices_v2/index.php/api/militarydb/search/af/in/serial/K5012
					//var url = string.Format ("http://www.scramble.nl/webservices_v2/index.php/api/" + dbName + "/search/format/json/" + parameter);
					var url = "http://www.scramble.nl/webservices_v2/index.php/api/militarydb/search/format/json/af/in/serial/";
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
						MyList = JsonConvert.DeserializeObject<List<APIMilitaryDatabaseSearch>> (resp.Content.ReadAsStringAsync ().Result);
					}
					return MyList;
				}

			} catch (Exception ex) {
				return MyList;
			}
		}

		public async Task<List<APImilitarydbSearch>> SearchMilitaryDb (string af = ""
			, string serial = ""
			, string code = ""
			, string type = ""
			, string unit = ""
			, string constrno = ""
			, string status = "")
		{
			
			List<APImilitarydbSearch> MyList = new List<APImilitarydbSearch> ();
			try {
				string parameter = string.Empty;
				parameter += af == string.Empty ? "" : string.Format ("af/{0}/", af);
				parameter += serial == string.Empty ? "" : string.Format ("serial/{0}/", serial);
				parameter += code == string.Empty ? "" : string.Format ("code/{0}/", code);
				parameter += type == string.Empty ? "" : string.Format ("type/{0}/", type);
				parameter += unit == string.Empty ? "" : string.Format ("unit/{0}/", unit);
				parameter += constrno == string.Empty ? "" : string.Format ("constrno/{0}/", constrno);
				parameter += status == string.Empty ? "" : string.Format ("status/{0}/", status);

				using (var client = new HttpClient ()) {
//					var url = string.Format ("http://www.scramble.nl/webservices_v2/index.php/api/militarydb/search/format/json/" + parameter);
					var url = "http://www.scramble.nl/webservices_v2/index.php/api/militarydb/search/format/json/af/in/serial/K5012";
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
						MyList = JsonConvert.DeserializeObject<List<APImilitarydbSearch>> (resp.Content.ReadAsStringAsync ().Result);
					}
					return MyList;
				}

			} catch (Exception ex) {
				return MyList;
			}

		}

		public async Task<List<APImilitarydbSearch>> SearchMilitaryDbByCountry (string countryCode = "", string registrationNo = "")
		{

			List<APImilitarydbSearch> MyList = new List<APImilitarydbSearch> ();
			try {


				using (var client = new HttpClient ()) {
					//					var url = string.Format ("http://www.scramble.nl/webservices_v2/index.php/api/militarydb/search/format/json/" + parameter);
					var url = String.Format("http://www.scramble.nl/webservices_v2/index.php/api/militarydb/search/format/json/af/{0}/serial/{1}",countryCode,registrationNo);
					//var url = "http://www.scramble.nl/webservices_v2/index.php/api/militarydb/search/format/json/af/in/serial/K5012";
					var resp = await client.GetAsync (url);

					if (resp.IsSuccessStatusCode) {
						MyList = JsonConvert.DeserializeObject<List<APImilitarydbSearch>> (resp.Content.ReadAsStringAsync ().Result);
					}
					return MyList;
				}

			} catch (Exception ex) {
				return MyList;
			}

		}

		public async Task<List<APICountrydbSearch>> SearchCountryDb ()
		{
			string baseUrl = CommonHelper.BaseUrl;
			List<APICountrydbSearch> MyList = new List<APICountrydbSearch> ();
			try {
//				MyList.Add (new APICountrydbSearch (){ Country = "India", Code = "IN" });
//				MyList.Add (new APICountrydbSearch (){ Country = "Finland", Code = "FI" });
//				MyList.Add (new APICountrydbSearch (){ Country = "Netherlands", Code = "NL" });
				var countryList = (new CountriesDatabase()).GetCountriesList();
				foreach(var item in countryList)
				{
					MyList.Add (new APICountrydbSearch (){CountryID=item.fldCountryID, Country = item.fldCountryName, Code = item.fldScrambleMilitaryCode });
				}



				return MyList;
			} catch (Exception ex) {
				return MyList;
			}
		}
	}

	public class APIAircrafttypeSearch
	{
		public string basictype { get; set; }

		public string fulltype { get; set; }
	}

	public class APICivilSearch
	{
		public string Registration { get; set; }

		public string Type { get; set; }

		public string FullType { get; set; }

		public string Cnr { get; set; }

		public string Line { get; set; }

		public string Owner { get; set; }

		public string Datum { get; set; }

		public string Remarks { get; set; }

		public string Status { get; set; }

		public string email { get; set; }

	}

	public class APIMilitaryDatabaseSearch
	{
		public string CountryCode { get; set; }

		public string AirforceCode { get; set; }

		public string AirForceName { get; set; }

		public string SerialExample { get; set; }

		public string CodeExample { get; set; }

		public string TypeExample { get; set; }

		public string UnitExample { get; set; }

		public string TableName { get; set; }

		public string continent { get; set; }
	}

	public class APImilitarydbSearch
	{
		public string Serial { get; set; }

		public string Code { get; set; }

		public string Type { get; set; }

		public string CN { get; set; }

		public string Unit { get; set; }

		public string Status { get; set; }

		public string First { get; set; }

		public string Last { get; set; }

		public string Comment { get; set; }

		public string email { get; set; }
	}

	public class APICountrydbSearch
	{
		public string Country { get; set; }

		public string Code { get; set; }

		public string CountryID{ get; set;}

	}

}

