﻿using System;
using Xamarin.Forms;

namespace Aircraft_Logger
{
	public class EntryControl: Entry {

		public EntryControlAlignMent CAlignMent {
			get;
			set;
		}

		public EntryControlBorderStyle CBorderStyle {
			get;
			set;
		}
		public float CBorderWidth {
			get;
			set;
		}

		public Color CBorderColor {
			get;
			set;
		}
		public float CCornerRadius {
			get;
			set;
		}

		public string CFont {
			get;
			set;
		}

		public EntryControlFontStyle CFontStyle {
			get;
			set;
		}

		public float CFontSize {
			get;
			set;
		}
	}

	public enum EntryControlFontStyle
	{
		Regular,
		Bold,
		Italic
	}

	public enum EntryControlAlignMent
	{
		Start,
		Center,
		End
	}

	public enum EntryControlBorderStyle
	{
		Bezel,
		Line,
		None,
		RoundedRect
	}
}

