﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Aircraft_Logger
{

	public class SegmentedControl : View, IViewContainer<SegmentedControlOption>
	{
		public IList<SegmentedControlOption> Children { get; set; }

		public SegmentedControl ()
		{
			Children = new List<SegmentedControlOption> ();
		}

		public event ValueChangedEventHandler ValueChanged;

		public delegate void ValueChangedEventHandler (object sender, EventArgs e);

		public Action<bool> eventSetValue;

		private bool selectedValue;

		public bool SelectedValue {
			get{ return selectedValue; }
			set {
				selectedValue = value;
				if (ValueChanged != null)
					ValueChanged (this, EventArgs.Empty);
				if(eventSetValue!=null)
					eventSetValue(value);
			}
		}

		private float segmentWidth;

		public float SegmentWidth
		{
			get{ return segmentWidth; }
			set {
				segmentWidth = value;
			}
		}

		public void setValue(bool value)
		{
		}
	}

	public class SegmentedControlOption:View
	{
		public static readonly BindableProperty TextProperty = BindableProperty.Create<SegmentedControlOption, string> (p => p.Text, "");

		public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create<SegmentedControlOption, bool> (p => p.IsSelected, false);

		public string Text {
			get{ return (string)GetValue (TextProperty); }
			set{ SetValue (TextProperty, value); }
		}

		public bool IsSelected {
			get{ return (bool)GetValue (IsSelectedProperty); }
			set{ SetValue (IsSelectedProperty, value); }
		}

		public SegmentedControlOption ()
		{
		}
	}


}

