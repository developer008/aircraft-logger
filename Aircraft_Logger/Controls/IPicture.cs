﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Aircraft_Logger
{
	public  interface IPicture
	{
		Task SavePictureToDisk (ImageSource imgSrc, string Id, float maxWidth, float maxHeight);
		Task<ImageSource> ResizePicture (ImageSource imgSrc, float maxWidth, float maxHeight);
		event Action<bool,string> SavePictureComplete;
		Task<string> GetPictureFromDisk (string id);
		string GetPicture (string id);
		string GetPictureBase64 (string id);
		void ChoosePhoto();
		void TakePicture();
		void SavePicture(string imageName,string Directory,System.IO.Stream image);
		event Action<System.IO.Stream> GetImageEvent;
		Task DownloadFileFromUrl (string FileName, string uri);

	}
}

