﻿using System;
using Xamarin.Forms;

namespace Aircraft_Logger
{
	public class CImage:Image
	{
		public CImage ()
		{
		}

		public string ImageID {
			get;
			set;
		}

		public object Tag {
			get;
			set;
		}

		public bool IsPrimary {
			get;
			set;
		}
	}
}

