﻿using System;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;

namespace Aircraft_Logger
{
	public interface ILocationPicker
	{
		void GetCurrentLocation ();
		void StopListening();
		Tuple<double, double> FindCoordinatesFromCountryName(string countryName);
		event Action< Position > ChangeLoation ;
	}
}

