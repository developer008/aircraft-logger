﻿using System;
using Xamarin.Forms;

namespace Aircraft_Logger
{
	public class CustomViewCell
	{
		public CustomViewCell()
		{

		}
	}
	public class RegistrationDetailViewCell : ViewCell
	{
		public RegistrationDetailViewCell()
		{
			var regNoLabel = new Label()
			{
				FontSize = 11,
				TextColor = Color.Black,
				WidthRequest = 50,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand

			};
			regNoLabel.SetBinding(Label.TextProperty, "Registration");
			var stkRegNo = new StackLayout()
			{
				Spacing = 0,
				//WidthRequest = 70
			};
			stkRegNo.Children.Add(regNoLabel);

			var aircraftNameLabel = new Label()
			{
				FontSize = 11,
				TextColor = Color.Black,
				WidthRequest = 100,

				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
			};
			aircraftNameLabel.SetBinding(Label.TextProperty, "AircraftName");
			var stkAircraftName = new StackLayout()
			{
				Spacing = 0,
				//WidthRequest = 120
			};
			stkAircraftName.Children.Add(aircraftNameLabel);


			var airlineNameLabel = new Label()
			{
				FontSize = 11,
				TextColor = Color.Black,
				WidthRequest = 100,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				HeightRequest = 12,
			};
			airlineNameLabel.SetBinding(Label.TextProperty, "AirlineName");
			var stkAirlineName = new StackLayout()
			{
				Spacing = 0,
				WidthRequest = 120
			};
			stkAirlineName.Children.Add(airlineNameLabel);

			var arrowLabel = new Label()
			{
				FontSize = 11,
				TextColor = Color.FromHex("#DDD"),
				WidthRequest = 10,
				Text = ">",
				HorizontalOptions = LayoutOptions.EndAndExpand,

			};
			var stkArrow = new StackLayout()
			{
				Spacing = 0,
				WidthRequest = 120
			};
			stkArrow.Children.Add(arrowLabel);


			var cellLayout = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(0, 3, 0, 3),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
				//Children = { regNoLabel,aircraftNameLabel,airlineNameLabel,arrowLabel}

			};

			Grid grid = new Grid
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions=LayoutOptions.FillAndExpand,
				RowDefinitions =
				{
					//new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
					new RowDefinition { Height = GridLength.Auto }
				},
				ColumnDefinitions =
				{
					new ColumnDefinition { Width = new GridLength(100,GridUnitType.Star)},
					new ColumnDefinition { Width = new GridLength(100,GridUnitType.Star)},
					new ColumnDefinition { Width = new GridLength(100,GridUnitType.Star)},
					new ColumnDefinition { Width = new GridLength(20,GridUnitType.Absolute)}
				}
			};
			grid.Children.Add(stkRegNo, 0,0);
			grid.Children.Add(stkAircraftName, 1, 0);
			grid.Children.Add(stkAirlineName, 2, 0);
			grid.Children.Add(arrowLabel, 3, 0);

			cellLayout.Children.Add(regNoLabel);
			cellLayout.Children.Add(aircraftNameLabel);
			cellLayout.Children.Add(airlineNameLabel);
			cellLayout.Children.Add(arrowLabel);

			this.View = cellLayout;
		}
	}
}

