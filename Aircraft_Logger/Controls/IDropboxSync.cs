﻿using System;
using System.Threading.Tasks;

namespace Aircraft_Logger
{
	public interface IDropboxSync
	{
		bool CheckAuthentication ();

		void SyncDopbox();

		void DownloadFile (string FileName, string DropboxPath, string LocalPath);

		void UploadFile(string FileName , string LocalPath , string DropboxPath);

		void ChooseDropBoxPhoto(string DropboxPath);

	}
}

