﻿using System;

namespace Aircraft_Logger
{
	public interface ILoader
	{
		void DisplayLoader ();

		void HideLoader ();
	}
}

