﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using Dropbox.CoreApi.iOS;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;
using Xamarin.Forms;
using Dropins.Chooser.iOS;

namespace Aircraft_Logger.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			#region Resolver Init
			SimpleContainer container = new SimpleContainer ();
			container.Register<IDevice> (t => AppleDevice.CurrentDevice);
			container.Register<IDisplay> (t => t.Resolve<IDevice> ().Display);
			container.Register<INetwork> (t => t.Resolve<IDevice> ().Network);
			Resolver.SetResolver (container.GetResolver ());
			#endregion

			global::Xamarin.Forms.Forms.Init ();
			Xamarin.FormsMaps.Init ();
			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start ();
			new XLabs.Forms.Charting.Controls.ChartRenderer ();
			OxyPlot.Xamarin.Forms.Platform.iOS.Forms.Init ();
			#endif

			NetworkStatus internetStatus = Reachability.InternetConnectionStatus();
			if (internetStatus == NetworkStatus.NotReachable) {
				CommonHelper.IsConnected=false;
			
			} else {
				CommonHelper.IsConnected=true;
			}
			Reachability.ReachabilityChanged +=	(object sender, EventArgs e) => {
				//NetworkStatus internetStatus = Reachability.InternetConnectionStatus();
				if (internetStatus == NetworkStatus.NotReachable) {
					CommonHelper.IsConnected=false;
					CommonHelper.LastTimeSync =0;
					CommonHelper.IsSyncWorking=false;
				} else {
					CommonHelper.IsConnected=true;
					CommonHelper.CheckInternetRunCounter ();
				}
			};
			FileAccessHelper.GetLocalFilePath ("SpottedAircraft.sqlite");
			LoadApplication (new App ());

			return base.FinishedLaunching (app, options);
		}

		public override bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			if (Session.SharedSession.HandleOpenUrl (url) && Session.SharedSession.IsLinked) {
				new UIAlertView ("Successful", "Your App has been linked successfully!", null, "OK", null).Show ();
				NSNotificationCenter.DefaultCenter.PostNotificationName (MainViewController.UpdateViewNotification, null);
				if (DropboxCredentials.Window != null) {
					//DropboxCredentials.Window.Dispose ();
					UIApplication.SharedApplication.Windows [0].Hidden = true;
					DropboxCredentials.IsWindowOpen = false;
				}
			}
			if (DBChooser.DefaultChooser.HandleOpenUrl (url)) {
				// This was a Chooser response and handleOpenURL automatically ran the
				// completion handler	
				//return true;
				new UIAlertView ("Successful", "Your App has been linked successfully!", null, "OK", null).Show ();
				NSNotificationCenter.DefaultCenter.PostNotificationName (MainViewController.UpdateViewNotification, null);
				if (DropboxCredentials.Window != null) {
					//DropboxCredentials.Window.Dispose ();
					UIApplication.SharedApplication.Windows [0].Hidden = true;
					DropboxCredentials.IsWindowOpen = false;
				}
			}
			return true;
		}

		CounterServices longRunningTaskExample;

		void WireUpLongRunningTask ()
		{
			MessagingCenter.Subscribe<StartLongRunningTaskMessage> (this, "StartLongRunningTaskMessage", async message => {
				longRunningTaskExample = new CounterServices ();
				await longRunningTaskExample.Start ();
			});

			MessagingCenter.Subscribe<StopLongRunningTaskMessage> (this, "StopLongRunningTaskMessage", message => {
				longRunningTaskExample.Stop ();
			});
		}
	}
}

