﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

using MonoTouch.Dialog;
using Dropbox.CoreApi.iOS;
using Dropins.Chooser.iOS;
using System.Threading.Tasks;
using Xamarin.Forms;


#if __UNIFIED__
using Foundation;
using UIKit;

#else
using MonoTouch.Foundation;
using MonoTouch.UIKit;
#endif

namespace Aircraft_Logger.iOS
{
	public partial class MainViewController : DialogViewController
	{
		public const string UpdateViewNotification = "UpdateView";

		//		UIBarButtonItem btnLink;
		RestClient restClient;

		public MainViewController () : base (UITableViewStyle.Grouped, null)
		{
			Root = new RootElement ("Dropbox Core Api iOS Sample");

		}

		public override void ViewDidLoad ()
		{
			
			base.ViewDidLoad ();


		}

		public bool IsConnected ()
		{
			try {
				return Session.SharedSession.IsLinked;
//					Session.SharedSession.UnlinkAll();
//					return 	 false;
			} catch (Exception ex) {
				return false;
			}
		}

		public void ConnectSession ()
		{
			try {
				Session.SharedSession.AuthorizationFailureReceived += (object sender, SessionAuthorizationFailureReceivedEventArgs e) => {

				};
				Session.SharedSession.LinkFromController (this);
			} catch (Exception ex) {
				
			}
		}
		/*
		public void UploadFile (string FileName, string Directory, string DropboxPath)
		{
			if (Session.SharedSession.IsLinked) {

				if (Root.Count != 0)
					return;

				// Create the rest client that handle the download of all the files
				if (restClient == null) {
					restClient = new RestClient (Session.SharedSession);
					// Method to get the images
					restClient.MetadataLoaded += GetImages;
					// Handle if something went wrong when tried to get the images
					restClient.LoadMetadataFailed += (sender, e) =>
					{
						new UIAlertView ("Error getting images", e.Error.Description, null, "OK", null).Show ();
					};

					restClient.DeletePathFailed+= (object sender, RestClientErrorEventArgs e) => {
						int i = 0;
					};

					restClient.FileLoaded += (object sender, RestClientFileLoadedEventArgs e) => {
						int i = 0;
					};


					restClient.LoadDeltaFailed += (object sender, RestClientErrorEventArgs e) => {
						int i = 0;
					};
					restClient.SearchResultsLoaded += (object sender, RestClientSearchResultsLoadedEventArgs e) => 	{
						if(e.Results.Count()>0)
						{
							try {
								var _documentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.MyPictures);
								var _directoryname = System.IO.Path.Combine (_documentsDirectory, "AircraftLogger");
								if (System.IO.Directory.Exists (_directoryname) == false) {
									System.IO.Directory.CreateDirectory (_directoryname);
								}
								string _jpgFilename = System.IO.Path.Combine (
									_directoryname, "ef8055b0-ad70-49ed-9307-f171af52635e.jpg");
								restClient.DeletePath(e.Results[0].Path );
								restClient.LoadFile(e.Results[0].Path ,_jpgFilename.Replace(".jpg","_Copy.jpg"));
							} catch (Exception ex) {
								int i = 0;
							}
						}
						//restClient.DeletePath();
					};

					restClient.SearchFailed += (object sender, RestClientErrorEventArgs e) => {
						int i = 0;
					};
				}
				// Get all the files from your folder

				//string libFolder = Path.Combine (Path.GetTempPath (), "AircraftLogger");
				//
				//if (!Directory.Exists (libFolder)) {
				//	Directory.CreateDirectory (libFolder);
				//}
				//
				//string dbPath = Path.Combine (libFolder, "default.txt");
				//
				//var file = File.Create (dbPath);
				//file.Write (file,0,1000);

				var documentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.MyPictures);
				var directoryname = System.IO.Path.Combine (documentsDirectory, Directory);
				if (System.IO.Directory.Exists (directoryname) == false) {
					System.IO.Directory.CreateDirectory (directoryname);
				}
				string jpgFilename = System.IO.Path.Combine (
					                     directoryname, FileName);

				if (File.Exists (jpgFilename))
					restClient.UploadFile (FileName, DropboxPath, null, jpgFilename);
				
				restClient.SearchPath (DropboxPath, "b97790c7-8fd5-4b99-805d-f5f16ef9e858.jpg");

			} else {
			}
		}
		*/


		public void  ChooseDropBoxPhoto( string DropboxPath)
		{
			var strTempPath = string.Empty;
			if (Session.SharedSession.IsLinked) {

//				if (Root.Count != 0)
//					return;
				if (restClient == null) {
					restClient = new RestClient (Session.SharedSession);
					// Method to get the images
//					restClient.MetadataLoaded += GetImages;

		

				
					restClient.LoadMetadata (DropboxPath);
					restClient.MetadataLoaded +=  (object sender, RestClientMetadataLoadedEventArgs e) => {
						// If the path is a file, there is nothing to search
					
						if (!e.Metadata.IsDirectory) {
							new UIAlertView ("Not a directory", "The specified path is a file, not a directory", null, "OK", null).Show ();
							return;
						}

						var imagesName = new Section ("Images from Dropbox");
						foreach (var item in e.Metadata.Contents) {
							// Work with each metadata file or folder
							restClient.LoadStreamableURLForFile (item.Path);
							restClient.StreamableUrlLoaded += (object sender1, RestClientStreamableUrlLoadedEventArgs e1) => {

							};
						}
						if (imagesName.Count == 0) {

							new UIAlertView ("No images found", "Please add some images to the Dropbox Folder", null, "OK", null).Show ();
						} else {
							//Root.Add (imagesName);
						   	//return strTempPath;
						
						}
//						DropboxCredentials.Window.MakeKeyAndVisible ();
//						DropboxCredentials.IsWindowOpen = true;
					};


				}
			}

		}
		public void UploadFile (string FileName, string Directory, string DropboxPath)
		{
			if (Session.SharedSession.IsLinked) {

				if (Root.Count != 0)
					return;

				// Create the rest client that handle the download of all the files
				if (restClient == null) {
					restClient = new RestClient (Session.SharedSession);
					// Method to get the images
					//restClient.MetadataLoaded += GetImages;
					// Handle if something went wrong when tried to get the images
					restClient.LoadMetadataFailed += (sender, e) =>
					{
						new UIAlertView ("Error getting images", e.Error.Description, null, "OK", null).Show ();
					};

					restClient.DeletePathFailed+= (object sender, RestClientErrorEventArgs e) => {
						int i = 0;
					};

					restClient.FileLoaded += (object sender, RestClientFileLoadedEventArgs e) => {
						int i = 0;
					};

					restClient.LoadDeltaFailed += (object sender, RestClientErrorEventArgs e) => {
						int i = 0;
					};
					restClient.SearchResultsLoaded += (object sender, RestClientSearchResultsLoadedEventArgs e) => {
						if(e.Results.Count()>0)
						{
							try {
								var _documentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.MyPictures);
								var _directoryname = System.IO.Path.Combine (_documentsDirectory, "AircraftLogger");
								if (System.IO.Directory.Exists (_directoryname) == false) {
									System.IO.Directory.CreateDirectory (_directoryname);
								}
								string _jpgFilename = System.IO.Path.Combine (
									_directoryname, "ef8055b0-ad70-49ed-9307-f171af52635e.jpg");
								restClient.DeletePath(e.Results[0].Path );
								restClient.LoadFile(e.Results[0].Path ,_jpgFilename.Replace(".jpg","_Copy.jpg"));
							} catch (Exception ex) {
								int i = 0;
							}
						}
						//restClient.DeletePath();
					};

					restClient.SearchFailed += (object sender, RestClientErrorEventArgs e) => {
						int i = 0;
					};
				}
				// Get all the files from your folder


				//var documentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.MyPictures);
				var documentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
				//var directoryname = System.IO.Path.Combine (documentsDirectory, Directory);
				var directoryname =Path.Combine (documentsDirectory, "Photos","Xlarge");

				if (System.IO.Directory.Exists (directoryname) == false) {
					System.IO.Directory.CreateDirectory (directoryname);
				}
				string jpgFilename = System.IO.Path.Combine (
					directoryname, FileName);

				if (File.Exists (jpgFilename))
					restClient.UploadFile (FileName, DropboxPath, null, jpgFilename);


				restClient.SearchPath (DropboxPath, "b97790c7-8fd5-4b99-805d-f5f16ef9e858.jpg");

			} else {
			}
		}
		public void DownloadFile (string FileName, string DropboxPath, string LocalPath)
		{
			
		}

		// Verifies that the App Key is in the plist file
		bool VerifyInfoPlist ()
		{
			var plistDict = NSDictionary.FromFile (NSBundle.MainBundle.PathForResource ("Info", "plist"));
			var urlTypes = plistDict ["CFBundleURLTypes"] as NSArray;
			var urlSchemes = urlTypes.GetItem<NSDictionary> (0) ["CFBundleURLSchemes"] as NSArray;

			if (urlSchemes.GetItem<NSString> (0) == "db-APP_KEY") {
				new UIAlertView ("DB App Key missing in Info.plist", "Set your DB App Key in your Url Scheme in Info.plist", null, "OK", null).Show ();
				return false;
			}

			return true;
		}

		#region RestClient Events

		// Get all the images from the Dropbox folder
		void GetImages (object sender, RestClientMetadataLoadedEventArgs e)
		{
			// If the path is a file, there is nothing to search
			if (!e.Metadata.IsDirectory) {
				new UIAlertView ("Not a directory", "The specified path is a file, not a directory", null, "OK", null).Show ();
				return;
			}

			var imagesName = new Section ("Images from Dropbox");
			foreach (var item in e.Metadata.Contents) {
				// Work with each metadata file or folder

				imagesName.Add (new UIView(){BackgroundColor=UIColor.Red});
				//var header = new UIImageView (Xamarin.Forms.Image.s.FromFile ("sample.png"));
				//imagesName.Add (header);
			}
		

			if (imagesName.Count == 0) {

				new UIAlertView ("No images found", "Please add some images to the Dropbox Folder", null, "OK", null).Show ();
			} else {
				Root.Add (imagesName);
			}

			DropboxCredentials.Window.MakeKeyAndVisible ();
			DropboxCredentials.IsWindowOpen = true;
		}

		#endregion
	}
}
