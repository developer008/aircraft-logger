﻿using System;
using System.IO;
using Foundation;

namespace Aircraft_Logger.iOS
{
	public class FileAccessHelper
	{
		public static string GetLocalFilePath (string filename)
		{
			string docFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			string libFolder = Path.Combine (docFolder, "..", "Library", "Databases");

			if (!Directory.Exists (libFolder)) {
				Directory.CreateDirectory (libFolder);
			}

			string dbPath = Path.Combine (libFolder, filename);

			CopyDatabaseIfNotExists (dbPath);
			CopyImagesIfNotExists ();
			return dbPath;
		}

		private static void CopyImagesIfNotExists ()
		{
			string PresonalPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			PresonalPath = Path.Combine (PresonalPath, "Photos");

			if (!Directory.Exists (PresonalPath)) {
				Directory.CreateDirectory (PresonalPath);
			}

			string docFolder = NSBundle.MainBundle.ResourcePath;
			/*
			string libFolder = Path.Combine (docFolder, "2015", "11");
			if (Directory.Exists (PresonalPath)) {
				var files = Directory.GetFiles (libFolder);
				foreach (var file in files) {
					File.Copy (file, Path.Combine (PresonalPath, Path.GetFileName (file)), true);
				}
			}
			*/
		}

		private static void CopyDatabaseIfNotExists (string dbPath)
		{

//			var existingDb = NSBundle.MainBundle.PathForResource ("SpottedAircraft", "sqlite");
//    		File.Copy (dbPath, existingDb, true);
	
			if (!File.Exists (dbPath)) {
				var existingDb = NSBundle.MainBundle.PathForResource ("SpottedAircraft", "sqlite");
				File.Copy (existingDb, dbPath);
			}
//			var existingDb = NSBundle.MainBundle.PathForResource ("SpottedAircraft", "sqlite");
//			File.Copy (existingDb, dbPath, true);

		}
	}
}

