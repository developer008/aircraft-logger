﻿using System;
using Xamarin.Forms;
using UIKit;
using Dropbox.CoreApi.iOS;
using Dropins.Chooser.iOS;
using System.Threading.Tasks;

[assembly: Dependency (typeof(Aircraft_Logger.iOS.IDropboxSync_iOS))]
namespace Aircraft_Logger.iOS
{
	public static class DropboxCredentials
	{
//		public static string AppKey = "vsdj8zpv3m07j0s";
//		public static string AppSecret = "99onao7d2ghhmij";
		public static string AppKey = "qzp3p744jnznnhi";
		public static string AppSecret = "p96nmr5loiydwxo";
		public static string FolderPath = "/AircraftLogger/Photos/";

		public static UIWindow Window {
			get;
			set;
		}

		public static bool IsWindowOpen {
			get;
			set;
		}
	}

	public class IDropboxSync_iOS:IDropboxSync
	{
		UINavigationController navigationController;
		MainViewController mainVC;
		//
		//		public UIWindow Window {
		//			get;
		//			set;
		//		}

		public IDropboxSync_iOS ()
		{
			
			mainVC = new MainViewController ();
			navigationController = new UINavigationController (mainVC);
			DropboxCredentials.Window = new UIWindow (UIScreen.MainScreen.Bounds);
			DropboxCredentials.Window.RootViewController = navigationController;
			var session = new Session (DropboxCredentials.AppKey, DropboxCredentials.AppSecret, Session.RootDropbox);

			Session.SharedSession = session;

			session.AuthorizationFailureReceived += (sender, e) => {
				if (DropboxCredentials.IsWindowOpen == false) {
					// You can try again with the link if something went wrong
					var alertView = new UIAlertView ("Dropbox Session Ended", "Do you want to relink?", null, "No, thanks", new [] { "Yes, please" });
					alertView.Clicked += (avSender, avE) => {
						if (avE.ButtonIndex == 0)
							return;

						// Try to link the app
				
						DropboxCredentials.Window.MakeKeyAndVisible ();
						DropboxCredentials.IsWindowOpen = true;
						Session.SharedSession.LinkUserId (e.UserId, mainVC);
					
					};
					alertView.Show ();
				}
			};


		}

		public bool CheckAuthentication ()
		{
			return mainVC.IsConnected ();
		}

		public void SyncDopbox ()
		{
			if (!mainVC.IsConnected () && DropboxCredentials.IsWindowOpen == false) {
				DropboxCredentials.Window.MakeKeyAndVisible ();
				DropboxCredentials.IsWindowOpen = true;
				mainVC.ConnectSession ();
			}
		}

		public void DownloadFile (string FileName, string DropboxPath, string LocalPath)
		{
			mainVC.DownloadFile (FileName, DropboxPath, LocalPath);
		}

		public void UploadFile (string FileName, string LocalPath, string DropboxPath)
		{
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			mainVC.UploadFile (FileName, documentsDirectory, DropboxPath);
		}

		public void ChooseDropBoxPhoto ( string DropboxPath)
		{
		   mainVC.ChooseDropBoxPhoto (DropboxPath);
		}

	}
}

