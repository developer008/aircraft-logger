﻿using System;
using Aircraft_Logger.iOS;
using UIKit;
using AssetsLibrary;
using Foundation;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Net;
using System.Text;

[assembly: Xamarin.Forms.Dependency (typeof(PictureRenderer))]
namespace Aircraft_Logger.iOS
{
	public class PictureRenderer:IPicture
	{
		static	CameraViewModel cameraOps = new CameraViewModel ();
		static UIImagePickerController imagePicker;
		static	UIWindow window;

		public event Action<System.IO.Stream> GetImageEvent;

		public event Action<bool,string> SavePictureComplete;

		public PictureRenderer ()
		{
		}

		public void Init ()
		{
			if (imagePicker != null)
				return;

			imagePicker = new UIImagePickerController ();
			imagePicker.Delegate = new CameraDelegate ();
		}

		public void Dismiss ()
		{

			window = new UIWindow ();
			window.Dispose ();
			//GetImageEvent (null);
		}

		public class CameraDelegate : UIImagePickerControllerDelegate
		{
			ALAssetsLibrary library = new ALAssetsLibrary ();
			string albumName = "PlanLogger";
			bool albumWasFound = false;

			public override void FinishedPickingMedia (UIImagePickerController picker, NSDictionary info)
			{

				picker.DismissModalViewController (true);

				var photo = info.ValueForKey (new NSString ("UIImagePickerControllerOriginalImage")) as UIImage;
				var meta = info.ValueForKey (new NSString ("UIImagePickerControllerMediaMetadata")) as NSDictionary;
				if (meta != null) {
					// This bit of code saves to the Photo Album with metadata
					library.WriteImageToSavedPhotosAlbum (photo.CGImage, meta, (assetUrl, error) => {
						if (error != null)
							return;
						//SaveImageToAlbum (assetUrl, albumName);
					});
					// dismiss the picker
					window.Dispose ();
					imagePicker.DismissModalViewController (true);
					cameraOps.ImageStreamSource = photo.AsPNG ().AsStream ();
					//GetImageEvent (cameraOps.ImageStreamSource);
					//Picture.CallBack(photo.AsPNG().AsStream());

				}		

				//cb (info);
			}

			public void SaveImageToAlbum (NSUrl assetURL, string albumName)
			{
				try {
					library.Enumerate (ALAssetsGroupType.Album, (ALAssetsGroup group, ref bool libStop) => {
						if (group != null) {
							var groupName = group.Name;
							if (groupName.ToString ().ToLower () == albumName.ToLower ()) {
								albumWasFound = true;
								if (group != null) {
									library.AssetForUrl (assetURL, (assetSuccess) => {
										group.AddAsset (assetSuccess);
									}, (assetError) => {

									});

								}
							} else {
							}

						}
						if (albumWasFound == false && group == null) {
							//Console.WriteLine ("assetUrl:" + assetUrl);
							library.AddAssetsGroupAlbum (albumName, (assetGroup) => {

								library.AssetForUrl (assetURL, (assetSuccess) => {
									assetGroup.AddAsset (assetSuccess);
								}, (assetError) => {

								});
							}, (asseterror) => {
								Console.WriteLine ("fail");

							});
						}
					}, error => {
					});
				} catch (Exception ex) {
					
				}

			}
		}

		public void TakePicture ()
		{
			Init ();

			imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;
			imagePicker.ShowsCameraControls = true;

			UINavigationBar.Appearance.TintColor = UIColor.FromRGB (29, 131, 219);
			window = new UIWindow (UIScreen.MainScreen.Bounds);	
			window.BackgroundColor = UIColor.White;
			window.Bounds = UIScreen.MainScreen.Bounds;

			window.RootViewController = imagePicker;
			window.MakeKeyAndVisible ();
			//GetImageEvent (cameraOps.ImageStreamSource);

		}

		public void ChoosePhoto ()
		{
			// create a new picker controller
			imagePicker = new UIImagePickerController ();
			imagePicker.Title = "Select Image";

			// set our source to the photo library
			imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;

			// set what media types
			imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes (UIImagePickerControllerSourceType.PhotoLibrary);

			imagePicker.FinishedPickingMedia += Handle_FinishedPickingMedia;
			imagePicker.Canceled += Handle_Canceled;

			UINavigationBar.Appearance.TintColor = UIColor.FromRGB (29, 131, 219);
			window = new UIWindow (UIScreen.MainScreen.Bounds);	
			window.BackgroundColor = UIColor.White;
			window.Bounds = UIScreen.MainScreen.Bounds;

			window.RootViewController = imagePicker;
			window.MakeKeyAndVisible ();

		}
		// Do something when the
		void Handle_Canceled (object sender, EventArgs e)
		{
			Console.WriteLine ("picker cancelled");
			imagePicker.DismissModalViewController (true);
			window.Dispose ();
		}
		// This is a sample method that handles the FinishedPickingMediaEvent
		protected void Handle_FinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
		{
			cameraOps = new CameraViewModel ();
			// determine what was selected, video or image
			bool isImage = false;
			switch (e.Info [UIImagePickerController.MediaType].ToString ()) {
			case "public.image":
				Console.WriteLine ("Image selected");
				isImage = true;
				break;

			case "public.video":
				Console.WriteLine ("Video selected");
				break;
			}

			Console.Write ("Reference URL: [" + UIImagePickerController.ReferenceUrl + "]");

			// get common info (shared between images and video)
			NSUrl referenceURL = e.Info [new NSString ("UIImagePickerControllerReferenceUrl")] as NSUrl;
			if (referenceURL != null)
				Console.WriteLine (referenceURL.ToString ());

			// if it was an image, get the other image info
			if (isImage) {

				// get the original image
				UIImage originalImage = e.Info [UIImagePickerController.OriginalImage] as UIImage;

				if (originalImage != null) {

//					originalImage.SaveToPhotosAlbum ((image, error) => {
//						if (error == null) {
//							Console.WriteLine (image);
//
//							new UIAlertView ("", "Saved", null, "OK", null).Show ();
//						} else
//							new UIAlertView ("", "Failed", null, "OK", null).Show ();
//					});

					// do something with the image
					Console.WriteLine ("got the original image");
					cameraOps.ImageStreamSource = originalImage.AsPNG ().AsStream ();
					//imageView.Image = originalImage;
				}

				// get the edited image
				UIImage editedImage = e.Info [UIImagePickerController.EditedImage] as UIImage;
				if (editedImage != null) {
					// do something with the image
					Console.WriteLine ("got the edited image");
					cameraOps.ImageStreamSource = editedImage.AsPNG ().AsStream ();
					//imageView.Image = editedImage;
				}

				//- get the image metadata
				NSDictionary imageMetadata = e.Info [UIImagePickerController.MediaMetadata] as NSDictionary;
				if (imageMetadata != null) {
					// do something with the metadata
					//imageMetadatasave = imageMetadata;
					Console.WriteLine ("got image metadata");
				}

			}
			// if it's a video
			else {
				// get video url
				NSUrl mediaURL = e.Info [UIImagePickerController.MediaURL] as NSUrl;
				if (mediaURL != null) {
					//
					Console.WriteLine (mediaURL.ToString ());
				}
			}

			// dismiss the picker
			imagePicker.DismissModalViewController (true);
			window.Dispose ();
			GetImageEvent (cameraOps.ImageStreamSource);


		}

		public async void SavePicture (string imageName, string Directory, System.IO.Stream image)
		{
			var documentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.MyPictures);
			var directoryname = System.IO.Path.Combine (documentsDirectory, Directory);
			if (System.IO.Directory.Exists (directoryname) == false) {
				System.IO.Directory.CreateDirectory (directoryname);
			}
			string jpgFilename = System.IO.Path.Combine (
				                     directoryname, imageName + ".jpg");
			
			//var file = System.IO.File.Exists(jpgFilename);

			NSData imgData = NSData.FromStream (image);
			NSError err = null;
			if (imgData.Save (jpgFilename, false, out err)) {
				Console.WriteLine ("saved as " + jpgFilename);
			} else {
				Console.WriteLine ("NOT saved as " + jpgFilename +
				" because" + err.LocalizedDescription);
			}
		}

		public async Task<ImageSource> ResizePicture (ImageSource imgSrc, float maxWidth, float maxHeight)
		{
			
				var renderer = new StreamImagesourceHandler();
				UIImage photo = await renderer.LoadImageAsync(imgSrc);
				photo = ResizeImage(photo, maxWidth, maxHeight);
				//var bitmap = qrEncoder.Write(text);
				return Xamarin.Forms.ImageSource.FromStream(() => photo.AsJPEG().AsStream());
				//return renderer.

		}

	


		public async Task SavePictureToDisk (ImageSource imgSrc, string Id, float maxWidth, float maxHeight )
		{
			try {
				var renderer = new StreamImagesourceHandler ();
				var photo = await renderer.LoadImageAsync (imgSrc);

				photo = ResizeImage(photo,maxWidth,maxHeight);

				var documentsDirectory = Environment.GetFolderPath
					(Environment.SpecialFolder.Personal);
				documentsDirectory = Path.Combine (documentsDirectory, "Photos","Xlarge");
				string jpgFilename = System.IO.Path.Combine (
					                     documentsDirectory, Id.ToString () + ".jpg");

				if(!Directory.Exists(documentsDirectory))
				{
					Directory.CreateDirectory(documentsDirectory);
				}
				if(File.Exists(jpgFilename))
					return;
				NSData imgData = photo.AsJPEG ();
				NSError err = null;
				if (imgData.Save (jpgFilename, false, out err)) {
					Console.WriteLine ("saved as " + jpgFilename);
					if (SavePictureComplete != null)
						SavePictureComplete (true, jpgFilename);
				} else {
					Console.WriteLine ("NOT saved as " + jpgFilename +
					" because" + err.LocalizedDescription);
					if (SavePictureComplete != null)
						SavePictureComplete (false, "");
				}
			} catch (Exception ex) {
				
			}
		}

		public UIImage MaxResizeImage(UIImage sourceImage, float maxWidth, float maxHeight)
		{
			var sourceSize = sourceImage.Size;
			var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
			if (maxResizeFactor > 1) return sourceImage;
			var width = maxResizeFactor * sourceSize.Width;
			var height = maxResizeFactor * sourceSize.Height;
			UIGraphics.BeginImageContext(new SizeF( Convert.ToSingle( width), Convert.ToSingle(height)));
			sourceImage.Draw(new RectangleF(0, 0, Convert.ToSingle(width), Convert.ToSingle(height)));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

		// resize the image (without trying to maintain aspect ratio)
		public UIImage ResizeImage(UIImage sourceImage, float width, float height)
		{
			UIGraphics.BeginImageContext(new SizeF((float)width, (float)height));
			sourceImage.Draw(new RectangleF(0, 0, (float)width, (float)height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

		// crop the image, without resizing
		private UIImage CropImage(UIImage sourceImage, int crop_x, int crop_y, int width, int height)
		{
			var imgSize = sourceImage.Size;
			UIGraphics.BeginImageContext(new SizeF(width, height));
			var context = UIGraphics.GetCurrentContext();
			var clippedRect = new RectangleF(0, 0, width, height);
			context.ClipToRect(clippedRect);
			var drawRect = new RectangleF(-crop_x, -crop_y, (float)imgSize.Width, (float)imgSize.Height);
			sourceImage.Draw(drawRect);
			var modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return modifiedImage;
		}

		public async Task<string> GetPictureFromDisk (string Id)
		{
			if (Id == null)
				return "";
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			documentsDirectory = Path.Combine (documentsDirectory, "Photos","Xlarge");
			string jpgFilename = System.IO.Path.Combine (documentsDirectory, (Id + (Id.Contains(".jpg")?"":".jpg")));
			return jpgFilename;
		}

		public string GetPicture (string Id)
		{
			
			if (Id == null)
				return "";
			string jpgFilename = string.Empty;
			if (!string.IsNullOrEmpty (Id) && Id.Contains (".jpg")) 
			{
				var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				documentsDirectory = Path.Combine (documentsDirectory, "Photos", "Xlarge");
				jpgFilename = System.IO.Path.Combine (documentsDirectory, Id);
				return jpgFilename;	
			} 
			else 
			{
				var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				documentsDirectory = Path.Combine (documentsDirectory, "Photos", "Xlarge");
				jpgFilename = System.IO.Path.Combine (documentsDirectory, (Id + (Id.Contains (".jpg") ? "" : ".jpg")));
				return jpgFilename;	
			}

		}

		public string GetPictureBase64 (string Id)
		{

			if (Id == null)
				return "";
			string jpgFilename = string.Empty;
			if (!string.IsNullOrEmpty (Id) && Id.Contains (".jpg")) 
			{
				var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				documentsDirectory = Path.Combine (documentsDirectory, "Photos", "Xlarge");
				jpgFilename = System.IO.Path.Combine (documentsDirectory, Id);
				// provide read access to the file
				FileStream fs = new FileStream(jpgFilename, FileMode.Open,FileAccess.Read);
				// Create a byte array of file stream length
				byte[] ImageData = new byte[fs.Length];
				//Read block of bytes from stream into the byte array
				fs.Read(ImageData,0,System.Convert.ToInt32(fs.Length));
				//Close the File Stream
				fs.Close();
				string _base64String = Convert.ToBase64String (ImageData);
				return _base64String;	
			} 
			else 
			{
				var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				documentsDirectory = Path.Combine (documentsDirectory, "Photos", "Xlarge");
				jpgFilename = System.IO.Path.Combine (documentsDirectory, (Id + (Id.Contains (".jpg") ? "" : ".jpg")));
				// provide read access to the file
				FileStream fs = new FileStream(jpgFilename, FileMode.Open,FileAccess.Read);
				// Create a byte array of file stream length
				byte[] ImageData = new byte[fs.Length];
				//Read block of bytes from stream into the byte array
				fs.Read(ImageData,0,System.Convert.ToInt32(fs.Length));
				//Close the File Stream
				fs.Close();
				string _base64String = Convert.ToBase64String (ImageData);
				return _base64String;	

			}

		}
		public async Task DownloadFileFromUrl (string FileName, string uri)
		{
			try {
				FileName=FileName+".jpg";
				string documentsPath =Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				string localPath = Path.Combine (documentsPath,"Photos","Xlarge");
				if (!Directory.Exists (localPath))
					Directory.CreateDirectory (localPath);
				string FilePath = Path.Combine (localPath, FileName);
				if (!File.Exists (FilePath)) {
					var webClient = new WebClient ();
					var url = new Uri (uri+"/"+FileName); 
					// Html home page
					webClient.Encoding = Encoding.UTF8;
					var bytes = await webClient.DownloadDataTaskAsync (url);
					File.WriteAllBytes (FilePath, bytes); // writes to local storage
					Console.WriteLine ("Download : " + FileName);
				}
				return;
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
		}


	}
}

