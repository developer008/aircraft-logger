using System;
using Aircraft_Logger;
using Aircraft_Logger.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(pickerControl), typeof(picker_ios))]
namespace Aircraft_Logger.iOS
{

	public class picker_ios : PickerRenderer
	{


		public picker_ios()
		{

		}
		protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{

				var view = (pickerControl)Element;
				string titlepicker = view.Title;
				var mytext = new NSAttributedString(titlepicker, new UIStringAttributes() { ForegroundColor = UIColor.White });
				Color temp = view.textColor;
				this.Control.TextColor = temp.ToUIColor();
				this.Control.TintColor = view.tintColor.ToUIColor();
				this.Control.AttributedText = mytext;
				this.Control.BorderStyle = UITextBorderStyle.None;
			}
		}
	}
}

