﻿using System;
using Dropins.Chooser.iOS;
using Aircraft_Logger.iOS;
using UIKit;

[assembly: Xamarin.Forms.Dependency (typeof(DropboxChooserRender))]
namespace Aircraft_Logger.iOS
{
	public class DropboxChooserRender:IDropboxChooser
	{
		public DropboxChooserRender ()
		{
		}
		public void ChooseDropBoxPhoto ()
		{
			
			UIApplication.SharedApplication.Windows [0].Hidden = false;
			DBChooser.DefaultChooser.OpenChooser (DBChooserLinkType.Direct, UIApplication.SharedApplication.Windows[0].RootViewController, (results) => {
				// results is null if the user cancels
				if (results == null)
				{
					//UIApplication.SharedApplication.Windows [0].Hidden = true;
					new UIAlertView ("Cancelled", "User cancelled!", null, "Continue").Show ();
				}
				// Do something with the results
				else {
					string fileName = results[0].Name;
					new UIAlertView ("File Selected", fileName , null, "Continue").Show ();
				} 
			});

		}
	}
}

