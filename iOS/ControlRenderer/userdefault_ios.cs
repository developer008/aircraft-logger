﻿using System;
using Foundation;
using Xamarin.Forms;
using MonoTouch;
using UIKit;
using Photos;
using System.Drawing;

[assembly: Dependency(typeof(Aircraft_Logger.iOS.userdefault_ios))]
namespace Aircraft_Logger.iOS
{
	public class userdefault_ios : userdefault
	{
		ImageSource imageSource;
		public ImageSource getlastimage()
		{
			try
			{
				

				var asset = PHAsset.FetchAssets(PHAssetMediaType.Image, null).LastObject as PHAsset;
				var thumbnail = new UIImage();
				PHImageManager imgmanage = new PHImageManager();
				CoreGraphics.CGSize size = new CoreGraphics.CGSize();
				size.Height = 200;
				size.Width = 400;
				PHImageRequestOptions option1 = new PHImageRequestOptions();
	
				PHImageContentMode mod = PHImageContentMode.AspectFill;

				option1.Synchronous = true;
				imgmanage.RequestImageForAsset(asset, size, mod, option1, (UIImage result, NSDictionary info) => { thumbnail = result; });

				var count = PHAsset.FetchAssets(PHAssetMediaType.Image, null).Count;
		
				UIImageView _tmpImageView = new UIImageView(thumbnail);
				using (System.IO.Stream imageStream = _tmpImageView.Image.AsPNG().AsStream())
				{
					imageSource = Xamarin.Forms.ImageSource.FromStream(() => imageStream);
				//	Pages.MyPage.MyImage.Source = imageSource;
				}

			UIGraphics.BeginImageContext(new SizeF((float)300, (float)200));
				thumbnail.Draw(new RectangleF(0, 0, (float)300, (float)200));
				var resultImage = UIGraphics.GetImageFromCurrentImageContext();
				UIImageOrientation ori = thumbnail.Orientation;
				return	Xamarin.Forms.ImageSource.FromStream(() => resultImage.AsJPEG().AsStream());

			}
			catch (Exception ex) 
			{
				return imageSource;
			}
		}

		public string getUserlastcountry()
		{
			string value = NSUserDefaults.StandardUserDefaults.StringForKey("country");
			return value;

		}

		public void setuserCountry(string countryname)
		{

			NSUserDefaults.StandardUserDefaults.SetString(countryname, "country");
			NSUserDefaults.StandardUserDefaults.Synchronize();


		}
		public void setmyCountry(string countryname)
		{

			NSUserDefaults.StandardUserDefaults.SetString(countryname, "mycountryid");
			NSUserDefaults.StandardUserDefaults.Synchronize();


		}
		public string getmycountry()
		{
			string value = NSUserDefaults.StandardUserDefaults.StringForKey("mycountryid");
			return value;

		}
	}
}

