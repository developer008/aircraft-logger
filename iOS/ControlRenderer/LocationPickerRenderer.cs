﻿using System;
using CoreLocation;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using System.IO;
using System.Data;

[assembly: Dependency (typeof(Aircraft_Logger.iOS.LocationPickerRenderer))]
namespace Aircraft_Logger.iOS
{
	public class LocationPickerRenderer:ILocationPicker
	{
		CLLocationManager locationManager;

		public event Action< Position > ChangeLoation;

		public LocationPickerRenderer ()
		{
			locationManager = new CLLocationManager ();
		}

		public void GetCurrentLocation ()
		{
			try {
				locationManager.RequestWhenInUseAuthorization ();
				locationManager.StartUpdatingLocation ();
				locationManager.StartUpdatingHeading ();

				locationManager.LocationsUpdated += delegate(object sender, CLLocationsUpdatedEventArgs e) {
					foreach (CLLocation loc in e.Locations) {
						if (ChangeLoation != null)
							ChangeLoation (new Position (loc.Coordinate.Latitude, loc.Coordinate.Longitude));
					}
				};
			} catch (Exception ex)
			{

			}
		}

		public void StopListening ()
		{
			try {
				if (locationManager != null) {
					locationManager.StopUpdatingHeading ();
					locationManager.StopUpdatingLocation ();
				}
			} catch (Exception ex) {
				
			}
		}
		public Tuple<double, double> FindCoordinatesFromCountryName(string countryName)
		{
			
			try {
				Tuple<double, double> tuple = new Tuple<double, double>(0,0);
				string url = "http://maps.google.com/maps/api/geocode/xml?address=" + countryName + "&sensor=false";
				WebRequest request = WebRequest.Create(url);
				using (WebResponse response = (HttpWebResponse)request.GetResponse())
				{
					using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
					{
						DataSet dsResult = new DataSet();
						dsResult.ReadXml(reader);
						DataTable dtCoordinates = new DataTable();
						dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
							new DataColumn("Address", typeof(string)),
							new DataColumn("Latitude",typeof(string)),
							new DataColumn("Longitude",typeof(string)) });
						foreach (DataRow row in dsResult.Tables["result"].Rows)
						{
							string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
							DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
							tuple = new Tuple<double, double>(Convert.ToDouble(location["lat"]),Convert.ToDouble(location["lng"]));

							//dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
						}
//						GridView1.DataSource = dtCoordinates;
//						GridView1.DataBind();
					}
				}

				return tuple;
			} catch (Exception ex) {
				return new Tuple<double, double>(0,0);
			}
		}
	}
}

