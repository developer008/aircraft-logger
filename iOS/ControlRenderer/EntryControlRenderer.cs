﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using Aircraft_Logger.iOS;
using Aircraft_Logger;
using Foundation;

[assembly: ExportRenderer (typeof(EntryControl), typeof(EntryControlRenderer))]
namespace Aircraft_Logger.iOS
{
	public class EntryControlRenderer:EntryRenderer
	{
		
		public EntryControlRenderer ()
		{
		}



		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);


			if (e.OldElement == null) {

				// do whatever you want to the UITextField here!
				//this.Control.Layer.CornerRadius = 10f;
				//this.Control.Layer.BorderColor = Color.Red.ToCGColor();
				//this.Control.Layer.BorderWidth = 0.5f;
				//Control.BackgroundColor = UIColor.Gray;
				//Control.BorderStyle = UITextBorderStyle.None;
				//Control.TextAlignment = UITextAlignment.Center;

				Control.SetValueForKeyPath (UIColor.White, new NSString ("_placeholderLabel.textColor"));

				var view = (EntryControl)Element;
				Control.AutocorrectionType = UITextAutocorrectionType.No;
				//Control.AutocapitalizationType = UITextAutocapitalizationType.AllCharacters;
				//view.Keyboard = Keyboard.Create (KeyboardFlags.CapitalizeSentence);

				switch (view.CAlignMent) {
				case EntryControlAlignMent.Center:
					Control.TextAlignment = UITextAlignment.Center;
					break;

				case EntryControlAlignMent.Start:
					Control.TextAlignment = UITextAlignment.Left;
					break;
				case EntryControlAlignMent.End:
					Control.TextAlignment = UITextAlignment.Right;
					break;
				default:
					Control.TextAlignment = UITextAlignment.Left;
					break;
				}


				switch (view.CBorderStyle) {
				case EntryControlBorderStyle.Bezel:
					Control.BorderStyle = UITextBorderStyle.Bezel;
					break;

				case EntryControlBorderStyle.Line:
					Control.BorderStyle = UITextBorderStyle.Line;
					break;
				case EntryControlBorderStyle.None:
					Control.BorderStyle = UITextBorderStyle.None;
					break;
				case EntryControlBorderStyle.RoundedRect:
					Control.BorderStyle = UITextBorderStyle.RoundedRect;
					break;
				default:
					Control.BorderStyle = UITextBorderStyle.None;
					break;
				}

				if (view.CBorderWidth != null)
					this.Control.Layer.BorderWidth = view.CBorderWidth;

				if (view.CBorderColor != null)
					this.Control.Layer.BorderColor = view.CBorderColor.ToCGColor ();


				if (view.CCornerRadius != null)
					this.Control.Layer.CornerRadius = view.CCornerRadius;

				if (view.CFont != null && view.CFont != "")
					Control.Font = UIFont.FromName (view.CFont, view.CFontSize != null ? view.CFontSize : 20);
				if (view.CFontStyle == EntryControlFontStyle.Bold)
					Control.Font = UIFont.BoldSystemFontOfSize (view.CFontSize != null ? view.CFontSize : 20);
				else if (view.CFontStyle == EntryControlFontStyle.Italic)
					Control.Font = UIFont.ItalicSystemFontOfSize (view.CFontSize != null ? view.CFontSize : 20);
				
				//Control.BorderStyle = UITextBorderStyle.None;
			
			}
		}


	}
}

