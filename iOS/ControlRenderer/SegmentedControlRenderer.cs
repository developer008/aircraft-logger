﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CoreGraphics;
using System.Drawing;

[assembly:ExportRenderer(typeof(Aircraft_Logger.SegmentedControl), typeof(Aircraft_Logger.iOS.SegmentedControlRenderer))]
namespace Aircraft_Logger.iOS
{
	public class SegmentedControlRenderer: ViewRenderer<SegmentedControl, UISegmentedControl>
	{
		UISegmentedControl segmentedControl ;
		public SegmentedControlRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SegmentedControl> e)
		{
			base.OnElementChanged (e);

			segmentedControl = new UISegmentedControl ();

			segmentedControl.TintColor = UIColor.FromRGB(36,175,227);

			var view = (SegmentedControl)Element;

			view.eventSetValue += (bool value) => {
				if (value)
					segmentedControl.SelectedSegment = 0;
				else
					segmentedControl.SelectedSegment = 1;
			};

			if (e.NewElement != null) {
				for (var i = 0; i < e.NewElement.Children.Count; i++) {
					segmentedControl.InsertSegment (e.NewElement.Children [i].Text, i, true);
				}
				if (view.SelectedValue == true) 
					segmentedControl.SelectedSegment = 0;
				else 
					segmentedControl.SelectedSegment = 1;

				segmentedControl.ValueChanged += (sender, eventArgs) => {
					e.NewElement.SelectedValue = segmentedControl.SelectedSegment == 0 ? true : false;
				};

				SetNativeControl (segmentedControl);
				if (e.NewElement.SegmentWidth != null) {
					segmentedControl.SetWidth (e.NewElement.SegmentWidth, 0);
					segmentedControl.SetWidth (e.NewElement.SegmentWidth, 1);
				}



				//segmentedControl.Frame = new CGRect(20,20,280,40);
			}
		}

//		private void SetValue(bool Value){
//			if (Value == true) {
//				segmentedControl.SelectedSegment = 0;
//			} else {
//				segmentedControl.SelectedSegment = 1;
//			}
//		}
	}

}

