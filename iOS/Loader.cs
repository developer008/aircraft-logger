﻿using System;
using Xamarin.Forms;
using UIKit;

[assembly: Dependency (typeof (Aircraft_Logger.iOS.Loader))]
namespace Aircraft_Logger.iOS
{
	public class Loader:UIViewController ,ILoader
	{
		LoadingOverlay loadingOverlay;
		protected LoadingOverlay _loadPop = null;
		UIViewController controller;
		public Loader ()
		{
		}

		#region ILoader implementation

		public void DisplayLoader ()
		{
			controller = new UIViewController ();

			var bounds = UIScreen.MainScreen.Bounds;

			// show the loading overlay on the UI thread using the correct orientation sizing
			loadingOverlay = new LoadingOverlay (bounds);
			//View.Add (loadingOverlay);
			controller.View = loadingOverlay;
			UIViewController currentController = UIApplication.SharedApplication.KeyWindow.RootViewController;
			//currentController.PresentViewController(controller, false, null);
			currentController.Add(loadingOverlay);
		}

		public void HideLoader ()
		{
			loadingOverlay.Hide ();
			//controller.
		}

		#endregion
	}
}

