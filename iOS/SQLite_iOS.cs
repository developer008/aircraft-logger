﻿using System;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency (typeof (Aircraft_Logger.iOS.SQLite_iOS))]
namespace Aircraft_Logger.iOS
{
	public class SQLite_iOS:ISQLite
	{
		public SQLite_iOS ()
		{
		}

		public SQLite.SQLiteConnection GetConnection ()
		{

			var sqliteFilename = "SpottedAircraft.sqlite";
			string docFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			string libFolder = Path.Combine (docFolder, "..", "Library", "Databases");

			if (!Directory.Exists (libFolder)) {
				Directory.CreateDirectory (libFolder);
			}

			string dbPath = Path.Combine (libFolder, sqliteFilename);

			var conn = new SQLite.SQLiteConnection(dbPath);

			// Return the database connection
			return conn;
		}

		public SQLite.SQLiteConnection GetUserDBConnection ()
		{
			var sqliteFilename = "UserDB.sqlite";
			string docFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			string libFolder = Path.Combine (docFolder, "..", "Library", "Databases");

			if (!Directory.Exists (libFolder)) {
				Directory.CreateDirectory (libFolder);
			}

			string dbPath = Path.Combine (libFolder, sqliteFilename);

			var conn = new SQLite.SQLiteConnection(dbPath);
			// Return the database connection
			return conn;
		}
	}
}

